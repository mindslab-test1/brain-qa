Build
=====

From PyPI
=========

::
  pip install grpcio
  pip install grpcio-tools

Prepare protobuf or grpc files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


::
  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/brain_qa.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/question_analysis.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/generate_structured_candidate.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/semanticSearch.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/generate_unstructured_candidate.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/generate_unstructured_candidate_merge.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/inference_answer_constraint.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/filter_answer_candidate.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/evidence_retrieval.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/inference_answer_hypothesis.proto

  python -m grpc.tools.protoc --python_out=. --grpc_python_out=. \
    -I /usr/local/include -I ~/git/brain-qa/proto \
    ~/git/brain-qa/proto/maum/brain/qa/inference_answer.proto

Install
=======

Use cmake.
