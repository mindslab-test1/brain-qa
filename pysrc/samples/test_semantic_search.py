#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import grpc

from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from maum.brain.qa import semanticSearch_pb2
from maum.brain.qa import semanticSearch_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

import datetime

class SemanticSearch:
    conf = Config()
    stub = None

    def __init__(self):
        # 서버 호출할 때 사용할 port를 conf를 통해 모듈에 따라 가져옴
        channel = grpc.insecure_channel(self.conf.get("brain-qa.semantic-search.port"))
        # 서버에서 시맨틱검색 함수를 호출하기 위해 stub 생성
        self.stub = semanticSearch_pb2_grpc.SemanticSearchServiceStub(channel)

    # 클라이언트-서버 간 통신을 위해 proto에서 선언한 형태로 input(message)을 맞춰준다.

    # bm25를 이용한 검색
    def bm25_search(self, nlqa_result):
        nlqa_result = nlqa_result + "||0||\n"
        search_input = semanticSearch_pb2.SemanticSearchInput(questionAnalysis=nlqa_result)
        bm25_result = self.stub.SearchBM25(search_input)
        bm25_result = bm25_result.semanticSearchResult.encode("utf-8")
        return bm25_result.strip()

    # TIC를 이용한 검색
    def tic_search(self, nlqa_result):
        nlqa_result = nlqa_result + "||0||\n"
        search_input = semanticSearch_pb2.SemanticSearchInput(questionAnalysis=nlqa_result)
        tic_result = self.stub.SearchTIC(search_input)
        tic_result = tic_result.semanticSearchResult.encode("utf-8")
        return tic_result.strip()

    # boolean을 이용한 검색
    def boolean_search(self, nlqa_result):
        nlqa_result = nlqa_result + "||0||\n"
        search_input = semanticSearch_pb2.SemanticSearchInput(questionAnalysis=nlqa_result)
        boolean_result = self.stub.SearchTIC(search_input)
        boolean_result = boolean_result.semanticSearchResult.encode("utf-8")
        return boolean_result.strip()


if __name__ == "__main__":

    conf = Config()
    conf.init("brain-qa.conf")

    semantic_search = SemanticSearch()

    # 입력은 파일단위로 진행되기 때문에 미리 생성한 파일을 이용하여 input 생성
    with open("semantic_search_input", "r") as f:
        input_list = f.readlines()

    search_input = ""
    for input_text in input_list:
        search_input += input_text

    # 시맨틱검색 결과를 저장하기 위해
    # 현재는 bm25 결과만을 가져오도록 되어 있음
    file_writer = open("semantic_search_output", "w")
    bm25_result = semantic_search.bm25_search(search_input)

    print(bm25_result)
    file_writer.write(bm25_result + "\n")
    file_writer.close()


