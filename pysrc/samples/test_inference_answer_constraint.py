#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import grpc, json

from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from maum.brain.qa import brain_qa_pb2
from maum.brain.qa import brain_qa_pb2_grpc
from maum.brain.qa import inference_answer_constraint_pb2
from maum.brain.qa import inference_answer_constraint_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

class InferenceAnswerConstraint:
    conf = Config()
    stub = None

    def __init__(self):
        # 서버 호출할 때 사용할 port를 conf를 통해 모듈에 따라 가져옴
        remote = "localhost:" + self.conf.get("brain-qa.inference-answer-constraint.port")
        channel = grpc.insecure_channel(remote)
        # 서버에서 정답 타입 및 제약 추론 함수를 호출하기 위해 stub 생성
        self.stub = brain_qa_pb2_grpc.QuestionAnswerServiceStub(channel)

    def inference_answer_constraint(self, text, start_index=1, end_index=5):
        # 클라이언트-서버 간 통신을 위해 proto에서 선언한 형태로 input(message)로 맞춰준다.
        in_text = inference_answer_constraint_pb2.InferenceAnswerConstraintInputText()
        in_text.text = text
        in_text.start_index = start_index
        in_text.end_index = end_index
        result = self.stub.InferenceAnswerConstraint(in_text)
        # 정답 타입 및 제약 추론 결과의 인코딩을 utf-8로 변환
        result = result.result.encode("utf-8")
        return result.strip()

if __name__ == "__main__":
    conf = Config()
    conf.init("brain-qa.conf")

    inference_answer_constraint = InferenceAnswerConstraint()

    # 입력은 파일단위로 진행되기 때문에 미리 생성한 파일을 이용하여 input 생성
    with open("inference_answer_constraint_input", "r") as f:
        line_list = f.readlines()

    input_text = ""
    for line in line_list:
        input_text = input_text + line + "\n"

    input_text = input_text.strip()
    temp_start_index = 1
    temp_end_index = 5

    # 정답 타입 및 제약 추론 결과를 저장하기 위해
    file_writer = open("inference_answer_constraint_output", "w")
    result = inference_answer_constraint.inference_answer_constraint(input_text, temp_start_index, temp_end_index)

    print(result)
    file_writer.write(result+"\n")
    file_writer.close()
