#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from maum.brain.qa import brain_qa_pb2
from maum.brain.qa import brain_qa_pb2_grpc
from maum.brain.qa import question_analysis_pb2
from maum.brain.qa import question_analysis_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

class QuestionAnalysis:
    conf = Config()
    stub = None

    def __init__(self):
        # 서버 호출할 때 사용할 port를 통해 모듈에 따라 가져옴
        remote = "localhost:" + self.conf.get("brain-qa.question-analysis.port")
        channel = grpc.insecure_channel(remote)
        # 서버에서 질문분석 함수를 호출하기 위해 stub 생성
        self.stub = brain_qa_pb2_grpc.QuestionAnswerServiceStub(channel)

    def get_provider(self):
        ret = self.stub.GetProvider(empty_pb2.Empty())
        json_ret = json_format.MessageToJson(ret, True)
        print json_ret

    def analyze(self, text):
        # 클라이언트-서버 간 통신을 위해 proto에서 선언한 형태(message)로 맞춰준다.
        in_text = question_analysis_pb2.QuestionAnalysisInputText()
        in_text.text = text
        ret = self.stub.QuestionAnalyze(in_text)
        # 질문분석 결과를 인코딩을 utf-8로 변환
        nlqa_result = ret.result.encode("utf-8")
        return nlqa_result.strip()

if __name__ == '__main__':

    conf = Config()
    conf.init("brain-qa.conf")
    nlqa = QuestionAnalysis()
    #nlqa.get_provider()

    input_text = raw_input("입력 : ")

    # 질문분석 결과를 저장하기 위해
    file_writer = open("question_analysis_output" ,"w")
    if input_text.lower() == "q" or input_text.lower() == "quit":
        sys.exit(1)
    else:
        nlqa_result = nlqa.analyze(input_text.strip())
        print(nlqa_result)
        file_writer.write(nlqa_result.strip()+"\n")
        file_writer.close()
