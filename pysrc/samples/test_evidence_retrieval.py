#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys 
import grpc

from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from maum.brain.qa import evidence_retrieval_pb2
from maum.brain.qa import evidence_retrieval_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

class EvidenceRetrieval:

    conf = Config()
    stub = None

    def __init__(self):
        # 서버 호출할 때 사용할 port를 conf를 통해 모듈에 따라 가져옴
        channel = grpc.insecure_channel(self.conf.get("brain-qa.evidence-retrieval.port"))
        # 서버에서 근거검색 함수를 호출하기 위해 stub 생성
        self.stub = evidence_retrieval_pb2_grpc.EvidenceRetrievalServiceStub(channel)

    def evidence_retrieval(self, text):
        # 클라이언트-서버 간 통신을 위해 proto에서 선언한 형태로 input(message)을 맞춰준다.
        in_text = evidence_retrieval_pb2.EvidenceInput()
        in_text.softFilterResult = text
        result = self.stub.EvidenceRetrieval(in_text)
        # 근거검색 결과의 인코딩을 utf-8로 변환
        result = result.evidenceResult.encode("utf-8")
        return result.strip()


if __name__ == "__main__":

    conf = Config()
    conf.init("brain-qa.conf")

    evidence_retrieval = EvidenceRetrieval()

    # 입력은 파일단위로 진행되기 때문에 미리 생성한 파일을 이용하여 input 생성
    with open("evidence_retrieval_input", "r") as f:
        input_list = f.readlines()

    evidence_retrieval_input = ""
    for input_text in input_list:
        evidence_retrieval_input += input_text

    evidence_retrieval_input = evidence_retrieval_input.strip()

    # 근거검색 결과를 저장하기 위해
    file_writer = open("evidence_retrieval_output", "w")
    evidence_retrieval_result = evidence_retrieval.evidence_retrieval(evidence_retrieval_input)
    print(evidence_retrieval_result)
    file_writer.write(evidence_retrieval_result + "\n")
    file_writer.close()
