#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import grpc, json

from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from maum.brain.qa import generate_unstructured_candidate_merge_pb2
from maum.brain.qa import generate_unstructured_candidate_merge_pb2_grpc
from maum.common import lang_pb2
from common.config import Config

class GenerateUnstructuredCandidateMerge:
    conf = Config()
    stub = None

    def __init__(self):
        # 서버 호출할 때 사용할 port를 conf를 통해 모듈에 따라 가져옴
        channel = grpc.insecure_channel(self.conf.get("brain-qa.generate-unstructured-candidate-merge.port"))
        # 서버에서 비정형 정답 후보 통합 함수를 호출하기 위해 stub 생성
        self.stub = generate_unstructured_candidate_merge_pb2_grpc.GenerateUnstructuredCandidateMergeServiceStub(channel)


    def generate_unstructured_candidate_merge(self, text):
        # 클라이언트-서버간 통신을 위해 proto에서 선언한 형태로 input(message)을 맞춰준다.
        in_text = generate_unstructured_candidate_merge_pb2.GenerateUnstructuredCandidateMergeInput(bm25Result=text,
                                                                    ticResult=text, booleanResult=text)
        mergeOutput = self.stub.MergeCandidate(in_text)
        # 비정형 정답 후보 통합 결과의 인코딩을 utf-8로 변환
        mergeOutput = mergeOutput.generateStructuredCandidateResult.encode("utf-8")
        return mergeOutput.strip()


if __name__ == "__main__":
    
    conf = Config()
    conf.init("brain-qa.conf")

    generate_unstructured_candidate_merge = GenerateUnstructuredCandidateMerge()

    # 입력은 파일단위로 진행되기 떄문에 미리 생성한 파일을 이용하여 input 생성
    with open("generate_unstructured_candidate_merge_input", "r") as f:
        input_list = f.readlines()

    generate_unstructured_candidate_merge_input = ""

    for input_text in input_list:
        generate_unstructured_candidate_merge_input += input_text

    # 비정형 정답 후보 통합 결과를 저장하기 위해
    file_writer = open("generate_unstructured_candidate_merge_output", "w")
    generate_unstructured_candidate_merge_result = generate_unstructured_candidate_merge.generate_unstructured_candidate_merge(generate_unstructured_candidate_merge_input)

    print(generate_unstructured_candidate_merge_result)
    file_writer.write(generate_unstructured_candidate_merge_result+"\n")
    file_writer.close()
