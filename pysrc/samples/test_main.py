#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)
 
from test_question_analysis import QuestionAnalysis
from test_generate_structured_candidate import GenerateStructuredCandidate
from test_semantic_search import SemanticSearch
from test_generate_unstructured_candidate import GenerateUnstructuredCandidate
from test_generate_unstructured_candidate_merge import GenerateUnstructuredCandidateMerge
from test_inference_answer_constraint import InferenceAnswerConstraint
from test_filter_answer_candidate import FilterAnswerCandidate
from test_evidence_retrieval import EvidenceRetrieval
from test_inference_answer_hypothesis import InferenceAnswerHypothesis
from test_inference_answer import InferenceAnswer

from maum.common import lang_pb2
from common.config import Config	

import time

# 질문분석-최종정답추론까지 end to end로 진행되는 main 함수
if __name__ == "__main__":

    conf = Config()
    conf.init("brain-qa.conf")

    question_analysis = QuestionAnalysis()
    generate_structured_candidate = GenerateStructuredCandidate()
    semantic_search = SemanticSearch()
    generate_unstructured_candidate = GenerateUnstructuredCandidate()
    generate_unstructured_candidate_merge = GenerateUnstructuredCandidateMerge()
    inference_answer_constraint = InferenceAnswerConstraint()
    filter_answer_candidate = FilterAnswerCandidate()
    evidence_retrieval = EvidenceRetrieval()
    inference_answer_hypothesis = InferenceAnswerHypothesis()
    inference_answer = InferenceAnswer()

    input_text = raw_input("입력 : ")
    input_text = input_text.strip()

    # 질문분석
    question_analysis_result = question_analysis.analyze(input_text)

    # KB 정형 정답 후보 생성
    #generate_structured_candidate_result = generate_structured_candidate.generate_structured_candidate(question_analysis_result)

    # 시맨틱검색
    semantic_search_result = semantic_search.bm25_search(question_analysis_result)
    
    # 비정형 정답 후보 생성
    generate_unstructured_candidate_result = generate_unstructured_candidate.generate_unstructured_candidate(semantic_search_result)

    # 비정형 정답 후보 통합
    generate_unstructured_candidate_merge_result = generate_unstructured_candidate_merge.generate_unstructured_candidate_merge(generate_unstructured_candidate_result)

    # 정답 타입/제약 추론
    inference_answer_constraint_result = inference_answer_constraint.inference_answer_constraint(generate_unstructured_candidate_merge_result)

    # 정답 후보 축소
    filter_answer_candidate_result = filter_answer_candidate.filter_answer_candidate(inference_answer_constraint_result)

    # 근거 검색
    evidence_retrieval_result = evidence_retrieval.evidence_retrieval(filter_answer_candidate_result)

    # 근거 추론
    inference_answer_hypothesis_result = inference_answer_hypothesis.inference_answer_hypothesis(evidence_retrieval_result)

    # 최적 정답 추론
    inference_answer_result = inference_answer.inference_answer(inference_answer_hypothesis_result)

    print(inference_answer_result)
