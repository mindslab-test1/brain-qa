#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
설정 파일의 내용을 읽어서 이를 반환한다.
싱글톤으로 동작한다.
"""

import sys, os

class Singleton(type):
    _instances = dict()

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

        return cls._instances[cls]

class Config(object):

    """ 
    설정파일을 로딩한다.
    """
    __metaclass__ = Singleton
    config_file = ""
    config_map = dict()
    minds_root = ""
    conf_path = ""

    def __init__(self):
        self.minds_root = self.get_minds_root()
        cur_path = os.path.realpath(sys.argv[0])
        if os.environ.has_key("PYTHONPATH") == True:
            # for simple server connect test and unit test
            cur_path = os.environ.get("PYTHONPATH").split(":")[0]
            self.conf_path = os.path.join(cur_path.split("pysrc")[0], "config")
        else:
            #for installed environment
            self.conf_path = os.path.realpath(os.path.join(self.minds_root, "etc"))
        pass

    def init(self, file_name):
        self.find_config_file(file_name)
        self.parse_config()

    def find_config_file(self, file_name):
        conf_file = os.path.realpath(os.path.join(self.conf_path, file_name))
        if not os.path.exists(conf_file):
            raise RuntimeError("config file not exist:"+conf_file)
        self.config_file = conf_file

    def get_minds_root(self):
        minds_root = os.environ.get("MINDS_ROOT")
        if minds_root is None:
            home = os.environ.get("HOME")
            if home is not None:
                minds_root = os.path.join(home, "minds-qa")
            else:
                exe_path = os.path.realpath(sys.argv[0])
                bin_path = os.path.dirname(exe_path)
                p_path = os.path.realpath(bin_path, "/..")
                print "set as minds_root = ", p_path
                minds_root = p_path
        return minds_root

    def parse_config(self):
        with open(self.config_file) as my_file:
            while True:
                line = my_file.readline()
                if not line:
                    break
                if len(line) == 0 or line[0] == "#":
                    continue
                name, var = line.partition("=")[::2]
                var = var.strip().replace("${MINDS_ROOT}", self.minds_root)
                self.config_map[name.strip()] = var

    def get(self, field):
        if self.config_map.has_key(field):
            return self.config_map[field]
        else:
            return ""
