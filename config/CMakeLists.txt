# Wise-QA 관련 리소스가 이미 설치 또는 준비됐다는 가정하에 진행하는 작업이기 때문에
# resource 관련 setting은 불필요
install(FILES
        brain-qa.conf
        DESTINATION etc
        COMPONENT config)

#install(DIRECTORY systemd.system DESTINATION etc
#        PATTERN "*.target"
#        PATTERN "*.service"
#        PATTERN "*.timer"
#        PATTERN "*.wants"
#        PATTERN "*.sample")

