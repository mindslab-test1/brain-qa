package ai.mindslab.lucene.analysis.eng;

import io.grpc.ManagedChannel;
import io.grpc.internal.DnsNameResolverProvider;
import io.grpc.okhttp.OkHttpChannelBuilder;
import java.io.Reader;
import java.util.Map;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import org.apache.lucene.analysis.util.CharFilterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnglishCharFilterFactory extends CharFilterFactory {

  private static Logger logger = LoggerFactory.getLogger(EnglishCharFilterFactory.class);

  private static final String ENG_SERVER_IP_PARAM = "serverIP";

  private static final String ENG_SERVER_PORT_PARAM = "serverPort";

  private final String serverIP;

  private final int serverPort;

  private ManagedChannel channel;

  private NaturalLanguageProcessingServiceBlockingStub stub;

  /**
   * Initialize this factory via a set of key-value pairs.
   */
  public EnglishCharFilterFactory(Map<String, String> args) {
    super(args);

    serverIP = get(args, ENG_SERVER_IP_PARAM, "localhost");
    serverPort = getInt(args, ENG_SERVER_PORT_PARAM, 9814);
    logger.info("serverIP : " + serverIP);
    logger.info("serverPort : " + serverPort);
    channel = OkHttpChannelBuilder
        .forAddress(serverIP, serverPort)
        .nameResolverFactory(new DnsNameResolverProvider()).usePlaintext(true).build();
    stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
    if (!args.isEmpty()) {
      throw new IllegalArgumentException("Unknown parameters: " + args);
    }

  }

  @Override
  public Reader create(Reader input) {
    return new EnglishCharFilter(input, stub);
  }
}
