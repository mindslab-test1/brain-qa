package ai.mindslab.lucene.analysis.ko;

import java.io.Reader;
import java.util.Map;
import org.apache.lucene.analysis.util.CharFilterFactory;

public class KoreanCharFilterNluFactory extends CharFilterFactory {

  private static final String KOR_SERVER_IP_PARAM = "serverIP";

  private static final String KOR_SERVER_PORT_PARAM = "serverPort";

  private final String serverIP;

  private final int serverPort;

  /**
   * Initialize this factory via a set of key-value pairs.
   */
  public KoreanCharFilterNluFactory(Map<String, String> args) {
    super(args);
    serverIP = get(args, KOR_SERVER_IP_PARAM, "localhost");
    serverPort = getInt(args, KOR_SERVER_PORT_PARAM, 9823);
    if (!args.isEmpty()) {
      throw new IllegalArgumentException("Unknown parameters: " + args);
    }

  }

  @Override
  public Reader create(Reader input) {
    return new KoreanCharFilterNlu(input, serverIP, serverPort);
  }
}
