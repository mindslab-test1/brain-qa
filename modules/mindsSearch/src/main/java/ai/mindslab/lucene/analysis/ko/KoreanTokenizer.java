package ai.mindslab.lucene.analysis.ko;

import ai.mindslab.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.lucene.analysis.CharacterUtils;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeFactory;

public class KoreanTokenizer extends Tokenizer {

  private static final int IO_BUFFER_SIZE = 4096;

  private static final Properties PROPERTIES = PropertiesManager.getProperties();

  private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
  private final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
  private final TypeAttribute typeAtt = addAttribute(TypeAttribute.class);

  private final CharacterUtils.CharacterBuffer ioBuffer = CharacterUtils
      .newCharacterBuffer(IO_BUFFER_SIZE);

  private ManagedChannel channel;

  public KoreanTokenizer(AttributeFactory attributeFactory) {
    super(attributeFactory);
    init();
  }

  private void init() {
    channel = ManagedChannelBuilder.forAddress(PROPERTIES.getProperty("KOR_NLP_SERVER_IP"),
        Integer.parseInt(PROPERTIES.getProperty("KOR_NLP_SERVER_PORT"))).usePlaintext(true).build();
  }

  @Override
  public boolean incrementToken() throws IOException {
    clearAttributes();
    char[] buffer = termAtt.buffer();
    System.out.println("====================char=======================================");
    System.out.println(buffer);
    System.out.println("====================char=======================================");
    CharacterUtils.fill(ioBuffer, input);
    System.out.println("====================iobuffer=======================================");
    System.out.println(ioBuffer.getBuffer());
    String text = String.valueOf(ioBuffer.getBuffer());
    Document document = doNlp(text);
    List<Sentence> sentenceList = document.getSentencesList();
    System.out.println("====================iobuffer=======================================");
    return false;
  }

  private Document doNlp(String text) {
    InputText inputText = InputText.newBuilder().setText(text).setLang(LangCode.kor).build();
    NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
    return stub.analyze(inputText);
  }
}
