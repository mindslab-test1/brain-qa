package ai.mindslab.lucene.analysis.ko;

import java.util.Map;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;

public class KoreanTokenizerFactory extends TokenizerFactory {

  public KoreanTokenizerFactory(Map<String, String> args) {
    super(args);
    if (!args.isEmpty()) {
      throw new IllegalArgumentException("Unknown parameters: " + args);
    }
  }

  @Override
  public Tokenizer create(AttributeFactory attributeFactory) {
    return new KoreanTokenizer(attributeFactory);
  }
}
