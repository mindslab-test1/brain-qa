package ai.mindslab.lucene.analysis.ko;

import io.grpc.ManagedChannel;
import io.grpc.internal.DnsNameResolverProvider;
import io.grpc.okhttp.OkHttpChannelBuilder;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.charfilter.BaseCharFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KoreanCharFilterNlu extends BaseCharFilter {

  private Reader transformedInput;

  private static Logger logger = LoggerFactory.getLogger(KoreanCharFilterNlu.class);

  private final String serverIP;

  private final int serverPort;

  public KoreanCharFilterNlu(Reader input, String serverIP, int serverPort ) {
    super(input);
    this.serverIP = serverIP;
    this.serverPort = serverPort;
  }


  @Override
  public int read(char[] cbuf, int off, int len) throws IOException {
    if (transformedInput == null) {
      fill();
    }
    return transformedInput.read(cbuf, off, len);
  }

  private void fill() throws IOException {
    StringBuilder buffered = new StringBuilder();
    char[] temp = new char[1024];
    for (int cnt = input.read(temp); cnt > 0; cnt = input.read(temp)) {
      buffered.append(temp, 0, cnt);
    }
    transformedInput = new StringReader(doNlp(buffered.toString()));
  }

  @Override
  public int read() throws IOException {
    if (transformedInput == null) {
      fill();
    }

    return transformedInput.read();
  }

  @Override
  protected int correct(int currentOff) {
    return Math.max(0, super.correct(currentOff));
  }

  private String doNlp(String text) {
    ManagedChannel channel = null;
    try {
      channel = OkHttpChannelBuilder
          .forAddress(serverIP, serverPort)
          .nameResolverFactory(new DnsNameResolverProvider()).usePlaintext(true).build();
      NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      logger.info("=================================================");
      logger.info("input Text : {}", text);
      logger.info("=================================================");
      List<String> strList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(text).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true).setLevel(
              NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = stub.analyze(inputText);
      channel.shutdownNow();
      List<Sentence> sentenceList = document.getSentencesList();
      for (Sentence sentence : sentenceList) {
        List<Morpheme> morpsList = sentence.getMorpsList();
        for (int i = 0; i < morpsList.size(); i++) {
          Morpheme morpheme = morpsList.get(i);
          if (i > 0 && ((StringUtils.equalsAny(morpsList.get(i - 1).getType(), "NNG", "NNP", "NNB", "NP","NR") && StringUtils
              .equals(morpheme.getType(), "XSN"))
              || (StringUtils.equals(morpsList.get(i - 1).getType(), "VV") && StringUtils
              .equals(morpheme.getType(), "ETN")))) {
            String temp = strList.get(strList.size() - 1);
            strList.remove(strList.size() - 1);
            strList.add(temp + morpheme.getLemma());
          } else if (StringUtils.equalsAny(morpheme.getType(), "NNG", "NNP", "NNB", "NR")) {
            strList.add(morpheme.getLemma());
          }
        }
      }
      String result = StringUtils.join(strList, " ");
      logger.info("=================================================");
      logger.info("result Text : {}", result);
      logger.info("=================================================");
      return result;
    } catch (Exception e) {
      e.printStackTrace();
      return text;
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }
}
