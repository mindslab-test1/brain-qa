package ai.mindslab.lucene.analysis.eng;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.charfilter.BaseCharFilter;

public class EnglishCharFilter extends BaseCharFilter {

  private Reader transformedInput;

  private NaturalLanguageProcessingServiceBlockingStub stub;

  public EnglishCharFilter(Reader input, NaturalLanguageProcessingServiceBlockingStub stub) {
    super(input);
    this.stub = stub;
  }


  @Override
  public int read(char[] cbuf, int off, int len) throws IOException {
    if (transformedInput == null) {
      fill();
    }
    return transformedInput.read(cbuf, off, len);
  }

  private void fill()  {
    StringBuilder buffered = new StringBuilder();
    char[] temp = new char[1024];
    try {
      for (int cnt = input.read(temp); cnt > 0; cnt = input.read(temp)) {
        buffered.append(temp, 0, cnt);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    transformedInput = new StringReader(doNlp(buffered.toString()));
  }

  @Override
  public int read() throws IOException {
    if (transformedInput == null) {
      fill();
    }

    return transformedInput.read();
  }

  @Override
  protected int correct(int currentOff) {
    return Math.max(0, super.correct(currentOff));
  }

  private String doNlp(String text) {
    System.out.println("=================================================");
    System.out.println("input Text : " + text);
    System.out.println("=================================================");
    List<String> strList = new ArrayList<>();
    InputText inputText = InputText.newBuilder().setText(text).setLang(LangCode.eng)
        .setSplitSentence(true).setUseTokenizer(true).setLevel(
            NlpAnalysisLevel.NLP_ANALYSIS_ALL)
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
    Document document = stub.analyze(inputText);
    List<Sentence> sentenceList = document.getSentencesList();
    for (Sentence sentence : sentenceList) {
      List<Morpheme> morpsList = sentence.getMorpsList();
      for (int i = 0; i < morpsList.size(); i++) {
        Morpheme morpheme = morpsList.get(i);
        System.out.println("Type : " + morpheme.getType());
        System.out.println("morp" + morpheme.toString());
        if (StringUtils.equalsAny(morpheme.getType(), "RB", "JJ") || StringUtils.startsWith(morpheme.getType(), "NN")) {
          strList.add(morpheme.getLemma());
        }
      }
    }
    String result = StringUtils.join(strList, " ");
    System.out.println("===============================================");
    System.out.println("result Text : " + result);
    System.out.println("===============================================");

    return result;
  }
}
