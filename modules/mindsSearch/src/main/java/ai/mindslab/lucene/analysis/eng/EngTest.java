package ai.mindslab.lucene.analysis.eng;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.List;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;

public class EngTest {

  private ManagedChannel channel;

  private NaturalLanguageProcessingServiceBlockingStub stub;

  public static void main(String[] args) {
    EngTest engTest = new EngTest();
    engTest.run();
  }

  private void run() {
    channel = ManagedChannelBuilder
        .forAddress("10.122.64.73", 9814)
        .usePlaintext(true).build();
    stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
    InputText inputText = InputText.newBuilder().setText("i am a boy").setLang(LangCode.eng)
        .setSplitSentence(true).setUseTokenizer(false).setLevel(NlpAnalysisLevel.NLP_ANALYSIS_ALL)
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_ALL).build();
    Document document = stub.analyze(inputText);
    List<Sentence> sentenceList = document.getSentencesList();
    for (Sentence sentence : sentenceList) {
      List<Morpheme> morpsList = sentence.getMorpsList();
      for (int i = 0; i < morpsList.size(); i++) {
        Morpheme morpheme = morpsList.get(i);
        System.out.println("Type : " + morpheme.getType());
        System.out.println("morp" + morpheme.toString());
        if (StringUtils.equalsAny(morpheme.getType(), "RB", "JJ", "NN")) {
          System.out.println(morpheme.getLemma());
        }
      }
    }
    channel.shutdown();
  }
}
