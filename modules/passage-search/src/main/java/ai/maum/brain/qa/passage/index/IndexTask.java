package ai.maum.brain.qa.passage.index;

import ai.maum.brain.qa.passage.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.passage.PassageSearch.IndexingInput;
import maum.brain.qa.passage.PassageSearch.IndexingStatus;
import maum.brain.qa.passage.PassageSearchServiceGrpc;
import maum.brain.qa.passage.PassageSearchServiceGrpc.PassageSearchServiceBlockingStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component(value = "indexTask")
public class IndexTask {

  private static Logger logger = LoggerFactory.getLogger(IndexTask.class);

  private static Properties properties = PropertiesManager.getProperties();

  public void doIndexing() {
    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",
        Integer.parseInt(properties.getProperty("PASSAGE_SEARCH_PORT", "50051"))).usePlaintext().build();
    PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
    IndexingInput indexingInput = IndexingInput.newBuilder().setClean(false).build();
    IndexingStatus status = stub.additionalIndexing(indexingInput);
    logger.info("Indexing Schedule Status: {}", status.getStatus());
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
