package ai.maum.brain.qa.passage;

import ai.maum.brain.qa.passage.util.PropertiesManager;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.brain.qa.passage.PassageSearch.IndexingInput;
import maum.brain.qa.passage.PassageSearch.IndexingStatus;
import maum.brain.qa.passage.PassageSearch.PassageRequest;
import maum.brain.qa.passage.PassageSearch.PassageResponse;
import maum.brain.qa.passage.PassageSearch.PassageResponse.Builder;
import maum.brain.qa.passage.PassageSearch.PassageResult;
import maum.brain.qa.passage.PassageSearch.ScheduleInfo;
import maum.brain.qa.passage.PassageSearchServiceGrpc.PassageSearchServiceImplBase;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PassageSearchService extends PassageSearchServiceImplBase {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(PassageSearchService.class);

  // NNG: 일반명사, NNP: 고유명사, NNB: 의존명사, NR: 수사
  private static String[] contextMorphTypes = null;

  private static String[] questionMorphTypes = null;

  private static StringBuffer nounTypes = new StringBuffer();

  private static StringBuffer verbTypes = new StringBuffer();

  private SolrClient solrClient;

  private static IndexingCore indexingCore;

  private DocumentIndexingHandler documentIndexingHandler;

  private ClassPathXmlApplicationContext context;

  public PassageSearchService(SolrClient solrClient, ClassPathXmlApplicationContext context) {
    this.solrClient = solrClient;
    setMorphTypes();
    setNounTypes();
    this.context = context;
    this.documentIndexingHandler = new DocumentIndexingHandler(solrClient);
  }

  private void setNounTypes() {
    if ("3".equals(properties.getProperty("NLP_TYPE", "3"))) {
      nounTypes.append("NNG").append("NNP").append("NNB").append("NP").append("NR");
      verbTypes.append("VV");
    } else {
      nounTypes.append("nc").append("np").append("nb").append("nn");
      verbTypes.append("pv");
    }
  }

  private void setMorphTypes() {
    String contextMorphs = properties.getProperty("CONTEXT_MORPH_TYPES", "NNG,NNP,NNB,NR");
    String questionMorphs = properties
        .getProperty("QUESTION_MORPH_TYPES", "NNG,NNP,NNB,NP,NR,VV,VA,MM,SL,NF,SN,XPN,MAG");
    String[] contextMorph = StringUtils.split(contextMorphs, ",");
    contextMorphTypes = contextMorph;
    String[] questionMorph = StringUtils.split(questionMorphs, ",");
    questionMorphTypes = questionMorph;
  }

  @Override
  public void findPassage(PassageRequest request,
      StreamObserver<PassageResponse> responseObserver) {
    Builder passageResponse = PassageResponse.newBuilder();
    passageResponse.setQuestion(request.getQuestion());

    // Step 0. Check question
    if (StringUtils.isEmpty(request.getQuestion())) {
      responseObserver.onNext(passageResponse.build());
      responseObserver.onCompleted();
      return;
    }

    SolrQuery solrQuery = new SolrQuery();

    // Step 1. Question exact match
    solrQuery.set("q", "question:" + request.getQuestion());
    solrQuery.set("defType", "edismax");
    solrQuery.setFields("*", "score");
    solrQuery.addSort("score", ORDER.desc);
    solrQuery.setStart(0);
    solrQuery.setRows(request.getNtop());

    logger.info("Exact Match Solr Query: {}", solrQuery.getQuery());

    long documentSize = getDocuments(passageResponse, solrQuery);
    if (documentSize > 0) {
      responseObserver.onNext(passageResponse.build());
      responseObserver.onCompleted();
      return;
    }

    // Step 2. Question NLP match
    Map<String, String> nlpResultMap = doNlp(request.getQuestion());


    if (StringUtils.isEmpty(nlpResultMap.get("qresult"))) {
      responseObserver.onNext(passageResponse.build());
      responseObserver.onCompleted();
      return;
    }
    String[] words = StringUtils.split(nlpResultMap.get("qresult"), " ");
    String[] qmorphs = makeQuery("question_morph", words);

    StringBuffer qbuffer = new StringBuffer(StringUtils.join(qmorphs, " AND "));
    addQuery(request, qbuffer);
    solrQuery.set("q", qbuffer.toString());

    logger.info("Question NLP AND Match Solr Query: {}", solrQuery.getQuery());
    documentSize = getDocuments(passageResponse, solrQuery);
    if (documentSize > 0) {
      responseObserver.onNext(passageResponse.build());
      responseObserver.onCompleted();
      return;
    }

    // Setp 3. Question NLP OR match
    qbuffer.setLength(0);
    qbuffer.append("("+StringUtils.join(qmorphs, " OR ") + ")");
    addQuery(request, qbuffer);
    solrQuery.set("q", qbuffer.toString());

    logger.info("Question NLP OR Match Solr Query: {}", solrQuery.getQuery());
    documentSize = getDocuments(passageResponse, solrQuery);
    if (documentSize > 0) {
      responseObserver.onNext(passageResponse.build());
      responseObserver.onCompleted();
      return;
    }

    // Setp 4. Context NLP match
    String[] cwords = StringUtils.split(nlpResultMap.get("cresult"), " ");
    String[] cmorphs = makeQuery("passage_nlp", cwords);

    StringBuffer cbuffer = new StringBuffer(
        "(" + StringUtils.join(cmorphs, " " + properties.getProperty("OPERATION", "OR") + " ")
            + ")");
    addQuery(request, cbuffer);
    solrQuery.set("q", cbuffer.toString());

    logger.info("passage Match Solr Query: {}", solrQuery.getQuery());
    documentSize = getDocuments(passageResponse, solrQuery);
    responseObserver.onNext(passageResponse.build());
    responseObserver.onCompleted();

    logger.info("document size: {}", documentSize);

  }

  private void addQuery(PassageRequest request, StringBuffer qbuffer) {
    if (request.getSkillId() > 0) {
      qbuffer.append(" AND skill_id:\"" + request.getSkillId() + "\"");
    }
    if (StringUtils.isNotEmpty(request.getSrc())) {
      qbuffer.append(" AND src:\"" + request.getSrc() + "\"");
    }
    if (StringUtils.isNotEmpty(request.getSection())) {
      qbuffer.append(" AND section:\"" + request.getSection() + "\"");
    }
  }

  private long getDocuments(Builder passageResponse, SolrQuery solrQuery) {
    try {
      QueryResponse queryResponse = solrClient.query(solrQuery);
      SolrDocumentList documentList = queryResponse.getResults();
      logger.info("documentList size : " + documentList.size());
      for (SolrDocument document : documentList) {
        List<String> questionList = new ArrayList<>();
        List<String> questionMorphList = new ArrayList<>();
        if (document.getFieldValues("question") != null) {
          questionList.addAll((ArrayList) document.getFieldValues("question"));
        }
        if (document.getFieldValues("question_morph") != null) {
          questionMorphList.addAll((ArrayList) document.getFieldValues("question_morph"));
        }

        PassageResult passageResult = PassageResult.newBuilder()
            .setId(String.valueOf(document.get("id")))
            .setPassage(String.valueOf(document.get("passage")))
            .setPassageMorph(String.valueOf(document.get("passage_morph")))
            .setWordList(String.valueOf(document.get("word_list")))
            .setSkillId(Integer.parseInt(String.valueOf(document.get("skill_id"))))
            .setSrc(String.valueOf(document.get("src")))
            .setSection(String.valueOf(document.get("section")))
            .addAllQuestion(questionList)
            .addAllQuestionMorph(questionMorphList)
            .setScore(Float.parseFloat(String.valueOf(document.get("score"))))
            .build();
        passageResponse.addPassageResult(passageResult);
      }
      return documentList.size();
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return 0;
  }

  private String[] makeQuery(String field, String[] words) {
    String[] tempWords = new String[words.length];
    for (int i = 0; i < words.length; i++) {
      tempWords[i] = field + ":" + words[i];
    }
    return tempWords;
  }

  private Map<String, String> doNlp(String question) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    Map<String, String> resultMap = new HashMap<>();
    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> strList = new ArrayList<>();
      List<String> qlist = new ArrayList<>();

      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true)
          .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      logger.info("NLP Start Time: {} ", simpleDateFormat.format(new Date()));
      Document document = nlpStub.analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      for (Sentence sentence : sentenceList) {
        List<Morpheme> morpsList = sentence.getMorpsList();
        for (int i = 0; i < morpsList.size(); i++) {
          Morpheme morpheme = morpsList.get(i);

          if (i > 0 && ((StringUtils.equalsAnyIgnoreCase(morpsList.get(i - 1).getType(), nounTypes)
              && StringUtils.equalsIgnoreCase(morpheme.getType(), "XSN"))
              || (StringUtils.equalsIgnoreCase(morpsList.get(i - 1).getType(), verbTypes)
              && StringUtils.equalsIgnoreCase(morpheme.getType(), "ETN")))) {
            String temp = strList.get(strList.size() - 1);
            strList.remove(strList.size() - 1);
            strList.add(temp + morpheme.getLemma());

            String qtemp = qlist.get(qlist.size() - 1);
            qlist.remove(qlist.size() - 1);
            qlist.add(qtemp + morpheme.getLemma());
          } else if (isQuestionMorphType(morpheme.getType())) {
            qlist.add(morpheme.getLemma());
            if (isContextMorphType(morpheme.getType())) {
              strList.add(morpheme.getLemma());
            }
          }
        }
      }
      String cresult = StringUtils.join(strList, " ");
      String qresult = StringUtils.join(qlist, " ");

      logger.info("NLP End Time: {} ", simpleDateFormat.format(new Date()));
      resultMap.put("cresult", cresult);
      resultMap.put("qresult", qresult);
      return resultMap;
    } catch (Exception e) {
      e.printStackTrace();
      return resultMap;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  private boolean isContextMorphType(String type) {
    return StringUtils.equalsAnyIgnoreCase(type, contextMorphTypes);
  }

  private boolean isQuestionMorphType(String type) {
    return StringUtils.equalsAnyIgnoreCase(type, questionMorphTypes);
  }

  @Override
  public void fullIndexing(IndexingInput request, StreamObserver<IndexingStatus> responseObserver) {
    logger.info("Start Indexing===========================");
    try {
      if (documentIndexingHandler.isIndexing()) {
        getStatus(responseObserver);
        return;
      }
      documentIndexingHandler.setStop(false);
      documentIndexingHandler.setIndexing(true);
      indexingCore = new IndexingCore(request, documentIndexingHandler, solrClient,
          null);
      indexingCore.start();

      Thread.sleep(10);
      IndexingStatus searchStatus = IndexingStatus.newBuilder()
          .setStatus(documentIndexingHandler.isIndexing())
          .setTotal(documentIndexingHandler.getTotal())
          .setFetched(0)
          .setProcessed(0).build();
      responseObserver.onNext(searchStatus);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  private void getStatus(StreamObserver<IndexingStatus> responseObserver) {
    IndexingStatus indexingStatus = IndexingStatus.newBuilder()
        .setStatus(documentIndexingHandler.isIndexing())
        .setTotal(documentIndexingHandler.getTotal())
        .setFetched(documentIndexingHandler.getFetched())
        .setProcessed(documentIndexingHandler.getProcessed()).build();
    responseObserver.onNext(indexingStatus);
    responseObserver.onCompleted();
  }

  @Override
  public void additionalIndexing(IndexingInput request,
      StreamObserver<IndexingStatus> responseObserver) {
    try {
      if (documentIndexingHandler.isIndexing()) {
        getStatus(responseObserver);
        return;
      }
      documentIndexingHandler.setStop(false);
      documentIndexingHandler.setIndexing(true);
      String indexingFilePath = PropertiesManager
          .resolveValueWithProVars(properties.getProperty("INDEXING_TIME_FILE_PATH"));
      logger.info("Indexing FIle: {}", indexingFilePath);
      if (StringUtils.isEmpty(indexingFilePath)) {
        logger.warn("INDEXING_FILE_PATH is not defined");
      } else {
        FileInputStream fileInputStream = new FileInputStream(indexingFilePath);
        Properties indexingProperties = new Properties();
        indexingProperties.load(fileInputStream);
        indexingCore = new IndexingCore(request, documentIndexingHandler, solrClient,
            indexingProperties);
        indexingCore.start();
        fileInputStream.close();
      }
      Thread.sleep(10);
      IndexingStatus indexingStatus = IndexingStatus.newBuilder()
          .setStatus(documentIndexingHandler.isIndexing())
          .setTotal(documentIndexingHandler.getTotal())
          .setFetched(documentIndexingHandler.getFetched())
          .setProcessed(documentIndexingHandler.getProcessed()).build();
      responseObserver.onNext(indexingStatus);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public void getIndexingStatus(Empty request, StreamObserver<IndexingStatus> responseObserver) {
    try {
      getStatus(responseObserver);
    } catch (Exception e) {
      responseObserver.onError(e);
    }
  }

  @Override
  public void abortIndexing(Empty request, StreamObserver<IndexingStatus> responseObserver) {
    try {
      if (indexingCore != null && indexingCore.isAlive()) {
        indexingCore.setStop(true);
        documentIndexingHandler.setStop(true);
        indexingCore.interrupt();
//        indexingCore.stop();
        documentIndexingHandler.setIndexing(false);
        documentIndexingHandler.setTotal(0);
        documentIndexingHandler.setFetched(0);
        documentIndexingHandler.setProcessed(0);
        IndexingStatus indexingStatus = IndexingStatus.newBuilder().setStatus(false).setTotal(0)
            .setFetched(0).setProcessed(0).setMessage("Indexing Abort Success").build();
        responseObserver.onNext(indexingStatus);
        responseObserver.onCompleted();
      } else {
        IndexingStatus indexingStatus = IndexingStatus.newBuilder().setStatus(false).setTotal(0)
            .setFetched(0).setProcessed(0).setMessage("Indexing Already Aborted").build();
        responseObserver.onNext(indexingStatus);
        responseObserver.onCompleted();
      }
    } catch (Exception e) {
      IndexingStatus indexingStatus = IndexingStatus.newBuilder().setStatus(false).setTotal(0)
          .setFetched(0).setProcessed(0).setMessage("Indexing Abort Error: " + e.getMessage())
          .build();
      responseObserver.onNext(indexingStatus);
      responseObserver.onCompleted();
    }
  }

  @Override
  public void setSchedule(ScheduleInfo request, StreamObserver<ScheduleInfo> responseObserver) {
    ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
    CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
    StdScheduler stdScheduler = beanfactory
        .getBean("indexSchedulerFactoryBean", StdScheduler.class);
    try {
      cronTrigger.setCronExpression(request.getCron());
      stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
      String schedulePath = PropertiesManager.resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
      logger.info("schedulePath: {}", schedulePath);
      if (StringUtils.isNotEmpty(schedulePath)) {
        File scheduleFile = new File(schedulePath);
        Properties newScheduleProperties = new Properties();
        newScheduleProperties.setProperty("schedule_info", request.getCron());
        FileOutputStream fileOutputStream = new FileOutputStream(scheduleFile);
        newScheduleProperties.store(fileOutputStream, "Schedule Info");
        fileOutputStream.close();
      }
      responseObserver.onNext(request);
      responseObserver.onCompleted();
    } catch (ParseException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (SchedulerException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (IOException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    }
  }

  @Override
  public void getSchedule(Empty request, StreamObserver<ScheduleInfo> responseObserver) {
    try {
      ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
      CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
      ScheduleInfo scheduleInfo = ScheduleInfo.newBuilder().setCron(cronTrigger.getCronExpression())
          .build();
      responseObserver.onNext(scheduleInfo);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }
}
