package ai.maum.brain.qa.passage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentIndexingHandler implements ResultHandler<PassageDocument> {

  private static Logger logger = LoggerFactory.getLogger(DocumentIndexingHandler.class);

  private SolrClient solrClient;

  @Getter
  @Setter
  private int processed = 0;

  @Getter
  @Setter
  private int fetched = 0;

  @Getter
  @Setter
  private int total = 0;

  @Getter
  @Setter
  private boolean isIndexing = false;

  @Getter
  @Setter
  private boolean isStop = false;

  Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();

  NlpService nlpService = new NlpService();

  public DocumentIndexingHandler(SolrClient solrClient) {
    this.solrClient = solrClient;
  }

  @Override
  public void handleResult(ResultContext<? extends PassageDocument> resultContext) {

    try {

      if (isStop) {
        resultContext.stop();
        return;
      }

      fetched = resultContext.getResultCount();

      PassageDocument passageDocument = resultContext.getResultObject();
      logger.info(passageDocument.toString());
      List<String> questionList = new ArrayList<>();
      List<String> questionMoprhList = new ArrayList<>();
      List<QuestionDocument> questions = passageDocument.getQuestions();
      for (QuestionDocument question : questions) {
        questionList.add(question.getQuestion());
        if (StringUtils.isEmpty(question.getQuestion_morph())) {
          questionMoprhList.add(nlpService.doNlp(question.getQuestion()));
        } else {
          questionMoprhList.add(question.getQuestion_morph());
        }
      }

      SolrInputDocument solrInputDocument = new SolrInputDocument();
      solrInputDocument.addField("id", passageDocument.getId());
      solrInputDocument.addField("passage", passageDocument.getPassage());
      solrInputDocument.addField("passage_morph", passageDocument.getPassage_morph());
      solrInputDocument.addField("passage_nlp", nlpService.doNlpFull(passageDocument.getPassage()));
      solrInputDocument.addField("word_list", passageDocument.getWord_list());
      solrInputDocument.addField("section", passageDocument.getSection());
      solrInputDocument.addField("src", passageDocument.getSrc());
      solrInputDocument.addField("skill_id", passageDocument.getSkill_id());
      solrInputDocument.addField("workspace_id", passageDocument.getWorkspace_id());
      solrInputDocument.addField("question", questionList);
      solrInputDocument.addField("question_morph", questionMoprhList);

      docs.add(solrInputDocument);
      processed++;

      if (docs.size() > 0 && ((fetched % 100) == 0 || total == fetched)) {
        solrClient.add(docs);
        solrClient.commit();
        docs.clear();
        logger.info("Solr Fetched: {}", fetched);
      }
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
