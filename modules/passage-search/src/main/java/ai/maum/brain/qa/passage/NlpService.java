package ai.maum.brain.qa.passage;

import ai.maum.brain.qa.passage.observer.DocumentObserver;
import ai.maum.brain.qa.passage.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.Getter;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NlpService {

  private static Logger logger = LoggerFactory.getLogger(NlpService.class);

  private static String[] morpTypes = null;

  @Getter
  private String noun = "";

  private static Properties properties = PropertiesManager.getProperties();

  public NlpService() {
    this.morpTypes = StringUtils
        .split(properties.getProperty("QUESTION_MORPH_TYPES", "NNG,NNP,NNB,NP,NR,VV,VA,MM,SL,NF,SN,XPN,MAG"), ",");
  }

  public String doNlp(String question) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> strList = new ArrayList<>();
      List<String> nounList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true)
          .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
          .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      setMorphSentence(strList, nounList, sentenceList);
      String result = StringUtils.join(strList, " ");
      noun = StringUtils.join(nounList, " ");
      return result;
    } catch (Exception e) {
      e.printStackTrace();
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  public String doNlpFull(String question) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);
      List<String> strList = new ArrayList<>();
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
          .setSplitSentence(true).setUseTokenizer(true)
          .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
          .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
          .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
      Document document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      nlpChannel.shutdown();
      List<Sentence> sentenceList = document.getSentencesList();
      for (Sentence sentence : sentenceList) {
        List<Morpheme> morpList = sentence.getMorpsList();
        for (Morpheme morpheme : morpList) {
          strList.add(morpheme.getLemma());
        }
      }
      String result = StringUtils.join(strList, " ");
      return result;
    } catch (Exception e) {
      e.printStackTrace();
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  public List<String> doMultiNlp(List<String> questions) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceStub nlpStub = null;
    List<String> results = new ArrayList<>();
    try {
      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext(true).build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newStub(nlpChannel);

      DocumentObserver documentObserver = new DocumentObserver();
      CountDownLatch finishLatch = new CountDownLatch(1);
      documentObserver.setCountDownLatch(finishLatch);
      StreamObserver<InputText> requestObserver = nlpStub.analyzeMultiple(documentObserver);
      for (String question : questions) {
        InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
            .setSplitSentence(true).setUseTokenizer(true)
            .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
            .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
            .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
        requestObserver.onNext(inputText);
      }
      requestObserver.onCompleted();

      if (!finishLatch.await(100, TimeUnit.SECONDS)) {
        logger.error("NLP Timeout");
        return questions;
      }

      List<Document> documentList = documentObserver.getDocumentList();
      nlpChannel.shutdown();
      for (Document document : documentList) {
        List<String> strList = new ArrayList<>();
        List<String> nounList = new ArrayList<>();
        List<Sentence> sentenceList = document.getSentencesList();
        setMorphSentence(strList, nounList, sentenceList);
        String result = StringUtils.join(strList, " ");
        results.add(result);
      }
      return results;
    } catch (Exception e) {
      e.printStackTrace();
      return questions;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  private void setMorphSentence(List<String> strList, List<String> nounList,
      List<Sentence> sentenceList) {
    for (Sentence sentence : sentenceList) {
      List<Morpheme> morpsList = sentence.getMorpsList();
      for (int i = 0; i < morpsList.size(); i++) {
        Morpheme morpheme = morpsList.get(i);
        if (i > 0 && ((StringUtils
            .equalsAny(morpsList.get(i - 1).getType(), "NNG", "NNP", "NNB", "NP", "NR")
            && StringUtils
            .equals(morpheme.getType(), "XSN"))
            || (StringUtils.equals(morpsList.get(i - 1).getType(), "VV") && StringUtils
            .equals(morpheme.getType(), "ETN"))
            || (StringUtils.equals(morpsList.get(i - 1).getType(), "XPN") && StringUtils
            .equalsAny(morpheme.getType(), "NNG", "NNP", "NNB", "NP", "NR")))) {
          String temp = strList.get(strList.size() - 1);
          if (strList.size() > 0) {
            strList.remove(strList.size() - 1);
          }
          strList.add(temp + morpheme.getLemma());
          if (nounList.size() > 0) {
            nounList.remove(nounList.size() - 1);
          }
          nounList.add(temp + morpheme.getLemma());
        } else if (StringUtils.equalsAny(morpheme.getType(), morpTypes)) {
          strList.add(morpheme.getLemma());
          if (!StringUtils.equalsAny(morpheme.getType(), "VV", "VA")) {
            nounList.add(morpheme.getLemma());
          }
        }
      }
    }
  }
}
