package ai.maum.brain.qa.passage;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class PassageDocument {
  private String id;
  private List<QuestionDocument> questions = new ArrayList<>();
  private String passage;
  private String passage_morph;
  private String word_list;
  private String section;
  private String src;
  private String skill_id;
  private String workspace_id;
}
