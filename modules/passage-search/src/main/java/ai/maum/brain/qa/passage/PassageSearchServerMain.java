package ai.maum.brain.qa.passage;

import ai.maum.brain.qa.passage.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PassageSearchServerMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(PassageSearchServerMain.class);

  private Server server;

  public static void main(String[] args) {
    PassageSearchServerMain server = new PassageSearchServerMain();
    server.start();
    server.blockUntilShutdown();
  }

  private void start() {
    try {
      ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring/index-quartz.xml");
      initSchedule(context);
      long idleTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      long ageTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      int port = Integer.parseInt(properties.getProperty("PASSAGE_SEARCH_PORT", "50055"));
      SolrClient solrClient = new HttpSolrClient.Builder(properties.getProperty("SOLR_URL", "http://127.0.0.1:8983/solr/mrc")).build();
      server = NettyServerBuilder.forPort(port)
          .maxConnectionIdle(idleTimeout, TimeUnit.SECONDS)
          .maxConnectionAge(ageTimeout, TimeUnit.SECONDS)
          .addService(new PassageSearchService(solrClient, context)).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          // Use stderr here since the logger may have been reset by its JVM shutdown hook.
          logger.error("*** shutting down gRPC server since JVM is shutting down");
          PassageSearchServerMain.this.stop();
          logger.error("*** server shut down");
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void initSchedule(ClassPathXmlApplicationContext context) {
    String schedulePath = PropertiesManager.resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
    if (StringUtils.isNotEmpty(schedulePath )) {
      File scheduleFile = new File(schedulePath);
      if (scheduleFile.exists() && scheduleFile.isFile()) {
        try {
          FileInputStream fileInputStream = new FileInputStream(scheduleFile);
          Properties scheduleProperties = new Properties();
          scheduleProperties.load(fileInputStream);
          ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
          CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
          StdScheduler stdScheduler = beanfactory
              .getBean("indexSchedulerFactoryBean", StdScheduler.class);
          String scheduleInfo = scheduleProperties.getProperty("schedule_info");
          if (StringUtils.isNotEmpty(scheduleInfo)) {
            cronTrigger.setCronExpression(scheduleInfo);
            stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
          }
          fileInputStream.close();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (SchedulerException e) {
          e.printStackTrace();
        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else {
        return;
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
