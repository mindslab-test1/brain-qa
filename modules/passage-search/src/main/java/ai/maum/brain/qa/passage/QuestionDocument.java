package ai.maum.brain.qa.passage;

import lombok.Data;

@Data
public class QuestionDocument {
  private String id;
  private String question;
  private String question_morph;
}
