#!/usr/bin/env bash
  #JAVA_HOME=/home/msl/kbqa_server/java/jdk1.8.0_101
  #JAVA_HOME=/data2/kbqa/java/jdk1.8.0_172
  PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/../" >/dev/null
  APP_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

  APP_NAME="kbqa"
  APP_BASE_NAME=`basename "$0"`

  # Add default JVM options here.
  DEFAULT_JVM_OPTS="-Xms512m -Xmx1024m -DAPP_HOME=${APP_HOME} -DpropertiesPath=${APP_HOME}/conf/kbqa.properties"

  CLASSPATH=

  # Determine the Java command to use to start the JVM.
  if [ -n "$JAVA_HOME" ] ; then
      if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
          # IBM's JDK on AIX uses strange locations for the executables
          JAVACMD="$JAVA_HOME/jre/sh/java"
      else
          JAVACMD="$JAVA_HOME/bin/java"
      fi
      if [ ! -x "$JAVACMD" ] ; then
          die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

  Please set the JAVA_HOME variable in your environment to match the
  location of your Java installation."
      fi
  else
      JAVACMD="java"
      which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

  Please set the JAVA_HOME variable in your environment to match the
  location of your Java installation."
  fi

echo $JAVA_HOME 
echo "$JAVACMD" ${DEFAULT_JVM_OPTS} -jar $APP_HOME/lib/${APP_NAME}.jar
exec "$JAVACMD" ${DEFAULT_JVM_OPTS} -jar $APP_HOME/lib/${APP_NAME}.jar

