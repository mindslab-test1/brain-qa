package com.nanum.exobrain.jni;

public class QAnalWiki {
    static {
        try {
            System.out.println(System.getProperty("java.library.path"));
            System.loadLibrary("QAnalWiki");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public native long [] InitQAnal(String rsc);
    public native byte[] QAnalyzer(String question, long [] ptr);
    public native void CloseQAnal(long [] ptr);
    public native long[] UpdateQAnalRule(String rsc, long [] ptr);
    public native byte[] QAnalTest(String rsc, long [] ptr);
}
