package ai.maum.brain.kbqa.client;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.qa.Kbqa.KbqaInput;
import maum.brain.qa.Kbqa.KbqaOutput;
import maum.brain.qa.KbqaServiceGrpc;
import maum.brain.qa.KbqaServiceGrpc.KbqaServiceBlockingStub;

public class KbqaClientMain {

  public static void main(String[] args) {
    KbqaClientMain kbqaClientMain = new KbqaClientMain();
    kbqaClientMain.connect();
  }

  private void connect() {
    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051).usePlaintext(true).build();
    KbqaServiceBlockingStub stub = KbqaServiceGrpc.newBlockingStub(channel);
    KbqaInput kbqaInput = KbqaInput.newBuilder().setQuestion("질문").build();

    KbqaOutput kbqaOutput = stub.question(kbqaInput);
    channel.shutdown();
    try {
      System.out.println(JsonFormat.printer().print(kbqaOutput));
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }

  }
}
