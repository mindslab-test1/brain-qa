package ai.maum.brain.kbqa;

public class NE {
	String text = "";
	String ne_type ="";
	Integer begin = -1;
	Integer end = -1;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getNEtype() {
		return ne_type;
	}
	public void setNEtype(String ne_type) {
		this.ne_type = ne_type;
	}
	public Integer getBegin() {
		return begin;
	}
	public void setBegin(Integer begin) {
		this.begin = begin;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
}
