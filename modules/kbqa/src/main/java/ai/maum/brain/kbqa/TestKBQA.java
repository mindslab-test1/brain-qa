package ai.maum.brain.kbqa;

import java.io.BufferedReader;
import java.io.FileReader;

public class TestKBQA {
	public static String type = "";
	public KBCandidateManager mgr = null;
    public static void main(String[] args) {		
		  //args = new String[]{"D:/exobrain/단문튜닝/단문질문/질문분석/Q_out_0807."};
		  if (args.length < 3) {
				System.out.println("Usage : java TestKBQA QueryFile DB_Path entity_property_pettern [type]");
				return;
			}
		  
		  for(String arg : args){
		  	if(arg.equals("-m")){
		  		type = "multi";
		  	}else if(arg.equals("-s")){
		  		type = "single";
		  	}
		  }		  
		  String input_file = args[0];	
		  String db_path = args[1];	//"/data2/KB_Bae/data/kb/db"; 
		  String entity_property_pettern_path = args[2];
		  
		  System.out.println("start - "+type +" db_path: "+db_path);
		  
		  if(type.equals("single")){
		  	testSingle(input_file,db_path,entity_property_pettern_path);
			}else if(type.equals("multi")){
		  	testMulti(input_file,db_path,entity_property_pettern_path);		
			} 
			//else if(type.equals("thread")){
		  	//testMultiWithThread(input_file);		
		  //}
		}   
		
    public static void testSingle(String input_file,String db_path,String entity_property_pettern_path){
    	try {				 
			
			BufferedReader in = new BufferedReader(new FileReader(input_file));
			String line1 ="";
			KBCandidateManager mgr = new KBCandidateManager();	// 정답후보 생성
			boolean flag = mgr.init(db_path,entity_property_pettern_path);
			Util.showMemory();
			if( !flag ){
				return;
			}
			String qanal_json = "";
			while((line1 = in.readLine()) != null) {				
				qanal_json = qanal_json+line1;
			}
			qanal_json = qanal_json + "||0";
			
			System.out.println("start search-----------------------------------------");
			String candidate_json = mgr.do_kbcangen(qanal_json);			
			mgr.close();
			String r=candidate_json;
			System.out.println(r);
			System.out.println("result-----------------------------------------------");
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
    }
    public static void testMulti(String input_file,String db_path,String entity_property_pettern_path){
    	try {								
			BufferedReader in = new BufferedReader(new FileReader(input_file));
			String line1 ="";
			KBCandidateManager mgr = new KBCandidateManager();	// 정답후보 생성
			boolean flag = mgr.init(db_path,entity_property_pettern_path);
			Util.showMemory();
			if( !flag ){
				return;
			}
			String qanal_json = "";
			
			String preline = "";
			String json_info = "";
			System.out.println("start search-----------------------------------------");
			Util.start();
			while((line1 = in.readLine()) != null) {
				if(line1.equals(line1.equals("-------------- Copy ---------------"))){					
				}else if(line1.equals("") && preline.equals("}")){
										
					qanal_json = json_info;					
					qanal_json = qanal_json + "||0";
					
					if( !flag ){
						return;
					}		
					//StructuredDB.current_json = qanal_json;		파키스탄			
					qanal_json = qanal_json.replaceAll("-------------- Copy ---------------", "");					
					String candidate_json = mgr.do_kbcangen(qanal_json);					
					mgr.close();					
					//String r=candidate_json.substring(candidate_json.lastIndexOf("||0||{\"candidate_list\":")+"||0||".length());
					String r=candidate_json;
					//System.out.println(r);
					json_info = "";
				}else{
					json_info = json_info+line1;						
				}					
				preline = line1;
			}
			System.out.println("end-----------------------------------------------");
			Util.end();
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
}
