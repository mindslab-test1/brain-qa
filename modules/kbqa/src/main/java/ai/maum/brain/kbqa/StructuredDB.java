package ai.maum.brain.kbqa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;

public class StructuredDB {

	private static Logger logger = LoggerFactory.getLogger(StructuredDB.class);

	//knowledge base를 저장한 자료구조
	public KBResource kr = null;
	public Integer count = 0;
	public String key = "";
	
	public int u = 0;
	public int b = 0;
	public int g = 0;
	public int n = 0;
	public int e = 0;
	
	public String current_json = "";
	public ArrayList<KBResult> kb_result = new ArrayList<KBResult>();
	
	public int check = 0;
	
	public String question ="";
	public String pattern_type = "";
	
	public SPO current_spo = null;
	
	public StructuredDB(KBResource kr){
		this.kr = kr;
	}
	
	public String getKB(String key){
		String candidate_answer = "";
		try {			
			if(kr.kb.getString(key) != null){
				candidate_answer = kr.kb.getString(key).toString();
				if(candidate_answer.contains("[B@")){
					int index = candidate_answer.indexOf(", ");
					candidate_answer = candidate_answer.substring(index+1, candidate_answer.length()).trim();
				}
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("StructuredDB - getKB() - "+key);
			e.printStackTrace();
		}
		return candidate_answer;
	}
	public void addKBResult(String answer, Double confidence, String basis, Double ratio){
		KBResult kr = new KBResult();		
		kr.setAnswer(answer); //현재 정답을 저장		
		kr.setConfidence(confidence);
		kr.setBasis(basis);
		kr.setRatio(ratio);								
		kb_result.add(kr);
	}
	public void findAnswerSP(QAnal qanl){
		Property property = null;
		SPO spo = qanl.getSPO();
		/**
		 * 속성이 2개이상인 경우 LAT와 같은 것을 우선으로 선택
		 */
		if(spo.property.size() > 1){
			for(Property p : spo.property){
				if(p.getFirstNorProperty().equals(qanl.LAT)){
					property = p;
				}else if(p.getSecondNorProperty().equals(qanl.LAT)){
					property = p;
				}
			}
		}
		if(property == null){
			property = spo.getProperty(0);
		}	
		
		String subject = spo.getSubject();
		String object = spo.getObject();
		String condition = spo.getCondition();
		String key = "";
		/**
		 * 1. 개체는 정규화를 하지 않고 검색
		 */					
		key = subject+"_"+property.getFirstNorProperty();					
		logger.info(" key : " + key);
		searchAnswer(key,condition);			
		/**
		 * 2. 없다면 속성을 2차 후보로 정규화 후 검색
		 */
		if(kb_result.size() == 0 || property.getFirstNorProperty().equals("작가")){								
			key = subject+"_"+property.getSecondNorProperty();
			searchAnswer(key,condition);
		}
		/**
		 * 3. 개체와 속성의 정규화를 진행한 후 검색
		 */
		if(kb_result.size() == 0){
			/**
			 * 필터링 규칙: 저자의 경우 개체를 정규화 하지 않는 것이 좋음
			 */
			if(!property.getFirstNorProperty().equals("저자")){
				subject = spo.getSubjectNor();
				if(!subject.trim().equals("")){
					key = subject+"_"+property.getFirstNorProperty();
					searchAnswer(key,condition);
				}
				/**
				 * 4. 없다면 속성을 2차 후보로 정규화 후 검색
				 */
				if(kb_result.size() == 0){
					if(!subject.trim().equals("")){
						key = subject+"_"+property.getSecondNorProperty();
						searchAnswer(key,condition);
					}
				}
			}
		}
	}
	public void findAnswerPattern(QAnal qanl){
		try {	
			SPO spo = qanl.getSPO();
			Property property = spo.getProperty(0);
			String subject = spo.getSubject();
			String object = spo.getObject();
			String condition = spo.getCondition();
			String key = "";
			/**
			 * 패턴을 기반으로 한 결과에 대해 정답 후보를 찾음 
			 */
			key = subject+"_"+property.getOriginalPropery();
			searchAnswer(key,condition);
			/**
			 * 현재 대통령과 전후임은 기본 검색 결과에 후처리를 통해 추가적인 검색을 진행
			 * 현재 대통령은 현재 년도의 대통령을 추출한 후 대수가 높은 대통령을 선택
			 * 전후임은 언급된 왕, 대통령의 대수(계보)를 검색한 후 계보를 기준으로 +1 또는 -1을 하고 다시 검색 
			 */
			if(qanl.pattern_type.contains("_현재") && kb_result.size() > 0){
				
				String answer = "";
				if(kb_result.size() > 1){
					answer = kb_result.get(0).getAnswer()+","+kb_result.get(1).getAnswer();
				}else{
					answer = kb_result.get(0).getAnswer();
				}
				
				
				/**
				 * 현재 대통령, 미국대통령인 경우 처리
				 */
				HashMap<String, String> hm = new HashMap<String, String>();						
				String[] temp = answer.split(",");
				/**
				 * 임기가 겹치는 년도가 존재, 2명의 대통령이 나옴
				 * 각각의 대통령의 계보를 기본으로 뒤의 대통령을 선택
				 */
				if(temp.length > 1){						
					int max = -10;
					String final_answer = "";							
					for(String t: temp){
						if(hm.get(t) == null){
							String candidate_answer = getKB(t.replaceAll(" ", "")+"_계보");										
							Integer order =  Integer.parseInt(Util.findReqularExpresstion("[0-9]+", candidate_answer));									
							if(order > max){
								max = order;
								final_answer= t;
							}
							hm.put(t, "");
						}								
					}		
					kb_result.get(0).setAnswer(final_answer);
				}					
			}else if(qanl.pattern_type.contains("_전후임") && kb_result.size() > 0){
				/**
				 * 전 후임인 경우 처리
				 */
				String type = "";
				if(qanl.pattern_type.contains("대통령")){
					property.original_propery = Util.findReqularExpresstion("(미국)*", question)+"대통령";
					type = "대통령";
				}else{
					property.original_propery = Util.findReqularExpresstion("(신라|백제|고구려|고려|조선)+", question)+"왕";
					type = "왕";
				}
				/**
				 * 검색된 계보를 기준으로 전, 후임의 계보를 계산			
				 * 			 
				 */			
				String[] country_order = kb_result.get(0).getAnswer().split(" ");
				Integer order =  Integer.parseInt(Util.findReqularExpresstion("[0-9]+", country_order[1]));
				
				if(question.contains("다음") || question.contains("후임")){
					order= order + 1;							
				}else if(question.contains("이전") || question.contains("전임")){
					order= order - 1;							
				}
				
				String answer = "";
				if(order == 0){
					answer = subject+"은 초대"+type+"입니다."; //현재 정답을 저장
				}else if((property.getOriginalPropery().equals("신라왕") && order >= 57) || (property.getOriginalPropery().equals("고구려왕") && order >= 29) ||
					(property.getOriginalPropery().equals("백제왕") && order >= 32) || (property.getOriginalPropery().equals("고려왕") && order >= 34) 
					|| (property.getOriginalPropery().equals("조선왕") && order >= 28)){
					answer = subject+"은 마지막왕입니다."; //현재 정답을 저장														
				}else{
					subject = order.toString() +"대";
					key = subject+"_"+country_order[0].replaceAll("대한민국", "")+type;						
										
					if(kr.kb.get(key) != null){				
						String candidate_answer = getKB(key);	
						String[] value = candidate_answer.split("@=@");
						answer = value[0];
					}else if(kr.kb.get(key) == null){
						answer = "현재 "+type+"입니다."; //현재 정답을 저장
					}
				}
				kb_result.clear();
				addKBResult(answer, 1.0, "-",1.0);
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
		}
		
	}
	public void findAnswer(QAnal qanl){
		current_spo = qanl.getSPO();
		SPO spo =  qanl.getSPO();
		key = "";
		kb_result.clear();

//		System.out.println("111" + current_spo.subject_nor);
//		System.out.println("222" + spo.subject_nor);

		if(spo != null){
			try {			
				if(spo.property.get(0).getOriginalPropery().equals("")){				
				}
//				else if(spo.property.get(0).checkEntityLinking()){					
//					String result = Util.findReqularExpresstion("[0-9]+년.*[0-9]+월.*[0-9]+일.*[0-9]+년.*[0-9]+월.*[0-9]+일", QAnal.entity_explain);					
//					if(result.equals("")){
//						result = Util.findReqularExpresstion("[0-9]+년.*[0-9]+월.*[0-9]+일.*\\)", QAnal.entity_explain);
//					}
//					if(!result.equals("")){
//						addKBResult(result, 1.0, "-",1.0);
//						pattern_type = "b_d";
//					}else{
//						findAnswerSP(spo);
//					}
//				}
				else if(!qanl.pattern_type.equals("-")){
					findAnswerPattern(qanl);
				}else{
					findAnswerSP(qanl);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//System.out.println(subject+"     "+property+"   "+object);;
				e.printStackTrace();
			}
		} else {
			logger.warn("Not exist SPO : " + qanl.question);
		}
	}
	public void searchAnswer(String key,String condition){		
		try {			
			setInfo();			
			String candidate_answer ="";
			key = key.replaceAll(" ", "");
			
			if(kr.kb.get(key) != null){	
				candidate_answer = getKB(key);				
				HashMap<String, Integer> check_subject = new HashMap<String, Integer>();				
				
				if(candidate_answer != null){
					//2개이상의 정답이 있는 경우는 각각을 처리
					String[] candidate = candidate_answer.split(";");
					boolean check = false;
					for(int i = 0; i < candidate.length; i++){						
						KBResult kr = new KBResult();
						//값과 출처를 구분, 공립@=@1731319_wp
						String[] value = candidate[i].split("@=@");
						
						value[0] = value[0].replaceAll("<.+>", "").trim();						
						if(check_subject.get(value[0]) == null){
							if(value.length > 1){
								if(!question.contains(value[0])){
									kr.setAnswer(value[0]); //현재 정답을 저장									
									String source_original = "";					
									//출처를 가져오기										
									if(getKB("si_"+value[1]) != null){
										source_original = getKB("si_"+value[1]);
//									if(StructuredDB.kb_source.get(value[1]) != null){
//										source_original = StructuredDB.kb_source.get(value[1]);
									}else{
										source_original = "-";
									}
									/**
									 * 질문에서 추출된 조건이 있는경우 
									 * 출처의 키워드에 있으면 1.0, 없으면 0.1
									 */
									if(condition != null && !condition.equals("")){	
										
										if(getKB("sk_"+value[1]) != null){
											String keywords = getKB("sk_"+value[1]);
											
//										if(StructuredDB.source_keyword.get(value[1]) != null){
//											String keywords = source_keyword.get(value[1]);										
											if(keywords.contains("@=@")){
												String info = keywords.split("@=@")[0];
												//주제에 근거 정보가 있는데 
												if(!info.equals("")){
													//조건과 같으면
													if(condition.equals(info)){													
														kr.setRatio(1.0);											
													}else{
														//조건과 틀리면 
														kr.setRatio(0.5);	
													}
												}else{
													//주제에 근거 정보가 없으면
													String words = ";"+keywords.split("@=@")[1]+";";
													if(words.contains(";"+condition+";")){													
														kr.setRatio(1.0);
													}else{
														kr.setRatio(0.8);
													}
												}
											}else{
												String words = ";"+keywords+";";
												if(words.contains(";"+condition+";")){												
													kr.setRatio(1.0);
												}else{
													kr.setRatio(0.8);
												}
											}										
										}else{
											kr.setRatio(1.0);
										}
									}else{
										/**
										 * 질문에서 추출된 조건이 없는경우 
										 * 모두 1.0
										 */
										kr.setRatio(1.0);
									}
									//출처의 타입에 기반한 신뢰도르 추출					
									kr.setConfidence(Double.parseDouble(value[2]));
									kr.setBasis(source_original);
									kb_result.add(kr);									
									check_subject.put(kr.getAnswer(), 1);									
								}
							}else{
								addKBResult(value[0], 1.0, "-", 1.0);								;
								check_subject.put(value[0], 1);
							}
						}
					}
					for(KBResult kr : kb_result){
						if(kr.getRatio() == 0){
							kr.setRatio(1.0);
						}
						kr.setConfidence(kr.getConfidence() * kr.getRatio());
					}
				}		
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block
			//System.out.println(subject+"     "+property+"   "+object);;
			e.printStackTrace();
		}
	}
	public void setInfo(){
		key = key+" !! "+ current_spo.getInfo()+"==="+key;
	}
}
