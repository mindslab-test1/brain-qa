package ai.maum.brain.kbqa;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class QAnal {

  public JSONParser parser = new JSONParser();
  public Object obj = new Object();
  public JSONObject jsonObject = null;
  public JSONObject orgQInfo = null;
  public JSONObject orgQUnit = null;
  public JSONObject orgSFrame = null;

  public Iterator<JSONObject> iterator = null;

  public String LAT = "";
  public String SAT = "";
  public String question = "";
  public Integer q_length = 0;
  public String entity_explain = "";
  public ArrayList<String> NNG_list = new ArrayList<String>();
  public ArrayList<NE> ne_list = new ArrayList<NE>();
  public ArrayList<SPO> spo_list = new ArrayList<SPO>();
  public String pattern_type = "";

  public Pattern pattern = new Pattern();

  String QType = "";

  public void readJSON(String qanal_json) {
    try {
      /**
       * JSON을 파싱해서 읽어들임
       */
      obj = parser.parse(new StringReader(qanal_json));
      jsonObject = (JSONObject) obj;
      //JSON 구조: orgQInfo.orgSFrame
      //JSON 구조: orgQInfo.orgQUnit
      orgQInfo = (JSONObject) jsonObject.get("orgQInfo");
      orgQUnit = (JSONObject) orgQInfo.get("orgQUnit");
      orgSFrame = (JSONObject) orgQInfo.get("orgSFrame");

      init();

    } catch (ParseException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void init() {
    LAT = "";
    SAT = "";
    question = "";
    ne_list.clear();
    spo_list.clear();
  }

  public void setQuestion() {
    /**
     * question 원문읽기,  질문의 원본을 가져옴
     */
    question = orgQUnit.get("strQuestion").toString().replaceAll("//", "||");
  }

  /**
   * LAT는 여러개인 경우 Top1의 결과를 가져옴
   */
  public void setEntityLinking() {

    try {
      JSONArray jsonObject_entitylinking = (JSONArray) orgQUnit.get("vTitles");
      iterator = jsonObject_entitylinking.iterator();
      //개수만큰 반복
      while (iterator.hasNext()) {
        JSONObject latObject = (JSONObject) iterator.next();
        JSONArray jsonObject_entities = (JSONArray) latObject.get("vEntityInfo");
        Iterator<JSONObject> iterator_entities = jsonObject_entities.iterator();
        while (iterator_entities.hasNext()) {
          JSONObject entityObject = (JSONObject) iterator_entities.next();
          entity_explain = (String) entityObject.get("strExplain");/**
           * 제일 첫번째것만 추출
           */
          break;
        }
        break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * SAT는 여러개인 경우 Top1의 결과를 가져옴
   */
  public void setSAT() {
    try {
      JSONArray jsonObject_lat = (JSONArray) orgQUnit.get("vSATs");
      iterator = jsonObject_lat.iterator();
      //개수만큰 반복
      while (iterator.hasNext()) {
        JSONObject latObject = (JSONObject) iterator.next();
        SAT = (String) latObject.get("strSAT");
        break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   *
   */
  public void setLAT() {
    try {
      JSONArray jsonObject_lat = (JSONArray) orgQUnit.get("vLATs");
      iterator = jsonObject_lat.iterator();
      //개수만큰 반복
      while (iterator.hasNext()) {
        JSONObject latObject = (JSONObject) iterator.next();
        LAT = (String) latObject.get("strLAT");
        break;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   *
   */
  public void setNNG() {
    try {
      JSONObject jsonObject_ndoc = (JSONObject) orgQUnit.get("ndoc");
      JSONArray jsonObject_sentence = (JSONArray) jsonObject_ndoc.get("sentence");

      iterator = jsonObject_sentence.iterator();
      //개수만큰 반복
      while (iterator.hasNext()) {
        JSONObject sentenceObject = (JSONObject) iterator.next();
        //
        JSONArray jsonObject_ne = (JSONArray) sentenceObject.get("morp_eval");
        Iterator<JSONObject> iterator_ne = jsonObject_ne.iterator();
        //개수만큰 반복
        Integer pre_end = -1;
        NE pre_ne = null;
        while (iterator_ne.hasNext()) {
          JSONObject neObject = (JSONObject) iterator_ne.next();
          //
          String result = (String) neObject.get("result").toString();
          if (result.contains("/NNG") && !result.split("/")[0].equals(LAT)) {
            NNG_list.add(result.split("/")[0]);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 개체명은 인식된 순서대로 모두 추가
   */
  public void setNE() {
    try {
      JSONObject jsonObject_ndoc = (JSONObject) orgQUnit.get("ndoc");
      JSONArray jsonObject_sentence = (JSONArray) jsonObject_ndoc.get("sentence");

      iterator = jsonObject_sentence.iterator();
      //개수만큰 반복
      while (iterator.hasNext()) {
        JSONObject sentenceObject = (JSONObject) iterator.next();
        //
        JSONArray jsonObject_ne = (JSONArray) sentenceObject.get("NE");
        Iterator<JSONObject> iterator_ne = jsonObject_ne.iterator();
        //개수만큰 반복
        Integer pre_end = -1;
        NE pre_ne = null;
        while (iterator_ne.hasNext()) {
          NE ne = new NE();

          JSONObject neObject = (JSONObject) iterator_ne.next();
          //
          ne.setText((String) neObject.get("text").toString());
          ne.setNEtype((String) neObject.get("type").toString());
          if (!ne.getText().equals(LAT)) {
            /**
             * 개체명을 형태소 id기준으로 읽어옴
             */
            ne.setBegin(Integer.parseInt(neObject.get("begin").toString()));
            ne.setEnd(Integer.parseInt(neObject.get("end").toString()));
            /**
             * 개체명이 이전 개체명과 바로 이어서 나온다면 붙여서 하나의 개체명으로 인식
             */
            if (pre_end == -1) {
              ne_list.add(ne);
            } else {
              if (pre_end == -1 && ne.getBegin() == 0) {
                ne_list.add(ne);
              } else if ((pre_end + 1) == ne.getBegin() && pre_ne.getNEtype().equals("LCP_COUNTRY")
                  && ne.getNEtype().contains("OGG_")) {
                ne.setText(pre_ne.getText() + "" + ne.getText());
                ne_list.set(ne_list.size() - 1, ne);
              } else if ((pre_end + 1) == ne.getBegin() && !pre_ne.getNEtype()
                  .equals("CV_OCCUPATION") && !ne.getNEtype().equals("CV_OCCUPATION")
                  && !pre_ne.getNEtype().equals("LCP_COUNTRY") && !pre_ne.getNEtype()
                  .equals("PS_NAME")) {
                ne.setText(pre_ne.getText() + "" + ne.getText());
                ne_list.set(ne_list.size() - 1, ne);
              } else {
                ne_list.add(ne);
              }
            }
            pre_end = ne.getEnd();
            pre_ne = ne;
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void setQLength() {
    String temp_Q = question;
    for (NE ne : ne_list) {
      temp_Q = temp_Q.replaceAll(ne.getText(), "ne");
    }
    String[] temp = temp_Q.split(" ");
    q_length = temp.length;
  }

  /**
   *
   */
  public void setSPO(StructuredDB db) {
    try {
      pattern.checkPattern(this, db);
      SPO spo = new SPO(db);

      if (pattern_type == "-") {
        /**
         * 정형DB 검색을 위한 entity와 property를 추출
         * orgQInfo.orgSFrame.FactoidQ
         * FactoidQ를 추출함.
         */
        JSONObject jsonObjectFactoidQ = (JSONObject) orgSFrame
            .get("qFactoidQ"); //FactoidQ에 포함되어 있는 모든 오브젝트
        /**
         * 단문 Factoid 분석 결과 중 질문의 타입을 가져옴
         * 0=비정형 질문 1=기네스 질문, 2=정형 질문
         */
        QType = jsonObjectFactoidQ.get("nQType").toString();

        if (QType.equals("2")) {
          /**
           * 개체/속성 인식 모듈 기반(사전+기계학습), JSON
           */
          spo.setSPObyJSON(jsonObjectFactoidQ, this);
        } else if (QType.equals("0")) {
          if (ne_list.size() == 1 && !LAT.equals("")) {
            /**
             * 인식모듈 대상이 아닌 경우 개체명이 1개이고 LAT가 존재하는 경우에 대해 처리
             */
            spo.setSubject(ne_list.get(0).getText());
            spo.setProperty(LAT);
            spo_list.add(spo);
          } else if (ne_list.size() == 0 && !LAT.equals("") && q_length < 3) {
            if (NNG_list.size() > 0) {
              spo.setSubject(NNG_list.get(0));
              spo.setProperty(LAT);
              spo_list.add(spo);
            } else if (SAT.equals("PS_NAME")) {
              System.out.println(LAT);
              spo.setSubject(LAT);
              spo.setProperty("정의");
              spo_list.add(spo);
            }
          } else if (ne_list.size() != 0 && LAT.equals("")) {
            if (SAT.equals("DS_DEFINITION")) {
              spo.setSubject(ne_list.get(0).getText());
              spo.setProperty("정의");
              spo_list.add(spo);
            }
          } else if (ne_list.size() == 0 && LAT.equals("")) {
            if (NNG_list.size() > 0 && SAT.equals("DS_DEFINITION")) {
              spo.setSubject(NNG_list.get(0));
              spo.setProperty("정의");
              spo_list.add(spo);
            }
          }
        }
      } else if (pattern_type.contains("@=@")) {
        String[] ep = pattern_type.split("@=@");
        if (ep.length == 2) {
          spo.setSubject(ep[0]);
          spo.setProperty(ep[1]);
          spo_list.add(spo);
        }
      } else {
        /**
         * 패턴기반 SPO인식
         */
        spo.setSPObyPattern(pattern_type, question, ne_list, spo_list);
      }
    } catch (Exception e) {
      e.printStackTrace();
      //System.out.println("qanal input error=["+StructuredDB.current_json+"]");
      //System.exit(-1);
      //return candidateStruct;
    }
  }

  /**
   *
   */
  public SPO getSPO() {
    if (spo_list.size() > 0) {
      int init = 0;
      return spo_list.get(init);
    } else {
      return null;
    }
  }


}