package ai.maum.brain.kbqa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PropertiesManager {

  private static Properties properties;

  private static Logger logger = LoggerFactory.getLogger(PropertiesManager.class);

  private PropertiesManager() {
  }

  public static Properties getProperties() {
    if (properties == null) {
      properties = new Properties();
      InputStream is = null;
      try {
        String propertiesPath = System.getProperty("propertiesPath");
        if (propertiesPath != null) {
          is = new FileInputStream(propertiesPath);
        } else {
          logger.warn("System Property is Null. PropertyName is 'propertiesPath'");
          is = PropertiesManager.class.getClassLoader().getResourceAsStream("kbqa.properties");
        }
        properties.load(is);
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        try {
          is.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return properties;
  }

  public static String resolveValueWithProVars(String value) {
    if (null == value) {
      return null;
    }

    Pattern p = Pattern.compile("\\$\\{(\\w+)\\}|\\$(\\w+)");
    Matcher m = p.matcher(value);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      String proVarName = null == m.group(1) ? m.group(2) : m.group(1);
      String proVarValue = System.getProperty(proVarName);
      m.appendReplacement(sb,
              null == proVarValue ? "" : Matcher.quoteReplacement(proVarValue));
    }
    m.appendTail(sb);
    return sb.toString();
  }
}
