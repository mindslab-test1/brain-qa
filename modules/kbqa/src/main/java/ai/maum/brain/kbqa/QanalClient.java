package ai.maum.brain.kbqa;

import java.io.*;
import java.util.*;
import java.net.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class QanalClient {

	Socket qifServer = null;
	PrintWriter out = null;
	BufferedReader in = null;
	
	String host = null; //호스트 아이피
	int port = -1; //포트
	
	//생성자를 통해 호스트 아이피와 포트를 설정
	public QanalClient(String host, int port) {
		this.host = host; 
		this.port = port;
	}
	
	//서버 연결 및 입출력 인스턴스 생성
	private void connect() throws UnknownHostException, IOException, BindException {
		while( true ) {
			try {
				qifServer = new Socket(host, port); //서버와 연결된 인스턴스 생성
				out = new PrintWriter(new OutputStreamWriter(qifServer.getOutputStream(), "UTF-8"), true);
				in = new BufferedReader(new InputStreamReader(qifServer.getInputStream(), "UTF-8"));				
				break;
			} catch (BindException e) {
				try { Thread.sleep(1000); } catch (Exception e1) {}
			}
		}
	}

	// Json에서 질문만 추출
	private String strip_json(String question) {
		if (question.indexOf("{\"quizMetaData\" :")==-1) return question;
		else {
			String b="\"strQuestion\" : \"";
			String e="\", \"mQOptions\" :";
			
			int begin=question.indexOf(b);
			int end=question.indexOf(e);
			
			return question.substring(begin+b.length(), end);
		}
	}
	
	// 객관식 질문처리
	private String processExampleQuestion(String question)
	{
		//System.out.println("question="+question);
		String r="";
		
		try
		{
			if (question.indexOf("quizMetaData")==-1)
			{
				return question;
			} else {
				
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(new StringReader(question));
				JSONObject jsonObject = (JSONObject) obj;
				JSONObject jsonObject_quizMetaData = (JSONObject) jsonObject.get("quizMetaData");
				String question_sentence=(String)jsonObject_quizMetaData.get("strQuestion");

				if (question_sentence.indexOf("@E=")==-1)
				{
					JSONArray ExampleArray = (JSONArray)  jsonObject_quizMetaData.get("mQOptions");
					Iterator<JSONObject> iterator = ExampleArray.iterator();
					String example_list="";
					while (iterator.hasNext() ) {
						JSONObject answerObject = (JSONObject)iterator.next();
						String example = (String) answerObject.get("QOpt_string");
						if (example_list.length()>0) {
							example_list=example_list+"||";
						}
						example_list=example_list+example;
					}
					if (example_list.length()>0) {
						question_sentence=question_sentence+"@E="+example_list;
					}
				}
				//System.out.println("return="+question_sentence);
				return question_sentence;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	//질문을 입력받아 분석하는 메소드
	public String processQuestion(String question) {
		if( question==null ) 
		return null;
		 
		String result = "";
		String line;
		String prevLine = "";
		String prevPrevLine = "";
		
		try {			
			connect(); //서버연결
			//여기서 서버에서 분석이 진행되고, in 변수에 스트림이 들어가기 시작
			//
			out.println(question);			
			qifServer.shutdownOutput(); 
		
			int sizeOfJson = 0;
			int readn=0;
			//외부에서 입력하는 질문들이 여기로 입력됨(?)
			while( (line=in.readLine())!=null ) {
				readn=readn+1;
				
				if( line.indexOf("ETRIIRTE")!=-1 )
					break;
				
				
				if( line.equals("{}") ) {
					result = line;
					break;
				}
				else if (readn==1) {					
					sizeOfJson=Integer.parseInt(line.trim());					
				} else {
				
					result = result + line + "\n";
					
					sizeOfJson = sizeOfJson - line.length();
					
					if( sizeOfJson<=0 )
						break;
					
					prevPrevLine = prevLine;
					prevLine = line.trim();
				}
			}
			
			result = result.replaceAll("\n\"", "\"");
			return result;
		
		} catch(BindException e) {
			e.printStackTrace();
			return "";
		} catch(UnknownHostException e) {
			e.printStackTrace();
			return "";
		} catch(IOException e) {
			e.printStackTrace();
			return "";
		}
	}


	public void close() {
		if( out!=null )
			out.close();
		if( in!=null )
			try { in.close(); } catch (IOException e) {}
		if( qifServer!=null )
			try { qifServer.close();} catch (IOException e) {}
	}
	

}