package ai.maum.brain.kbqa.build;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.rocksdb.RocksDBException;
import kr.re.etri.kmtk.io.Rocks;
import org.rocksdb.RocksIterator;

public class RocksDB {
	Rocks kb = null;
	PrintWriter pw = null;	
	
	public void makeRockDB(String out_folder){		
		try {
//			File file = new File(out_folder);
//			if(file.exists()){
//			}else{
//				file.mkdir();
//			}
			kb = new Rocks(out_folder, false);
			System.out.println("reading rockDB.");
		} catch (RocksDBException e) {
			// TODO Auto-generated catch block
			System.out.println("error - making rockDB");
			e.printStackTrace();
		}	
	}

	public void makeRockDB(String out_folder, Rocks inputKb){
		try {
			if(inputKb != null) {
				kb = inputKb;
			} else {
				kb = new Rocks(out_folder, false);
			}
			System.out.println("reading rockDB.");
		} catch (RocksDBException e) {
			// TODO Auto-generated catch block
			System.out.println("error - making rockDB");
			e.printStackTrace();
		}
	}
	
	public void KB2RockDB(String source_file){
		BufferedReader in;
		String line="";
		try {			
			if(kb == null){				
			}else if(!source_file.equals("")){
				in = new BufferedReader(new FileReader(source_file));
				line = "";
				Integer count = 0;
				while((line = in.readLine()) != null) {
					/**
					 * source file의 데이터는 탭으로 구분되어 있어야한다.
					 * 빌호툴로스	국적	핀란드@=@259301_wd@=@0.824;
					 * 0: subject
					 * 1: property
					 * 2: object, object는 object@=@출처아이디@=@신뢰도로 구성되어 있어야한다.
					 */
					String[] temp = line.split("\t");					
					if(temp.length <= 3){
						if(temp[0].contains("fp_") || temp[0].contains("sp_") || temp[0].contains("en_")){
							String key  = temp[0];
							String value = temp[1];
							String mode = "";
							if(temp.length == 3){
								mode = temp[2];
							}else{
								mode = "i";
							}							
							check(mode, count, key, value, line);
						}
					}else{
						/**
						 * subject_property를 키로 저장
						 */
						String key = temp[0]+"_"+temp[1];
						/**
						 * object는 value
						 */
						String value = temp[2];	
						String[] format = value.split("@=@");
						if(format.length != 3){
							pw.println(count+" check object(object@=@출처아이디@=@신뢰도) :"+line);
							// object가 여러개인 경우, 각 object 에 대한 데이터 유효성 검사
							String[] objs = value.split(";");
							boolean validate = true;
							for (int i = 0 ; i < objs.length ; i++ ) {
								String[] subObjs = objs[i].split("@=@");
								if (subObjs.length != 3) {
									validate = false;
									break;
								}
							}

							// 이상 없을시, Data Insert
							if (validate) {
								String mode = "";
								if (temp.length == 4) {
									mode = temp[3];
								} else {
									mode = "i";
								}
								check(mode, count, key, value, line);
							}
						}else{
							/**
							 * 마지막은 추가 및 삭제 여부    
							 */
							String mode = "";
							if(temp.length == 4){
								mode = temp[3];
							}else{
								mode = "i";
							}							
							check(mode, count, key, value, line);
						}
					}
					count++;
				}
				System.out.println("finish.");
			}else{
				System.out.println("source file is empty.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(source_file);
			e.printStackTrace();
		}		
	}
	
	public void check(String mode, Integer count, String key, String value, String line){
		try {			
			if(mode.equals("d")){
				pw.println(count+" check delete before:"+kb.getString(key));
				if(kb.get(key) != null){
					kb.delete(key);
				}
				pw.println(count+" check delete after:"+kb.getString(key));
			}else if(mode.equals("u")){
				pw.println(count+" check update before:"+kb.getString(key));
				if(kb.get(key) == null){
					kb.put(key, value);		
				}else{
					kb.delete(key);
					kb.put(key, value);
				}
				pw.println(count+" check update after :"+kb.getString(key));
			}else if(mode.equals("i")){
				pw.println(count+" check insert before:"+kb.getString(key));
				if(kb.get(key) == null){
					kb.put(key, value);		
				}else{
					pw.println(count+" insert error. key is existed in DB. - " + line);
				}
				pw.println(count+" check insert after:"+kb.getString(key));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	public void initPW(String out_file){
		try {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(out_file))));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public void closePW(){
		pw.close();
	}

	public void printDB() {
		RocksIterator it = kb.iterator();
		while (it.isValid()) {
			String key = new String(it.key());
			String value = new String(it.value());
			pw.println(key + " : " + value);
			it.next();
		}
	}
}
