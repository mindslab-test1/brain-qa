package ai.maum.brain.kbqa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SPO {
	String condition = "";	
	String subject = "";
	String subject_nor = "";	
	String subject_ne_type = "";	
	ArrayList<Property> property = new ArrayList<Property>();	
	String object = "";
	String SPO_type ="";
	StructuredDB db = null;
	
	public SPO(StructuredDB db){
		this.db = db;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;		
		subject_nor = applyEntityNor(this.subject);
	}
	public String getSubjectNor() {
		return subject_nor;
	}
	public void setSubjectNor(String subject_nor) {
		this.subject_nor = subject_nor;
	}
	public String getSubjectNEtype() {
		return subject_ne_type;
	}
	public void setSubjectNEtype(String subject_ne_type) {
		this.subject_ne_type = subject_ne_type;
	}
	public Property getProperty(int index) {
		if(index > property.size() || index < 0){
			return null;
		}else{
			return property.get(index);
		}
	}
	public void setProperty(String property){
		Property p = new Property(db);
		p.setOriginalPropery(property);		
		this.property.add(p);
	}	
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getSPOtype() {
		return SPO_type;
	}
	public void setSPOtype(String SPO_type) {
		this.SPO_type = SPO_type;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}	
	/**
	 * 
	 */
	public void applyRule(){
		try {
			if(subject.equals("우리나라")){
				subject = "대한민국";
			}
			/**
			 * 간단한 규칙을 통해 보정하는 소스 추가
			 */
			if(subject_ne_type.equals("LCP_COUNTRY") && property.equals("크기")){
				property.get(0).setOriginalPropery("면적");
			}else if(subject.equals("러시아") && property.get(0).equals("크기")){
				property.get(0).setOriginalPropery("면적");
			}else if(condition != null && condition.equals("미국") && property.get(0).equals("대통령")){
				property.get(0).setOriginalPropery(condition+property.get(0));
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
		}
		
	}
	public void setSPObyJSON(JSONObject jsonObjectFactoidQ, QAnal qanl){
		ArrayList<SPO> spo_list = qanl.spo_list;
		try {
			JSONArray EntityNPropertyArray = (JSONArray)  jsonObjectFactoidQ.get("vResult");		
			JSONArray candidateList = new JSONArray();
			JSONArray sparqlArray = new JSONArray();
			//유너리인 경우 1개
			Iterator<JSONObject> iterator = EntityNPropertyArray.iterator();		
			
			int count = 0;
			//개수만큰 반복		
			while (iterator.hasNext() ) {	
				SPO spo = new SPO(db);
				
				JSONObject answerObject = (JSONObject)iterator.next();
				//Property와 
				JSONObject entityObject = (JSONObject) answerObject.get("Entity");
				String entity = (String)entityObject.get("lemma");
				String entity_netype = (String)entityObject.get("ne_type");
				
				JSONObject conditionObject = (JSONObject) answerObject.get("Condition");
				String condition = (String)conditionObject.get("lemma");
				if(condition == null){
					condition = "";
				}	
				
				if(entity.equals("우리나라")){
					entity = "대한민국";
				}			
				entity = changeEntity(entity,qanl);
				/**
				 * 간단한 규칙을 통해 보정하는 소스 추가
				 */										
				spo.setSubject(entity);
				spo.setCondition(condition);
				spo.setSubjectNEtype(entity_netype);
				/**
				 * property는 2개이상이 올 수 있음
				 */				
				JSONArray PropertyArray = (JSONArray)  answerObject.get("vProperty");
				Iterator<JSONObject> iterator_property = PropertyArray.iterator();
				//개수만큰 반복	
				
				while (iterator_property.hasNext() ) {	
					JSONObject answerObject1 = (JSONObject)iterator_property.next();
					String property = answerObject1.get("text").toString();
					/**
					 * 규칙
					 */
					if(entity_netype.equals("LCP_COUNTRY") && property.equals("크기")){
						property = "면적";
					}else if(entity.equals("러시아") && property.equals("크기")){
						property = "면적";
					}else if(condition != null && condition.equals("미국") && property.equals("대통령")){
						property = condition+property;
					}
					spo.setProperty(property);
					spo_list.add(spo);
				}							
				count++;
			}
		}catch (Exception e) {
			// TODO Auto-generated catch block			
			e.printStackTrace();
		}
		
	}
	/**
	 * entity가 개체명에 포함되는 경우 개체명으로 선택.
	 * 8대, 9대 등인 경우 정규화
	 * @param entity
	 * @return
	 */
	public static String changeEntity(String entity, QAnal qanl){
		for(NE ne:qanl.ne_list){
			if(!ne.equals("")){
				if(ne.getText().contains(entity)){
					entity = ne.getText();
				}
			}
		}
		return entity;
	}
	public String getYear(){
		Date d = new Date(System.currentTimeMillis());
		SimpleDateFormat sf = new SimpleDateFormat("yyyy"); 
		String dt = sf.format(d);
		return dt;
	}
	public String findYear(String property, Integer current_year,String type){
		String result = "";
		if(type.equals("지난")){
			for(Integer i = current_year; i > 0 ; i--){
				if(!db.getKB(i.toString()+"년_"+property).equals("")){
					result = i.toString();
					break;
				}
			}
		}else if(type.equals("이번")){
			for(Integer i = current_year; i < 3000 ; i++){
				if(!db.getKB(i.toString()+"년_"+property).equals("")){
					result = i.toString();
					break;
				}
			}
		}
		
		return result+"년";
	}
	/**
	 * 패턴기반 SPO인식
	 */
	public SPO setSPObyPattern(String pattern_type,String question,ArrayList<NE> ne_list, ArrayList<SPO> spo_list){
		SPO spo = new SPO(db);
		try {
			/**
			 * 절기, 역대대통령, 역대왕, 문화재인 경우 패턴으로 처리
			 * 정형 데이터 형태
			 * 
			 * xx대	대통령	ㄱㄱㄱ 
			 * xx대	미국대통령	ㄱㄱㄱ
			 * xx대	--왕	ㄱㄱㄱ, 여기서 --는 신라, 고구려, 백제, 고려, 조선				  
			 * x호	--문화재	ㄱㄱ
			 * x호	국보	ㄱㄱ
			 * xxxx년	대통령	ㄱㄱㄱ
			 * xxxx년	미국대통령	ㄱㄱㄱ
			 * x번째	절기	ㄱㄱㄱ
			 * xx회	하계올림픽	ㄱㄱㄱ, 여기서 ㄱㄱㄱ 는 국가, 도시, 년도
			 * xxxx년	하계올림픽 ㄱㄱㄱ				 *  
			 */
			
			if(pattern_type.equals("문화재")){
				/**
				 * 패턴의 형태가 문화재인 경우 Property는 국보|보물|기념물|문화재자료|문화재|명승|사적 중 하나를 포함한 문자열을 찾음
				 * Subject는 문화재의 호수를 추축하여 입력
				 * 예) 1호	국보	숭례문
				 */
				spo.setProperty(Util.findReqularExpresstion("(국보|보물|기념물|문화재자료|문화재|명승|사적)+", question));
				spo.setSubject(Util.findReqularExpresstion("[0-9]+호", question));							
			}else if(pattern_type.contains("_대") || pattern_type.contains("_전후임")){
				/**
				 * 왕, 대통령이 몇대인지, 전임과 후임이 누구지를 물어보는 경우
				 * 
				 * 1. 현재 왕, 대통령의 계보(xx대)를 검색
				 * 2. 검색된 계보를 기반으로 전임, 후임을 다시 검색  
				 *  
				 * 개체명으로 인식된 것 중 타입이 PS_NAMe인것을 subject로 추출
				 * 속성을 일괄적으로 계보를 입력
				 * 예) 광개토대왕 계보 19대
				 * 이 경우는 실제 검색시에 현재 subject, property를 기준으로 xx대를 구한 후 +1, -1을 통해 
				 * 전임과 후임을 다시 검색. 
				 * 위의 예에서 19대를 찾으면, 19+1 = 20으로 다음 대를 찾고 
				 * subject: 20대, property: 고구려왕 
				 * 위의 정보를 기준으로 검색.
				 *     
				 */
				for(NE ne : ne_list){
					if(ne.getNEtype().equals("PS_NAME")){
						spo.setSubject(ne.getText());
					}
				}
				spo.setProperty("계보");
			}else if(pattern_type.contains("_현재")){
				/**
				 * 현재 대통령, 미국대통령을 물어보는 경우
				 * 속성은 pattern_type에서 _현재를 제거, 대통령 또는 미국대통령 둘중 하나가 추출됨
				 * subject는 현재 년도를 추출하여 입력
				 * 현재 대통령은? => subject: 2017년, 	property: 대통령
				 */
				spo.setProperty(pattern_type.split("_")[0]);	
				spo.setSubject(getYear()+"년");							
			}else if(pattern_type.contains("올림픽")){
				/**
				 * 올림픽은 개최지가 특정 도시, 개체명에 도시가 있으면 subject로
				 * 없으면 xx호, xxxx년을 추출하여 입력으로 
				 */
				String subject = "";
				/**
				 * 올림픽은 열리는 도시를 찾아면 subject
				 */
				String preNE ="";
				for(NE ne : ne_list){
					if(ne.getNEtype().contains("CITY")){
						subject = ne.getText();
					}else if(ne.getNEtype().contains("SPORTS")){
						subject = ne.getText();						
						subject= subject.replaceAll(" ", "");
						subject= subject.replaceAll("동계올림픽", "");
						subject= subject.replaceAll("겨울올림픽", "");
						subject= subject.replaceAll("하계올림픽", "");
						subject= subject.replaceAll("올림픽", "");						
						if(subject.equals("")){
							subject = preNE;
						}else{
							preNE = subject;
						}						
					}	
				}
				
				if(!Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", subject).equals("")){
					subject = Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", subject);
				}
				/**
				 * 올림픽은 동계, 하계올림픽으로 구분하여 property로 저장되어 있음
				 * properyt는 올림픽은 동계, 겨울이 명시되어 있지않으면 모두 하계올림픽으로
				 * subject는 xx회, xxxx년을 추출하여 입력으로
				 * 1988년 올림픽 개최지는? -> subject: 1988년, property: 올림픽 
				 */
				if(question.contains("동계") || question.contains("겨울")){
					spo.setProperty("동계올림픽");		
				}else if(question.contains("여름") || question.contains("하계")){
					spo.setProperty("하계올림픽");		
				}else{
					if(!subject.equals("")){
						if(!db.getKB(subject+"_하계올림픽").equals("")){
							spo.setProperty("하계올림픽");
						}else if(!db.getKB(subject+"_동계올림픽").equals("")){
							spo.setProperty("동계올림픽");
						}else{
							spo.setProperty("하계올림픽");
						}
					}else{
						spo.setProperty("하계올림픽");
					}
				}
				/**
				 * subject를 못찾으면 ~회 또는 ~년을 찾음
				 */
				if(subject.equals("")){
					subject = Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", question);
				}
				/**
				 * 이것도 없으면, 이번, 또는 지난
				 */
				Integer current_year = Integer.parseInt(getYear());
				if(subject.equals("") && question.contains("이번")){
					subject = findYear(spo.getProperty(0).getOriginalPropery(),current_year,"이번");
				}else if(subject.equals("") && question.contains("지난")){
					subject = findYear(spo.getProperty(0).getOriginalPropery(),current_year,"지난");
				}
				spo.setSubject(subject);
			}else if(pattern_type.contains("월드컵")){
				/**
				 * 월드컵은 property는 월드컵으로 고정
				 * subject는 개체명 중 나라를 찾아보고 있으면 사용
				 * 없으면  xx호, xxxx년을 추출하여 입력으로 
				 */
				spo.setProperty("월드컵");							
				String subject = "";					
				for(NE ne : ne_list){
					if(ne.getNEtype().contains("COUNTRY")){
						subject = ne.getText();
					}else if(ne.getNEtype().contains("SPORTS")){
						subject = ne.getText();
						subject= subject.replaceAll(" ", "");
						subject= subject.replaceAll("피파월드컵", "");	
						subject= subject.replaceAll("FIFA월드컵", "");	
						subject= subject.replaceAll("월드컵", "");	
					}
				}
				if(subject.equals("")){
					subject = Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", question);
				}else if(!subject.equals("") && !Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", subject).equals("")){
					subject = Util.findReqularExpresstion("([0-9]+회|[0-9]+년)+", question);
				}
				Integer current_year = Integer.parseInt(getYear());
				if(subject.equals("") && (question.contains("이번") || question.contains("다음"))){
					subject = findYear(spo.getProperty(0).getOriginalPropery(),current_year,"이번");
				}else if(subject.equals("") && question.contains("지난")){
					subject = findYear(spo.getProperty(0).getOriginalPropery(),current_year,"지난");
				}
				spo.setSubject(subject);
			}else{
				/**
				 * 이외 패턴은 xx번째, xx대, 초대, 최초, 마지막을 subject로
				 * 이외에 패턴형태를 property로, 
				 */
				spo.setProperty(pattern_type);					
				String subject = Util.findReqularExpresstion("([0-9]+번째|[ㄱ-힝]+번째|[0-9]+대|초대|최초|마지막|[0-9]+년)+", question);
				if(subject.equals("최초")){
					subject = "초대";
				}
				if(pattern_type.contains("대통령") && subject.contains("번째")){
					subject = changeEntityOrder(subject+"_대");
				}					
				spo.setSubject(changeEntityOrder(subject));						
			}
			spo_list.add(spo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("SPO - setSPObyPatterncount"+"  " +question);
			e.printStackTrace();
		}
		return spo;
	}
	/**
	 * entity가 개체명에 포함되는 경우 개체명으로 선택.
	 * 8대, 9대 등인 경우 정규화
	 * @param entity
	 * @return
	 */
	public String changeEntityOrder(String entity){
		if(!db.getKB("on_"+entity).equals("")){			
			entity = db.getKB("on_"+entity);			
//		if(StructuredDB.order_nor_dic.get(entity) != null){
//			entity = StructuredDB.order_nor_dic.get(entity);
		}		
		return entity;
	}
	public String applyEntityNor(String entity){
		String entity_nor ="";
		entity = entity.replaceAll(" ", "");		
		if(!this.db.getKB("en_"+entity).equals("")){	
			entity_nor = db.getKB("en_"+entity).trim();
//		if(StructuredDB.entity_matching_dic.get(entity) != null){	
//			entity_nor = StructuredDB.entity_matching_dic.get(entity).trim();
//			if(entity_nor.contains("(")){
//				entity_nor = entity_nor.split("\\(")[0];
//			}
		}
		return entity_nor;
	}
	public String getInfo(){
		String result = "";
		result = subject+"@=@"+ subject_nor+"/"+property.get(0).getInfo()+"/"+condition;
		return result;
	}
}
