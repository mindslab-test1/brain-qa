package ai.maum.brain.kbqa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
	static Runtime r = Runtime.getRuntime();
	static long startTime = 0; 
	public static PrintWriter pw = null;
	static int count = 0;
	/**
     * 폴더에 있는 파일 전체 경로 목록을 추출하는 함수
     * @param String path 파일이 있는 폴더
     * @return ArrayList<String> 파일 목록이 저장된 ArrayList 반환
     */
	public static ArrayList<String> extractFileFullPath(String path)
	{		
		ArrayList<String> result =  new ArrayList<String>();
		String error = "";
		try{			
			File file = new File(path);			
			if(file.exists()){
				File[] files = file.listFiles();
				for(Integer i = 0 ; i < files.length; i++){
					error = i.toString();
					if(!files[i].isDirectory()){
						result.add(files[i].getPath());
					}					
				}
			}else{
				System.out.println("file not found!");
			}
		}catch(Exception e){
			System.out.println("extractFileFullPath error("+error+" "+path+") : "+e);
			result = null;
		}
		return result;		
	}
	public static void readFileToHashMapSS(String source_file, HashMap<String,String> hm,int key, int value,String split){
		String line = "";
		try {
			BufferedReader in = new BufferedReader(new FileReader(source_file));
			while((line = in.readLine()) != null) {					
				line = line.trim();
				if(!line.equals("")){					
					if(split.equals("")){
						if(hm.get(line) == null){
							hm.put(line, "1");
						}
					}else{
						//subject property object
						String[] temp = line.split(split);
						if(key < 0 || value <0){
							System.err.println(line+" ---- The index is invaild(<0). Please check the parameter(key or value). ");
						}else if(key >= temp.length || value >= temp.length){
							System.err.println(line+" ---- The index is invaild(>length). Please check the parameter(key or value). "+ source_file);
						}else{
							if(hm.get(temp[key]) == null){
								hm.put(temp[key], temp[value]);
							}else{
//								String preValue = hm.get(temp[key]);
//								hm.remove(temp[key]);
//								hm.put(temp[key], preValue+" "+temp[value]);
							}
						}
					}
				}
			}
			in.close();
			if(hm.size() == 0){
				System.out.println("please check file - "+source_file);;
			}else{
				//System.out.println("read file to hashmap - "+hm.size());;
			}
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}	
		
		
		
	}
	public static void mergeFiles(String source_file1, String source_file2){
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(source_file1,true)));
			BufferedReader in = new BufferedReader(new FileReader(source_file2));
			String line ="";
			while((line = in.readLine()) != null) {		
				pw.println(line);				
			}
			pw.close();			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public static void mergeFiles(String source_file1, String source_file2,String split, String out_file,boolean duplicate){
		String line = "";
		try {
			
			HashMap<String,String> hm = new HashMap<String,String>();
			
			if(duplicate){
				readFileToHashMapSS(source_file1, hm, 0, 1, split);
				System.out.println("source1 - "+hm.size());;
				
				BufferedReader in = new BufferedReader(new FileReader(source_file2));
				while((line = in.readLine()) != null) {		
					line = line.trim();
					//subject property object
					String[] temp = line.split(split);
					if(temp.length > 1 && !line.equals("")){
						if(hm.get(temp[0]) == null){						
							hm.put(temp[0], temp[1]);
						}
					}
				}
				in.close();
				if(hm.size() == 0){
					System.out.println("please check file - "+source_file1);;
				}else{
					PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(out_file))));			
					for(Entry<String,String> entry : hm.entrySet()){
						if(split.equals("")){
							pw.println(entry.getKey());
						}else{
							pw.println(entry.getKey()+"\t"+entry.getValue());
						}					
					}
					pw.close();
					System.out.println("read file to hashmap - "+hm.size());;
				}
			}else{
				
				BufferedReader in = new BufferedReader(new FileReader(source_file1));
				PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(out_file))));		
				while((line = in.readLine()) != null) {		
					line = line.replaceAll("<.+>", "").trim();
					String[] temp = line.split("\t");
					if(temp.length > 3){
						pw.println(line);
					}
				}
				System.out.println("source 1");
				in = new BufferedReader(new FileReader(source_file2));
				while((line = in.readLine()) != null) {		
					line = line.replaceAll(" ", "-").trim();
					String[] temp = line.split("\t");
					if(temp.length > 3){
						pw.println(line);
					}
				}
				System.out.println("source 2");
				in.close();
				pw.close();				
			}
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}	
	}
	
	public static String unique(String ori_value, String new_value){
		String result ="";
		
		ArrayList<String> u_values = new ArrayList<>();
		
		String[] temp = ori_value.split(";");
		
		if(ori_value.contains(",")){
			for(String v: temp){
				u_values.add(v);
			}			
		}else{
			u_values.add(ori_value);
		}		
		u_values.add(new_value);
		
		for(int i =0; i < u_values.size(); i++){
			int flg =0;
			for(int j =0; j < u_values.size(); j++){
				if(i != j){
					if(u_values.get(j).equals(u_values.get(i)) ){
						flg = 1;
						break;
					}
				}
			}
			if(flg == 0){
				result = result + u_values.get(i)+";";
			}
		}
		//System.out.println(ori_value+" == "+new_value+" ==> "+result);;
		result = result.replaceAll("\\;{2,}","\\;");
		return result;
	}
	public static void extractUniqu(String source_file1, String source_file2,String split, String out_file){
		String line = "";
		try {			
			HashMap<String,String> hm_u = new HashMap<String,String>();	
			HashMap<String,String> hm_d = new HashMap<String,String>();
			HashMap<String,String> hm_source = new HashMap<String,String>();
			
			BufferedReader in = new BufferedReader(new FileReader(source_file1));
			while((line = in.readLine()) != null) {		
				line = line.trim();
				//subject property object
				String[] temp = line.split(split);
				if(temp.length > 3 && !line.equals("")){
					if(hm_d.get(temp[0]+"\t"+temp[1]) == null && hm_u.get(temp[0]+"\t"+temp[1]) == null){						
						hm_u.put(temp[0]+"\t"+temp[1], temp[2]);
						hm_source.put(temp[0]+"\t"+temp[1], temp[3]);
					}else if(hm_d.get(temp[0]+"\t"+temp[1]) == null && hm_u.get(temp[0]+"\t"+temp[1]) != null){
						String value = hm_u.get(temp[0]+"\t"+temp[1]);						
						hm_u.remove(temp[0]+"\t"+temp[1]);
						
						String result = unique(value,temp[2]);						
						hm_d.put(temp[0]+"\t"+temp[1], result);
						
					}else if(hm_d.get(temp[0]+"\t"+temp[1]) != null && hm_u.get(temp[0]+"\t"+temp[1]) == null){
						String value = hm_d.get(temp[0]+"\t"+temp[1]);	
						hm_d.remove(temp[0]+"\t"+temp[1]);
						
						String result = unique(value,temp[2]);						
						hm_d.put(temp[0]+"\t"+temp[1], result);
					}
				}
			}
			in.close();
			
			System.out.println("read");;
			
			if(!source_file2.equals("")){
				in = new BufferedReader(new FileReader(source_file2));
				while((line = in.readLine()) != null) {		
					line = line.trim();
					//subject property object
					String[] temp = line.split(split);
					if(temp.length > 3 && !line.equals("")){
						if(hm_d.get(temp[0]+"\t"+temp[1]) == null && hm_u.get(temp[0]+"\t"+temp[1]) == null){						
							hm_u.put(temp[0]+"\t"+temp[1], temp[2]);
							hm_source.put(temp[0]+"\t"+temp[1], temp[3]);
						}else if(hm_d.get(temp[0]+"\t"+temp[1]) == null && hm_u.get(temp[0]+"\t"+temp[1]) != null){
							String value = hm_u.get(temp[0]+"\t"+temp[1]);						
							hm_u.remove(temp[0]+"\t"+temp[1]);
							
							String result = unique(value,temp[2]);						
							hm_d.put(temp[0]+"\t"+temp[1], result);
							
							
						}else if(hm_d.get(temp[0]+"\t"+temp[1]) != null && hm_u.get(temp[0]+"\t"+temp[1]) == null){
							String value = hm_d.get(temp[0]+"\t"+temp[1]);	
							hm_d.remove(temp[0]+"\t"+temp[1]);
							String result = unique(value,temp[2]);						
							hm_d.put(temp[0]+"\t"+temp[1], result);
						}
					}
				}
				in.close();
			}
			System.out.println("read1");;
			
			System.out.println("please check file - "+hm_u.size()+"   "+hm_d.size());;
			PrintWriter pw1 = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(out_file.replaceAll(".txt", "_a.txt")))));			
			if(hm_u.size() == 0){
				System.out.println("please check file - "+source_file1);;
			}else{
				PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(out_file.replaceAll(".txt", "_u.txt")))));			
				for(Entry<String,String> entry : hm_u.entrySet()){
					if(split.equals("")){
						pw.println(entry.getKey());
					}else{
						String result = entry.getValue().trim();
						if(!result.equals("")){							
							if(result.substring(result.length()-1).equals(",") && result.length() > 1){
								result = result.substring(0,result.length()-1);
							}
							pw.println(entry.getKey()+"\t"+result+"\t"+hm_source.get(entry.getKey()));
							pw1.println(entry.getKey()+"\t"+result+"\t"+hm_source.get(entry.getKey()));
						}
					}					
				}
				pw.close();
				System.out.println("read file to hashmap - "+hm_u.size());;
			}
			System.out.println("read3");;
			if(hm_d.size() == 0){
				System.out.println("please check file - "+source_file1);;
			}else{
				PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(out_file.replaceAll(".txt", "_d.txt")))));			
				for(Entry<String,String> entry : hm_d.entrySet()){
					if(split.equals("")){
						pw.println(entry.getKey());
					}else{
						String result = entry.getValue().trim();						
						if(!result.equals("")){
							if(result.substring(result.length()-1).equals(",") && result.length() > 1){
								//result = result.substring(0,result.length()-1);
							}
							result = result.replaceAll("\\;\t", "");
							result = result.replaceAll("\t\\;", "");							
							pw.println(entry.getKey()+"\t"+result+"\t"+hm_source.get(entry.getKey()));
							pw1.println(entry.getKey()+"\t"+result+"\t"+hm_source.get(entry.getKey()));
						}
					}					
				}
				pw.close();
				pw1.close();
				System.out.println("read file to hashmap - "+hm_d.size());;
			}			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}	
	}
	
	public static void extractUniqu(String source_file1, String source_file2,String split, int level, String out_file){
		String line = "";
		try {
			HashMap<String,String> hm_source = new HashMap<String,String>();
			
			BufferedReader in = new BufferedReader(new FileReader(source_file1));
			while((line = in.readLine()) != null) {		
				line = line.trim();
				//subject property object
				String[] temp = line.split(split);
				if(temp.length > 3 && !line.equals("")){
					if(hm_source.get(temp[0]+"\t"+temp[1]) == null){						
						hm_source.put(temp[0]+"\t"+temp[1], temp[3]);						
					}
				}
			}
			in.close();
			
			in = new BufferedReader(new FileReader(source_file2));
			while((line = in.readLine()) != null) {		
				line = line.trim();
				//subject property object
				String[] temp = line.split(split);
				if(temp.length > 3 && !line.equals("")){
					if(hm_source.get(temp[0]+"\t"+temp[1]) == null){						
						hm_source.put(temp[0]+"\t"+temp[1], temp[3]);						
					}
				}
			}
			in.close();
								
			if(hm_source.size() == 0){
				System.out.println("please check file - "+source_file1);;
			}else{
				PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(out_file.replaceAll(".txt", "_u.txt")))));			
				for(Entry<String,String> entry : hm_source.entrySet()){
					if(split.equals("")){
						pw.println(entry.getKey());
					}else{
						String result = entry.getValue().trim();						
						pw.println(entry.getKey()+"\t"+result);
						
					}					
				}
				pw.close();
				System.out.println("read file to hashmap - "+hm_source.size());;
			}
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}	
	}
	
	
	
	public static void removeSameData(String source_file,String out_file){
		String line = "";
		try {			
			HashMap<String,String> hm = new HashMap<String,String>();
						
			BufferedReader in = new BufferedReader(new FileReader(source_file));
			while((line = in.readLine()) != null) {		
				line = line.trim();
				if(hm.get(line) == null){						
					hm.put(line, "1");
				}
			}
			in.close();
			if(hm.size() == 0){
				System.out.println("please check file - "+source_file);;
			}else{
				PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(out_file))));			
				for(Entry<String,String> entry : hm.entrySet()){
					pw.println(entry.getKey());
				}
				pw.close();
				System.out.println("read file to hashmap - "+hm.size());;
			}
			
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}	
	}
	public static void initPW(String out_file){
		try {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(out_file))));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	public static void closePW(){
		pw.close();
	}
	public static void showMemory(){	
        //JVM이 현재 시스템에 요구 가능한 최대 메모리량, 이 값을 넘으면 OutOfMemory 오류가 발생 합니다.
        long max = r.maxMemory() / (1024*1024);
        //JVM이 현재 시스템에 얻어 쓴 메모리의 총량
        long total = r.totalMemory() / (1024*1024) ;
        //JVM이 현재 시스템에 청구하여 사용중인 최대 메모리(total)중에서 사용 가능한 메모리
        long free = r.freeMemory() / (1024*1024);
        System.out.println("Max:" + max + ", Total:" +total + ", Free:"+free);           

	}
	public static void start(){
		startTime = System.nanoTime();
		//System.out.println("start nano-time = " + startTime);
	}
	public static void end(){
		long endTime = System.nanoTime();  
		//System.out.println("end nano-time = " + endTime);  
		long elapsedTime = endTime - startTime;
		System.out.println("total elapsed time = " + (double)elapsedTime / 1000000000.0);
	}
	/**
     * key값이 있는 배열과 value 값이 있는 두배열을 퀵정렬함
     * @param String keyArray 빈도수를 계산할 문자
     * @param String[] valueArray 문자들이 저장되어 있는 배열
     * @return left 처음 값은 0
     * @return right 처음 값은 배열의 크기 - 1 
     */
	public static void quickSort(ArrayList<String> keyArray, ArrayList<Double> valueArray, int left, int right,String type) throws Exception
	{
 		int i = left;
 		int j = right;
 		Double mid;
        if ( right > left){
           mid =  valueArray.get( (left + right ) / 2 );
           while( i <= j ){
        	  if(type.equals("asc"))
          	  {
        		   while( ( i < right ) && (  valueArray.get(i) < mid )) ++i;
                   while( ( j > left ) && ( valueArray.get(j) > mid )) --j;
                   if( i <= j ){
                 	  swapArrayElement(keyArray , valueArray, i, j);
                      ++i;
                      --j;}   
          	  }else if(type.equals("desc")){
          		  while( ( i < right ) && (  valueArray.get(i) > mid )) ++i;
          		  while( ( j > left ) && ( valueArray.get(j) < mid )) --j;
          		  if( i <= j ){
          			  swapArrayElement(keyArray , valueArray, i, j);
          			  ++i;
          			  --j;} 
          	  }
           }
           if( left < j )
        	   quickSort( keyArray , valueArray, left, j ,type);

           if( i < right )
        	   quickSort( keyArray , valueArray, i, right ,type );
        }
    }
	/**
     * key값이 있는 배열과 value 값이 있는 두배열의 값을 바꿈
     * @param String keyArray 빈도수를 계산할 문자
     * @param String[] valueArray 문자들이 저장되어 있는 배열
     * @return i 바꿀 값의 인덱스
     * @return j 바꿀 값의 인덱스  
     */
	public static void swapArrayElement(ArrayList<String> keyArray, ArrayList<Double> valueArray, int i, int j) throws Exception
	{
		Double temp;   	 	
   	 	temp = valueArray.get(i);
   	 	valueArray.set(i, valueArray.get(j)) ;
   	 	valueArray.set(j, temp) ;
   	 	
   	 	String temp1 = keyArray.get(i);
   	    keyArray.set(i, keyArray.get(j)) ;
   	    keyArray.set(j, temp1) ;            
    }
	public static ArrayList<String> makeArrayList(String line,String split){
		ArrayList<String> result = new ArrayList<String>();
		String[] temp = line.split(split);
		for(String t:temp){
			result.add(t);
		}
		return result;
	}
	public static Double similarity(ArrayList<String> source, ArrayList<String> target){
		HashMap<String, Double> s = new HashMap<String, Double>();
		HashMap<String, Double> t = new HashMap<String, Double>();		
		for(String w : source){
			s.put(w, 1.0);
		}
		for(String w : target){
			t.put(w, 1.0);
		}
		Double result = similarity(s, t);
		return result;
	}	
	/**
	 * 하나의 벡터의 자질과 가중치를 hashmap에 저장 했을 때 두 벡터간의 코사인 유사도 계산 
	 * @param source
	 * @param target
	 * @return
	 */
	public static Double similarity(HashMap<String, Double> source, HashMap<String, Double> target){
		int i = 0;
		Double result_weight =0.0;
		try{
			Double numerator = 0.0;
			Double sum_source = 0.0;
			Double sum_target = 0.0;			
			if(source != null && target != null){
				Object[] s = source.entrySet().toArray();
				/**
				 * 하나의 벡터의 자질들을 배열로 변환
				 */
				for(i = 0; i < s.length; i++)	{					
					String[] temp = s[i].toString().split("=");
					/**
					 * 하나의 벡터의 자질이 다른 벡터에 있으면 가중치 계산에 적용
					 */
					if(target.get(temp[0]) != null){ //다른 벡터에 같은 단어가 있으면
						numerator = numerator + (target.get(temp[0]) * Double.parseDouble(temp[1]));
					}
					sum_source = sum_source + Double.parseDouble(temp[1]) * Double.parseDouble(temp[1]);
				}
				Object[] t = target.entrySet().toArray();
				/**
				 * 하나의 벡터의 자질들을 배열로 변환
				 */
				for(i = 0; i < t.length; i++)	{	
					String[] temp = t[i].toString().split("=");					
					sum_target = sum_target + Double.parseDouble(temp[1]) * Double.parseDouble(temp[1]);
				}
				sum_source = Math.sqrt(sum_source);
				sum_target = Math.sqrt(sum_target);
				
				if((sum_source*sum_target) != 0.0){
					result_weight = numerator / (sum_source * sum_target);
				}
								
			}else{
				System.out.println("source vetor is null.");
			}
		}catch(Exception e){
			result_weight = null;		
		}		
		return result_weight;
	}
	public static boolean checkReqularExpression(String pattern, String sentence){
		boolean result = true;
		String error ="";
		try{
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(sentence);
			error = pattern +"\t"+sentence;
			if(!m.find()) { //템플릿의 페턴 체크				
				result = false;
			}else{				
				result = true;			
			}
		}catch(Exception e){
			System.out.println("checkReqularExpression error("+error+") : "+e);
			result = false;
		}		
		return result;
	}
	public static String findReqularExpresstion(String pattern, String sentence){
		String error ="";
		String result = "";
		try{
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(sentence);
			error = pattern +"\t"+sentence;
			if(!m.find()) { //템플릿의 페턴 체크
			}else{				
				result = m.group(0);			
			}			
		}catch(Exception e){
			System.out.println("checkReqularExpression error("+error+") : "+e);
			result = "";
		}
		return result;
	}
}
