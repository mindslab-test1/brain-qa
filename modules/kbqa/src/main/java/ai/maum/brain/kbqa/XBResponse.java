package ai.maum.brain.kbqa;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class XBResponse
{
	public class KeyValuePairMap
	{
		private HashMap<String, String> m_kv;
		
		public KeyValuePairMap()
		{
			m_kv=new HashMap<String, String>();
		}

		// # 아래 유형의 json문자열을 파싱하여 key, value 목록을 추출한다
		// # 예) key는 'uri'와 'score'를 추출하고, value로는 'http~~'와 '100'을 추출한다
		// #     {"uri":"http://xb.saltlux.com/resource/0000588146","score":100}
		public Boolean parse(String json)
		{
			
			ContainerFactory containerFactory = new ContainerFactory()
			{
				public List creatArrayContainer() 
				{
					return new LinkedList();
				}

				public Map createObjectContainer() 
				{
					return new LinkedHashMap();
				}
			};

			try
			{
				json=json.trim();
				
				if (json.length()==0) return false;
				if (json.charAt(0)!='{' || json.charAt(json.length()-1)!='}') return false;
				if (json.equals("{}")==true) return false;
				else 
				{
					JSONParser parser = new JSONParser();
					Map json_map = (Map) parser.parse(json, containerFactory);
					Iterator iter = json_map.entrySet().iterator();
					while (iter.hasNext())
					{
						Map.Entry entry = (Map.Entry)iter.next();
						
						String keyname = (String)entry.getKey();
						if (keyname.equals("dic_info")==true)
						{
							LinkedList dic_lst = (LinkedList) entry.getValue();
							Iterator<String> iter2 = dic_lst.iterator();
							while (iter2.hasNext())
							{
								m_kv.put(keyname, iter2.next());
							}
						}
						if (keyname.equals("confidence")==true)
						{
							//System.out.println("confidnece. put="+entry.getValue().toString());
							m_kv.put(keyname, entry.getValue().toString());
						}
						else 
						{
							//System.out.println("keyname="+keyname);
							m_kv.put(keyname, (String)entry.getValue());
						}
					}
					return true;
				}
			} catch (ParseException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		public String get(String key)
		{
			Object rc = m_kv.get(key);
			if (rc==null)
			{
				return "";
			} else {
				return (String)rc;
			}
		}
		
		public int size()
		{
			return m_kv.size();
		}
		

		//{"candidate":"1900년하계올림픽사이클","confidence":1.0,"basis":"http:\/\/xb.saltlux.com\/resource\/0000562357"},
		public String toString()
		{
			String r="{";
			
			r=r+"\"candidate\":\""+m_kv.get("candidate")+"\",";
			r=r+"\"confidence\":"+m_kv.get("confidence")+",";
			r=r+"\"basis\":\""+m_kv.get("basis")+"\"";
			r=r+"}";
			return r;
		}
	}

	/**/
	private String m_duration;
	private List<KeyValuePairMap> m_results;
	
	public XBResponse()
	{
		this("0.0");
	}
	
	public XBResponse(String duration)
	{
		m_duration = duration;
		m_results = new Vector<KeyValuePairMap>();
	}
	
	public String duration()
	{
		return m_duration;
	}
	
	// # 예) [{"uri":"http://xb.saltlux.com/resource/0000588146","score":100},
	// #	  {"uri":"http://xb.saltlux.com/resource/0000577763","score":69.1},
	// #	  {"uri":"http://xb.saltlux.com/resource/0000240701","score":69.1},
	// #	  {"uri":"http://xb.saltlux.com/resource/0000588146","score":69.1},
	// #	  {"uri":"http://xb.saltlux.com/resource/0000283464","score":61.8}]
	public Boolean parse(String response)
	{
		response=response.trim();
		
		if (response.length()==0) return false;
		if (response.equals("[]")==true) 
		{
			return true;
		} else {
			if (response.charAt(0)=='[' && response.charAt(response.length()-1)==']') 
			{
				response=response.substring(1, response.length()-1);
			}
			response=response.replace("},{", "}\t{");
			StringTokenizer tk = new StringTokenizer(response, "\t");
			while(tk.hasMoreTokens()) 
			{
				String candidate = tk.nextToken();
				
				//System.out.println("candidate="+candidate);
				
				if (candidate.equals("{}")==false && candidate.equals("{\"result\":[]}")==false)
				{
					KeyValuePairMap kv = new KeyValuePairMap();
					
					kv.parse(candidate);
					m_results.add(kv);
				}
			}
			return true;
		}
	}
	
	public int size()
	{
		return m_results.size();
	}
	
	public String getKeyValue(int index, String keyname)
	{
		if (index >=0 && index < m_results.size()) 
		{
			KeyValuePairMap kv = m_results.get(index);
			
			return kv.get(keyname);
		} else {
			return new String("");
		}
	}
	
	public KeyValuePairMap get(int index)
	{
		if (index >=0 && index < m_results.size()) 
		{
			return m_results.get(index);
		} else {
			return null;
		}
	}
	
	// keyname의 값이 value인 요소를 찾아서 인덱스를 반환한다
	// 찾는데 실패하면 -1을 반환한다
	public int find(String keyname, String value)
	{
		for (int i=0; i < m_results.size(); i++)
		{
			KeyValuePairMap kv = m_results.get(i);
			
			String value_A = kv.get(keyname);
			
			if (value_A.equals(value)==true) 
			{
				return i;
			}
		}
		return -1;
	}
	
	// # source의 결과들을 append한다
	// # candidate키의 값이 같은 요소가 이미 있으면 confidence의 값을 비교해서 큰 요소가 append되고
	// # 작은 요소는 append되지 않고 무시된다
	public void append(XBResponse source)
	{
		// # m_results에 keyname의 값이 같은 요소가 있는 지 검사한다
		for (int i=0; i < source.size(); i++)
		{
			String label=source.getKeyValue(i, "candidate");
			if (this.size()>0)
			{
				int target_index = this.find("candidate", label);
				if (target_index==-1)
				{	// 없는 경우, 추가한다
					m_results.add(source.get(i));
				} else {
					// 이미 있는 경우, confidence값을 비교해서 큰 것만 유지한다
					String source_confidence=source.getKeyValue(i, "confidence");
					if (source_confidence.length()==0)
					{
						//System.out.println("source label="+source.getKeyValue(i, "candidate"));
						//System.out.println("source size="+String.valueOf(source.size()));
						source_confidence="0.0";
					}
					String target_confidence=this.getKeyValue(target_index, "confidence");
					if (target_confidence.length()==0)
					{
						//System.out.println("this label="+this.getKeyValue(i, "candidate"));
						target_confidence="0.0";
					}
					if (Double.parseDouble(source_confidence) > Double.parseDouble(target_confidence))
					{
						m_results.set(target_index, source.get(i));	// overwrite
					}
				}
			} else {// this에 result가 없는 경우
				//System.out.println("this에 없다");
				m_results.add(source.get(i));
			}
		}
	}

	// # 하나의 라인에 다음 형식의 문자열로 변환한다
	//{"candidate":"1900년하계올림픽사이클","confidence":1.0,"basis":"http:\/\/xb.saltlux.com\/resource\/0000562357"},
	//{"candidate":"1904년하계올림픽체조남자링","confidence":0.9682267094929861,"basis":"http:\/\/xb.saltlux.com\/resource\/0000603941"},
	//{"candidate":"1904년하계올림픽체조남자복합","confidence":0.9568404591764565,"basis":"http:\/\/xb.saltlux.com\/resource\/0000502201"},
	//{"candidate":"제30회하계올림픽태권도","confidence":0.9567183298800692,"basis":"http:\/\/xb.saltlux.com\/resource\/0000172870"},
	//{"candidate":"1904년하계올림픽테니스","confidence":0.9487958934604493,"basis":"http:\/\/xb.saltlux.com\/resource\/0000140291"}
	public String toString(int MaxLimit)
	{
		String r="";
		
		for (int i=0; i < m_results.size() && i < MaxLimit; i++)
		{
			if (r.length()>0) 
			{
				r=r+",";
			}
			r=r+m_results.get(i).toString();
		}
		return r;
	}
	

	// # Score기반 정렬
	class ConfidenceComparator implements Comparator 
	{
		public int compare(Object arg0, Object arg1)
		{
			KeyValuePairMap A = (KeyValuePairMap)arg0;
			KeyValuePairMap B = (KeyValuePairMap)arg1;
			
			double A_confidence = Double.parseDouble(A.get("confidence"));
			double B_confidence = Double.parseDouble(B.get("confidence"));
			
			if (A_confidence > B_confidence)
			{
				return -1;
			} 
			else if (A_confidence == B_confidence)
			{
				return 0;
			} 
			else 
			{
				return 1;
			}
		}
	}
	
	public void sort()
	{
		Collections.sort(m_results, new ConfidenceComparator());
	}
	
}
