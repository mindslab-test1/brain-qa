package ai.maum.brain.kbqa;

public class Property {
	String original_propery = ""; //원본속성
	String first_nor_property = ""; //1차 정규화
	String second_nor_property = ""; //2차 정규화
	
	StructuredDB db = null;
	
	public Property(StructuredDB db){
		this.db = db;
	}
	
	public String getOriginalPropery() {
		return original_propery;
	}
	public void setOriginalPropery(String original_propery) {
		this.original_propery = original_propery;
		setFirstNorProperty();
		setSecondNorProperty();
	}
	public String getFirstNorProperty() {
		return first_nor_property;
	}
	public void setFirstNorProperty() {
		this.first_nor_property = applyPropertyNor(original_propery, db.question,"fp_");
	}
	public String getSecondNorProperty() {
		return second_nor_property;
	}
	public void setSecondNorProperty() {
		this.second_nor_property = applyPropertyNor(original_propery,  db.question, "sp_");		
	}
	public String applyPropertyNor(String property,String question,String tag){
		String property_nor ="";
		try {
			property = property.replaceAll(" ", "");
			/**
			 * 기본은 들어온 속성
			 */
			property_nor = property;
			if(db.getKB(tag+property) != null && !db.getKB(tag+property).equals("")){	
				property_nor = db.getKB(tag+property).trim();
			}
//			if(tag.equals("fp_")){
//				if(StructuredDB.property_matching_dic.get(property) != null){	
//					property_nor = StructuredDB.property_matching_dic.get(property).trim();
//				}
//			}else if(tag.equals("sp_")){
//				if(StructuredDB.property_second_matching_dic.get(property) != null){	
//					property_nor = StructuredDB.property_second_matching_dic.get(property).trim();
//				}
//			}
			
			String question_temp = question.replaceAll(" ", "");
			
			if(property_nor.equals("출생일") && question_temp.contains("어디")){
				property_nor = "출생지";
			}else if(property_nor.equals("사망일") && question_temp.contains("어디")){
				property_nor = "사망장소";
			}else if(property_nor.equals("발생일") && question_temp.contains("어디")){
				property_nor = "발생지";
			}else if(property_nor.equals("날짜") && question_temp.contains("어디")){
				property_nor = "장소";
			}else if(property_nor.contains("년도") && question_temp.contains("사망")){
				property_nor = "사망일";
			}
					
			else if(question_temp.contains("살고") && question_temp.contains("어디")){
				property_nor = "거주지";
			}else if(question_temp.contains("창립") && question_temp.contains("어디")){
				property_nor = "창립지";
			}else if(question_temp.contains("사망") && question_temp.contains("언제")){
				property_nor = "사망일";
			}else if(question_temp.contains("어디") && question_temp.contains("태어")){
				property_nor = "출생지";
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
				
		return property_nor;
	}
	public boolean checkEntityLinking(){
		try {
			if(original_propery.equals("출생일") || original_propery.equals("사망일")){
				return true;
			}else if(first_nor_property.equals("출생일") || first_nor_property.equals("사망일")){
				return true;
			}else if(second_nor_property.equals("출생일") || second_nor_property.equals("사망일")){
				return true;
			}else{
				return false;
			}
		}catch (Exception e) {
			e.printStackTrace();			
		}
		return false;
		
	}
	public String getInfo(){
		return original_propery+"@=@"+first_nor_property+"@=@"+second_nor_property;
	}
}
