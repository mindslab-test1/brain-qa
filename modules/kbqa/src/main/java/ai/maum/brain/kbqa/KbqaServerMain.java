package ai.maum.brain.kbqa;

import ai.maum.brain.kbqa.util.PropertiesManager;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.netty.NettyServerBuilder;
import maum.brain.qa.QuestionAnswerServiceGrpc;
import maum.brain.qa.QuestionAnswerServiceGrpc.QuestionAnswerServiceBlockingStub;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class KbqaServerMain {

  private static Properties properties = PropertiesManager.getProperties();

  private Server server;
  private ManagedChannel channel;

  public static void main(String[] args) {
    KbqaServerMain server = new KbqaServerMain();
    server.setLog();
    server.start();
    server.blockUntilShutdown();
  }

  private void start() {
    try {
      int port = Integer.parseInt(properties.getProperty("KBQA_PORT", "51000"));
      long idleTimeout = Integer.parseInt(properties.getProperty("TIME_OUT", "10"));
      long ageTimeout = Integer.parseInt(properties.getProperty("TIME_OUT", "10"));

      String questionAnalysisIP = properties.getProperty("QUESTION_ANALYSIS_IP", "127.0.0.1");
      int questionAnalysisPort = Integer
          .parseInt(properties.getProperty("QUESTION_ANALYSIS_PORT", "9861"));
      System.setProperty("java.library.path",System.getProperty("java.library.path") + ":" + properties.getProperty("QUESTION_ANALYSIS_LIB_PATH"));

      // Library Path Update.
      final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
      sysPathsField.setAccessible(true);
      sysPathsField.set(null, null);

      channel = ManagedChannelBuilder
          .forAddress(questionAnalysisIP, questionAnalysisPort).usePlaintext(true).build();
      QuestionAnswerServiceBlockingStub stub = QuestionAnswerServiceGrpc.newBlockingStub(channel);
      server = NettyServerBuilder.forPort(port)
          .maxConnectionIdle(idleTimeout, TimeUnit.SECONDS)
          .maxConnectionAge(ageTimeout, TimeUnit.SECONDS)
          .addService(new KbqaService(stub)).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          KbqaServerMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (NoSuchFieldException e) {
      e.printStackTrace();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
    if (channel != null) {
      channel.shutdown();
    }
  }

  private void setLog() {
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    JoranConfigurator joranConfigurator = new JoranConfigurator();
    joranConfigurator.setContext(loggerContext);
    loggerContext.reset();
    try {
      joranConfigurator.doConfigure(PropertiesManager.resolveValueWithProVars(properties.getProperty("LOG_CONFIG_PATH")));

    } catch (JoranException e) {
      e.printStackTrace();
    }
    StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);

  }
}
