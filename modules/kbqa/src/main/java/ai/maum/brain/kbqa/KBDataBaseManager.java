package ai.maum.brain.kbqa;

import org.rocksdb.RocksDB;
import org.rocksdb.Options;
import org.rocksdb.RocksDBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KBDataBaseManager {
    private RocksDB db;
    private String dbPath;
    private static Logger logger = LoggerFactory.getLogger(KbqaService.class);

    public KBDataBaseManager(String path) {
        dbPath = path;
    }

    public void open() {
        RocksDB.loadLibrary();

        // the Options class contains a set of configurable DB options
        // that determines the behaviour of the database.
        try (Options options = new Options().setCreateIfMissing(true)) {
            db = RocksDB.open(options, dbPath);

        } catch (RocksDBException e) {
            // do some error handling
            logger.error(e.getMessage());
        }
    }

    public byte[] get(String key) {
        try {
            byte[] value = db.get(key.getBytes());
            return value;
        } catch (RocksDBException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    public String getString(String key) {
        try {
            byte[] value = db.get(key.getBytes());
            String ret = new String(value);
            return ret;

        } catch (RocksDBException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    public void put(String key, String value) {
        try {
            if (db.get(key.getBytes()) == null) {
                logger.info(key + " " + value);
                db.put(key.getBytes(), value.getBytes());
            } else {
                logger.warn("already exist data. " + key);
            }
        } catch (RocksDBException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public void close() {
        try {
            db.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
