package ai.maum.brain.kbqa;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import kr.re.etri.kmtk.io.Rocks;

public class KBResource {
	//knowledge base를 저장한 자료구조
	long startTime = 0; 
	public Rocks kb = null;
	int count = 0;
	
	HashMap<String, String> pattern_ep = new HashMap<String, String>();
	
	public void start(){
		startTime = System.nanoTime();
		System.out.println("start nano-time = " + startTime);
	}
	public void end(){
		long endTime = System.nanoTime();  
		System.out.println("end nano-time = " + endTime);  
		long elapsedTime = (endTime - startTime) / 1000000000;
		System.out.println("total elapsed time = " + elapsedTime);
	}
	
	public void addKB2Rocks(String source_file){
		BufferedReader in;
		String line="";
		try {
			if(!source_file.equals("")){
				in = new BufferedReader(new FileReader(source_file));
				line = "";
				while((line = in.readLine()) != null) {				
					String[] temp = line.split("\t");					
					if(temp.length < 3){
					}else{
						String key = temp[0]+"_"+temp[1];
						String value = temp[2];						
						if(kb.get(key) == null){
							kb.put(key, value);		
						}else{							
							String value_list = kb.get(key).toString();
							kb.delete(key);
							kb.put(key, value_list+", "+value);
						}
					}	
					if(count % 1000000 == 0){
						System.out.print(".");;
					}
					count++;					
				}
				System.out.println();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(count+"  " +line);
			e.printStackTrace();
		}		
	}
	public void addResource2Rocks(String source_file,String tag,int key,int value){
		BufferedReader in;
		String line="";
		try {
			if(!source_file.equals("")){
				in = new BufferedReader(new FileReader(source_file));
				line = "";		
				while((line = in.readLine()) != null) {				
					String[] temp = line.split("\t");
					if(temp.length > 1){
						if(kb.get(tag+temp[key]) == null){
							kb.put(tag+temp[key], temp[value]);		
						}
					}						
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(count+"  " +line);
			e.printStackTrace();
		}			
	}
	
	public void readDB(boolean readonly, String source_file,String property_matching_dic_file, String entity_matching_dic_file,
			String source_keyword_file,String source_index_file,String order_nor_dic_file){
		BufferedReader in;
		String line="";
		int count = 0;
		
		try {	
			System.out.print("[KBDB]");			
			kb = new Rocks("/data2/KB_bae/data/kb/db", readonly);	
			//kb = new Rocks("/data2/KB_bae/data/kb/db", readonly);
			if(readonly == false){
				addResource2Rocks(property_matching_dic_file, "fp_", 1, 0); //1차 속성 정규화 사전
				addResource2Rocks(property_matching_dic_file.replaceAll("matching", "second_matching"), "sp_", 1, 0); // 2차속성정규화 사전
				addResource2Rocks(entity_matching_dic_file, "en_", 0, 1); //개체 정규화 사전
				addResource2Rocks(source_keyword_file, "sk_", 0, 1); //출처 키워드
				addResource2Rocks(order_nor_dic_file, "on_", 1, 0); //계보 정규화
				addResource2Rocks(source_index_file, "si_", 1, 0);//출처 인덱스
				
				addKB2Rocks(source_file); //정형데이터 읽기
			}//	
			System.out.println(" read db.");	
//				System.out.println("read KBDB(key_property/value) - "+kb.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(count+"  " +line);
			e.printStackTrace();
		}
	}
	public void readDB(boolean readonly, String db_file){
		BufferedReader in;
		String line="";
		int count = 0;
		
		try {	
			System.out.print("[KBDB]");			
			kb = new Rocks(db_file, readonly);
			System.out.println(" read db.");
//				System.out.println("read KBDB(key_property/value) - "+kb.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(count+"  " +line);
			e.printStackTrace();
		}
	}
	public void readDB(boolean readonly, String db_file,String pattern_spo_file){
		BufferedReader in;
		String line="";
		int count = 0;
		
		try {	
			System.out.print("[KBDB]");			
			kb = new Rocks(db_file, readonly);
			if(!pattern_spo_file.equals("")){
				in = new BufferedReader(new FileReader(pattern_spo_file));
				line = "";
				while((line = in.readLine()) != null) {				
					String[] temp = line.split("@==@");	//패턴 부분과 개체/속성을 구분하여 분리				
					if(temp.length < 2){
					}else{
						String key = temp[0];
						String value = temp[1];						
						if(pattern_ep.get(key) == null){
							pattern_ep.put(key, value);		
						}
					}										
				}
				System.out.println("read pettern - "+pattern_ep.size());
			}
			System.out.println(" read db.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(count+"  " +line);
			e.printStackTrace();
		}
	}
	public void closeDB() {
		kb.close();
		return;
	}
}
