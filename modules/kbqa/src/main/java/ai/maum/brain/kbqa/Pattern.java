package ai.maum.brain.kbqa;

import java.util.Map.Entry;

public class Pattern {
	public String pattern_country = "(신라|백제|고구려|고려|조선)+";
	public String pattern_order = "([0-9]+번째|[ㄱ-힝]+번째|[0-9]+대|초대|최초|마지막|[0-9]+년)+";
	public String pattern_king = pattern_country +".+(왕|임금)+";
	public String pattern_current = "(우리나라|대한민국|한국|현재|현|지금|누구)+";
	public String pattern_number_year = "([0-9]+호|[0-9]+회|[0-9]+년)+";
	public String pattern_pre_next = "(다음|이전|전임|후임)+";
	
	public String findSPbyPattern(String question,StructuredDB db){
		String result ="";
		/**
		 * 패턴기반 개체/속성인식 - 정규식 패턴 사전 기반으로 인식
		 */				
		for(Entry<String,String> entry: db.kr.pattern_ep.entrySet()){
			/**
			 * 적용할 패턴이 여러개인 경우 모두 체크
			 */
			String[] re = entry.getKey().split("@=@");
			boolean check = false;
			for(String r : re){
				if(Util.checkReqularExpression(r, question)){
					check = true;
				}else{
					/**
					 * 정규식이 적용이 되지 않으면 false
					 */
					check = false;
					break;
				}
			}
			/**
			 * 모든 정규식이 적용이 되는 경우 종료
			 * 현재 정규식 패턴에 매칭되는 entity, propery를 반환
			 */
			if(check){
				result = entry.getValue();
				break;
			}
		}
		return result;
	}
	
	public void checkPattern(QAnal qanl,StructuredDB db){
		String question = qanl.question;
		try {
			boolean check_country = checkCountry(qanl);
			String pattern_type = "";
			
			pattern_type = findSPbyPattern(question,db);			
			
			if(pattern_type.contains("@=@")){				
			}else if(Util.checkReqularExpression(pattern_order, question) && Util.checkReqularExpression("[0-9]{0,}절기", question)){
				pattern_type = "절기";
			}
//			else if(Util.checkReqularExpression("(다음|이전)+", question) && Util.checkReqularExpression("[0-9]{0,}절기", question)){
//				pattern_type = "절기_전후";
//			}		
			/**
			 * 몇대인지 계보를 인식하는 패턴
			 */
			else if(Util.checkReqularExpression("[몇]+(번째|대)+", question) && Util.checkReqularExpression("대통령", question) && check_country){
				pattern_type = "대통령_대";
			}else if(Util.checkReqularExpression("[몇]+(번째|대)+", question) && Util.checkReqularExpression(pattern_king, question)){
				pattern_type = "왕_대";
			}
			/**
			 * 왕과 대통령의 전임, 후임을 인식하는 패턴
			 */
			else if(Util.checkReqularExpression(pattern_pre_next, question) && Util.checkReqularExpression(pattern_king, question)){
				pattern_type = "왕_전후임";
			}else if(Util.checkReqularExpression(pattern_pre_next, question) && Util.checkReqularExpression("대통령", question) && check_country){
				pattern_type = "대통령_전후임";
			}else if(Util.checkReqularExpression(pattern_pre_next+".*(왕|임금)+", question)){
				pattern_type = "왕_전후임";
			}else if(Util.checkReqularExpression("(왕|임금)+.*"+pattern_pre_next, question)){
				pattern_type = "왕_전후임";
			}
			/**
			 * 역대 대통령, 미국 대통령을 인식하는 패턴
			 */
			else if(Util.checkReqularExpression(pattern_order, question) && Util.checkReqularExpression("미국.+대통령", question) && check_country){
				pattern_type = "미국대통령";
			}else if(Util.checkReqularExpression(pattern_order, question) && Util.checkReqularExpression("대통령", question) && check_country){
				pattern_type = "대통령";
			}
			/**
			 * 역대 왕을 인식하는 패턴
			 */
			else if(Util.checkReqularExpression(pattern_order, question) && Util.checkReqularExpression(pattern_king, question)){
				pattern_type = Util.findReqularExpresstion(pattern_country, question)+"왕";
			}
			
			else if(Util.checkReqularExpression(pattern_number_year, question) && Util.checkReqularExpression("(국보|보물|기념물|문화재자료|문화재|명승|사적)+", question)){
				pattern_type = "문화재";
			}
			/**
			 * 현재 대통령을 인식하는 패턴
			 */
			else if(Util.checkReqularExpression(pattern_current, question) && Util.checkReqularExpression("미국.*대통령", question) 
					&& !Util.checkReqularExpression("(임기|대행|출생지)+", question) && qanl.LAT.equals("대통령") && check_country){
				pattern_type = "미국대통령_현재";
			}else if(Util.checkReqularExpression(pattern_current, question) && Util.checkReqularExpression("대통령", question) 
					&& !Util.checkReqularExpression("(임기|대행|출생지)+", question) && qanl.LAT.equals("대통령")  && check_country){
				pattern_type = "대통령_현재";
			}
			/**
			 * 올립픽과 월드컵을 인식하는 패턴
			 */
			else if(Util.checkReqularExpression(pattern_number_year, question) && Util.checkReqularExpression("(올림픽|월드컵)+", question) && Util.checkReqularExpression("(열린|개최|언제|몇)+", question)){
				pattern_type = Util.findReqularExpresstion("(올림픽|월드컵)+", question);
			}else if(Util.checkReqularExpression("(올림픽|월드컵)+", question) && Util.checkReqularExpression("(열린|개최|언제|몇)+", question)){
				pattern_type = Util.findReqularExpresstion("(올림픽|월드컵)+", question);
			}else if(Util.checkReqularExpression("(올림픽|월드컵)+", question) && Util.checkReqularExpression("(이번|다음|지난)+", question)){
				pattern_type = Util.findReqularExpresstion("(올림픽|월드컵)+", question);
			}else{
				pattern_type = "-";
			}	
			
			db.pattern_type = pattern_type;
			qanl.pattern_type = pattern_type;
			
		} catch (Exception e) {
			e.printStackTrace();			
		}		
	}
	public static boolean checkCountry(QAnal qanl){
		boolean result = false;
		try {
			String country = "";
			for(NE ne : qanl.ne_list){
				if(ne.getNEtype().contains("COUNTRY")){
					country = ne.getText();
				}
			}		
			if(country.equals("한국") || country.equals("우리나라") || country.equals("대한민국") || country.equals("미국") || country.equals("")){
				result = true;
			}
		}catch (Exception e) {
			e.printStackTrace();			
		}	
		
		return result;
	}
}
