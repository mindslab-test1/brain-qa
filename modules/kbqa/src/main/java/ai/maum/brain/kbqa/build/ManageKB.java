package ai.maum.brain.kbqa.build;

public class ManageKB {
	static String out_folder = "";
	static String source_file = "";	
	static String mode = "";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * 경로에는 한글이 없어야 한다.
		 */
		//String path = "D:/exobrain/단문튜닝/정형DB/StructuredData-20170712";
		//args = new String[]{"-s","./input_delete_test.txt","-d","./test_rocks"}; 
		
		readArgs(args);
				
		if(!source_file.equals("") && !out_folder.equals("")){			
			RocksDB db = new RocksDB();			
			db.makeRockDB(out_folder);		
			db.initPW(out_folder+"_results.txt");
			db.KB2RockDB(source_file);
			db.closePW();
		}else{
			System.out.println("please check source file or out folder");
		}
	}
	
	public static void readArgs(String[] args){
		if(args.length < 1){
			System.out.println("please check args.");
		}else{
			for(int i = 0 ; i < args.length; i++){
				if(args[i].equals("-d")){
					if(args.length > (i + 1)){
						out_folder = args[i+1];
					}else{
						System.out.println("please check option - out directory");
					}
				}else if(args[i].equals("-s")){
					if(args.length > (i + 1)){
						source_file = args[i+1];
					}else{
						System.out.println("please check option - source file");
					}
				}
			}
		}
	}
}
