package ai.maum.brain.kbqa;

import kr.re.etri.kmtk.io.Rocks;
import org.rocksdb.RocksDBException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MariaDB {

    private static Logger logger = LoggerFactory.getLogger(MariaDB.class);

    private Connection con = null;

    public MariaDB () {
        // ,..
    }

    public void init (String dbURL, String id, String pw) {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            con = DriverManager.getConnection(dbURL, id, pw);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public void close () {
        try {
            if (con != null) {
                con.close(); // 필수 사항
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void select (String stmt) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

       if(con == null) {
           logger.error("DB is not connected!");
           return ;
       }

       try {
           pstmt = con.prepareStatement(stmt);
           rs = pstmt.executeQuery();

           logger.info("Select Result ");
           while (rs.next()) {
//               logger.info(rs.getString("S"));
           }
       } catch (SQLException e) {
           logger.error(e.getMessage());
           return;
       }
       logger.info("Select Complete!");
    }

    public void executeQuery (String stmt) {
        PreparedStatement pstmt = null;

        if(con == null) {
            logger.error("DB is not connected!");
            return ;
        }

        // parse text
        logger.info("Statemant : " + stmt);
        try {
            pstmt = con.prepareStatement(stmt);
            pstmt.executeQuery();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return;
        }
        logger.info("Query Complete!");
    }

    public void ParseToDB(String source_file, String domain){
        BufferedReader in;
        String line="";
        String dom="common";

        // assign domain
        if(domain != null && !domain.isEmpty()) {
            dom = domain;
        }

        try {
            if(con == null) {
                logger.error("DB is not connected!");
                return ;
            }

            in = new BufferedReader(new FileReader(source_file));
            line = "";
            while ((line = in.readLine()) != null) {
                /**
                 * source file의 데이터는 탭으로 구분되어 있어야한다.
                 * 빌호툴로스	국적	핀란드@=@259301_wd@=@0.824;
                 * 0: subject
                 * 1: property
                 * 2: object, object는 object@=@출처아이디@=@신뢰도로 구성되어 있어야한다.
                 */
                String[] temp = line.split("\t");
                if (temp.length <= 3) {
                    String mode = "";
                    if (temp.length == 3) {
                        mode = temp[2];
                    } else {
                        mode = "i";
                    }

                    if (temp[0].contains("fp_")) {
                        logger.info("property info");
                        String key = dom + "_" + temp[0];
                        if (mode.equals("i")) {
                            String stmt = "INSERT INTO PROPERTY_DIC(main, allomorph, domain, domain_allo) VALUE(\'" + temp[0] + "\',\'" + temp[1] + "\',\'" + dom + "\',\'" + key + "\')";
                            executeQuery(stmt);
                        } else if (mode.equals("d")) {
                            String stmt = "DELETE FROM PROPERTY_DIC WHERE domain_allo=\'" +  key + "\'";
                            executeQuery(stmt);
                        } else if (mode.equals("u")) {
                            String stmt = "UPDATE PROPERTY_DIC SET main=\'" +  temp[0] + "\', allomorph=\'" +  temp[1] + "\', domain=\'" +  dom + "\' WHERE domain_allo=\'" +  key + "\'";
                            executeQuery(stmt);
                        }
                    } else if (temp[0].contains("en_")) {
                        logger.info("entity info");
                        String key = dom + "_" + temp[0];
                        if (mode.equals("i")) {
                            String stmt = "INSERT INTO ENTITY_DIC(main, allomorph, domain, domain_allo) VALUE(\'" + temp[0] + "\',\'" + temp[1] + "\',\'" + dom + "\',\'" + key + "\')";
                            executeQuery(stmt);
                        } else if (mode.equals("d")) {
                            String stmt = "DELETE FROM ENTITY_DIC WHERE domain_allo=\'" +  key + "\'";
                            executeQuery(stmt);
                        } else if (mode.equals("u")) {
                            String stmt = "UPDATE ENTITY_DIC SET main=\'" +  temp[0] + "\', allomorph=\'" +  temp[1] + "\', domain=\'" +  dom + "\' WHERE domain_allo=\'" +  key + "\'";
                            executeQuery(stmt);
                        }
                    }
                } else {
                    /**
                     * subject_property를 키로 저장
                     */
                    String key = dom + "_" + temp[0] + "_" + temp[1];
                    /**
                     * object는 value
                     */
                    String value = temp[2];
                    String[] format = value.split("@=@");
                    boolean validate = true;
                    if (format.length != 3) {
                        logger.info(" check object(object@=@출처아이디@=@신뢰도) :" + line);
                        // object가 여러개인 경우, 각 object 에 대한 데이터 유효성 검사
                        String[] objs = value.split(";");
                        for (int i = 0; i < objs.length; i++) {
                            String[] subObjs = objs[i].split("@=@");
                            if (subObjs.length != 3) {
                                validate = false;
                                break;
                            }
                        }
                    } else {
                        /**
                         * 마지막은 추가 및 삭제 여부
                         */
                        validate = true;
                    }

                    // 이상 없을시, Data Insert
                    if (validate) {
                        String mode = "";
                        if (temp.length == 4) {
                            mode = temp[3];
                        } else {
                            mode = "i";
                        }

                        if (mode.equals("i")) {
                            String stmt = "INSERT INTO SPO(s, p, o, domain, domain_s_p) VALUE(\'"
                                    + temp[0] + "\',\'" + temp[1] + "\',\'" + value + "\',\'" + dom + "\',\'" + key + "\')";
                            executeQuery(stmt);
                        } else if (mode.equals("d")) {
                            String stmt = "DELETE FROM SPO WHERE domain_s_p=\'" +  key + "\'";
                            executeQuery(stmt);
                        } else if (mode.equals("u")) {
                            String stmt = "UPDATE SPO SET s=\'" +  temp[0] + "\', p=\'" +  temp[1] + "\', o=\'" + value + "\', domain=\'" +  dom + "\' WHERE domain_s_p=\'" +  key + "\'";
                            executeQuery(stmt);
                        }
                    }
                }
            }
            logger.info("finish.");
        } catch (Exception e) {
            logger.error(source_file + " : " + e.getMessage());
        }
    }

    public void MakeRocksDB(String out_folder, String domain) {
        Rocks kb = null;
        try {
            kb = new Rocks(out_folder, false);
        } catch (RocksDBException e) {
            logger.error(out_folder + " : " + e.getMessage());
        }

        // read SPO, ENTITY_DIC, PROPERTY_DIC
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if(con == null) {
            logger.error("DB is not connected!");
            return ;
        }

        // read SPO table
        try {
            pstmt = con.prepareStatement("SELECT * FROM SPO WHERE domain =\'"+ domain +"\'");
            rs = pstmt.executeQuery();

            logger.info("SPD Table Select Result ");
            int count = 1;
            while (rs.next()) {
                String s = rs.getString("s");
                String p = rs.getString("p");
                String o = rs.getString("o");

                // remove space
                s = s.replaceAll(" ", "");
                p = p.replaceAll(" ", "");

                // wiki data
                if (domain.equals("wp")) {
                    s = s.replace("(", ":");
                    s = s.replace(")", "");
                }

                String key = s + "_" + p;

                if(kb.get(key) == null){
                    kb.put(key, o);
                }else{
                    logger.info(count+" insert error. key is existed in DB. - " + key);
                }
                logger.info(count+" check insert after:"+kb.getString(key));
                count++;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (RocksDBException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        // read ENTITY_DIC table
        try {
            pstmt = con.prepareStatement("SELECT * FROM ENTITY_DIC WHERE domain =\'"+ domain +"\'");
            rs = pstmt.executeQuery();

            logger.info("ENTITY_DIC Table Select Result ");
            int count = 1;
            while (rs.next()) {
                String key = rs.getString("main");
                String allomorph = rs.getString("allomorph");

                if(kb.get(key) == null){
                    kb.put(key, allomorph);
                }else{
                    logger.info(count+" insert error. key is existed in DB. - " + key);
                }
                logger.info(count+" check insert after:"+kb.getString(key));
                count++;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (RocksDBException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        // read PROPERTY_DIC table
        try {
            pstmt = con.prepareStatement("SELECT * FROM PROPERTY_DIC WHERE domain =\'"+ domain +"\'");
            rs = pstmt.executeQuery();

            logger.info("PROPERTY_DIC Table Select Result ");
            int count = 1;
            while (rs.next()) {
                String key = rs.getString("main");
                String allomorph = rs.getString("allomorph");

                if(kb.get(key) == null){
                    kb.put(key, allomorph);
                }else{
                    logger.info(count+" insert error. key is existed in DB. - " + key);
                }
                logger.info(count+" check insert after:"+kb.getString(key));
                count++;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (RocksDBException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        logger.info("Make RocksDB Complete!");
    }

    public void QueryToSPO(String s, String p, String o, String domain, String mode) {
        String key = domain + "_" + s + "_" + p;
        String stmt = "";

        if (mode.equals("i")) {
            stmt = "INSERT INTO SPO(s, p, o, domain, domain_s_p) VALUE(\'"
                    + s + "\',\'" + p + "\',\'" + o + "\',\'" + domain + "\',\'" + key + "\')";
        } else if (mode.equals("d")) {
            stmt = "DELETE FROM SPO WHERE domain_s_p=\'" +  key + "\'";
        } else if (mode.equals("u")) {
            stmt = "UPDATE SPO SET s=\'" +  s + "\', p=\'" +  p + "\', o=\'" + o + "\', domain=\'" +  domain + "\' WHERE domain_s_p=\'" +  key + "\'";
        }

        if(!stmt.isEmpty())
            executeQuery(stmt);

        return;
    }

    public void QueryToEN(String allo, String main, String domain, String mode) {
        String key = domain + "_en_" + allo;
        String stmt = "";

        if (mode.equals("i")) {
            stmt = "INSERT INTO ENTITY_DIC(main, allomorph, domain, domain_allo) VALUE(\'"
                    + main + "\',\'" + allo + "\',\'" + domain + "\',\'" + key + "\')";
        } else if (mode.equals("d")) {
            stmt = "DELETE FROM ENTITY_DIC WHERE domain_allo=\'" +  key + "\'";
        } else if (mode.equals("u")) {
            stmt = "UPDATE ENTITY_DIC SET main=\'" +  main + "\', allomorph=\'" +  allo
                    + "\', domain=\'" +  domain + "\' WHERE domain_allo=\'" +  key + "\'";
        }

        if(!stmt.isEmpty())
            executeQuery(stmt);

        return;
    }
}
