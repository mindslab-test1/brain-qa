package ai.maum.brain.kbqa;

import ai.maum.brain.kbqa.util.PropertiesManager;
import ai.maum.brain.kbqa.util.RestCall;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nanum.exobrain.jni.QAnalWiki;
import io.grpc.stub.StreamObserver;

import maum.brain.qa.Kbqa.*;
import maum.brain.qa.Kbqa.KbqaOutput.Builder;
import maum.brain.qa.KbqaServiceGrpc.KbqaServiceImplBase;
import maum.brain.qa.QuestionAnalysis.QuestionAnalysisInputText;
import maum.brain.qa.QuestionAnswerServiceGrpc.QuestionAnswerServiceBlockingStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

public class KbqaService extends KbqaServiceImplBase {

    private static Logger logger = LoggerFactory.getLogger(KbqaService.class);

    private static Properties properties = PropertiesManager.getProperties();
    private final QuestionAnswerServiceBlockingStub stub;

    private KBCandidateManager mgr_global;
    private boolean flag;
    private static long []ptr;
    private static QAnalWiki qanal;
    private static ReentrantLock lock;
    private boolean state;

    private boolean getState(){
        return this.state;
    }


    private synchronized void setState(boolean state){
        this.state = state;
    }

    public KbqaService(
            QuestionAnswerServiceBlockingStub stub) {
        this.stub = stub;
        qanal = new QAnalWiki();
        try {
            //  path  설정.
            ptr = qanal.InitQAnal(properties.getProperty("QUESTION_ANALYSIS_RSC_PATH"));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        // init 시 rocksDB read
        mgr_global = new KBCandidateManager();  // 정답후보 생성
        flag = mgr_global.init(properties.getProperty("KBDB_PATH"), properties.getProperty("ENTITY_PROPERTY_PATTERN_PATH"));
        setState(true);

        lock = new ReentrantLock();

    }

    @Override
    public void question(KbqaInput request, StreamObserver<KbqaOutput> responseObserver) {

        QuestionAnalysisInputText inputText = QuestionAnalysisInputText.newBuilder().setText(request.getQuestion()).build();
        logger.info(request.getQuestion());

        if (!getState()) {
            Builder kbqaOutput = KbqaOutput.newBuilder();
            kbqaOutput.setAnswer("Please try again later.");
            responseObserver.onNext(kbqaOutput.build());
            responseObserver.onCompleted();
            logger.warn("state is false!!");
            return;
        }

        KBCandidateManager mgr = new KBCandidateManager();  // 정답후보 생성
        // rocksDB 를 별도로 읽지 않음
        flag = mgr.init(mgr_global.kb);
        if (!flag) {
            return;
        }

        String qaResult = null;

        lock.lock();
        try {
            qaResult = new String(qanal.QAnalyzer(inputText.getText(), ptr), "UTF-8");
        }catch (Exception e){
            logger.error(e.getMessage());
        } finally {
            lock.unlock();
        }

        String candidate_json = mgr.do_kbcangen(qaResult);

        logger.info("qa res : " + qaResult);
        String[] resultArray = candidate_json.split("\\|\\|0\\|\\|");
        Builder kbqaOutput = KbqaOutput.newBuilder();
        if (resultArray.length > 1) {
            logger.info("KBQA Result: {}", resultArray[1]);
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(resultArray[1]);
            try {
                String answer = jsonElement.getAsJsonObject().getAsJsonArray("candidate_list").get(0).getAsJsonObject().get("candidate").getAsString();
                kbqaOutput.setAnswer(answer);
            } catch (Exception e) {
                kbqaOutput.setAnswer("");
            }
        } else {
            kbqaOutput.setAnswer("");
        }
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("question end");
    }

    @Override
    public void insertDB(InputFile input, StreamObserver<KbqaOutput> responseObserver) {
        // MySQL insert DB
        Builder kbqaOutput = KbqaOutput.newBuilder();

        logger.info("file path : " + input.getFilepath());

        if(!input.getFilepath().equals("")){
            MariaDB mdb = new MariaDB();
            try {
                logger.info("file path : " + properties.getProperty("MYSQL_URL"));
                logger.info("file path : " + properties.getProperty("MYSQL_ID"));
                mdb.init(properties.getProperty("MYSQL_URL"), properties.getProperty("MYSQL_ID"), properties.getProperty("MYSQL_PW"));
                mdb.ParseToDB(input.getFilepath(),properties.getProperty("MYSQL_DOMAIN"));
            } catch ( Exception e) {
                logger.error(e.getMessage());
            } finally {
                mdb.close();
                logger.info("close DB");
            }
        }else{
            logger.info("please check source file or out folder");
        }

        kbqaOutput.setAnswer("complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("insertDB end");
    }

    @Override
    public void makeLocalDB(InputFile input, StreamObserver<KbqaOutput> responseObserver) {
        // make RocksDB
        Builder kbqaOutput = KbqaOutput.newBuilder();

        logger.info("file path : " + input.getFilepath());

        if(!input.getFilepath().equals("")){
            MariaDB mdb = new MariaDB();
            try {
                logger.info("file path : " + properties.getProperty("MYSQL_URL"));
                logger.info("file path : " + properties.getProperty("MYSQL_ID"));
                mdb.init(properties.getProperty("MYSQL_URL"), properties.getProperty("MYSQL_ID"), properties.getProperty("MYSQL_PW"));
                // make rocksDB from  MariaDB
                mdb.MakeRocksDB(properties.getProperty("STORE_DB_PATH"), properties.getProperty("MYSQL_DOMAIN"));
            } catch ( Exception e) {
                logger.error(e.getMessage());
            } finally {
                mdb.close();
                logger.info("close DB");
            }
        }else{
            logger.info("please check source file or out folder");
        }

        kbqaOutput.setAnswer("complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("makeLocalDB end");
    }

    @Override
    public void updateLocalDB(UpdateLocalDBRequest request, StreamObserver<KbqaOutput> responseObserver) {
        String updateType = request.getUpdateType();
        String db_path = properties.getProperty("STORE_DB_PATH") + "/" + request.getDbName();
        logger.info(db_path);
        if (updateType.equals("update")) {
            try {
                KBDataBaseManager new_db = new KBDataBaseManager(db_path);
                new_db.open();
                for (SPOInput spoData : request.getSPOListList()) {
                    logger.info(spoData.getSubj());
                    logger.info(spoData.getProper());
                    logger.info(spoData.getObj());
                    new_db.put(spoData.getSubj() + "_" + spoData.getProper(), spoData.getObj());
                }
                new_db.close();

                flag = mgr_global.init(db_path, properties.getProperty("ENTITY_PROPERTY_PATTERN_PATH"));
            } catch (Exception e) {
                logger.error(e.getMessage());
                flag = mgr_global.init(properties.getProperty("STORE_DB_PATH"), properties.getProperty("ENTITY_PROPERTY_PATTERN_PATH"));
            }

        } else {
            // make RocksDB
            flag = mgr_global.init(properties.getProperty("STORE_DB_PATH"), properties.getProperty("ENTITY_PROPERTY_PATTERN_PATH"));
        }

        Builder kbqaOutput = KbqaOutput.newBuilder();
        kbqaOutput.setAnswer("complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("updateLocalDB end");
    }

    @Override
    public void insertSPO(SPOInput input, StreamObserver<KbqaOutput> responseObserver) {
        if (!getState()) {
            Builder kbqaOutput = KbqaOutput.newBuilder();
            kbqaOutput.setAnswer("Please try again later.");
            responseObserver.onNext(kbqaOutput.build());
            responseObserver.onCompleted();
            logger.warn("state is false!!");
            return;
        }

        // MySQL insert DB
        Builder kbqaOutput = KbqaOutput.newBuilder();

        String s = input.getSubj();
        String p = input.getProper();
        String o = input.getObj();
        String domain = input.getDomain();

        if(!s.isEmpty() && !p.isEmpty() && !o.isEmpty()){
            MariaDB mdb = new MariaDB();
            try {
                logger.info("MySQL info : " + properties.getProperty("MYSQL_URL") + " / " + properties.getProperty("MYSQL_ID"));
                mdb.init(properties.getProperty("MYSQL_URL"), properties.getProperty("MYSQL_ID"), properties.getProperty("MYSQL_PW"));
                if(domain.isEmpty()) {
                    logger.info("insert SPO info : " + s + " / " + p + " / " + o + " / " + properties.getProperty("MYSQL_DOMAIN"));
                    mdb.QueryToSPO(s, p, o, properties.getProperty("MYSQL_DOMAIN"), "i");
                }
                else {
                    logger.info("insert SPO info : " + s + " / " + p + " / " + o + " / " + domain);
                    mdb.QueryToSPO(s, p, o, domain,"i");
                }
            } catch ( Exception e) {
                logger.error(e.getMessage());
            } finally {
                mdb.close();
                logger.info("close DB");
            }
        }else{
            logger.info("please check SPO data");
        }

        kbqaOutput.setAnswer("InsertSPO complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("InsertSPO end");
    }

    @Override
    public void readQRscRules(KbqaInput request, StreamObserver<KbqaOutput> responseObserver) {
        if (!getState()) {
            Builder kbqaOutput = KbqaOutput.newBuilder();
            kbqaOutput.setAnswer("Please try again later.");
            responseObserver.onNext(kbqaOutput.build());
            responseObserver.onCompleted();
            logger.warn("state is false!!");
            return;
        }

        lock.lock();
        try {
            // readQRscRules
            setState(false);
            ptr = qanal.UpdateQAnalRule("QRsc", ptr);
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            setState(true);
            lock.unlock();
        }

        Builder kbqaOutput = KbqaOutput.newBuilder();

        kbqaOutput.setAnswer("readQRscRules complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("readQRscRules end");
    }

    @Override
    public void newQuestion(KbqaInput request, StreamObserver<KbqaOutput> responseObserver) {
        String qaResult = null;
        String question = request.getQuestion();
        logger.info("input : " + question);

        // TODO : 유사질문 -> 대표질문 변환
        try {
            // 동의어 -> 대표어 변환
            logger.info("rest start");

            Gson gson = new Gson();
            String data = "";
            JsonObject jsonData = new JsonObject();
            jsonData.addProperty("sentence", request.getQuestion());
            data = gson.toJson(jsonData);

            logger.info("rest call start");

            // init rest server
            RestCall restCall = new RestCall(properties.getProperty("SUBSITUTE_REST_IP"), properties.getProperty("SUBSITUTE_REST_PORT"));
//            RestCall restCall = new RestCall("http://localhost:8080");
            // send to rest server
            String response = restCall.sendPost(properties.getProperty("SUBSITUTE_REST_URL"), data);
            logger.info("rest response : " + response);

            // response JSON
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(response);
            question = jsonElement.getAsJsonObject().get("sentence").getAsString();
            logger.info("rest end - " + question);
        } catch (Exception e){
            logger.error(e.getMessage());
        }

        // init Qanal
        lock.lock();
        try {
            logger.info("qa start!");
            qaResult = new String(qanal.QAnalyzer(question, ptr), "UTF-8");
            logger.info("qa end!");
        } catch (Exception e){
            logger.error(e.getMessage());
        } finally {
            lock.unlock();
        }

        KBCandidateManager mgr = new KBCandidateManager();  // 정답후보 생성
        // rocksDB 를 별도로 읽지 않음
        flag = mgr.init(mgr_global.kb);

        logger.info("kb start!");
        String candidate_json = mgr.do_kbcangen(qaResult);
        logger.info("kb end!");

//        logger.info("qa res : " + qaResult);
        String[] resultArray = candidate_json.split("\\|\\|0\\|\\|");
        Builder kbqaOutput = KbqaOutput.newBuilder();

        if (resultArray.length > 1) {
            JsonParser jsonParser = new JsonParser();
            JsonElement jsonElement = jsonParser.parse(resultArray[1]);
            try {
                String answer = jsonElement.getAsJsonObject().getAsJsonArray("candidate_list").get(0).getAsJsonObject().get("candidate").getAsString();
                kbqaOutput.setAnswer(answer);
            } catch (Exception e) {
                kbqaOutput.setAnswer("");
            }
        } else {
            kbqaOutput.setAnswer("");
        }

//        kbqaOutput.setAnswer("complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("test end");
    }

    // TODO : dic real-time update
    @Override
    public void test(KbqaInput request, StreamObserver<KbqaOutput> responseObserver) {
        logger.info("test start!");

        Builder kbqaOutput = KbqaOutput.newBuilder();
        kbqaOutput.setAnswer("complete!");
        responseObserver.onNext(kbqaOutput.build());
        responseObserver.onCompleted();
        logger.info("test end");
    }
}
