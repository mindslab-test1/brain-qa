package ai.maum.brain.kbqa.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestCall {

    private static Logger logger = LoggerFactory.getLogger(RestCall.class);

    private static String API_SERVER_HOST = "http://localhost:8080";
    private final String USER_AGENT = "Mozilla/5.0";

    public RestCall(String host, String port) {
        if(port == null)
            API_SERVER_HOST = "http://" + host;
        else
            API_SERVER_HOST = "http://" + host + ":" + port;
    }

    // HTTP POST request
    public String sendPost(String apiPath, String data) {

        String requestUrl = API_SERVER_HOST + apiPath;
//        logger.info(requestUrl);
        try {
            URL obj = new URL(requestUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-type", "application/json");
            con.setRequestProperty("Content-Length", Integer.toString(data.length()));

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(data.getBytes("UTF8"));
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
//            System.out.println("Sending 'POST' request to URL : " + requestUrl);
//            System.out.println("Post parameters : " + data);
//            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
