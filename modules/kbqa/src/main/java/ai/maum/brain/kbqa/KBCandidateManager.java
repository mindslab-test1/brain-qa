package ai.maum.brain.kbqa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class KBCandidateManager {

	private String m_kburl = "http://129.254.165.45:8080/api/qa";
	private String m_config = "/home/qa/exobrain/WISEQAService/config/config.properties";
	//private String m_config = "./config.properties";
	private int m_KBCandidateLimit = 5;		// default
	private boolean m_MultiQIF=false;		// default
	
	QAnal qanl = null;
	StructuredDB db = null;
	KBResource kb = null;
	
	public KBCandidateManager() 
	{
		loadConfig(m_config);
	}
	
	public boolean loadConfig(String fname)
	{
		try {
			File f = new File(fname);
			
			if (f.isFile()) {
				FileInputStream fis = new FileInputStream(m_config);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				
				String line;
				while((line = br.readLine()) != null) {
					line=line.trim();
					if (line.length() <=0 ) continue;
					if (line.charAt(0)=='#') continue;
					
					//System.out.println("line="+line);
					
					if (line.indexOf('=')!=-1) {
						StringTokenizer tk = new StringTokenizer(line, "=");
						String key=tk.nextToken().trim();
						if (key.equals("KBCandidateLimit")==true && tk.hasMoreTokens()) {
							String limit = tk.nextToken().trim();
							m_KBCandidateLimit=Integer.parseInt(limit);
						}
						else if (key.equals("MultiQIF")==true && tk.hasMoreTokens()) {
							String truefalse = tk.nextToken().trim();
							m_MultiQIF=Boolean.parseBoolean(truefalse);
						}
					}
				}				
				br.close();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean init(KBResource kb){
		try {
			/**
			 * 정형 DB 기반 정답 후보 생성을 위해 기본 데이터를 읽어옴
			 * kb: DBpedia, infobxo
			 * dic: 속성 매칭 사전, 개체 매칭 사전, 속성 조합 사전 
			 */	
			this.kb = kb;
			db = new StructuredDB(kb);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}
	
	public boolean init(){
		try {
			/**
			 * 정형 DB 기반 정답 후보 생성을 위해 기본 데이터를 읽어옴
			 * kb: DBpedia, infobxo
			 * dic: 속성 매칭 사전, 개체 매칭 사전, 속성 조합 사전 
			 */	
			kb = new KBResource();
			kb.readDB(true,"/data2/KB_Bae/data/kb/db","/data2/KB_Bae/data/kb/entity_property_pattern.txt");
			db = new StructuredDB(kb);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}
	
	public boolean init(String db_path,String entity_property_pattern_file){
		try {
			/**
			 * 정형 DB 기반 정답 후보 생성을 위해 기본 데이터를 읽어옴
			 * kb: DBpedia, infobxo
			 * dic: 속성 매칭 사전, 개체 매칭 사전, 속성 조합 사전 
			 */	
			kb = new KBResource();
			kb.readDB(true,db_path,entity_property_pattern_file);
			db = new StructuredDB(kb);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
	}

	public boolean close() {
		return true;
	}

	public boolean closeDB() {
		kb.closeDB();
		return true;
	}

	//질문분석된 JSON결과를 기반으로 지식베이스 기반 정답 후보 생성.
	public String do_kbcangen(String qanal_json) {
		if (m_MultiQIF==false)
		{
			return do_kbcangenSingleQIF(qanal_json);
		}else{
			return qanal_json+ "||0";
		}		
	}
	//하나의 질문에 대한 KBQA 진행, query info를 사용(gif)	
	public String do_kbcangenSingleQIF(String qanal_json){
		qanal_json = qanal_json.replaceAll("\\|\\|0", "");
		JSONObject candidateStruct = new JSONObject();
		// QIF generation
		try {
			
			if(db == null){
				System.out.println("db is null");;
			}
			
			qanl = new QAnal();
			qanl.readJSON(qanal_json);
			qanl.setQuestion(); //질문을 JSON에서 읽어오기
			qanl.setLAT();
			qanl.setSAT();			
			qanl.setNE();
			qanl.setEntityLinking();
			qanl.setQLength();
			qanl.setNNG();
			/**
			 * 질문에 공백만 존재하거나, 특수문자로만 존재하는 경우 처리
			 */
			String test = qanl.question.replaceAll(" ", "");
			test = test.replaceAll("\t", "");
			test = test.replaceAll("\\p{Punct}", "");
			if(test.equals("")){
				return qanal_json+ "||0";
			}
			/**
			 * 			
			 */
			db.question = qanl.question;
//			Util.pw.println();
//			Util.pw.print(qanl.question);
//			System.out.print(Util.count+" "+qanl.question+"\t");;
//			Util.count++;
//			System.out.println();
			/**
			 * 
			 */			
			db.kb_result.clear();
			qanl.setSPO(db);			
			
			JSONArray candidateList = new JSONArray();
			JSONArray sparqlArray = new JSONArray();
			
			db.findAnswer(qanl);
			
			ArrayList<String> index = new ArrayList<String>();				
			sort(index);
			
			if(db.kb_result.size() > 0){		
				double ratio = 1.0;
				for(String in : index){
					int i = Integer.parseInt(in);
					//Util.pw.print("\t"+db.pattern_type+"\to\t"+db.key+"\t"+db.kb_result.get(i).getAnswer()+"\t"+db.kb_result.get(i).getConfidence());					
					JSONObject candidateObject = new JSONObject();
					candidateObject.put("candidate", db.kb_result.get(i).getAnswer());
					double confidence = db.kb_result.get(i).getConfidence();
					if(confidence == 0){
						confidence = 0.9;
					}
					candidateObject.put("confidence", confidence * ratio);
					candidateObject.put("basis", db.kb_result.get(i).getBasis());				
					candidateList.add(candidateObject);
					ratio = ratio - 0.1;
				}
				candidateStruct.put("sparql_list", sparqlArray);
				candidateStruct.put("candidate_list", candidateList);
				db.count++;
			}else{
				//Util.pw.print("\te\tx\t"+db.key+"\t"+"---");
			}
			/**
			 * 서브 의미 프레임 목록을 가져옴. 
			 */
			String r = candidateStruct.toJSONString();
			if (r.length()>0){
				String kb_return = candidateStruct.toJSONString();
				kb_return=kb_return.replace("||", "//");				
				return qanal_json+ "||0||" + kb_return;
			} else {
				//Util.pw.print("\t-1\t-\t"+qanl.question+"\t \t \t");				
				return qanal_json+ "||0";
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("qanal input error=["+StructuredDB.current_json+"]");			
			//System.exit(-1);
			return qanal_json+ "||0";
		}
	}
	public void sort(ArrayList<String> index){		
		try {
			//신뢰도 기반으로 우선 정렬
			ArrayList<Double> confidence = new ArrayList<Double>();			
			for(Integer i =0; i < db.kb_result.size(); i++){
				index.add(i.toString());
				confidence.add(db.kb_result.get(i).getConfidence());
			}
			Util.quickSort(index, confidence, 0, index.size()-1, "desc");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
