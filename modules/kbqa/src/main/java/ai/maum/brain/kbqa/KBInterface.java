package ai.maum.brain.kbqa;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class KBInterface {
	
	public static String httpPost(String urlStr, String query) throws Exception {
		try {
            URL targetUrl = new URL(urlStr);
            HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
            httpConnection.setDoOutput(true);
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "application/json");
	
			long startTime = System.currentTimeMillis();		// 시작시간
	
            OutputStream outputStream = httpConnection.getOutputStream();
            
            //System.out.println("[query]="+query);
            
            outputStream.write(query.getBytes());
            outputStream.flush();
/*
            if (httpConnection.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + httpConnection.getResponseCode());
            }
*/
            BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                    (httpConnection.getInputStream())));

            String output;
            //System.out.println("Output from Server:\n");

            String outStr = "";
            while ((output = responseBuffer.readLine()) != null) {
                //System.out.println(output);
                outStr = outStr + output;
            }
			//System.out.println("[kb_reply]="+outStr);
			
            httpConnection.disconnect();
            
            long endTime = System.currentTimeMillis();			// 종료시간
            Float duration = (endTime - startTime) / 1000.0f;	// 경과시간
            
            return duration.toString()+"#@#"+outStr;
		} catch (MalformedURLException e) {
            e.printStackTrace();
		} catch (IOException e) {
            e.printStackTrace();
		}
		
		return "";
	}
	


}





