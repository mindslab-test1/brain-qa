#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
from time import sleep

import grpc
import json
import sys
import argparse, textwrap

sys.path.insert(0, '../lib/python')

from datetime import datetime

from maum.brain.qa import kbqa_pb2
from maum.brain.qa import kbqa_pb2_grpc
from google.protobuf import json_format
import openpyxl

def run(text):
    channel = grpc.insecure_channel('localhost:51000')
    stub = kbqa_pb2_grpc.KbqaServiceStub(channel)
    kbqaInput = kbqa_pb2.KbqaInput(question=text)
    resultOutput = stub.Question(kbqaInput)
    jsonResult = json_format.MessageToJson(resultOutput, True)
    jobj = json.loads(jsonResult)
    print(jobj["answer"])
    return jobj["answer"]


def test_run(text):
    channel = grpc.insecure_channel('localhost:51000')
    stub = kbqa_pb2_grpc.KbqaServiceStub(channel)
    kbqaInput = kbqa_pb2.KbqaInput(question=text)
    resultOutput = stub.Test(kbqaInput)
    jsonResult = json_format.MessageToJson(resultOutput, True)
    jobj = json.loads(jsonResult)
    print(jobj["answer"])
    return jobj["answer"]


def update_rocksdb(file):
    channel = grpc.insecure_channel('localhost:51000')
    stub = kbqa_pb2_grpc.KbqaServiceStub(channel)

    kbqaInput = kbqa_pb2.UpdateLocalDBRequest()
    kbqaInput.updateType = "update"

    try:
        wb = openpyxl.load_workbook(file)
        # ws = wb.actvie
        ws = wb.get_sheet_by_name("Sheet1")

        for r in ws.rows:
            # 첫 행 생략
            if r[0].value is None:
                break

            if r[0].value is "Subject":
                continue
            dbData = kbqa_pb2.SPOInput()
            dbData.subj = r[0].value.encode('utf8')
            dbData.proper = r[1].value.encode('utf8')
            dbData.obj = r[2].value.encode('utf8')
            dbData.domain = "테스트"

            # add spo data
            kbqaInput.SPOList.extend([dbData])

        wb.close()
        resultOutput = stub.UpdateLocalDB(kbqaInput)
        jsonResult = json_format.MessageToJson(resultOutput, True)
        jobj = json.loads(jsonResult)
        print(jobj["answer"])
        return jobj["answer"]

    except Exception as e:
        dbData = kbqa_pb2.SPOInput()
        dbData.subj = "test"
        dbData.proper = "test"
        dbData.obj = "test"
        dbData.domain = "테스트"

        # add spo data
        kbqaInput.SPOList.extend([dbData])
        print(str(e))

    return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="KBQA CLIENT",
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-t", "--text",
                        nargs="?",
                        help=textwrap.dedent('''1)question'''))

    parser.add_argument("-u", "--update",
                        help="update rocksdb",
                        action="store_true")

    parser.add_argument("-f", "--file",
                        nargs="?",
                        help=textwrap.dedent('''update rockDB file'''))

    args = parser.parse_args()

    start = datetime.now()
    if args.update and args.file:
        update_rocksdb(args.file)
    elif args.update:
        update_rocksdb("update")
    elif args.text:
        run(args.text)
    else:
        print("Not available. please check help command")

    end = datetime.now()
    print("응답시간 : " + str(end-start))


