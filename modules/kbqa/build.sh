#!/usr/bin/env bash

PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/" >/dev/null
  BUILD_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

kbqa_root=${KBQA_ROOT}
if [ -z "${kbqa_root}" ]; then
  echo "KBQA_ROOT is not defined!"
  exit 1
fi

test -d ${kbqa_root} || mkdir -p ${kbqa_root}
test -d ${kbqa_root}/lib/ || mkdir -p ${kbqa_root}/lib/
test -d ${kbqa_root}/logs/ || mkdir -p ${kbqa_root}/logs/


cd $BUILD_HOME

chmod 755 ${BUILD_HOME}/gradlew
chmod 755 ${BUILD_HOME}/bin/kbqa_server.sh

APP_VERSION="1.0.0"
./gradlew clean shadowJar

if [ $? -eq 0 ]; then
  cp -r ${BUILD_HOME}/bin ${kbqa_root}/
  if [ -e "${BUILD_HOME}/conf" ]; then
    echo "conf directory exists"
  else
    cp -r ${BUILD_HOME}/conf ${kbqa_root}/
  fi
  cp -r ${BUILD_HOME}/lib ${kbqa_root}/
  if [ -e "${BUILD_HOME}/test" ]; then
    echo "conf directory exists"
  else
    cp -r ${BUILD_HOME}/test ${kbqa_root}/
  fi
  cp -r ${BUILD_HOME}/data ${kbqa_root}/
  cp ${BUILD_HOME}/build/libs/kbqa-${APP_VERSION}.jar ${kbqa_root}/lib/
  cd ${kbqa_root}/lib/
  sed -i "s#\${APP_HOME}#${kbqa_root}#g" ${kbqa_root}/conf/kbqa.properties
  sed -i "s#\${APP_HOME}#${kbqa_root}#g" ${kbqa_root}/conf/logback.xml
  ln -s kbqa-${APP_VERSION}.jar kbqa.jar
  else
    echo "Gradle Build Fail!"
    exit 1
fi
