#!/usr/bin/env bash

PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/" >/dev/null
  BUILD_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

basicqa_root=${BASICQA_ROOT}
if [ -z "${basicqa_root}" ]; then
  echo "BASICQA_ROOT is not defined!"
  exit 1
fi

test -d ${basicqa_root} || mkdir -p ${basicqa_root}
test -d ${basicqa_root}/lib/ || mkdir -p ${basicqa_root}/lib/
test -d ${basicqa_root}/logs/ || mkdir -p ${basicqa_root}/logs/


cd $BUILD_HOME

chmod 755 ${BUILD_HOME}/gradlew

APP_VERSION="1.1.2"
./gradlew clean shadowJar

if [ $? -eq 0 ]; then
  chmod 755 ${BUILD_HOME}/bin/*.sh
  cp -r ${BUILD_HOME}/bin ${basicqa_root}/
  cp -r ${BUILD_HOME}/conf ${basicqa_root}/
  cp ${BUILD_HOME}/build/libs/basic-qa-${APP_VERSION}.jar ${basicqa_root}/lib/
  cd ${basicqa_root}/lib/
  ln -s basic-qa-${APP_VERSION}.jar basic-qa.jar
  cd -
  cp -r ${BUILD_HOME}/.aws ~/
  python pysrc/aws-s3/get_basicqa_resources.py basicqa-solr 20180417 ${BUILD_HOME}/build/tmp/ ${BUILD_HOME}/build/tmp/
  mv ${BUILD_HOME}/build/tmp/solr1 ${basicqa_root}/
  mv ${BUILD_HOME}/build/tmp/solr2 ${basicqa_root}/
  mv ${BUILD_HOME}/build/tmp/solr3 ${basicqa_root}/
  mv ${BUILD_HOME}/build/tmp/zookeeper ${basicqa_root}/
  cp ${BUILD_HOME}/supervisord.conf ${basicqa_root}/
  sed -i "s#\${APP_HOME}#${basicqa_root}#g" ${basicqa_root}/conf/qa.properties
  sed -i "s#\${APP_HOME}#${basicqa_root}#g" ${basicqa_root}/zookeeper/conf/zoo1.cfg
  sed -i "s#\${APP_HOME}#${basicqa_root}#g" ${basicqa_root}/zookeeper/conf/zoo2.cfg
  sed -i "s#\${APP_HOME}#${basicqa_root}#g" ${basicqa_root}/zookeeper/conf/zoo3.cfg
  else
    echo "Gradle Build Fail!"
    exit 1
fi
