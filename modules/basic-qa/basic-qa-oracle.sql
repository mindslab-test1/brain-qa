create table MLT_BSQA_CATEGORY
(
    ID VARCHAR2(40 char) not null
        primary key,
    CATNAME VARCHAR2(40 char) not null,
    CATPATH VARCHAR2(200 char) not null,
    CREATEDAT TIMESTAMP(6),
    CREATORID VARCHAR2(40 char),
    PARCATID VARCHAR2(40 char),
    UPDATEDAT TIMESTAMP(6),
    UPDATERID VARCHAR2(40 char)
)
;

create table MLT_BSQA_INDEXING_HISTORY
(
    ID NUMBER(10) not null
        primary key,
    CREATEAT TIMESTAMP(6),
    CREATEID VARCHAR2(255 char),
    FETCHED NUMBER(10),
    MESSAGE VARCHAR2(100 char),
    PROCESSED NUMBER(10),
    STATUS NUMBER(1),
    STOPYN NUMBER(1),
    TOTAL NUMBER(10),
    TYPE VARCHAR2(30 char),
    UPDATEAT TIMESTAMP(6),
    UPDATERID VARCHAR2(255 char)
)
;

create table MLT_BSQA_PRPRS_MAIN_WD
(
    ID NUMBER(10) not null
        primary key,
    CREATEAT TIMESTAMP(6),
    CREATEID VARCHAR2(255 char),
    UPDATEAT TIMESTAMP(6),
    UPDATERID VARCHAR2(255 char),
    USEYN VARCHAR2(5 char) not null,
    WORD VARCHAR2(40 char) not null
)
;

create table MLT_BSQA_PRPRS_WD
(
    ID NUMBER(10) not null
        primary key,
    CREATEAT TIMESTAMP(6),
    CREATEID VARCHAR2(255 char),
    WORD VARCHAR2(40 char) not null
)
;

create table MLT_BSQA_PRPRS_REL
(
    MAINID NUMBER(10) not null
        constraint FKDC2SH08NSRMNFDYGAMPIUYKP
            references MLT_BSQA_PRPRS_MAIN_WD,
    PARAPHRASEID NUMBER(10) not null
        constraint FKRIR2XH9I1GWJ3AESMHEXADHW3
            references MLT_BSQA_PRPRS_WD,
    primary key (MAINID, PARAPHRASEID)
)
;

create table MLT_BSQA_SKILL
(
    ID NUMBER(10) not null
        primary key,
    CREATEAT TIMESTAMP(6),
    CREATEID VARCHAR2(255 char),
    NAME VARCHAR2(40 char) not null,
    UPDATEAT TIMESTAMP(6),
    UPDATERID VARCHAR2(255 char)
)
;

create table MLT_BSQA_ITEM
(
    ID NUMBER(10) not null
        primary key,
    ANSWER CLOB,
    C_ID NUMBER(10),
    CATEGORY VARCHAR2(40 char)
        constraint FKL3U4HJ0F2PTED1DODQLXB1E3X
            references MLT_BSQA_CATEGORY,
    CHANNEL VARCHAR2(40 char) not null
        constraint FKLFF1L7IHTYQFAIVKLKX9CTC9Y
            references MLT_BSQA_CATEGORY,
    CONFIRM_DTM TIMESTAMP(6),
    CONFIRMER VARCHAR2(40 char),
    CREATE_DTM TIMESTAMP(6),
    CREATOR VARCHAR2(40 char),
    QNA_ID VARCHAR2(255 char),
    QUESTION CLOB,
    SKILL_ID NUMBER(10)
        constraint FKSMV8CGOSKFIFR0L91HF8QVOST
            references MLT_BSQA_SKILL,
    SRC VARCHAR2(20 char),
    MAIN_YN      VARCHAR2(1 char),
    SUB_CATEGORY VARCHAR2(40 char)
        constraint FKK2TX365J8F8X4694XXFHH3F6O
            references MLT_BSQA_CATEGORY,
    SUB_SUB_CATEGORY VARCHAR2(40 char)
        constraint FKO06FGOY8T3II7OSELIL42QAU
            references MLT_BSQA_CATEGORY,
    UPDATE_DTM TIMESTAMP(6),
    UPDATOR VARCHAR2(40 char),
    USE_YN VARCHAR2(1 char),
    WEIGHT NUMBER(2,1)
)
;