# BasicQA 설치 및 구동 방법
1. Prepare Install
2. BasicQA Install

## 1. Prepare Install
- java 1.8 이상 설치
- gradle 3.3 이상 설치
- Online 환경(library, Solr Resource를 위해 필요)

## 2. BasicQA Install
```
export BASICQA_ROOT=/home/minds/basic-qa
$ ./build.sh
```

- 해당 위치에 아래와 같이 설치 됨.
```
bin: BasicQA 실행파일이 있는 path
conf: BasicQA 설정 파일이 있는 path
lib: BasicQA library path
solr: 검색기가 있는 path
```

 