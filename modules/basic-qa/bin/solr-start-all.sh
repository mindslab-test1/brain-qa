#!/usr/bin/env bash
  PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/../" >/dev/null
  APP_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

  cd $APP_HOME
  ./solr1/bin/solr start -c -s solr1/server/solr/basicqa -p 8983 -m 2g -z localhost:2181,localhost:2182,localhost:2183
  ./solr2/bin/solr start -c -s solr2/server/solr/basicqa -p 8984 -m 2g -z localhost:2181,localhost:2182,localhost:2183
  ./solr3/bin/solr start -c -s solr3/server/solr/basicqa -p 8985 -m 2g -z localhost:2181,localhost:2182,localhost:2183