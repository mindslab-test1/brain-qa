package ai.maum.basicqa;

import java.sql.Date;
import lombok.Data;

@Data
public class ParaphraseWord {
  private int id;
  private String main_word;
  private String paraphrase_word;
  private Date create_at;
}
