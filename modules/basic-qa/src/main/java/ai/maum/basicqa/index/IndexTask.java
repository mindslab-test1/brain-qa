package ai.maum.basicqa.index;

import ai.maum.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.BasicQAServiceGrpc;
import maum.brain.qa.BasicQAServiceGrpc.BasicQAServiceBlockingStub;
import maum.brain.qa.Basicqa.IndexingInput;
import maum.brain.qa.Basicqa.SearchStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component(value = "indexTask")
public class IndexTask {

  private static Logger logger = LoggerFactory.getLogger(IndexTask.class);

  private static Properties properties = PropertiesManager.getProperties();

  public void doIndexing() {
    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",
        Integer.parseInt(properties.getProperty("QA_PORT", "50051"))).usePlaintext(true).build();
    BasicQAServiceBlockingStub stub = BasicQAServiceGrpc.newBlockingStub(channel);
    IndexingInput indexingInput = IndexingInput.newBuilder().setClean(false).build();
    SearchStatus status = stub.additionalIndexing(indexingInput);
    logger.info("Indexing Schedule Status: {}", status.getStatus());
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
