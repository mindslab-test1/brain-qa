package ai.maum.basicqa;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.concurrent.TimeUnit;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.qa.BasicQAServiceGrpc;
import maum.brain.qa.BasicQAServiceGrpc.BasicQAServiceBlockingStub;
import maum.brain.qa.Basicqa.AnswerOutput;
import maum.brain.qa.Basicqa.QuestionInput;
import maum.brain.qa.Basicqa.Type;
import maum.common.LangOuterClass.LangCode;

public class BasicQAClientMain {

  private ManagedChannel channel;


  public static void main(String[] args) {
    BasicQAClientMain qaClientMain = new BasicQAClientMain();
    qaClientMain.connect();
  }

  private void connect() {
    channel = ManagedChannelBuilder.forAddress("10.122.64.73", 9823).usePlaintext(true).build();
    NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
    InputText inputText = InputText.newBuilder()
        .setText("LG U+ 무제한 요금제 알려줘")
        .setLang(LangCode.kor)
        .setSplitSentence(true)
        .setUseTokenizer(true)
        .setUseSpace(true)
        .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).
        build();
    Document result = stub.analyze(inputText);
    try {
      System.out.println(JsonFormat.printer().print(result));
    } catch (InvalidProtocolBufferException e) {
      e.printStackTrace();
    }

//    BasicQAServiceBlockingStub stub = BasicQAServiceGrpc.newBlockingStub(channel);
//    QuestionInput questionInput = QuestionInput.newBuilder().setQuestion("엑소 팬클럽 이름이 뭐야").setType(
//        Type.SEARCH).build();
//    AnswerOutput answerOutput = stub.question(questionInput);
//    System.out.println(resultQA.getAnswer());
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
