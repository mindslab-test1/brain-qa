package ai.maum.basicqa;

import ai.maum.util.PropertiesManager;
import ai.maum.util.SqlSessionManager;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import maum.brain.qa.Basicqa.IndexingInput;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexingCore extends Thread {

  private static Logger logger = LoggerFactory.getLogger(IndexingCore.class);

  private static Properties properties = PropertiesManager.getProperties();

  private Properties indexingPriperties;

  private IndexingInput request;

  private DocumentIndexingHandler documentIndexingHandler;

  private SolrClient solrClient;

  public IndexingCore(IndexingInput request, DocumentIndexingHandler documentIndexingHandler,
      SolrClient solrClient, Properties indexingPriperties) {
    this.request = request;
    this.documentIndexingHandler = documentIndexingHandler;
    this.solrClient = solrClient;
    this.indexingPriperties = indexingPriperties;
  }

  @Override
  public void run() {
    SqlSessionManager sqlSessionManager = SqlSessionManager.getInstance();
    SqlSession sqlSession = null;
    try {
      if (request.getClean()) {
        solrClient.deleteByQuery("*:*");
        solrClient.commit();
      }

      sqlSession = sqlSessionManager.getSqlSession();
      List<ParaphraseWord> paraphraseList = sqlSession.selectList("basicqa.selectParaphraseList");

      Map<String, List<String>> paraMap = new HashMap<>();

      for (ParaphraseWord paraphraseWord : paraphraseList) {
        if (paraMap.containsKey(paraphraseWord.getMain_word())) {
          paraMap.get(paraphraseWord.getMain_word()).add(paraphraseWord.getParaphrase_word());
        } else {
          ArrayList<String> paraphraseWordList = new ArrayList<>();
          paraphraseWordList.add(paraphraseWord.getParaphrase_word());
          paraMap.put(paraphraseWord.getMain_word(), paraphraseWordList);
        }
      }

      documentIndexingHandler.setParaMap(paraMap);
      logger.info("Para Map: {}", paraMap.toString());

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String nextIndexTime = dateFormat.format(new Date());

      Map<String, String> params = new HashMap<>();
      params.put("skill_id", String.valueOf(request.getSkillId()));
      if (StringUtils.isNotEmpty(request.getTargetDate())) {
        params.put("target_date", request.getTargetDate());
      } else if (indexingPriperties != null) {
        String lastIndexTime = indexingPriperties.getProperty("last_index_time");
        params.put("target_date", lastIndexTime);
      }

      logger.info("db params: {}", params);

      int total = sqlSession.selectOne("basicqa.selectAllCount", params);
      documentIndexingHandler.setTotal(total);
      documentIndexingHandler.setFetched(0);
      documentIndexingHandler.setProcessed(0);
      sqlSession.select("basicqa.selectAllDocument", params, documentIndexingHandler);

      String indexingTimeFile = PropertiesManager
          .resolveValueWithProVars(properties.getProperty("INDEXING_TIME_FILE_PATH"));
      if (StringUtils.isNotEmpty(indexingTimeFile)) {
        Properties newIndexProperties = new Properties();
        newIndexProperties.setProperty("last_index_time", nextIndexTime);
        FileOutputStream fileOutputStream = new FileOutputStream(indexingTimeFile);
        newIndexProperties.store(fileOutputStream, "Last Index Time");
        fileOutputStream.close();
      }
      sqlSession.close();
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      documentIndexingHandler.setIndexing(false);
      if (sqlSession != null) {
        sqlSession.close();
      }
    }
  }
}
