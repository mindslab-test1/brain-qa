package ai.maum.basicqa.observer;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import lombok.Getter;
import lombok.Setter;
import maum.brain.nlp.Nlp.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentObserver implements StreamObserver<Document> {

  private static Logger logger = LoggerFactory.getLogger(DocumentObserver.class);

  @Getter @Setter
  private CountDownLatch countDownLatch = null;

  @Getter
  private List<Document> documentList = new ArrayList<>();

  @Override
  public void onNext(Document value) {
    documentList.add(value);
  }

  @Override
  public void onError(Throwable t) {
    Status status = Status.fromThrowable(t);
    logger.error("NLP Failed: {}", status.getCode());
    countDownLatch.countDown();
  }

  @Override
  public void onCompleted() {
    countDownLatch.countDown();
  }
}
