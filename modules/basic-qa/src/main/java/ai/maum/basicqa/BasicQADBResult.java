package ai.maum.basicqa;

import lombok.Data;

@Data
public class BasicQADBResult {
  private String answer;
  private int skill_id;
  private String src;
  private String channel;
  private String category;
  private String sub_category;
  private String sub_sub_category;
}
