package ai.maum.basicqa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DocumentIndexingHandler implements ResultHandler<BasicDocument> {

  private static Logger logger = LoggerFactory.getLogger(DocumentIndexingHandler.class);

  private CloudSolrClient cloudSolrClient;

  @Getter
  @Setter
  private int processed = 0;

  @Getter
  @Setter
  private int fetched = 0;

  @Getter
  @Setter
  private int total = 0;

  @Getter
  @Setter
  private boolean isIndexing = false;

  @Setter
  private Map<String, List<String>> paraMap = new HashMap<>();

  Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();

  NlpService nlpService = new NlpService();

  public DocumentIndexingHandler(CloudSolrClient cloudSolrClient) {
    this.cloudSolrClient = cloudSolrClient;
  }

  @Override
  public void handleResult(ResultContext<? extends BasicDocument> resultContext) {

    try {

      fetched = resultContext.getResultCount();

      BasicDocument basicDocument = resultContext.getResultObject();
      SolrInputDocument solrInputDocument = new SolrInputDocument();
      solrInputDocument.addField("id", basicDocument.getId());
      solrInputDocument.addField("c_id", basicDocument.getCid());
      solrInputDocument.addField("qna_id", basicDocument.getQnaid());
      solrInputDocument.addField("question", basicDocument.getQuestion());
      solrInputDocument.addField("question_morph", nlpService.doNlp(basicDocument.getQuestion()));
      solrInputDocument.addField("answer", basicDocument.getAnswer());
      solrInputDocument.addField("skill_id", basicDocument.getSkill_id());
      solrInputDocument.addField("src", basicDocument.getSrc());
      solrInputDocument.addField("channel", basicDocument.getChannel());
      solrInputDocument.addField("category", basicDocument.getCategory());
      solrInputDocument.addField("sub_category", basicDocument.getSub_category());
      solrInputDocument.addField("sub_sub_category", basicDocument.getSub_sub_category());
      solrInputDocument.addField("create_dtm", basicDocument.getCreate_dtm());
      solrInputDocument.addField("update_dtm", basicDocument.getUpdate_dtm());
      solrInputDocument.addField("isMain", StringUtils.equalsAnyIgnoreCase("Y", basicDocument.getMain_yn()));
      docs.add(solrInputDocument);
      processed++;

      List<String> questionList = doParaphrase(basicDocument.getQuestion());

      List<String> morphList = nlpService.doMultiNlp(questionList);

      int qcnt = 0;
      for (String morph : morphList) {
        qcnt++;
        SolrInputDocument qdoc = new SolrInputDocument();
        qdoc.addField("id", basicDocument.getId() + "-" + qcnt);
        qdoc.addField("c_id", basicDocument.getCid());
        qdoc.addField("qna_id", basicDocument.getQnaid());
        qdoc.addField("question", questionList.get(qcnt-1));
        qdoc.addField("question_morph", morph);
        qdoc.addField("answer", basicDocument.getAnswer());
        qdoc.addField("skill_id", basicDocument.getSkill_id());
        qdoc.addField("src", basicDocument.getSrc());
        qdoc.addField("channel", basicDocument.getChannel());
        qdoc.addField("category", basicDocument.getCategory());
        qdoc.addField("sub_category", basicDocument.getSub_category());
        qdoc.addField("sub_sub_category", basicDocument.getSub_sub_category());
        qdoc.addField("create_dtm", basicDocument.getCreate_dtm());
        qdoc.addField("update_dtm", basicDocument.getUpdate_dtm());
        qdoc.addField("isMain", false);

        docs.add(qdoc);
        processed++;
      }

      if (docs.size() > 0 && ((fetched % 100) == 0 || total == fetched)) {
        cloudSolrClient.add(docs);
        cloudSolrClient.commit();
        docs.clear();
      }
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private List<String> doParaphrase(String question) {
    logger.info("question: {}", question);
    List<String> questionList = new ArrayList<>();
    if (StringUtils.isEmpty(question)) {
      return questionList;
    }
    Set<String> keySet = paraMap.keySet();
    int lastIndex = -1;

    // 질문의 끝에 특수문자가 있는 지 확인
    
    String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
    Pattern pattern = Pattern.compile(match);
    Stack<String> regxStack = new Stack<>();
    StringBuffer regxBuffer = new StringBuffer();
    for (int i = question.length() - 1; i >= 0; i--) {
      Matcher matcher = pattern.matcher(String.valueOf(question.charAt(i)));
      if (matcher.find()) {
        regxStack.push(matcher.group());
      }
    }

    for (int i = 0; i < regxStack.size(); i++) {
      regxBuffer.append(regxStack.pop());
    }

    // 질문의 특수문자 제거
    String cleanQuestion = StringUtils.substring(question, 0, question.length() - regxStack.size());

    StringBuilder questionBuilder = new StringBuilder(cleanQuestion);

    String[] orgWords = StringUtils.split(StringUtils.trim(cleanQuestion), " ");
    String lastWord = orgWords[orgWords.length - 1];

    for (String key : keySet) {
      boolean isEqual = false;
      List<String> paraphraseList = new ArrayList<>();
      paraphraseList.addAll(paraMap.get(key));
      // 질문 어미와 대표 단어가 같은 지 비교
      lastIndex = StringUtils.lastIndexOf(cleanQuestion, key);
      if (lastIndex > -1 && StringUtils.endsWith(cleanQuestion, key)) {
        for (String paraphrase : paraphraseList) {
          StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), paraphrase);
          if (StringUtils.equals(cleanQuestion, paraphraseBuilder.toString())) {
            continue;
          } else {
            questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          }
        }
      } else {
        for (String paraphrase : paraphraseList) {
          lastIndex = StringUtils.lastIndexOf(cleanQuestion, paraphrase);
          if (lastIndex > -1 && StringUtils.endsWith(cleanQuestion, paraphrase)) {
            isEqual = true;
            break;
          }
        }
        if (isEqual) {
          StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), key);
          questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          for (String paraphrase : paraphraseList) {
            if (StringUtils.equalsIgnoreCase(lastWord, paraphrase)) {
              continue;
            }
            paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(), paraphrase);
            questionList.add(paraphraseBuilder.toString() + regxBuffer.toString());
          }
        }
      }
    }
    return questionList;
  }
}
