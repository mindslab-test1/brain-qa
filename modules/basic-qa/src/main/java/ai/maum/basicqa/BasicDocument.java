package ai.maum.basicqa;

import java.sql.Date;
import lombok.Data;

@Data
public class BasicDocument {
  private String id;
  private String cid;
  private String qnaid;
  private String question;
  private String answer;
  private int skill_id;
  private String src;
  private String main_yn;
  private String channel;
  private String category;
  private String sub_category;
  private String sub_sub_category;
  private Date create_dtm;
  private Date update_dtm;
}
