package ai.maum.basicqa;

import ai.maum.util.PropertiesManager;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import io.grpc.Server;
import io.grpc.netty.NettyServerBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.LBHttpSolrClient;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BasicQAServerMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(BasicQAServerMain.class);

  private Server server;

  private CloudSolrClient cloudSolrClient;

  public static void main(String[] args) {
    BasicQAServerMain qaServerMain = new BasicQAServerMain();
    if ("Y".equalsIgnoreCase(properties.getProperty("TLO_USE"))) {
      qaServerMain.setLog();
    }
    qaServerMain.execute();
    qaServerMain.blockUntilShutdown();
  }

  private void setLog() {
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    JoranConfigurator joranConfigurator = new JoranConfigurator();
    joranConfigurator.setContext(loggerContext);
    loggerContext.reset();
    try {
      joranConfigurator.doConfigure(PropertiesManager.resolveValueWithProVars(properties.getProperty("TLO_CONFIG_PATH")));

      FileAppender fileAppender = new FileAppender();
      fileAppender.setContext(loggerContext);
      fileAppender.setName("FILE");
      fileAppender.setFile(getLogFileName());

      PatternLayoutEncoder encoder = new PatternLayoutEncoder();
      encoder.setContext(loggerContext);
      encoder.setPattern("%msg%n");
      encoder.start();

      fileAppender.setEncoder(encoder);
      fileAppender.start();

      ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger("TLO");
      logbackLogger.setLevel(Level.INFO);
      logbackLogger.addAppender(fileAppender);


      new ClassPathXmlApplicationContext("classpath:/spring/tlo-quartz.xml");
    } catch (JoranException e) {
      e.printStackTrace();
    }
    StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);

  }

  private void execute() {
    try {
      logger.info("{}", "Basic QA Server Start");
      ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring/index-quartz.xml");
      initSchedule(context);
      long idleTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      long ageTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      int port = Integer.parseInt(properties.getProperty("QA_PORT", "50051"));
      String solr1_url = properties.getProperty("SOLR_URL_1", "http://localhost:8983/solr");
      String solr2_url = properties.getProperty("SOLR_URL_2", "http://localhost:8984/solr");
      String solr3_url = properties.getProperty("SOLR_URL_3", "http://localhost:8985/solr");
      LBHttpSolrClient lbHttpSolrClient = new LBHttpSolrClient.Builder().withBaseSolrUrls(solr1_url, solr2_url, solr3_url).build();
      List<String> zkList = new ArrayList<>();
      zkList.add(properties.getProperty("ZOO_URL_1", "localhost:2181"));
      zkList.add(properties.getProperty("ZOO_URL_2", "localhost:2182"));
      zkList.add(properties.getProperty("ZOO_URL_3", "localhost:2183"));
      cloudSolrClient = new CloudSolrClient.Builder().withLBHttpSolrClient(lbHttpSolrClient).withZkHost(zkList).build();
      cloudSolrClient.setDefaultCollection(properties.getProperty("COLLECTION", "basicqa"));
      server = NettyServerBuilder.forPort(port)
          .maxConnectionIdle(idleTimeout, TimeUnit.SECONDS)
          .maxConnectionAge(ageTimeout, TimeUnit.SECONDS)
          .addService(new BasicQAService(cloudSolrClient, context)).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          BasicQAServerMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private void initSchedule(ClassPathXmlApplicationContext context) {
    String schedulePath = PropertiesManager.resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
    if (StringUtils.isNotEmpty(schedulePath )) {
      File scheduleFile = new File(schedulePath);
      if (scheduleFile.exists() && scheduleFile.isFile()) {
        try {
          FileInputStream fileInputStream = new FileInputStream(scheduleFile);
          Properties scheduleProperties = new Properties();
          scheduleProperties.load(fileInputStream);
          ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
          CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
          StdScheduler stdScheduler = beanfactory
              .getBean("indexSchedulerFactoryBean", StdScheduler.class);
          String scheduleInfo = scheduleProperties.getProperty("schedule_info");
          if (StringUtils.isNotEmpty(scheduleInfo)) {
            cronTrigger.setCronExpression(scheduleInfo);
            stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
          }
          fileInputStream.close();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (SchedulerException e) {
          e.printStackTrace();
        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else {
        return;
      }
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  public String getLogFileName() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    Date now = new Date();
    String nowDT = simpleDateFormat.format(now);
    int min = Integer.parseInt(StringUtils.substring(nowDT, -2));
    int amount = min % 5;
    Date logDate = DateUtils.addMinutes(now, -amount);
    String dt = simpleDateFormat.format(logDate);
    String logFileName = properties.getProperty("TLO_FILE_PRE_FORMAT", "BASICQA.001.")  + dt + ".log";
    String logPathName = PropertiesManager.resolveValueWithProVars(properties.getProperty("TLO_OUTPUT_PATH")) + File.separator + StringUtils.substring(dt, 0, 8);
    File logPath = new File(logPathName);
    if (!logPath.exists()) {
      logPath.mkdirs();
    }
    String fullPathName = logPathName + File.separator + logFileName;
    File logFile = new File(fullPathName);
    if (!logFile.exists()) {
      try {
        logFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return fullPathName;
  }
}
