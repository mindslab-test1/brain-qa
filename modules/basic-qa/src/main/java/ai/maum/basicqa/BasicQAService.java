package ai.maum.basicqa;

import ai.maum.util.PropertiesManager;
import ai.maum.util.SqlSessionManager;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import maum.brain.qa.BasicQAServiceGrpc.BasicQAServiceImplBase;
import maum.brain.qa.Basicqa.AnswerOutput;
import maum.brain.qa.Basicqa.AnswerOutput.Builder;
import maum.brain.qa.Basicqa.DBResult;
import maum.brain.qa.Basicqa.IndexingInput;
import maum.brain.qa.Basicqa.QuestionInput;
import maum.brain.qa.Basicqa.ScheduleInfo;
import maum.brain.qa.Basicqa.SearchResult;
import maum.brain.qa.Basicqa.SearchStatus;
import maum.brain.qa.Basicqa.Type;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BasicQAService extends BasicQAServiceImplBase {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(BasicQAService.class);

  private static Logger TLO = LoggerFactory.getLogger("TLO");

  private static CloudSolrClient cloudSolrClient;

  private static boolean useTLO = false;

  private static IndexingCore indexingCore;

  private DocumentIndexingHandler documentIndexingHandler;

  private ClassPathXmlApplicationContext context;


  public BasicQAService(CloudSolrClient cloudSolrClient, ClassPathXmlApplicationContext context) {
    this.cloudSolrClient = cloudSolrClient;
    this.useTLO =
        StringUtils.equalsIgnoreCase(properties.getProperty("TLO_USE", "N"), "Y") ? true : false;
    this.documentIndexingHandler = new DocumentIndexingHandler(cloudSolrClient);
    this.context = context;
  }

  @Override
  public void fullIndexing(IndexingInput request, StreamObserver<SearchStatus> responseObserver) {
    try {
      if (documentIndexingHandler.isIndexing()) {
        getStatus(responseObserver);
        return;
      }
      documentIndexingHandler.setIndexing(true);
      indexingCore = new IndexingCore(request, documentIndexingHandler, cloudSolrClient,
          null);
      indexingCore.start();

      Thread.sleep(10);
      getStatus(responseObserver);
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public void additionalIndexing(IndexingInput request,
      StreamObserver<SearchStatus> responseObserver) {
    try {
      if (documentIndexingHandler.isIndexing()) {
        getStatus(responseObserver);
        return;
      }
      String indexingFilePath = PropertiesManager
          .resolveValueWithProVars(properties.getProperty("INDEXING_TIME_FILE_PATH"));
      logger.info("Indexing FIle: {}", indexingFilePath);
      if (StringUtils.isEmpty(indexingFilePath)) {
        logger.warn("INDEXING_FILE_PATH is not defined");
      } else {
        FileInputStream fileInputStream = new FileInputStream(indexingFilePath);
        Properties indexingProperties = new Properties();
        indexingProperties.load(fileInputStream);
        documentIndexingHandler.setIndexing(true);
        indexingCore = new IndexingCore(request, documentIndexingHandler, cloudSolrClient,
            indexingProperties);
        indexingCore.start();
        fileInputStream.close();
      }
      Thread.sleep(10);
      getStatus(responseObserver);
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public void getIndexingStatus(Empty request, StreamObserver<SearchStatus> responseObserver) {
    try {
      getStatus(responseObserver);
    } catch (Exception e) {
      responseObserver.onError(e);
    }
  }

  @Override
  public void setSchedule(ScheduleInfo request, StreamObserver<ScheduleInfo> responseObserver) {
    ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
    CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
    StdScheduler stdScheduler = beanfactory
        .getBean("indexSchedulerFactoryBean", StdScheduler.class);
    try {
      cronTrigger.setCronExpression(request.getCron());
      stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
      String schedulePath = PropertiesManager.resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
      logger.info("schedulePath: {}", schedulePath);
      if (StringUtils.isNotEmpty(schedulePath)) {
        File scheduleFile = new File(schedulePath);
        Properties newScheduleProperties = new Properties();
        newScheduleProperties.setProperty("schedule_info", request.getCron());
        FileOutputStream fileOutputStream = new FileOutputStream(scheduleFile);
        newScheduleProperties.store(fileOutputStream, "Schedule Info");
        fileOutputStream.close();
      }
      responseObserver.onNext(request);
      responseObserver.onCompleted();
    } catch (ParseException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (SchedulerException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    } catch (IOException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    }
  }

  @Override
  public void getSchedule(Empty request, StreamObserver<ScheduleInfo> responseObserver) {
    try {
      ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
      CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
      ScheduleInfo scheduleInfo = ScheduleInfo.newBuilder().setCron(cronTrigger.getCronExpression())
          .build();
      responseObserver.onNext(scheduleInfo);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }

  }

  private void getStatus(StreamObserver<SearchStatus> responseObserver) {
    SearchStatus searchStatus = SearchStatus.newBuilder()
        .setStatus(documentIndexingHandler.isIndexing())
        .setTotal(documentIndexingHandler.getTotal())
        .setFetched(documentIndexingHandler.getFetched())
        .setProcessed(documentIndexingHandler.getProcessed()).build();
    responseObserver.onNext(searchStatus);
    responseObserver.onCompleted();
  }

  @Override
  public void question(QuestionInput questionInput, StreamObserver<AnswerOutput> responseObserver) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    Map<String, String> meta = new HashMap<>();
    meta.putAll(questionInput.getMetaMap());
    meta.put("req_time", simpleDateFormat.format(new Date()));
    meta.put("qa_engine", "BASICQA");
    Builder resultQA = AnswerOutput.newBuilder();
    try {
      logger.info("Question : " + questionInput.getQuestion());
      resultQA.setQuestion(questionInput.getQuestion());
      if (questionInput.getType() == Type.DB) {
        getDBAnswer(resultQA, questionInput);
      } else if (questionInput.getType() == Type.SEARCH) {
        getSearchAnswer(resultQA, questionInput);
      } else if (questionInput.getType() == Type.ALL) {
        getDBAnswer(resultQA, questionInput);
        getSearchAnswer(resultQA, questionInput);
      }
      meta.put("result_code", "20000000");
    } catch (Exception e) {
      e.printStackTrace();
      meta.put("result_code", "40000103");
    }
    responseObserver.onNext(resultQA.build());
    responseObserver.onCompleted();
    meta.put("rsp_time", simpleDateFormat.format(new Date()));
    if (useTLO) {
      writeTLO(meta);
    }
  }

  @Override
  public void abortIndexing(Empty request, StreamObserver<SearchStatus> responseObserver) {
    try {
      if (indexingCore != null && indexingCore.isAlive()) {
        indexingCore.interrupt();
        indexingCore.stop();
        documentIndexingHandler.setIndexing(false);
        SearchStatus searchStatus = SearchStatus.newBuilder().setStatus(false).setTotal(0)
            .setFetched(0).setProcessed(0).setMessage("Indexing Abort Success").build();
        responseObserver.onNext(searchStatus);
        responseObserver.onCompleted();
      } else {
        SearchStatus searchStatus = SearchStatus.newBuilder().setStatus(false).setTotal(0)
            .setFetched(0).setProcessed(0).setMessage("Indexing Already Aborted").build();
        responseObserver.onNext(searchStatus);
        responseObserver.onCompleted();
      }
    } catch (Exception e) {
      SearchStatus searchStatus = SearchStatus.newBuilder().setStatus(false).setTotal(0)
          .setFetched(0).setProcessed(0).setMessage("Indexing Abort Error: " + e.getMessage())
          .build();
      responseObserver.onNext(searchStatus);
      responseObserver.onCompleted();
    }
  }

  private void writeTLO(Map<String, String> meta) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    StringBuffer buffer = new StringBuffer();
    buffer.append("SEQ_ID=");
    buffer.append(checkNull(meta.get("seq_id")));
    buffer.append("|");
    buffer.append("LOG_TIME=");
    buffer.append(checkNull(simpleDateFormat.format(new Date())));
    buffer.append("|");
    buffer.append("LOG_TYPE=");
    buffer.append(checkNull(meta.get("log_type")));
    buffer.append("|");
    buffer.append("SID=");
    buffer.append(checkNull(meta.get("sid")));
    buffer.append("|");
    buffer.append("RESULT_CODE=");
    buffer.append(checkNull(meta.get("result_code")));
    buffer.append("|");
    buffer.append("REQ_TIME=");
    buffer.append(checkNull(meta.get("req_time")));
    buffer.append("|");
    buffer.append("RSP_TIME=");
    buffer.append(checkNull(meta.get("rsp_time")));
    buffer.append("|");
    buffer.append("CLIENT_IP=");
    buffer.append(checkNull(meta.get("client_ip")));
    buffer.append("|");
    buffer.append("DEV_INFO=");
    buffer.append(checkNull(meta.get("dev_info")));
    buffer.append("|");
    buffer.append("OS_INFO=");
    buffer.append(checkNull(meta.get("os_info")));
    buffer.append("|");
    buffer.append("NW_INFO=");
    buffer.append(checkNull(meta.get("nw_info")));
    buffer.append("|");
    buffer.append("SVC_NAME=");
    buffer.append(checkNull(meta.get("svc_name")));
    buffer.append("|");
    buffer.append("DEV_MODEL=");
    buffer.append(checkNull(meta.get("dev_model")));
    buffer.append("|");
    buffer.append("CARRIER_TYPE=");
    buffer.append(checkNull(meta.get("carrier_type")));
    buffer.append("|");
    buffer.append("TR_ID=");
    buffer.append(checkNull(meta.get("transaction_id")));
    buffer.append("|");
    buffer.append("MSG_ID=");
    buffer.append(checkNull(meta.get("message_id")));
    buffer.append("|");
    buffer.append("FROM_SVC_NAME=");
    buffer.append(checkNull(meta.get("from_svc_name")));
    buffer.append("|");
    buffer.append("TO_SVC_NAME=");
    buffer.append(checkNull(meta.get("to_svc_name")));
    buffer.append("|");
    buffer.append("SVC_TYPE=");
    buffer.append(checkNull(meta.get("svc_type")));
    buffer.append("|");
    buffer.append("DEV_TYPE=");
    buffer.append(checkNull(meta.get("dev_type")));
    buffer.append("|");
    buffer.append("DEVICE_TOKEN=");
    buffer.append(checkNull(meta.get("device_token")));
    buffer.append("|");
    buffer.append("QA_ENGINE=");
    buffer.append(checkNull(meta.get("qa_engine")));
    TLO.info(buffer.toString());
  }

  private String checkNull(String value) {
    return StringUtils.isEmpty(value) ? "" : value;
  }


  private Builder getSearchAnswer(Builder answerOutput, QuestionInput questionInput)
      throws Exception {
//    String question = questionInput.getQuestion();

    boolean isMatch = doExactMatch(questionInput, answerOutput);
    if (isMatch) {
      return answerOutput;
    }

    NlpService nlpService = new NlpService();
    String question = nlpService.doNlp(questionInput.getQuestion());
    logger.info("NLP Result: {}", question);

    String[] words = StringUtils.split(question, " ");
    SolrQuery solrQuery = buildQuery(questionInput, words, " AND ", "N");

    try {
      QueryResponse queryResponse = cloudSolrClient.query(solrQuery);
      SolrDocumentList documentList = queryResponse.getResults();
      int i = 0;
      logger.info("documentList size : " + documentList.size());
      if (documentList.size() > 0) {
        for (SolrDocument document : documentList) {
          if (i >= questionInput.getNtop()) {
            break;
          }
          String questionMorph = String.valueOf(document.get("question_morph"));
          String[] morps = StringUtils.split(questionMorph, " ");

          if (words.length != morps.length) {
            continue;
          }
          SearchResult searchResult = SearchResult.newBuilder()
              .setSkillId(Integer.parseInt(String.valueOf(document.get("skill_id"))))
              .setSrc(String.valueOf(document.get("src")))
              .setQuestionMorph(String.valueOf(document.get("question_morph")))
              .setQuestion(String.valueOf(document.get("question")))
              .setAnswer(String.valueOf(document.get("answer")))
              .setIsMain(Boolean.valueOf(String.valueOf(document.get("isMain"))))
              .setScore(Float.parseFloat(String.valueOf(document.get("score"))))
              .setChannel(String.valueOf(document.get("channel")))
              .setCategory(String.valueOf(document.get("category")))
              .setSubCategory(String.valueOf(document.get("sub_category")))
              .setSubSubCategory(String.valueOf(document.get("sub_sub_category")))
              .build();
          answerOutput.addSearchResult(searchResult);
          i++;
        }
      }
      if ((documentList.size() == 0 || answerOutput.getSearchResultCount() == 0) && StringUtils
          .equalsIgnoreCase("Y", properties.getProperty("NLG_YN", "N"))) {
        String[] secWords = StringUtils.split(nlpService.getNoun(), " ");
        if (secWords.length == 0) {
          return answerOutput;
        }
        solrQuery = buildQuery(questionInput, secWords, " OR ", "Y");
        QueryResponse secQueryResponse = cloudSolrClient.query(solrQuery);
        SolrDocumentList secDocumentList = secQueryResponse.getResults();
        if (secDocumentList.size() > 0) {
          for (SolrDocument document : secDocumentList) {
            if (i >= questionInput.getNtop()) {
              break;
            }
            StringBuffer questionBuffer = new StringBuffer();
            questionBuffer.append(String.valueOf(document.get("question")));

            SearchResult searchResult = SearchResult.newBuilder()
                .setSkillId(Integer.parseInt(String.valueOf(document.get("skill_id"))))
                .setSrc(String.valueOf(document.get("src")))
                .setQuestionMorph(String.valueOf(document.get("question_morph")))
                .setQuestion(String.valueOf(document.get("question")))
                .setAnswer(String.valueOf(document.get("answer")))
                .setQuestionNlg(questionBuffer.toString())
                .setIsMain(Boolean.valueOf(String.valueOf(document.get("isMain"))))
                .setScore(Float.parseFloat(String.valueOf(document.get("score"))))
                .setChannel(String.valueOf(document.get("channel")))
                .setCategory(String.valueOf(document.get("category")))
                .setSubCategory(String.valueOf(document.get("sub_category")))
                .setSubSubCategory(String.valueOf(document.get("sub_sub_category")))
                .build();
            answerOutput.addSearchResult(searchResult);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
    return answerOutput;
  }

  private boolean doExactMatch(QuestionInput questionInput, Builder answerOutput) {
    try {
      StringBuffer query = new StringBuffer("question:\"");
      query.append(questionInput.getQuestion());
      query.append("\" AND isMain:true");
      if (questionInput.getSkillId() > 0) {
        query.append(" AND skill_id:" + String.valueOf(questionInput.getSkillId()));
      }
      if (StringUtils.isNotEmpty(questionInput.getSrc())) {
        query.append(" AND src:\"" + questionInput.getSrc() +"\"");
      }
      if (StringUtils.isNotEmpty(questionInput.getChannel())) {
        query.append(" AND channel:\"" + questionInput.getChannel() +"\"");
      }
      if (StringUtils.isNotEmpty(questionInput.getCategory())) {
        query.append(" AND category:\"" + questionInput.getCategory() +"\"");
      }
      if (StringUtils.isNotEmpty(questionInput.getSubCategory())) {
        query.append(" AND sub_category:\"" + questionInput.getSubCategory() +"\"");
      }
      if (StringUtils.isNotEmpty(questionInput.getSubSubCategory())) {
        query.append(" AND sub_sub_category:\"" + questionInput.getSubSubCategory() +"\"");
      }

      SolrQuery solrQuery = new SolrQuery();
      solrQuery.set("q", query.toString());
      solrQuery.setFields("*", "score");
      solrQuery.addSort("score", ORDER.desc);
      QueryResponse queryResponse = cloudSolrClient.query(solrQuery);
      SolrDocumentList documentList = queryResponse.getResults();
      for (SolrDocument solrDocument : documentList) {
        SearchResult searchResult = SearchResult.newBuilder()
            .setSkillId(Integer.parseInt(String.valueOf(solrDocument.get("skill_id"))))
            .setSrc(String.valueOf(solrDocument.get("src")))
            .setQuestionMorph(String.valueOf(solrDocument.get("question_morph")))
            .setQuestion(String.valueOf(solrDocument.get("question")))
            .setAnswer(String.valueOf(solrDocument.get("answer")))
            .setIsMain(Boolean.valueOf(String.valueOf(solrDocument.get("isMain"))))
            .setScore(Float.parseFloat(String.valueOf(solrDocument.get("score"))))
            .setChannel(String.valueOf(solrDocument.get("channel")))
            .setCategory(String.valueOf(solrDocument.get("category")))
            .setSubCategory(String.valueOf(solrDocument.get("sub_category")))
            .setSubSubCategory(String.valueOf(solrDocument.get("sub_sub_category")))
            .build();
        answerOutput.addSearchResult(searchResult);
      }
      if (answerOutput.getSearchResultCount() > 0) {
        return true;
      }
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return false;
  }

  private SolrQuery buildQuery(QuestionInput questionInput, String[] words, String separator, String onlyMain) {
    String[] queryWords = new String[words.length];
    for (int i = 0; i < words.length; i++) {
      queryWords[i] = "question_morph:" + words[i];
    }

    SolrQuery solrQuery = new SolrQuery();
    StringBuffer qbuffer = new StringBuffer("(" + StringUtils.join(queryWords, separator) + ")");
    if (questionInput.getSkillId() > 0) {
      qbuffer.append(" AND skill_id:" + String.valueOf(questionInput.getSkillId()));
    }
    if (StringUtils.isNotEmpty(questionInput.getSrc())) {
      qbuffer.append(" AND src:\"" + questionInput.getSrc() +"\"");
    }
    if ("Y".equalsIgnoreCase(onlyMain)) {
      qbuffer.append(" AND isMain:true");
    }
    if (StringUtils.isNotEmpty(questionInput.getChannel())) {
      qbuffer.append(" AND channel:\"" + questionInput.getChannel() +"\"");
    }
    if (StringUtils.isNotEmpty(questionInput.getCategory())) {
      qbuffer.append(" AND category:\"" + questionInput.getCategory() +"\"");
    }
    if (StringUtils.isNotEmpty(questionInput.getSubCategory())) {
      qbuffer.append(" AND sub_category:\"" + questionInput.getSubCategory() +"\"");
    }
    if (StringUtils.isNotEmpty(questionInput.getSubSubCategory())) {
      qbuffer.append(" AND sub_sub_category:\"" + questionInput.getSubSubCategory() +"\"");
    }

    logger.info(
        "NLP Result : " + qbuffer.toString());
    solrQuery.set("q", qbuffer.toString());
    solrQuery.setFields("*", "score");
    solrQuery.addSort("score", ORDER.desc);
    return solrQuery;
  }

  private Builder getDBAnswer(Builder resultQA, QuestionInput questionInput) {
    logger.info("Start DB Select");
    Map<String, Object> params = new HashMap<>();
    params.put("question", questionInput.getQuestion());
    params.put("skill_id", questionInput.getSkillId());
    params.put("src", questionInput.getSrc());
    params.put("limit", questionInput.getNtop());
    params.put("channel", questionInput.getChannel());
    params.put("category", questionInput.getCategory());
    params.put("sub_category", questionInput.getSubCategory());
    params.put("sub_sub_category", questionInput.getSubSubCategory());
    SqlSessionManager sqlSessionManager = SqlSessionManager.getInstance();
    SqlSession sqlSession = null;
    try {
      sqlSession = sqlSessionManager.getSqlSession();
      List<BasicQADBResult> list = sqlSession.selectList("basicqa.selectQuestion", params);
      logger.info("End DB Select");
      sqlSession.close();
      for (BasicQADBResult result : list) {
        DBResult dbResult = DBResult.newBuilder()
            .setAnswer((result.getAnswer() == null ? "" : result.getAnswer()))
            .setSkillId(result.getSkill_id())
            .setSrc(result.getSrc())
            .setChannel(result.getChannel())
            .setCategory(result.getCategory())
            .setSubCategory(result.getSub_category())
            .setSubSubCategory(result.getSub_sub_category())
            .build();
        resultQA.addDbResult(dbResult);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (sqlSession != null) {
        sqlSession.close();
      }
    }
    return resultQA;
  }
}
