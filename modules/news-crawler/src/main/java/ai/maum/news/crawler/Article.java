package ai.maum.news.crawler;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;


@Data
public class Article {
  private String titleSub;
  private List<String> keywords = new ArrayList<>();
  private String mdName;
  private long crawlTimeOrg;
  private String mediaKindCode;
  private String articleTime;
  private String mediaKindName;
  private int refType;
  private String dupKey;
  private String mediaType;
  private String title;
  private String refLink;
  private String content;
  private Highlight highlight;
  private String orgLink;
  private long crawlTime;
  private String articleSerial;

  private List<String> tImg = new ArrayList<>();
  private String writer;
  private String category;
  private String refName;
  private int groupNo;
  private String mdOid;
  private int sameCount;

  public List<String> gettImg() {
    return tImg;
  }

  public void settImg(List<String> tImg) {
    this.tImg = tImg;
  }
}
