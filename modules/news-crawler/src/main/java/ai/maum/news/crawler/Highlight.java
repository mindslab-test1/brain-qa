package ai.maum.news.crawler;

import lombok.Data;

@Data
public class Highlight {
  private String title_sub;
  private String lead_content;
  private String writer;
  private String title;
  private String content;

}
