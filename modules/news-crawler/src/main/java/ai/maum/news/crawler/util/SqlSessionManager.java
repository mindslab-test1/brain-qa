package ai.maum.news.crawler.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlSessionManager {

	private static SqlSessionManager instance;

	private static SqlSessionFactory sqlSessionFactory = null;

	private static Properties properties = PropertiesManager.getProperties();

	private SqlSessionManager() {
		try {
			String resource = "mybatis/mysql-config.xml";
			if ("oracle".equalsIgnoreCase(properties.getProperty("DB_TYPE", "mysql"))) {
				resource = "mybatis/oracle-config.xml";
			}

			Reader reader = Resources.getResourceAsReader(resource);

			String dbConfig = System.getProperty("db.config");
			if (dbConfig != null) {
				try {
					Properties properties = new Properties();
					InputStream is = new FileInputStream(dbConfig);
					properties.load(is);
					sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, properties);
				} catch (Exception e) {
					sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
				}
			} else {
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public SqlSession getSqlSession() {
		return sqlSessionFactory.openSession();
	}
	
	public static SqlSessionManager getInstance() {
		if (instance == null) {
			instance = new SqlSessionManager();
		}
		return instance;
	}
}
