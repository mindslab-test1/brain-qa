package ai.maum.news.crawler;

import ai.maum.news.crawler.util.PropertiesManager;
import ai.maum.news.crawler.util.SqlSessionManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsCrawlerMain {

  private static Logger logger = LoggerFactory.getLogger(NewsCrawlerMain.class);

  private static Properties properties = PropertiesManager.getProperties();

  private static String NEWS_URL = "";

  public NewsCrawlerMain() {
    this.NEWS_URL = properties.getProperty("NEWS_URL", "https://txtnews.scrapmaster.co.kr/api/onlineNewsList.php?id=mindslab");
  }

  public static void main(String[] args) {
    NewsCrawlerMain newsCrawlerMain = new NewsCrawlerMain();
    if (args.length == 0) {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
      Date today = new Date();
      logger.info("Start Date: {}", simpleDateFormat.format(today));
      logger.info("End Date: {}", simpleDateFormat.format(today));
      newsCrawlerMain.execute(null, null, 0);
    } else if (args.length == 2) {
      String startDate = args[0];
      String endDate = args[1];
      logger.info("Start Date: {}", startDate);
      logger.info("End Date: {}", endDate);
      newsCrawlerMain.execute(startDate, endDate, 0);
    }
  }

  private void execute(String startDate, String endDate, int pageNo) {
    BufferedReader reader = null;
    try {
      String news_url = NEWS_URL;
      if (StringUtils.isNotEmpty(startDate) && StringUtils.isNotEmpty(endDate)) {
        news_url = news_url + "&begin_date=" + startDate + "&end_date=" + endDate;
      }
      URL url = new URL(news_url + "&sp=" + pageNo);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
      ObjectMapper mapper = new ObjectMapper();
      JsonNode jsonNode = mapper.readTree(reader);
      JsonNode articles = jsonNode.get("ArticleMap");
      con.disconnect();
      logger.info("[News Article Info]");
      logger.info("Total Num: {}", jsonNode.get("TotalNum").asInt());
      logger.info("Total Page: {}", jsonNode.get("TotalPage").asInt());
      logger.info("This Page: {}", jsonNode.get("ThisPage").asInt());

      SqlSessionManager sqlSessionManager = SqlSessionManager.getInstance();
      SqlSession sqlSession = sqlSessionManager.getSqlSession();
      if (articles.isArray()) {
        int cnt = 0;
        for (int i = 0; i < articles.size(); i++) {
          Article article = mapper.convertValue(articles.get(i), Article.class);
          String id = sqlSession.selectOne("news.selectNewsSerial", article);
          if (StringUtils.isEmpty(id)) {
            try {
              sqlSession.insert("news.insertNews", article);
              sqlSession.commit();
              cnt ++;
            } catch (Exception e) {
              e.printStackTrace();
              logger.error(e.getMessage());
              logger.info("ARTICLE INFO: {}", article.toString());
            }

          }
        }
        logger.info("Insert Article Count: {}", cnt);
      }
      reader.close();
      int totalPage = jsonNode.get("TotalPage").asInt();
      if (pageNo < totalPage - 1) {
        execute(startDate, endDate, pageNo + 1);
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    } catch (IOException e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
          logger.error(e.getMessage());
        }
      }
    }
  }
}
