#!/usr/bin/env bash

PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/" >/dev/null
  BUILD_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

prekbqa_root=${PREKBQA_ROOT}
if [ -z "${prekbqa_root}" ]; then
  echo "PREKBQA_ROOT is not defined!"
  exit 1
fi

test -d ${prekbqa_root} || mkdir -p ${prekbqa_root}
test -d ${prekbqa_root}/lib/ || mkdir -p ${prekbqa_root}/lib/
test -d ${prekbqa_root}/logs/ || mkdir -p ${prekbqa_root}/logs/


cd $BUILD_HOME

chmod 755 ${BUILD_HOME}/gradlew
chmod 755 ${BUILD_HOME}/bin/pre_kbqa_server.sh

APP_VERSION="1.0.0"
./gradlew clean shadowJar

if [ $? -eq 0 ]; then
  cp -r ${BUILD_HOME}/bin ${prekbqa_root}/
  cp -r ${BUILD_HOME}/conf ${prekbqa_root}/
  #cp -r ${BUILD_HOME}/lib ${prekbqa_root}/
  cp ${BUILD_HOME}/build/libs/preKbqa-${APP_VERSION}.jar ${prekbqa_root}/lib/
  cd ${prekbqa_root}/lib/
  ln -s preKbqa-${APP_VERSION}.jar preKbqa.jar
  sed -i "s#\${APP_HOME}#${prekbqa_root}#g" ${prekbqa_root}/conf/preKbqa.properties
  #sed -i "s#\${APP_HOME}#${prekbqa_root}#g" ${prekbqa_root}/conf/logback.xml
  else
    echo "Gradle Build Fail!"
    exit 1
fi
