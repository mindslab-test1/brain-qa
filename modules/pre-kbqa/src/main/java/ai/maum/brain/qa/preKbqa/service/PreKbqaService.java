package ai.maum.brain.qa.preKbqa.service;

import ai.maum.brain.qa.preKbqa.data.PreKbqaData;
import ai.maum.brain.qa.preKbqa.data.SentencePattrenRepository;
import ai.maum.brain.qa.preKbqa.data.SynonymRepository;

public interface PreKbqaService {
    String procSynonym(PreKbqaData preKbqaData, SynonymRepository synonymRepository);
    String procSynonymSent(String data, SentencePattrenRepository sentencePattrenRepository);
    String procSynonymSent(PreKbqaData preKbqaData, SentencePattrenRepository sentencePattrenRepository);
    String saveData(PreKbqaData preKbqaData, SynonymRepository synonymRepository);
}
