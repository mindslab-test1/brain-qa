package ai.maum.brain.qa.preKbqa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"ai.maum.brain.qa.preKbqa"})
public class PreKbqaMain {

    public static void main(String[] args) {
        SpringApplication.run(PreKbqaMain.class, args);
    }
}
