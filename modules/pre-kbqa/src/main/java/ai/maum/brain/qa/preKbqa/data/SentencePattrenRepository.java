package ai.maum.brain.qa.preKbqa.data;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SentencePattrenRepository extends JpaRepository<SentencePatternDAO, Integer> {
    List<SentencePatternDAO> findByPattern(String sent);
}