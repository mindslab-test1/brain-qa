package ai.maum.brain.qa.preKbqa.util;

import org.springframework.core.env.Environment;

public class MessageUtility {

    private final Environment env;
    public MessageUtility(Environment env) {
        this.env = env;
    }

    public String getProperty(String code, String def) {
        return env.getProperty(code, def);
    }

    public String getProperty(String code) {
        return getProperty(code, null);
    }
}
