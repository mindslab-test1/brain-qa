package ai.maum.brain.qa.preKbqa.data;

import lombok.Data;

// gson data
@Data
public class PreKbqaData {
    private String sentence;
    private String word;
    private String main_word;

    // constructor
    public PreKbqaData() {
        sentence = "";
        word = "";
        main_word = "";
    }

    public PreKbqaData(String sentence) {
        this.sentence = sentence;
        this.word = "";
        this.main_word = "";
    }

    public String getSentence() {
        return sentence;
    }

    public String getWord() {
        return word;
    }

    public String getMainWord() {
        return main_word;
    }

    @Override
    public String toString() {
        return String.format("PreKbqaData[sentence=%s, word='%s', mainWord='%s']", sentence, word, main_word);
    }
}
