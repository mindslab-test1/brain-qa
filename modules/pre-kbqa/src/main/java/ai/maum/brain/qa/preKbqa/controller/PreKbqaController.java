package ai.maum.brain.qa.preKbqa.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import ai.maum.brain.qa.preKbqa.data.PreKbqaData;
import ai.maum.brain.qa.preKbqa.data.SentencePattrenRepository;
import ai.maum.brain.qa.preKbqa.data.SynonymRepository;
import ai.maum.brain.qa.preKbqa.service.PreKbqaService;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
@RequestMapping(value = "/")
public class PreKbqaController {
    private static final Logger logger = LoggerFactory.getLogger(PreKbqaController.class);

    @Autowired
    private PreKbqaService preKbqaService;

    @Autowired
    SynonymRepository synonymRepository;

    @Autowired
    SentencePattrenRepository sentencePattrenRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String time(Locale locale) {
        logger.info("Welcome home!! The client locale is  {}.", locale);
        Date date = new Date();
        DateFormat dateFormat = DateFormat
                .getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
        String formattedDate = dateFormat.format(date);
        return formattedDate;
    }

    @RequestMapping(value = "/reserve/doReserve", method = RequestMethod.POST, produces = "application/json;charset=utf-8" )
    public String test(@RequestBody String request) {
        logger.info("Request Data {}", request);
        return request;
    }

    @PostMapping(value = "/synonym/all")
    public Object requestAll(@RequestBody String request) {
        logger.info("[requestAll] Call! - " + request);
        Gson gson = new GsonBuilder().create();
        PreKbqaData reqData ;
        try {
            // parse data
            reqData = gson.fromJson(request, PreKbqaData.class);
            logger.info(reqData.toString());
        } catch (JsonSyntaxException e) {
            return "Data is not Json Format";
        }

        // 동의어 치환
        String ret = preKbqaService.procSynonym(reqData, synonymRepository);

        // 유사질문 치환
        ret = preKbqaService.procSynonymSent(ret, sentencePattrenRepository);

        // Json Return
        PreKbqaData resData = new PreKbqaData(ret);
        return resData;
    }

    @PostMapping(value = "/synonym/word")
    public Object requestSynonym(@RequestBody String request) {
        logger.info("[requestSynonym] Call! - " + request);
        Gson gson = new GsonBuilder().create();
        PreKbqaData reqData ;
        try {
            // parse data
            reqData = gson.fromJson(request, PreKbqaData.class);
            logger.info(reqData.toString());
        } catch (JsonSyntaxException e) {
            return "Data is not Json Format";
        }

        // 동의어 치환
        String ret = preKbqaService.procSynonym(reqData, synonymRepository);

        // Json Return
        PreKbqaData resData = new PreKbqaData(ret);
        return resData;
    }

    @PostMapping(value = "/synonym/sentence")
    public Object requestSynonymSent(@RequestBody String request) {
        logger.info("[requestSynonymSent] Call! - " + request);
        Gson gson = new GsonBuilder().create();
        PreKbqaData reqData ;
        try {
            // parse data
            reqData = gson.fromJson(request, PreKbqaData.class);
            logger.info(reqData.toString());
        } catch (JsonSyntaxException e) {
            return "Data is not Json Format";
        }

        // 유사질문 치환
        String ret = preKbqaService.procSynonymSent(reqData, sentencePattrenRepository);

        // Json Return
        PreKbqaData resData = new PreKbqaData(ret);
        return resData;
    }

    @PostMapping(value = "/word/data/save")
    public Object saveData(@RequestBody String request) {
        Gson gson = new GsonBuilder().create();
        PreKbqaData reqData ;

        try {
            // parse data
            reqData = gson.fromJson(request, PreKbqaData.class);
            logger.info(reqData.toString());
        } catch (JsonSyntaxException e) {
            return "Data is not Json Format";
        }

        // proc
        String ret = preKbqaService.saveData(reqData, synonymRepository);

        return ret;
    }
/*
    @PostMapping(value = "/v1/UshopG7FAQ/query")
    public Object requestG7(@RequestBody BasicQADomain basicQADomain,  ModelMap model) {
        basicQADomain.setNtop(1);
        BasicQAResult basicQAResult = basicQAService.question(basicQADomain, 31);
        return basicQAResult;
    }
    */
}
