package ai.maum.brain.qa.preKbqa.data;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SynonymRepository extends JpaRepository<SynonymDAO, Integer> {
    List<SynonymDAO> findByWord(String word);
}