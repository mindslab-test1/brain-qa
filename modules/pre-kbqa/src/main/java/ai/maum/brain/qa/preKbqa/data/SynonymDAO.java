package ai.maum.brain.qa.preKbqa.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "synonym", schema = "SUBSTITUTE")
public class SynonymDAO implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "word")
    private String word;

    @Column(name = "main_word")
    private String mainWord;

    public SynonymDAO() {
    }

    public SynonymDAO(String word, String mainWord) {
        this.word = word;
        this.mainWord = mainWord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMainWord() {
        return mainWord;
    }

    public void setMainWord(String mainWord) {
        this.mainWord = mainWord;
    }

    @Override
    public String toString() {
        return String.format("SynonymDAO [id=%d, word='%s', mainWord='%s']", id, word, mainWord);
    }
}
