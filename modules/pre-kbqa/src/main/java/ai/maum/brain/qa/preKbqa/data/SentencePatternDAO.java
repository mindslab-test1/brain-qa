package ai.maum.brain.qa.preKbqa.data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sentence_pattern", schema = "SUBSTITUTE")
public class SentencePatternDAO implements Serializable {

    private static final long serialVersionUID = -3009157732242241606L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "pattern")
    private String pattern;

    @Column(name = "main_sentence")
    private String mainSentence;

    public SentencePatternDAO() {
    }

    public SentencePatternDAO(String pattern, String mainSentence) {
        this.pattern = pattern;
        this.mainSentence = mainSentence;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getMainSentence() {
        return mainSentence;
    }

    public void setMainSentence(String mainSentence) {
        this.mainSentence = mainSentence;
    }

    @Override
    public String toString() {
        return String.format("SynonymDAO [id=%d, pattern='%s', mainSentence='%s']", id, pattern, mainSentence);
    }
}
