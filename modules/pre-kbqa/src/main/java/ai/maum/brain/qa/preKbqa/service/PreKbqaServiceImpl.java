package ai.maum.brain.qa.preKbqa.service;

import ai.maum.brain.qa.preKbqa.data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PreKbqaServiceImpl implements PreKbqaService {

    private static final Logger logger = LoggerFactory.getLogger(PreKbqaServiceImpl.class);

    public PreKbqaServiceImpl() {
        logger.info("START Subsitute Word Service!");
        // Read DB & Store HashMap
    }

    public static List<String> ngrams(int n, String str) {
        List<String> ngrams = new ArrayList<String>();
        for (int i = 0; i < str.length() - n + 1; i++)
            ngrams.add(str.substring(i, i + n));
        return ngrams;
    }

    public String procSynonym(PreKbqaData preKbqaData, SynonymRepository synonymRepository) {
        String ret = null;

        String sentence = preKbqaData.getSentence();
        String retSentence = sentence;

        logger.info(preKbqaData.getSentence());
        // TODO

        logger.info("test start");
        for(int i = sentence.length(); i > 1; i--) {
            List<String> ngramList = ngrams(i, sentence);
            for (String word : ngramList) {
//                logger.info("word : " + word);
                // searchWord (select DB)
                List<SynonymDAO> result = synonymRepository.findByWord(word);

                if ( result.size() == 1 ) {
                    for (SynonymDAO syn : result) {
                        if (word.charAt(word.length()-1) == ' ') {
                            word = word.substring(0, word.length()-1);
                        }
                        retSentence = retSentence.replace(word, syn.getMainWord());
                        logger.info("syn.getMainWord() : " + syn.getMainWord());
                        logger.info("retSentence : " + retSentence);
                    }
                }
            }
        }

        // searchWord (select DB)
//        List<SynonymDAO> result = synonymRepository.findByWord(preKbqaData.getWord());
//
//        if ( result.size() == 1 ) {
//            for (SynonymDAO syn : result) {
//                ret = "{ \"main_word\" : \"" + syn.getMainWord() + "\" }";
//                logger.info(syn.getMainWord());
//            }
//        } else {
//            ret = "{ \"main_word\" : \"\" }";
//        }
//        logger.info(qres.getMainWord());
        logger.info("test end");

        return retSentence;
    }

    // override
    public String procSynonymSent(String data, SentencePattrenRepository sentencePattrenRepository) {
        PreKbqaData preKbqaData = new PreKbqaData(data);
        return procSynonymSent(preKbqaData, sentencePattrenRepository);
    }

    public String procSynonymSent(PreKbqaData preKbqaData, SentencePattrenRepository sentencePattrenRepository) {
        String ret = null;

        String sentence = preKbqaData.getSentence();
        String retSentence = sentence;

        logger.info(preKbqaData.getSentence());

        logger.info("[procSynonymSent] start");

        try {
            List<SentencePatternDAO> result = sentencePattrenRepository.findAll();

            for (SentencePatternDAO data : result) {
                logger.info(data.getPattern());
                String patternStr = data.getPattern();
                String regexPattern = patternStr.substring(patternStr.indexOf('('), patternStr.indexOf(')') + 1);
                String replaceStr = patternStr.replace(regexPattern, "");

                Pattern p = Pattern.compile(patternStr);
                Matcher m = p.matcher(retSentence);

                if (m.find()) {
                    retSentence = retSentence.replace(replaceStr, data.getMainSentence());
                }
            }
        } catch (Exception e ) {
            logger.error(e.getMessage());
        }

        logger.info("[procSynonymSent] end");

        return retSentence;
    }

    @Override
    public String saveData(PreKbqaData preKbqaData, SynonymRepository synonymRepository) {
        String ret = null;

        logger.info("word : " + preKbqaData.getWord() + ", main_word : " + preKbqaData.getMainWord());

        SynonymDAO data = new SynonymDAO(preKbqaData.getWord(), preKbqaData.getMainWord());
        SynonymDAO qRes = null;

        // save data (insert DB)
        try {
            qRes = synonymRepository.save(data);
        } catch (Exception e) {
            if(e.getMessage().contains("UNIQUE")) {
                logger.error("Duplicate word! - " + e.getMessage());
                return "Duplicate word - " + preKbqaData.getWord();
            } else
                return e.getMessage();
        }

        List<SynonymDAO> result = synonymRepository.findAll();
        logger.info("" + result.size());

        return qRes.toString();
    }
}
