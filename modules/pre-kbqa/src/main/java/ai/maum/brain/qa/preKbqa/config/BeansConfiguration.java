package ai.maum.brain.qa.preKbqa.config;

import ai.maum.brain.qa.preKbqa.util.MessageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
public class BeansConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    public MessageUtility messageUtility() {
        return new MessageUtility(env);
    }
}