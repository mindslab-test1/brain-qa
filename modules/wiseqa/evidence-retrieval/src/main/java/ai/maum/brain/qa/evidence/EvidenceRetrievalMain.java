package ai.maum.brain.qa.evidence;

import ai.maum.brain.qa.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.Properties;

public class EvidenceRetrievalMain {

    private static Properties properties = PropertiesManager.getProperties();

    private Server server;

    public static void main(String[] args) {
        EvidenceRetrievalMain evidenceRetrievalMain = new EvidenceRetrievalMain();
        evidenceRetrievalMain.runServer();
        evidenceRetrievalMain.blockUntilShutdown();
    }

    private void runServer() {
        try {
            int port = Integer.parseInt(properties.getProperty("EVIDENCE_RETRIEVAL_PORT", "30053"));
            server = ServerBuilder.forPort(port).addService(new EvidenceRetrievalService()).build().start();
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    EvidenceRetrievalMain.this.stop();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void blockUntilShutdown() {
        if (server != null) {
            try {
                server.awaitTermination();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }
}
