package ai.maum.brain.qa.evidence;

import com.nanum.exobrain.searchEngine.EvidenceRetrieval4Distribution_ORG;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.EvidenceRetrievalServiceGrpc.EvidenceRetrievalServiceImplBase;
import maum.brain.qa.Evidenceretrieval;
import maum.brain.qa.Evidenceretrieval.EvidenceOutput;

public class EvidenceRetrievalService extends EvidenceRetrievalServiceImplBase {
    @Override
    public void evidenceRetrieval(Evidenceretrieval.EvidenceInput request, StreamObserver<EvidenceOutput> responseObserver) {
        try {
            EvidenceRetrieval4Distribution_ORG evidenceRetrieval4Distribution_org = new EvidenceRetrieval4Distribution_ORG();
            evidenceRetrieval4Distribution_org.readAnsUnit(request.getSoftFilterResult());
            evidenceRetrieval4Distribution_org.do_evidenceRetrieval(1, 36);
            String result = evidenceRetrieval4Distribution_org.writeAnsUnit();
            Evidenceretrieval.EvidenceOutput semanticSearchOutput = Evidenceretrieval.EvidenceOutput.newBuilder().setEvidenceResult(result).build();
            responseObserver.onNext(semanticSearchOutput);
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(e);
            e.printStackTrace();
        }
    }
}
