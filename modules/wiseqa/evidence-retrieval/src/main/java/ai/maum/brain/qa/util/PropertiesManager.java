package ai.maum.brain.qa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;


public class PropertiesManager {

    private static Properties properties;

    private PropertiesManager() {
    }

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            InputStream is = null;
            try {
                String propertiesPath = System.getProperty("propertiesPath");
                if (propertiesPath != null) {
                    is = new FileInputStream(propertiesPath);
                } else {
                    is = PropertiesManager.class.getClassLoader().getResourceAsStream("evidenceRetrieval.properties");
                }
                properties.load(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return properties;
    }
}
