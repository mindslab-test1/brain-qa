#pragma execution_character_set("utf-8")

#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <thread>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <libmaum/common/encoding.h>
#include "inference-answer-impl.h"

using std::unique_ptr;
using grpc::Status;
using grpc::StatusCode;

InferenceAnswerImpl::InferenceAnswerImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void InferenceAnswerImpl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  unique_ptr<char[]> rsc_path(new char[rsc_path_.size() + 1]);
  strcpy(rsc_path.get(), rsc_path_.c_str());

  // 최적 정답 신뢰도 추론 관련 class 선언
  answer_rank_ = new AnswerRank;
  // 최적 정답 신뢰도 추론 관련 함수 초기화
  answer_rank_->ARank_THREAD_init();

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }
  std::cout << "Completed to Load!!" << endl;
}

void InferenceAnswerImpl::Stop(bool lock) {
  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  delete answer_rank_;
}

InferenceAnswerImpl::~InferenceAnswerImpl() {
  Stop();
}

/**
  @breif 최종 정답 추론을 실제 수행하는 함수
  @param InferenceAnswerInputText *text : 클라이언트에서 보낸 message
  @param InferenceAnswerResultDocument *result_document : 최종 정답 추론 결과를 클라이언트에 보낼 message
*/
void InferenceAnswerImpl::AnalyzeOne(const InferenceAnswerInputText *text,
                                    InferenceAnswerResultDocument *result_document) {
  const string input_text = text->text();
  string result;
  // 멀티쓰레드를 위해 사용 (추후 변경 예정)
#pragma omp parallel for
  // 최종 정답 추론 함수
  result = answer_rank_->answer_merge_4UIMA(input_text);
  result_document->set_result(result);
}

/**
  @breif 클라이언트에서 최종 정답 추론을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param InferenceAnswerInputText *text : 클라이언트에서 보낸 message
  @param InferenceAnswerResultDocument *result_document : 최종 정답 추론 결과를 클라이언트에 보낼 message
*/
grpc::Status InferenceAnswerImpl::InferenceAnswer(ServerContext *context,
                                                const InferenceAnswerInputText *text,
                                                InferenceAnswerResultDocument *result_document) {
  if(!CanService()) {
    return grpc::Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return grpc::Status::OK;
}
