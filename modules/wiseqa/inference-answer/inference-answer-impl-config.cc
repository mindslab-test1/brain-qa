#include "inference-answer-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void InferenceAnswerImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 최적 정답 추론 관련 리소스 경로 설정
  rsc_path_ = c.Get("brain-qa.inference-answer.path");
}
