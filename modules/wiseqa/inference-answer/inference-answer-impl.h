#ifndef PROJECT_INFERENCE_ANSWER_IMPL_H
#define PROJECT_INFERENCE_ANSWER_IMPL_H

#pragma execution_character_set("utf-8")

#include <iostream>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "common_util.h"
#include "QAHeader/AnswerRank.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;

using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::InferenceAnswerInputText;
using maum::brain::qa::InferenceAnswerResultDocument;


class InferenceAnswerImpl : public QuestionAnswerService::Service {
public:
  InferenceAnswerImpl();
  virtual ~InferenceAnswerImpl();
  const char *Name() {
    return "Question Answering Service";
  }

  grpc::Status InferenceAnswer(ServerContext *context, const InferenceAnswerInputText *text, 
                            InferenceAnswerResultDocument *result_document) override;
  void Start(bool lock = true);
private:
  void LoadConfig();
  // 최적 정답 신뢰도 추론을 위한 class 선언
  AnswerRank *answer_rank_;
  void AnalyzeOne(const InferenceAnswerInputText *text, InferenceAnswerResultDocument *result_document);

  std::string rsc_path_;
  void Stop(bool lock = true);

  bool CanService() {
    std::lock_guard <std::mutex> guard(m_);
    return loaded_ && loading_;
  }

  void LockRunning() {
    {   
      std::lock_guard <std::mutex> guard(m_);
      running_++;
    }   
    cv_.notify_one();
  }
  void UnlockRunning() {
    {   
      std::lock_guard <std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
    public:
      Accessor(InferenceAnswerImpl *ptr) : parent_(ptr) {
        parent_->LockRunning();
      }

      ~Accessor() {
        parent_->UnlockRunning();
      }
    private:
      InferenceAnswerImpl *parent_ = nullptr;
  };
  void Restart();
};
#endif // PROJECT_INFERENCE_ANSWER_IMPL_H
