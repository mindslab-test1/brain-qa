#ifndef PROJECT_QUESTION_ANALYSIS_IMPL_H
#define PROJECT_QUESTION_ANALYSIS_IMPL_H

#pragma execution_character_set("utf-8")

#ifndef WIN32

#include <iostream>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;
#endif

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "QAnalHeader/QAnalyzer.h"
#include "QAnalHeader/QAnalRSCMng.h"
#include "QAJsonHeader/QAnalJsonRW.h"
#include "QStruct.h"
#include <string>
#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using google::protobuf::Empty;
using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::QuestionAnalysisInputText;
using maum::brain::qa::QuestionAnalysisResultDocument;
using std::string;

class LMInterface;
class CQAnalRSCMng;
class CQAnalyzer;

class QuestionAnalysisImpl : public QuestionAnswerService::Service {

  public:

    QuestionAnalysisImpl();
    virtual ~QuestionAnalysisImpl();

    const char *Name() {
      return "Question Analysis Service";
    }

    Status GetProvider(ServerContext *context, const Empty *empty, Provider *provider) override;

    Status QuestionAnalyze(ServerContext *context, const QuestionAnalysisInputText *text,
                          QuestionAnalysisResultDocument *result_document) override;
    void Start(bool lock = true);

  private:

    void LoadConfig();

    // 글로벌 쓰레드로 언어분석을 위한 class 선언
    LMInterface *global_lmi_;

    // 서버 실행시 질문분석 리소스 매니저 생성을 위한 class 선언
    CQAnalRSCMng *qRscMngr;
    void AnalyzeOne(const QuestionAnalysisInputText *text, QuestionAnalysisResultDocument *result_document);

    std::string rsc_path_;

    void Stop(bool lock = true);

    bool CanService() {
      std::lock_guard <std::mutex> guard(m_);
      return loaded_ && loading_;
    }
    void LockRunning() {
      {   
        std::lock_guard <std::mutex> guard(m_);
        running_++;
      }   
      cv_.notify_one();
    }
    void UnlockRunning() {
      {   
        std::lock_guard <std::mutex> guard(m_);
        running_--;
      }   
      cv_.notify_one();
    }
    bool loaded_ = false;
    bool loading_ = false;
    int32_t running_ = 0;
    std::mutex m_; 
    std::condition_variable cv_;
    class Accessor {
      public:
        Accessor(QuestionAnalysisImpl *ptr) : parent_(ptr) {
          parent_->LockRunning();
        }   
        ~Accessor() {
          parent_->UnlockRunning();
        }

      private:
        QuestionAnalysisImpl *parent_ = nullptr;
    };
    void Restart();
};

#endif //PROJECT_QUESTION_ANALYSIS_IMPL_H
