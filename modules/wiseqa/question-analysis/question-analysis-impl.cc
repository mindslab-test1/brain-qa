#pragma execution_character_set("utf-8")
#ifndef WIN32

#include <iostream>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
#endif

#include "question-analysis-impl.h"
#include <fstream>
#include <iostream>
#include <libmaum/common/encoding.h>
#include <thread>
#include <stdio.h>
#include <string>

using std::unique_ptr;
using grpc::StatusCode;
using std::string;

QuestionAnalysisImpl::QuestionAnalysisImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void QuestionAnalysisImpl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  
  unique_ptr<char[]> path(new char[rsc_path_.size() + 1]);
  strcpy(path.get(), rsc_path_.c_str());

  // 질문분석을 위한 리소스 매니저 생성
  qRscMngr = new CQAnalRSCMng(ALL_DOMAIN);

  global_lmi_ = new LMInterface;
  // 질문분석을 위한 리소스를 로딩하는 함수
  qRscMngr->Open_Resource(global_lmi_, path.get());
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }
  std::cout << "Completed to Load!!" << endl;
}

void QuestionAnalysisImpl::Stop(bool lock) {
  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }

  // 질문분석을 위한 리소스를 닫는 함수
  qRscMngr->Close_Resource();
  delete global_lmi_;
}

QuestionAnalysisImpl::~QuestionAnalysisImpl() {
  Stop();
}

/**
  @breif 질문분석을 실제로 수행하는 함수
  @param QuestionAnalysisInputText *text : 클라이언트에서 보낸 message
  @param QuestionAnalysisResultDocument *result_document : 질문분석 결과를 클라이언트에 보낼 message
  @return None
*/
void QuestionAnalysisImpl::AnalyzeOne(const QuestionAnalysisInputText *text, 
                                      QuestionAnalysisResultDocument *result_document) {
    const string &otext = text->text();
    string result;

// 멀티쓰레드로 활용하기 위해 사용 (추후 변경 예정)
#pragma omp parallel
    {
#pragma omp for
      LMInterface *thread_lmi;
      thread_lmi = new LMInterface;

      // 멀티쓰레드를 위해 언어분석기 리소스를 로딩하는 함수
      qRscMngr->LMI_INIT_Thread(thread_lmi);

      // 리소스 관리 클래스에서 중요 리소스 정보를 가져와서 메모리 정보를 복사하는 class
      CQAnalyzer qAnalyzer;

      // 질분분석 결과가 저장되는 구조체
      QAnal_Result qAnalResult;

      // 리소스 매니저의 핸들을 등록하는 함수
      qAnalyzer.SetRscMng(qRscMngr, global_lmi_, thread_lmi);

      // 설정 정보에 따라서 시간을 측정하는 함수
      if(qRscMngr->confAnal.debugMsgLevel>0){
        qRscMngr->timeChecker.CheckStartTime(TC_ALL_QUESTION);
      }

      string question = otext;

      // 위키피디아 질문분석 통합 함수
      qAnalResult = qAnalyzer.QAnalysis4WikiQA(question);


      // 분석된 결과 또는 일반 string을 json 변환 관련 class
      CQAnalJsonRW qJsonRW;
      string strJson;

      // 질문분석된 결과를 json 스트링으로 변환하는 함수
      strJson = qJsonRW.GetJsonFromQAnalStruct(qAnalResult);

      std::cout << strJson << endl;
      result_document->set_result(strJson);

      qRscMngr->Free_QAnalResult(qAnalResult);
      qRscMngr->LMI_CLOSE_Thread(thread_lmi);
      delete thread_lmi;
    };
}

/**
  @breif 클라이언트에서 질문분석을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param QuestionAnalysisInputText *text : 클라이언트에서 보낸 message
  @param QuestionAnalysisResultDocument *result_document : 질문분석 결과를 클라이언트에게 보낼 message
  @return grpc Status Code
*/
Status QuestionAnalysisImpl::QuestionAnalyze(ServerContext *context, const QuestionAnalysisInputText *text,
                                            QuestionAnalysisResultDocument *result_document) {
  if( (!CanService()) || text->text().empty() || text->text() == " " ){
    return Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return Status::OK;
}

/**
  @breif 엔진 및 공급자 정보 제공하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param Provider *provider : 엔진 공급 정보를 클라이언트에 보낼 message
  @return grpc Status Code
*/
Status QuestionAnalysisImpl::GetProvider(ServerContext *context, const Empty *empty, Provider *provider) {
  provider->set_name("MindsLAB Question Analysis System");
  provider->set_vendor("ETRI");
  provider->set_version("1.0");
  provider->set_description("MindsLAB Question Analysis System provided by ETRI");
  provider->set_support_encoding("UTF-8");

  return Status::OK;
}
