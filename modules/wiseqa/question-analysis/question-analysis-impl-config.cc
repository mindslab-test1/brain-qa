#include "question-analysis-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void QuestionAnalysisImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 질문분석을 위한 리소스 경로 로딩
  rsc_path_ = c.Get("brain-qa.resource.question-analysis.path");
}
