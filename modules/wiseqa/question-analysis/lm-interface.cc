﻿// LMInterface.cpp: implementation of the LMInterface class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32
#pragma warning( disable : 4996 )
#pragma warning( disable : 4786 )
#pragma warning( disable : 4267 )
#pragma warning( disable : 4244 )
#pragma warning( disable : 4251 )
#endif

#include "LMInterface.h"
#include <omp.h>

string my_replace(string org, string from, string to)
{
		string result = org;
		string::size_type pos = 0;
		string::size_type offset = 0;

		while((pos = result.find(from, offset)) != string::npos) {
				result.replace(result.begin() + pos, result.begin() + pos + from.size(), to);
				offset = pos + to.size();
		}

		return result;

}

void tokenize(const string& str, vector<string>& tokens, const string& delimiters) {
    tokens.clear();
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos  = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos) {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
} 



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LMInterface::LMInterface()
{
	m_SR = NULL;
	m_scan = NULL;
	m_wsd = NULL;
	m_NER = NULL;
	m_DParse = NULL;
 	m_chunk  = NULL;
 	m_srl = NULL;
	m_coref = NULL;
	m_ZA = NULL;
	m_preNER = NULL;
	m_preprocessor = NULL;	
}

LMInterface::~LMInterface()
{
}


//-------------------------------------------------
//
//	문장 분리
//
//-------------------------------------------------

// 문장 분리기 생성 및 초기화
void LMInterface::init_SR(char *dir, QADOMAIN SR_domain)
{
	// 문장 분리기  초기화 
	m_SR = new CSR();
	m_SR->init(dir, SR_domain);
}

void LMInterface::init_SR_global(char *dir, QADOMAIN SR_domain)
{
	// 문장 분리기  초기화 
	m_SR = new CSR();
	m_SR->init_global(dir, SR_domain);
}

void LMInterface::init_SR_thread(char *dir, LMInterface& lmGlobal)
{
	// 문장 분리기  초기화 
	m_SR = new CSR();
	m_SR->init_thread(dir, lmGlobal.m_SR->crf);
}

int LMInterface::do_sentence_boundary_detection(string& target, vector<string>& sentVec)
{
	if (m_SR == NULL) {
		printf("먼저 문장분리기 초기화가 필요합니다.\n");
		exit(-1);
	}

	m_SR->mk_decision_sent_boundary(target, sentVec);

	return sentVec.size();
}

// 문장 분리기 종료 및 finalize
void LMInterface::close_SR()
{
	m_SR->close();
	delete m_SR;
	m_SR = NULL;
}
 
void LMInterface::close_SR_global()
{
	m_SR->close_global();
	delete m_SR;
	m_SR = NULL;
}

void LMInterface::close_SR_thread()
{
	m_SR->close_thread();
	delete m_SR;
	m_SR = NULL;
}



//-------------------------------------------------
//
//	형태소 분석
//
//-------------------------------------------------

///< 형태소 분석기 생성 및 사전 초기화 : rsc 디렉토리 지정
void LMInterface::init_morp(char *dir)
{
	// 형태소 분석기 초기화 
	m_scan = new MLTagger();
	m_scan->Init(dir);
}

void LMInterface::init_morp_global(char *dir)
{
	// 형태소 분석기 초기화 
	m_scan = new MLTagger();
	m_scan->Init(dir);
}

void LMInterface::init_morp_thread(char *dir, LMInterface& lmGlobal)
{
	// 형태소 분석기 초기화 
	m_scan = new MLTagger();
	m_scan->Init_thread(lmGlobal.m_scan, omp_get_thread_num());
}

bool LMInterface::load_morp_custom_dic(string fname,bool flag_append)
{
	if (m_scan == NULL) {
		printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
		exit(-1);
	}

	return m_scan->load_custom_dic(fname, flag_append);
}

void LMInterface::do_analyze_morph(N_Doc& ndoc)
{
	if (m_scan == NULL) {
		printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
		exit(-1);
	}

	m_scan->analyze_morph(ndoc);
}

void LMInterface::do_analyze_morph(N_Doc& ndoc, vector<N_Sentence>& paramSentence)
{
	if (m_scan == NULL) {
		printf("먼저 형태소 분석기 초기화가 필요합니다.\n");
		exit(-1);
	}

	m_scan->analyze_morph(ndoc, paramSentence);
}

//< 형태소 분석기 종료 및 finalize
void LMInterface::close_morp()
{
	m_scan->close();
	delete m_scan;
	m_scan = NULL;
}

void LMInterface::close_morp_global()
{
	m_scan->close();
	delete m_scan;
	m_scan = NULL;
}

void LMInterface::close_morp_thread()
{
	m_scan->close_thread();
	delete m_scan;
	m_scan = NULL;
}
 


//-------------------------------------------------
//
//	WSD
//
//-------------------------------------------------

///< WSD 생성 및 초기화 : rsc 디렉토리 지정
void LMInterface::init_WSD(char *dir)
{
	if (m_wsd != NULL)
	{
		printf("이미 WSD는 초기화 되어 있습니다.\n");
		return;
	}

	// WSD 초기화 
	m_wsd = new WSD();
	m_wsd->Init(dir);
}

void LMInterface::init_WSD_global(char *dir)
{
	if (m_wsd != NULL)
	{
		printf("이미 WSD는 초기화 되어 있습니다.\n");
		return;
	}

	// WSD 초기화 
	m_wsd = new WSD();
	m_wsd->Init(dir);
}

void LMInterface::init_WSD_thread(char *dir, LMInterface& lmGlobal)
{
	if (m_wsd != NULL)
	{
		printf("이미 WSD는 초기화 되어 있습니다.\n");
		return;
	}

	// WSD 초기화 
	m_wsd = new WSD();
	m_wsd->Init_thread(lmGlobal.m_wsd, omp_get_thread_num());
}

void LMInterface::do_WSD(N_Doc& ndoc)
{
	if (m_wsd == NULL) {
		printf("먼저 WSD 초기화가 필요합니다.\n");
		exit(-1);
	}

	m_wsd->do_wsd(ndoc);
}

void LMInterface::do_WSD(N_Doc &ndoc, vector<N_Sentence>& paramSentence)
{
	if (m_wsd == NULL) {
		printf("먼저 WSD 초기화가 필요합니다.\n");
		exit(-1);
	}

	m_wsd->do_wsd(ndoc, paramSentence);
}

//< WSD 종료 및 finalize
void LMInterface::close_WSD()
{
	m_wsd->close();
	delete m_wsd;
	m_wsd = NULL;
}
 
void LMInterface::close_WSD_global()
{
	m_wsd->close();
	delete m_wsd;
	m_wsd = NULL;
}

void LMInterface::close_WSD_thread()
{
	m_wsd->close_thread();
	delete m_wsd;
	m_wsd = NULL;
}




//-------------------------------------------------
//
//	NER
//
//-------------------------------------------------


/** AT인식기 초기화, LMInterface내부의 m_doc 사용
*/
void LMInterface::init_NER(char *dir)
{
	if (m_NER != NULL)
	{
		printf("이미 NER은 초기화 되어 있습니다.\n");
		return;
	}

	m_NER = new NERecognizer();

	m_NER->init_NER(dir);
}

void LMInterface::init_NER_global(char *dir)
{
	if (m_NER != NULL)
	{
		printf("이미 NER은 초기화 되어 있습니다.\n");
		return;
	}

	m_NER = new NERecognizer();

	m_NER->init_NER(dir);
}

void LMInterface::init_NER_thread(char *dir, LMInterface& lmGlobal)
{
	if (m_NER != NULL)
	{
		printf("이미 NER은 초기화 되어 있습니다.\n");
		return;
	}

	m_NER = new NERecognizer();

	m_NER->init_NER_thread(lmGlobal.m_NER, omp_get_thread_num());
}

void LMInterface::do_NER(N_Doc& ndoc)
{
	if (m_NER == NULL) {
		printf("ATR이 초기화 되지 않았습니다.\n");
		return;
	}

	m_NER->do_NER(ndoc);
}

void LMInterface::do_NER(N_Doc& ndoc, vector<N_Sentence>& paramSentence)
{
	if (m_NER == NULL) {
		printf("ATR이 초기화 되지 않았습니다.\n");
		return;
	}

	m_NER->do_NER(ndoc, paramSentence);
}

/** 개체명 인식기 종료
*/
void LMInterface::close_NER()
{
	if (m_NER == NULL) return;

	m_NER->close_NER();
	delete m_NER;

	m_NER = NULL;
}

void LMInterface::close_NER_global()
{
	if (m_NER == NULL) return;

	m_NER->close_NER();
	delete m_NER;

	m_NER = NULL;
}

void LMInterface::close_NER_thread()
{
	if (m_NER == NULL) return;

	m_NER->close_NER_thread();
	delete m_NER;

	m_NER = NULL;
}


//-------------------------------------------------
//
//	청킹
//
//-------------------------------------------------

void LMInterface::init_Chunk(char *rsc_dir)
{
	m_chunk = new CChunkAnalyzer();
	m_chunk->init_Chunk(rsc_dir); 
}

void LMInterface::init_Chunk_global(char *rsc_dir)
{
	m_chunk = new CChunkAnalyzer();
	m_chunk->init_Chunk_global(rsc_dir); 
}

void LMInterface::init_Chunk_thread(char *rsc_dir, LMInterface& lmGlobal)
{
	m_chunk = new CChunkAnalyzer();
	m_chunk->init_Chunk_thread(rsc_dir); 
}

void LMInterface::do_Chunk(N_Doc &ndoc)
{
	if (m_chunk == NULL)
	{
		printf("[do_Chunk()] CHUNK의 초기화가 필요합니다.\n");
		return ;
	}

	m_chunk->do_Chunk(ndoc);				// ndoc sentence
}

void LMInterface::do_Chunk(vector<N_Sentence>& paramSentence)
{
	if (m_chunk == NULL)
	{
		printf("[do_Chunk()] CHUNK의 초기화가 필요합니다.\n");
		return ;
	}

	m_chunk->do_Chunk(paramSentence);				// ndoc sentence
}

void LMInterface::close_Chunk()
{
	if (m_chunk == NULL)
	{
		printf("[close_Chunk()] CHUNK의 초기화가 필요합니다.\n");
		return ;
	}
	
	m_chunk->close_Chunk();
	delete m_chunk;
	m_chunk = NULL;
}

void LMInterface::close_Chunk_global()
{
	if (m_chunk == NULL)
	{
		printf("[close_Chunk_global()] CHUNK의 초기화가 필요합니다.\n");
		return ;
	}
	
	m_chunk->close_Chunk_global();
	delete m_chunk;
	m_chunk = NULL;
}

void LMInterface::close_Chunk_thread()
{
	if (m_chunk == NULL)
	{
		printf("[close_Chunk_thread()] CHUNK의 초기화가 필요합니다.\n");
		return ;
	}
	
	m_chunk->close_Chunk_thread();
	delete m_chunk;
	m_chunk = NULL;
}

//-------------------------------------------------
//
//	DParse
//
//-------------------------------------------------


void LMInterface::init_DParse(char *rsc_dir)
{
	if (m_DParse != NULL)
	{
		printf("DepenParse가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_DParse = new TParser(3000001);
	m_DParse->labeled = 1;
	string dir = rsc_dir;
	m_DParse->init(dir + "/parser");
}

void LMInterface::init_DParse_global(char *rsc_dir)
{
	if (m_DParse != NULL)
	{
		printf("DepenParse가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_DParse = new TParser(3000001);
	m_DParse->labeled = 1;
	string dir = rsc_dir;
	m_DParse->init(dir + "/parser");
}

void LMInterface::init_DParse_thread(char *rsc_dir, LMInterface& lmGlobal)
{
	if (lmGlobal.m_DParse == NULL)
	{
		printf("Global DParse init이 필요합니다.\n");
		return ;
	}
	
	m_DParse = lmGlobal.m_DParse;
}

void LMInterface::do_DParse_for_question(N_Doc &ndoc)
{
	// parsing
	do_DParse( ndoc, false );
	
	// // simple question post-processing
	// {
		// m_DParse->postprocess_simplequestion_last_np_sbj( ndoc );
		// m_DParse->postprocess_simplequestion_jx_jkb( ndoc );
		// m_DParse->postprocess_simplequestion_two_sbj( ndoc );
		// m_DParse->postprocess_simplequestion_time_pattern( ndoc );
		// m_DParse->postprocess_simplequestion_surface_pattern( ndoc );
	// }
	
	// // do_phrase_dependency
	// do_DParse_Phrase_Dependency(ndoc);
	
	// multi-question-sentence test example
	for (size_t i=0; i < ndoc.sentence.size(); i++) 
	{
		N_Doc ndoc2;
		ndoc2.sentence.push_back ( ndoc.sentence[i] );
		m_DParse->postprocess_simplequestion_last_np_sbj( ndoc2 );
		m_DParse->postprocess_simplequestion_jx_jkb( ndoc2 );
		m_DParse->postprocess_simplequestion_two_sbj( ndoc2 );
		m_DParse->postprocess_simplequestion_time_pattern( ndoc2 );
		m_DParse->postprocess_simplequestion_surface_pattern( ndoc2 );
		ndoc.sentence[i] = ndoc2.sentence.front();
	}

	// do_phrase_dependency
	do_DParse_Phrase_Dependency(ndoc);
	
}

void LMInterface::do_DParse(N_Doc &ndoc, bool is_preprocessed)
{
	if ( is_preprocessed == false )
	{
		do_DParse( ndoc.sentence );
		
		// do_phrase_dependency
		do_DParse_Phrase_Dependency(ndoc);
	}
	else
		do_DParse( ndoc.p_sentence );
}

void LMInterface::do_DParse(vector<N_Sentence> &sentence)
{
	if( m_DParse == NULL) {
		printf("파서가 초기화 되지 않았음\n");
		return;
	}

	for (size_t i=0; i < sentence.size(); i++) 
	{
		// 2016-05-26 / joonho.lim / note: split_qoute, merge_qoute 분리 구문 삭제
		
		vector<parse_unit> input = m_DParse->convert_nsent4korean(sentence[i]);
		vector<SymbolPair> inputSymbolPairVec = m_DParse->get_symbol_pair_vec(sentence[i]);
		m_DParse->add_ne_pair_vec( sentence[i], inputSymbolPairVec );
		vector<parse_unit> result = m_DParse->do_parse(input, inputSymbolPairVec);
		
		for (size_t j=0; j < result.size(); j++) {
			N_Dependency dep;
			dep.id = result[j].id - 1;
			assert(dep.id == (int)j);
			dep.text = sentence[i].word[j].text;
			dep.head = result[j].head - 1;
			dep.label = result[j].dep_label;
			dep.weight = result[j].weight;
			sentence[i].dependency.push_back(dep);
		}
		
		// mod
		for (size_t j=0; j < sentence[i].dependency.size(); j++) {
			int head_id = sentence[i].dependency[j].head;
			if (head_id >= 0) sentence[i].dependency[head_id].mod.push_back(j);
		}
		
		// 오류 후처리
		{
			m_DParse->postprocess_eseo_rago_vp( sentence[i] );
			m_DParse->postprocess_sbj_sbj_obj_vp_vp( sentence[i] );
			m_DParse->postprocess_sbj_mod_vnp_rule( sentence[i] );
			m_DParse->postprocess_ro_predicate( sentence[i] );
			m_DParse->postprocess_np_cnj_ap_rule( sentence[i] );
			m_DParse->postprocess_cnj_ne_pair( sentence[i], inputSymbolPairVec );
			m_DParse->postprocess_dt_rules( sentence[i] );
			m_DParse->postprocess_ps_af_last( sentence[i] );
			// m_DParse->postprocess_semantic_frame_rule( sentence[i] );
		}
	}
}

void LMInterface::do_DParse_Phrase_Dependency(N_Doc& ndoc)
{
	// cerr << endl << sprint_JSON(ndoc) << endl;
	
	vector<N_Sentence>& sentence = ndoc.sentence;
	for (size_t i=0; i < sentence.size(); i++) 
	{
		// cerr << sentence[i].text << endl;
		
		vector<SymbolPair> inputSymbolPairVec = m_DParse->get_symbol_pair_vec(sentence[i]);
		m_DParse->add_ne_pair_vec( sentence[i], inputSymbolPairVec );
		
		m_DParse->do_phrase_dependency( sentence[i], inputSymbolPairVec );
	}
}


void LMInterface::print_DParse(N_Doc &ndoc)
{
	for (size_t i=0; i < ndoc.sentence.size(); i++) {
		for (size_t j=0; j < ndoc.sentence[i].dependency.size(); j++) {
			int head = ndoc.sentence[i].dependency[j].head;
			string label = ndoc.sentence[i].dependency[j].label;
			string word = ndoc.sentence[i].word[j].text;
			string head_word = "ROOT";
			if (head >= 0) head_word = ndoc.sentence[i].word[head].text;
			cout << j << "\t" << word << "\t" << head << "\t" << head_word << "\t" << label << endl;
		}
	}
}

void LMInterface::print_DParse(FILE *out, N_Doc &ndoc)
{
	for (size_t i=0; i < ndoc.sentence.size(); i++) {
		fprintf(out, "# %s\n", ndoc.sentence[i].text.c_str());
		for (size_t j=0; j < ndoc.sentence[i].dependency.size(); j++) {
			int head = ndoc.sentence[i].dependency[j].head;
			string label = ndoc.sentence[i].dependency[j].label;
			string word = ndoc.sentence[i].word[j].text;
			string head_word = "ROOT";
			if (head >= 0) head_word = ndoc.sentence[i].word[head].text;
			fprintf(out, "%d\t%s\t%d\t%s\t%s\n", (int)j, word.c_str(), head, head_word.c_str(), label.c_str());
		}
	}
}


void LMInterface::close_DParse()
{
	if (m_DParse == NULL)
	{
		printf("DParse의 초기화가 필요합니다.\n");
		return ;
	}
	
	delete m_DParse;
	m_DParse = NULL;
}

void LMInterface::close_DParse_global()
{
	if (m_DParse == NULL)
	{
		printf("DParse의 초기화가 필요합니다.\n");
		return ;
	}
	
	delete m_DParse;
	m_DParse = NULL;
}

void LMInterface::close_DParse_thread()
{
	if (m_DParse == NULL)
	{
		printf("DParse의 초기화가 필요합니다.\n");
		return ;
	}
	
	m_DParse = NULL;
}



//-------------------------------------------------
//
//	Semantic Role Labeling
//
//-------------------------------------------------

void LMInterface::init_srl(char *rsc_dir)
{
	if (m_srl != NULL) {
		printf("SRL이 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_srl = new SRL();
	
	string dir = string(rsc_dir) + "/srl";
	m_srl->init( dir );
	
	// m_srl->load_dic(dir + "/srl/cluster.txt");
	// m_srl->load_model(dir + "/srl/srl_pred.bin", dir + "/srl/srl_arg.bin", 1);
}

void LMInterface::init_srl_global(char *rsc_dir)
{
	if (m_srl != NULL) {
		printf("SRL이 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_srl = new SRL();
	string dir = string(rsc_dir) + "/srl";
	m_srl->init_global( dir );
}

void LMInterface::init_srl_thread(char *rsc_dir, LMInterface& lmGlobal)
{
	if (m_srl != NULL) {
		printf("SRL이 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_srl = new SRL();
	string dir = string(rsc_dir) + "/srl";
	m_srl->init_thread( dir, &(lmGlobal.m_srl->pred_crf), &(lmGlobal.m_srl->arg_crf), 
			lmGlobal.m_srl->cluster,
			lmGlobal.m_srl->homo_cluster,
			lmGlobal.m_srl->notArg0,
			lmGlobal.m_srl->sense_frame_map,
			lmGlobal.m_srl->lemma_frame_map,
			lmGlobal.m_srl->pp_list
			);
}

void LMInterface::do_srl(N_Doc &ndoc, bool is_preprocessed )
{
	if ( is_preprocessed == false )
		do_srl( ndoc.sentence );
	else
		do_srl( ndoc.p_sentence );
}

void LMInterface::do_InfoExfromsrl(N_Doc &ndoc, vector<N_SRL_InfoEx>& ppEx, bool is_preprocessed )
{
	// if( m_srl == NULL) 
	// {
		// cout<<"SRL이 초기화되지 않았습니다."<<endl;
		// return;
	// }

	if ( is_preprocessed == false )
		m_srl->do_InfoExfromsrl( ndoc.sentence, ppEx );
	else
		m_srl->do_InfoExfromsrl( ndoc.p_sentence, ppEx );
}

void LMInterface::do_srl_za(N_Doc &ndoc)
{
	if( m_srl == NULL ) {
		printf("SRL이 초기화 되지 않았습니다.\n");
		return;
	}
	else if( m_ZA == NULL ) {
		printf("ZA를 실행하지 않았음\n");
		return;
	}
	m_srl->do_srl_za( ndoc);
}

void LMInterface::do_srl(vector<N_Sentence> &sentence)
{
	// srl needs parser
	if( m_srl == NULL ) {
		printf("SRL이 초기화 되지 않았습니다.\n");
		return;
	}
	
	if ( sentence.empty() == false && sentence[0].word.empty() == false && sentence[0].dependency.empty() == true )
	{
		printf("Parsing이 수행되지 않았습니다.\n");
		return ;
	}

	double prob;
	for (size_t i=0; i < sentence.size(); i++) {
		// 너무 많은 어절 skip
		if (sentence[i].word.size() > 100) continue;
		// POS, parsing info --> word_t
		//cout<<"::"<<sentence[i].text<<endl;
		vector<word_t> input = m_srl->convert_nsent4korean(sentence[i]);
		vector<word_t> result = m_srl->do_srl(input, prob);
		for (size_t j=0; j < result.size(); j++) {
			if (result[j].pred == "_") continue;
			N_SRL srl;
			vector<string> token;
			tokenize(result[j].pred, token, ".");
			if (token.size() != 2) {
				// cerr << "SRL:pred_error: " << result[j].pred << endl;
				continue;
			}
			// pred
			srl.verb = token[0];
			srl.sense = atoi(token[1].c_str());
			srl.word_id = j;

			//by mefong 2014.09.26 for weight
			//srl.weight = prob;
			// end 2014.09.26

			// arg
			double pas_weight =0.0;
			for (size_t k=0; k < result[j].arg_info.size(); k++) {
				N_SRL_Arg arg;
				// i = id - 1
				arg.word_id = result[j].arg_info[k].first - 1;
				arg.type = result[j].arg_info[k].second;
				///////////////////////////////////////////////////////////
				// 2017-06-26 phrase_dependency 적용
				// arg.text = sentence[i].word[arg.word_id].text;
				arg.text = m_srl->findPhrase(sentence[i], sentence[i].phrase_dependency, arg.word_id);
				if( arg.text.size() == 0) arg.text = m_srl->getText(sentence[i], arg.word_id, arg.word_id);


				//by mefong 2014.9.26 for weight
				arg.weight = result[j].arg_weight[k];
				pas_weight += arg.weight;
				// end 2014.09.26

				srl.argument.push_back(arg);
				// test
				//cerr << "pred=" << result[j].pred << " " << "arg=" << arg.text << ":" << arg.type << endl;
			}
			//by mefong 2014.9.26 for weight
			if ( result[j].arg_info.size() != 0 )
				srl.weight = pas_weight/ result[j].arg_info.size();
			else
				srl.weight = 0.0;

			// end 2014.09.26

			//cout<<"start do srl postprocess "<<endl;
			m_srl->do_srl_postprocess(sentence[i], srl);
			// by mefong 2015.12.07
			//cout<<"start do srl frame rsc"<<endl;
			m_srl->do_srl_frame_rsc(sentence[i], srl);
			// by mefong 2016.04.25, 2016.05.23
			/*
			for(size_t kk=0; kk<srl.argument.size(); kk++)
			{
				cout<<"id:"<<srl.argument[kk].word_id<<" label="<<srl.argument[kk].type<<endl;
			}
			*/
			if( srl.argument.size() == 0)
			{
				//cout<<"pre:"<<srl.argument.size()<<endl;
				m_srl->do_srl_conti_verb(sentence[i], srl);
				//cout<<"after:"<<srl.argument.size()<<endl;
				//getchar();
			}
			//cout<<"start NE frame rsc"<<endl;
			m_srl->do_srl_frame_rsc_Ne(sentence[i], srl, input);
			//cout<<"end NE frame rsc"<<endl;

			/*
			for(size_t kk=0; kk<srl.argument.size(); kk++)
			{
				cout<<"after id:"<<srl.argument[kk].word_id<<" label="<<srl.argument[kk].type<<endl;
			}
			*/
			//getchar();
			// by mefong 2016.05.16
			sentence[i].SRL.push_back(srl);
		}
	}
}

// SRL 학습데이터 출력을 위해
void LMInterface::do_srl(N_Doc &ndoc, vector<vector<word_t> >& result_doc)
{
	// srl needs parser
	if( m_srl == NULL ) {
		printf("SRL이 초기화 되지 않았습니다.\n");
		return;
	}

	result_doc.clear();
	double prob;
	for (size_t i=0; i < ndoc.sentence.size(); i++) {
		// 너무 많은 어절 skip
		if (ndoc.sentence[i].word.size() > 100) continue;
		// POS, parsing info --> word_t
		vector<word_t> input = m_srl->convert_nsent4korean(ndoc.sentence[i]);
		vector<word_t> result = m_srl->do_srl(input, prob);
		result_doc.push_back(result);
		for (size_t j=0; j < result.size(); j++) {
			if (result[j].pred == "_") continue;
			N_SRL srl;
			vector<string> token;
			tokenize(result[j].pred, token, ".");
			if (token.size() != 2) {
				// cerr << "SRL:pred_error: " << result[j].pred << endl;
				continue;
			}
			// pred
			srl.verb = token[0];
			srl.sense = atoi(token[1].c_str());
			srl.word_id = j;
			srl.weight = prob;
			// arg
			for (size_t k=0; k < result[j].arg_info.size(); k++) {
				N_SRL_Arg arg;
				// i = id - 1
				arg.word_id = result[j].arg_info[k].first - 1;
				arg.type = result[j].arg_info[k].second;
				arg.text = ndoc.sentence[i].word[arg.word_id].text;
				srl.argument.push_back(arg);
				// test
				//cerr << "pred=" << result[j].pred << " " << "arg=" << arg.text << ":" << arg.type << endl;
			}
			ndoc.sentence[i].SRL.push_back(srl);
		}
	}
}

// 학습데이터 포맷
string LMInterface::sprint_srl(N_Doc &ndoc, vector<vector<word_t> >& result_doc)
{
    string result;
    char temp[1000];
	for (int sent_i=0; sent_i < (int)result_doc.size(); sent_i++) {
		vector<word_t>& input = result_doc[sent_i];
		vector<N_Word>& word = ndoc.sentence[sent_i].word;
		vector<N_Morp>& morp = ndoc.sentence[sent_i].morp;
		// text
		if (sent_i > 0) result += "\n";
		result += "; " + ndoc.sentence[sent_i].text + "\n";
		// srl 정보
		for (int i=0; i < (int)input.size(); i++) {
			if (input[i].pred != "_") {
				sprintf(temp, ";; %d:%s", input[i].id, input[i].pred.c_str());
				result += temp;
				// arg_info
				for (int j=0; j < (int)input[i].arg_info.size(); j++) {
					int id = input[i].arg_info[j].first;
					string arg = input[i].arg_info[j].second;
					sprintf(temp, "\t%d:%s:%s", id, word[id-1].text.c_str(), arg.c_str());
					result += temp;
				}
				result += "\n";
			}
		}
		// conll 포맷
		for (int i=0; i < (int)input.size(); i++) {
			string pos_tag;
			for (int j=word[i].begin; j <= word[i].end; j++) {
				if (pos_tag != "") pos_tag += "+";
				pos_tag += morp[j].lemma + "/" + morp[j].type;
			}
			sprintf(temp, "%d\t%s\t%s\t%d\t%s\t%s", input[i].id, word[i].text.c_str(), pos_tag.c_str(), input[i].head, input[i].dep_label.c_str(), input[i].pred.c_str());
			result += temp;
			// arg
			for (int j=0; j < (int)input[i].arg.size(); j++) {
				result += "\t" + input[i].arg[j];
			}
			result += "\n";
		}
	}
    return result;
}

void LMInterface::close_srl()
{
	m_srl->close();
	delete m_srl;
	m_srl = NULL;
}

void LMInterface::close_srl_global()
{
	m_srl->close();
	delete m_srl;
	m_srl = NULL;
}

void LMInterface::close_srl_thread()
{
	m_srl->close_thread();  // by mefong 20160127
	delete m_srl;
	m_srl = NULL;
}


// 상호참조 해결
void LMInterface::init_coref(char *rsc_dir)	{ }
void LMInterface::init_coref_global(char *rsc_dir)	{ }
void LMInterface::init_coref_thread(char *rsc_dir, LMInterface& lmGlobal)	{ }
void LMInterface::do_coref(N_Doc &ndoc)	{ }
void LMInterface::do_coref_for_question(N_Doc &ndoc)	{ }				// 질문분석용 상호참조해결 API (2015.05.28)
void LMInterface::close_coref()	{ }
void LMInterface::close_coref_global()	{ }
void LMInterface::close_coref_thread()	{ }


// //-------------------------------------------------
// //
// //	상호참조 해결
// //
// //-------------------------------------------------

// void LMInterface::init_coref(char *rsc_dir)
// {
	// if (m_coref != NULL) {
		// printf("Coref가 이미 초기화되었습니다.\n");
		// return ;
	// }
	
	// m_coref = new CCoreference();
	// string dir = rsc_dir;
	// m_coref->init_coref( dir );
// }

// void LMInterface::init_coref_global(char *rsc_dir)
// {
	// if (m_coref != NULL) {
		// printf("Coref가 이미 초기화되었습니다.\n");
		// return ;
	// }
	
	// m_coref = new CCoreference();
	// string dir = rsc_dir;
	// m_coref->init_coref( dir );
// }

// void LMInterface::init_coref_thread(char *rsc_dir, LMInterface& lmGlobal)
// {
	// if (m_coref != NULL) {
		// printf("Coref가 이미 초기화되었습니다.\n");
		// return ;
	// }
	
	// m_coref = lmGlobal.m_coref;
// }

// void LMInterface::do_coref(N_Doc &ndoc)
// {
	// // coref 
	// if( m_coref == NULL ) {
		// printf("Coref가 초기화 되지 않았습니다.\n");
		// return;
	// }
	
	// m_coref->do_coref( ndoc );

	// // print entity
	// // for (int i=0; i < ndoc.entity.size(); i++) {
	// // 	m_coref->m_Rule_coref.pp.printEntity(ndoc.entity[i]);
	// // }
// }

// // 질문분석용 상호참조해결 API (2015.05.28)
// void LMInterface::do_coref_for_question(N_Doc &ndoc)
// {
	// if( m_coref == NULL ) {
		// printf("Coref가 초기화 되지 않았습니다.\n");
		// return;
	// }

	// // question 용 api 수행.
	// m_coref->do_coref_for_question( ndoc );
// }


// void LMInterface::close_coref()
// {
	// m_coref->close_coref();
	// delete m_coref;
	// m_coref = NULL;
// }

// void LMInterface::close_coref_global()
// {
	// m_coref->close_coref();
	// delete m_coref;
	// m_coref = NULL;
// }

// void LMInterface::close_coref_thread()
// {
	// m_coref = NULL;
// }


//-------------------------------------------------
//
//	Zero-anaphora 해결
//
//-------------------------------------------------
void LMInterface::init_ZA(char *path)
{
	if (m_ZA != NULL) {
		printf("zero-anaphora가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_ZA = new CZAAnalyzer() ;
	m_ZA->init( path, true ) ;	
}

void LMInterface::init_ZA_global(char *path)
{
	if (m_ZA != NULL) {
		printf("zero-anaphora가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_ZA = new CZAAnalyzer() ;
	m_ZA->init_global( path, true ) ;	
}

void LMInterface::init_ZA_thread(char *path, LMInterface& lmGlobal)
{
	if (m_ZA != NULL) {
		printf("zero-anaphora가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_ZA = new CZAAnalyzer() ;
	m_ZA->init_thread( lmGlobal.m_ZA ) ;	
}

void LMInterface::do_ZA(N_Doc &ndoc, bool is_title_sentence)
{
	// is_title_sentence == ture --> 첫 sentence를 문서의 title로 가정
	if ( is_title_sentence == true && ndoc.sentence.empty() == false )
	{
		string titleText = ndoc.sentence[0].text;
		ndoc.title.text = titleText.substr( 0, titleText.find_last_of('\n') );
		
		if ( ndoc.sentence[0].NE.empty() == false )
			ndoc.title.NE = ndoc.sentence[0].NE[0].type;
		
		// title sentence  제거.. 
		// ndoc.sentence.erase( ndoc.sentence.begin() );
	}
	
	m_ZA->do_ZA(ndoc, is_title_sentence); 
}

void LMInterface::do_TiZA(N_Doc &ndoc)
{
	//if( m_ZA != NULL) m_ZA->do_TiZA(ndoc);
	if( m_ZA != NULL) m_ZA->do_ZA(ndoc, true);
	else cout<<"not initialization TiZA"<<endl;
}

void LMInterface::do_onlyTiZA(N_Doc &ndoc, bool is_title)
{
	// is_title_sentence == ture --> 첫 sentence를 문서의 title로 가정
	if ( is_title == true && ndoc.sentence.empty() == false )
	{
		string titleText = ndoc.sentence[0].text;
		ndoc.title.text = titleText.substr( 0, titleText.find_last_of('\n') );

		if ( ndoc.sentence[0].NE.empty() == false )
			ndoc.title.NE = ndoc.sentence[0].NE[0].type;
	}

	if( m_ZA != NULL) m_ZA->do_onlyTiZA(ndoc, is_title);
	else cout<<"not initialization onlyTiZA"<<endl;
}

void LMInterface::do_geZA(N_Doc &ndoc, bool is_title_sentence)
{
	m_ZA->do_geZA(ndoc, is_title_sentence); 
}

void LMInterface::close_ZA()
{
	if (m_ZA == NULL) {
		printf("zero-anaphora가 초기화 되지 않았습니다.\n");
		return ;
	}
	
	m_ZA->close();
	delete m_ZA ;
	m_ZA = NULL;
}

void LMInterface::close_ZA_global()
{
	if (m_ZA == NULL) {
		printf("zero-anaphora가 초기화 되지 않았습니다.\n");
		return ;
	}
	
	m_ZA->close_global();
	delete m_ZA ;
	m_ZA = NULL;
}

void LMInterface::close_ZA_thread()
{
	if (m_ZA == NULL) {
		printf("zero-anaphora가 초기화 되지 않았습니다.\n");
		return ;
	}
	
	m_ZA->close_thread();
	delete m_ZA ;
	m_ZA = NULL;
}



//-------------------------------------------------
//
//	선행개체명인식기
//
//-------------------------------------------------

void LMInterface::init_preNER(char *rsc_dir)
{
	if (m_preNER != NULL) {
		printf("preNER이 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_preNER = new PreNER();
	m_preNER->init_PreNER( rsc_dir );
}

void LMInterface::do_preNER(N_Doc &ndoc)
{
	if (m_preNER == NULL)
	{
		printf("preNER의 초기화가 필요합니다.\n");
		return ;
	}

	m_preNER->do_PreNER(ndoc);
}

void LMInterface::do_preNER(N_Doc &ndoc, vector<N_Sentence>& paramSentence)
{
	if (m_preNER == NULL)
	{
		printf("preNER의 초기화가 필요합니다.\n");
		return ;
	}

	m_preNER->do_PreNER(ndoc, paramSentence);
}

void LMInterface::close_preNER()
{
	if (m_preNER == NULL)
	{
		printf("preNER의 초기화가 필요합니다.\n");
		return ;
	}
	
	// m_preNER->close_preNER();
	
	delete m_preNER;
	m_preNER = NULL;
}


//-------------------------------------------------
//
//	문장부호 인식용 전처리기
//
//-------------------------------------------------

void LMInterface::init_preprocessor(char *rsc_dir)
{
	if (m_preprocessor != NULL) {
		printf("preprocessor가 이미 초기화되었습니다.\n");
		return ;
	}
	
	m_preprocessor = new CPreprocessor();

	// string rsc = rsc_dir;
	// rsc += "/preprocessor/patternDatabase.txt";
	// m_preprocessor->Initialize( rsc );
}

void LMInterface::do_preprocessor(N_Doc &ndoc, IntIntPairIntMap &preprocessWordMap )
{
	if (m_preprocessor == NULL)
	{
		printf("preprocessor의 초기화가 필요합니다.\n");
		return ;
	}

	m_preprocessor->DoPreprocess(ndoc, preprocessWordMap);
}

void LMInterface::do_preprocessor_restore(N_Doc &ndoc, IntIntPairIntMap &preprocessWordMap)
{
	if (m_preprocessor == NULL)
	{
		printf("preprocessor의 초기화가 필요합니다.\n");
		return ;
	}

	m_preprocessor->DoRestore(ndoc, preprocessWordMap);
}

void LMInterface::close_preprocessor()
{
	if (m_preprocessor == NULL)
	{
		printf("preprocessor의 초기화가 필요합니다.\n");
		return ;
	}
	
	delete m_preprocessor;
	m_preprocessor = NULL;
}


//-------------------------------------------------
//
//	삭제된 문장 언어분석 함수
//
//-------------------------------------------------

void LMInterface::do_analyze_o_sentence( N_Doc& ndoc )
{
	vector<N_Sentence>& o_sentence = ndoc.o_sentence;
	if ( o_sentence.empty() )
		return ;
	
	do_preNER( ndoc, o_sentence );
	do_analyze_morph( ndoc, o_sentence );
	do_WSD( ndoc, o_sentence );
	do_NER( ndoc, o_sentence );
	//do_Chunk( o_sentence );
	do_DParse( o_sentence );
	do_srl( o_sentence );
	
	return ;
}



//-------------------------------------------------
//
//	Utility function
//
//-------------------------------------------------


int LMInterface::get_wsdId_morpIn ( int morpId, N_Sentence& sent )
{
	for ( int i = 0; i < sent.wsd.size(); i++ )
		if ( sent.wsd[i].begin <= morpId && morpId <= sent.wsd[i].end )
			return i;
	return -1;
}

int LMInterface::get_neId_morpIn ( int morpId, N_Sentence& sent )
{
	for ( int i = 0; i < sent.NE.size(); i++ )
		if ( sent.NE[i].begin <= morpId && morpId <= sent.NE[i].end )
			return i;
	return -1;
}

int LMInterface::get_wordId_morpIn ( int morpId, N_Sentence& sent )
{
	for ( int i = 0; i < sent.word.size(); i++ )
		if ( sent.word[i].begin <= morpId && morpId <= sent.word[i].end )
			return i;
	return -1;
}

int LMInterface::get_wordId_wsdIn ( int wsdId, N_Sentence& sent )
{
	if ( wsdId < 0 || wsdId >= sent.wsd.size() )
		return -1;
		
	for ( int i = 0; i < sent.word.size(); i++ )
		if ( sent.word[i].begin <= sent.wsd[wsdId].begin && sent.wsd[wsdId].end <= sent.word[i].end )
			return i;
	return -1;
}

pair<int,int> LMInterface::get_neId_wsdIn ( int wsdId, N_Sentence& sent )
{
	int begin = -1, end = -1;
	if ( wsdId < 0 || wsdId >= sent.wsd.size() )
		return make_pair(begin,end);
		
	for ( int i = 0; i < sent.NE.size(); i++ )
	{
		if ( sent.NE[i].begin <= sent.wsd[wsdId].begin && sent.wsd[wsdId].begin <= sent.NE[i].end )
			begin = i;
		if ( sent.NE[i].begin <= sent.wsd[wsdId].end && sent.wsd[wsdId].end <= sent.NE[i].end )
			end = i;
		if ( sent.wsd[wsdId].begin <= sent.NE[i].begin && sent.NE[i].end <= sent.wsd[wsdId].end )
		{
			if ( begin == -1 || i < begin )		begin = i;
			if ( end == -1 || i > end )			end = i;
		}
			
		if ( begin != -1 && end != -1 )
			break;
	}
	if ( begin > end )
		begin = end = -1;	// ndoc order error

	return make_pair(begin,end);
}

pair<int,int> LMInterface::get_wsdId_neIn ( int neId, N_Sentence& sent )
{
	int begin = -1, end = -1;
	if ( neId < 0 || neId >= sent.NE.size() )
		return make_pair(begin,end);
		
	for ( int i = 0; i < sent.wsd.size(); i++ )
	{
		if ( sent.wsd[i].begin <= sent.NE[neId].begin && sent.NE[neId].begin <= sent.wsd[i].end )
			begin = i;
		if ( sent.wsd[i].begin <= sent.NE[neId].end && sent.NE[neId].end <= sent.wsd[i].end )
			end = i;
			
		if ( begin != -1 && end != -1 )
			break;
	}
	if ( begin > end )
		begin = end = -1;	// ndoc order error
	
	return make_pair(begin,end);
}

pair<int,int> LMInterface::get_wordId_neIn ( int neId, N_Sentence& sent )
{
	int begin = -1, end = -1;
	if ( neId < 0 || neId >= sent.NE.size() )
		return make_pair(begin,end);
		
	for ( int i = 0; i < sent.word.size(); i++ )
	{
		if ( sent.word[i].begin <= sent.NE[neId].begin && sent.NE[neId].begin <= sent.word[i].end )
			begin = i;
		if ( sent.word[i].begin <= sent.NE[neId].end && sent.NE[neId].end <= sent.word[i].end )
			end = i;
			
		if ( begin != -1 && end != -1 )
			break;
	}
	if ( begin > end )
		begin = end = -1;	// ndoc order error

	return make_pair(begin,end);
}

pair<int,int> LMInterface::get_wsdId_wordIn ( int wordId, N_Sentence& sent )
{
	int begin = -1, end = -1;
	if ( wordId < 0 || wordId >= sent.word.size() )
		return make_pair(begin,end);
		
	for ( int i = 0; i < sent.wsd.size(); i++ )
	{
		if ( sent.wsd[i].begin <= sent.word[wordId].begin && sent.word[wordId].begin <= sent.wsd[i].end )
			begin = i;
		if ( sent.wsd[i].begin <= sent.word[wordId].end && sent.word[wordId].end <= sent.wsd[i].end )
			end = i;
			
		if ( begin != -1 && end != -1 )
			break;
	}
	if ( begin > end )
		begin = end = -1;	// ndoc order error

	return make_pair(begin,end);
}

pair<int,int> LMInterface::get_neId_wordIn ( int wordId, N_Sentence& sent )
{
	int begin = -1, end = -1;
	if ( wordId < 0 || wordId >= sent.word.size() )
		return make_pair(begin,end);
		
	for ( int i = 0; i < sent.NE.size(); i++ )
	{
		if ( sent.NE[i].begin <= sent.word[wordId].begin && sent.word[wordId].begin <= sent.NE[i].end )
			begin = i;
		if ( sent.NE[i].begin <= sent.word[wordId].end && sent.word[wordId].end <= sent.NE[i].end )
			end = i;
		if ( sent.word[wordId].begin <= sent.NE[i].begin && sent.NE[i].end <= sent.word[wordId].end )
		{
			if ( begin == -1 || i < begin )		begin = i;
			if ( end == -1 || i > end )			end = i;
		}
			
		if ( begin != -1 && end != -1 )
			break;
	}
	if ( begin > end )
		begin = end = -1;	// ndoc order error

	return make_pair(begin,end);
}


// JSON 포맷 출력 - sentence
string LMInterface::sprint_JSON(N_Doc &ndoc, bool is_print_p_sentence)
{
	return m_GetNDoc.sprint_JSON( ndoc, is_print_p_sentence);
}

///< JSON 포맷: 태그를 한글로 출력
string LMInterface::sprint_JSON_Korean(N_Doc &ndoc)
{
	return m_GetNDoc.sprint_JSON_Korean( ndoc );
}

///< JSON 포맷: 태그를 한글로 출력
string LMInterface::sprint_JSON_Sejong(N_Doc &ndoc)
{
	return m_GetNDoc.sprint_JSON_Sejong( ndoc );
}


int LMInterface::getWordID(int morpid, vector<N_Word>& vecWord)
{
	for(size_t i=0; i< vecWord.size(); i++)
	{
		if(morpid >= vecWord[i].begin && morpid <= vecWord[i].end)
		{
			return vecWord[i].id;
		}
	}

	return -1;
}


// CONLL 포맷 출력
string LMInterface::sprint_Conll(N_Doc &ndoc)
{
	stringstream ss;
	int curNeIndex = 0;
	
	// title
	// ss << "#index" << "\t" << "word" << "\t" << "morp" << "\t" << "NE_lex" << "\t" << "NE_tag" << "\t" << "dp_head" << "\t" << "dp_tag" << endl;
	ss << "#index" << "\t" << "Word" << "\t" << "Morp" << "\t" << "WSD" << "\t" << "NE" << "\t" << "Category" << "\t" << "Dependency Governor" << "\t" << "Dependency Relation" << endl;
	
	// per sentence
	for ( vector<N_Sentence>::iterator it_sent = ndoc.sentence.begin(); it_sent != ndoc.sentence.end(); ++it_sent )
	{
		curNeIndex = 0;
		
		for ( vector<N_Word>::iterator it_word = it_sent->word.begin(); it_word != it_sent->word.end(); ++it_word )
		{
			int beginMorp = it_word->begin;
			int endMorp = it_word->end;
			
			ss << it_word->id << "\t";
			ss << it_word->text << "\t";
			
			ss << left;
			for ( int i = beginMorp; i <= endMorp; ++i )
			{
				if( i >= it_sent->morp.size() )	cerr << "sprint_Conll : morp index error [ " << it_sent->text << "]" << endl;
					
				if ( i != it_word->begin )	ss << "+";
				ss << it_sent->morp[ i ].lemma << "/" << it_sent->morp[ i ].type;
			}
			ss << "\t";
			
			if ( it_sent->wsd.empty() == false )
			{
				ss << left;
				int wsdBegin = -1;
				int wsdEnd = -1;
				for ( int i = 0; i < it_sent->wsd.size(); i++ )
					if ( beginMorp == it_sent->wsd[i].begin )
					{
						wsdBegin = i;
						break;
					}
				for ( int i = wsdBegin; i < it_sent->wsd.size(); i++ )
					if ( endMorp == it_sent->wsd[i].end )
					{
						wsdEnd = i;
						break;
					}
				
				if ( wsdBegin == -1 || wsdBegin > it_sent->wsd.size() || wsdEnd == -1 || wsdEnd > it_sent->wsd.size() )
					cerr << "sprint_Conll : wsd index error [ " << it_sent->text << "]" << endl;
				else
				{
					for ( int i = wsdBegin; i <= wsdEnd; ++i )
					{
						if( i >= it_sent->wsd.size() )	cerr << "sprint_Conll : wsd index error [ " << it_sent->text << "]" << endl;
							
						if ( i != wsdBegin )	ss << "+";
						ss << it_sent->wsd[ i ].text << "_" << it_sent->wsd[ i ].scode;
					}
				}
				ss << "\t";
			}
			else
				ss << "\t";
			
			string neLex, neTag;
			sprint_Conll_NE( it_sent->NE, curNeIndex, beginMorp, endMorp, neLex, neTag );
			if ( neLex.empty() )	neLex = " ";
			if ( neTag.empty() )	neTag = " ";
			ss << neLex << "\t" << neTag << "\t";
			
			int depSize = it_sent->dependency.size();
			if( it_word->id > it_sent->dependency[ depSize-1 ].id )	cerr << "sprint_Conll : dependency index error [ " << it_sent->text << "]" << endl;
			int depId = -1;
			for ( int i = 0; i < it_sent->dependency.size(); i++ )
			{
				if ( it_word->id == it_sent->dependency[i].id )
				{
					depId = i;
					break;
				}
			}
			if ( depId == -1 )
				ss << "\t\t";
			else
			{
				ss << it_sent->dependency[ depId ].head << "\t";
				ss << it_sent->dependency[ depId ].label << "\t";
			}
			
			ss << endl;
		}
	
		ss << endl;
	}
	
	return ss.str();
}


void LMInterface::sprint_Conll_NE(vector<N_NE>& neVec, int& curNeIndex, int& beginMorp, int&endMorp, string& neLex, string& neTag)
{
	int i = 0;
	for ( i = curNeIndex; i < neVec.size(); i++ )
	{
		N_NE ne = neVec[i];
		
		int beginNE = ne.begin;
		int endNE = ne.end;
		
		if ( beginMorp <= endNE && beginNE <= endMorp )
		{
			if ( i != curNeIndex )	{ neLex += "+";	neTag += "+"; }

			if ( beginMorp <= beginNE )	{ neLex += ne.text;	neTag += ne.type; }
			else				{ neLex += "(cont.)";	neTag += "(cont.)"; }
			
			if ( endMorp < endNE )
				break;
		}
		else
			break;
	}
	curNeIndex = i;
	
	return ;
}
