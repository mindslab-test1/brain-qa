package ai.maum.brain.qa.semantic;

import ai.maum.brain.qa.util.PropertiesManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nanum.exobrain.search.etri.PrimarySearchBM25;
import io.grpc.stub.StreamObserver;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import maum.brain.qa.SemanticPassageSearchServiceGrpc.SemanticPassageSearchServiceImplBase;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchInput;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchOutput;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SemanticPassageSearchService extends SemanticPassageSearchServiceImplBase {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(SemanticPassageSearchService.class);

  private static String CONFIG_PATH = "";

  private ExecutorService executorService = Executors.newCachedThreadPool();


  private HttpSolrServer wikiPassageServer;
  private HttpSolrServer wikiDefinitionServer;
  private HttpSolrServer allInfoboxServer;
  private HttpSolrServer guinessPassageServer;
  private HttpSolrServer newsPassageServer;

  public SemanticPassageSearchService(HttpSolrServer wikiPassageServer,
      HttpSolrServer wikiDefinitionServer, HttpSolrServer allInfoboxServer,
      HttpSolrServer guinessPassageServer, HttpSolrServer newsPassageServer) {
    this.wikiPassageServer = wikiPassageServer;
    this.wikiDefinitionServer = wikiDefinitionServer;
    this.allInfoboxServer = allInfoboxServer;
    this.guinessPassageServer = guinessPassageServer;
    this.newsPassageServer = newsPassageServer;
    this.CONFIG_PATH = properties.getProperty("CONFIG_PATH", "ETRI_WIKIPEDIA_QA.cfg");

  }

  @Override
  public void passageSearchBM25(SemanticPassageSearchInput request,
      StreamObserver<SemanticPassageSearchOutput> responseObserver) {
    try {
      logger.info("START");
      PrimarySearchBM25 primarySearchBM25 = new PrimarySearchBM25(CONFIG_PATH);
      primarySearchBM25.readAnsUnit(request.getKbResult());
      primarySearchBM25.do_primarySearch();
      String result = primarySearchBM25.writeAnsUnit();
      logger.info("BM25 after");
      logger.info("kbResult : {}", request.getKbResult());
      logger.info("BM25 Result : {}", result);
      logger.info("Parsing before");
      SemanticPassageSearchOutput semanticPassageSearchOutput = parsingSearchResult(result,
          request.getThresHold(), request.getWindowSize());
      searchOutput(responseObserver,
          (semanticPassageSearchOutput == null ? SemanticPassageSearchOutput.newBuilder().build()
              : semanticPassageSearchOutput));
      logger.info("END");
    } catch (Exception e) {
      responseObserver.onError(e);
      e.printStackTrace();
    }
  }

  private SemanticPassageSearchOutput parsingSearchResult(String result, float thresHold, int windowSize) {

    String[] tempResults = StringUtils.splitByWholeSeparatorPreserveAllTokens(result, "||0||");
    if (tempResults.length != 3) {
      logger.info("parsingSearchResult size is {}", tempResults.length);
      return null;
    }
    ObjectMapper mapper = new ObjectMapper();
    try {
      JsonNode node = mapper.readTree(tempResults[2]);

      JsonNode wikiPassage = node.findValue("wiki_passage");
      JsonNode wikiDefinition = node.findValue("wiki_definition");
      JsonNode allInfobox = node.findValue("all_infobox");
      JsonNode guinessPassage = node.findValue("guiness_passage");
      JsonNode newsPassage = node.findValue("news_passage");

      final SemanticPassageSearchOutput.Builder semanticPassageSearchOutput = SemanticPassageSearchOutput.newBuilder();

      SearchTask wikiPassageTask = new SearchTask(wikiPassage, mapper, thresHold, windowSize, true,
          wikiPassageServer, executorService);
      SearchTask wikiDefinitionTask = new SearchTask(wikiDefinition, mapper, thresHold, windowSize,
          false, wikiDefinitionServer, executorService);
      SearchTask allInfoboxTask = new SearchTask(allInfobox, mapper, thresHold, windowSize, false,
          allInfoboxServer, executorService);
      SearchTask guinessPassageTask = new SearchTask(guinessPassage, mapper, thresHold, windowSize,
          false, guinessPassageServer, executorService);
      SearchTask newsPassageTask = new SearchTask(newsPassage, mapper, thresHold, windowSize, true,
          newsPassageServer, executorService);


      List<Future<List<SemanticPassageSearchResult>>> futures = new LinkedList<>();

      futures.add(executorService.submit(wikiPassageTask));
      futures.add(executorService.submit(wikiDefinitionTask));
      futures.add(executorService.submit(allInfoboxTask));
      futures.add(executorService.submit(guinessPassageTask));
      futures.add(executorService.submit(newsPassageTask));

      for (Future<List<SemanticPassageSearchResult>> future : futures) {
        addSearchResult(semanticPassageSearchOutput,future.get());
      }
      return semanticPassageSearchOutput.build();

    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
    return null;
  }

  private void addSearchResult(SemanticPassageSearchOutput.Builder semanticPassageSearchOutput,
      List<SemanticPassageSearchResult> list) {
    if (list != null) {
      semanticPassageSearchOutput.addAllSemanticResults(list);
    }
  }

  private void searchOutput(StreamObserver<SemanticPassageSearchOutput> responseObserver,
      SemanticPassageSearchOutput semanticSearchOutput) {
    responseObserver.onNext(semanticSearchOutput);
    responseObserver.onCompleted();
  }
}