package ai.maum.brain.qa.semantic;

import ai.maum.brain.qa.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;
import java.util.Properties;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SemanticPassageSearchMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(SemanticPassageSearchMain.class);

  private Server server;

  public static void main(String[] args) {
    SemanticPassageSearchMain semanticPassageSearchMain = new SemanticPassageSearchMain();
    semanticPassageSearchMain.runServer();
    semanticPassageSearchMain.blockUntilShutdown();
  }

  private void runServer() {
    try {
      HttpSolrServer wikiPassageServer = new HttpSolrServer(
          properties.getProperty("WIKI_PASSAGE_URL", "http://127.0.0.1:8983/solr/wiki_passage"));
      HttpSolrServer wikiDefinitionServer = new HttpSolrServer(properties
          .getProperty("WIKI_DEFINITION_URL", "http://127.0.0.1:8983/solr/wiki_definition"));
      HttpSolrServer allInfoboxServer = new HttpSolrServer(
          properties.getProperty("ALL_INFOBOX_URL", "http://127.0.0.1:8983/solr/all_infobox"));
      HttpSolrServer guinessPassageServer = new HttpSolrServer(properties
          .getProperty("GUINESS_PASSAGE_URL", "http://127.0.0.1:8983/solr/guiness_passage"));
      HttpSolrServer newsPassageServer = new HttpSolrServer(
          properties.getProperty("NEWS_PASSAGE_URL", "http://127.0.0.1:8983/solr/news_passage"));
      int port = Integer.parseInt(properties.getProperty("SEMANTIC_SEARCH_PORT", "30052"));
      server = ServerBuilder.forPort(port).addService(
          new SemanticPassageSearchService(wikiPassageServer, wikiDefinitionServer, allInfoboxServer,
              guinessPassageServer, newsPassageServer)).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          SemanticPassageSearchMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }
}
