package ai.maum.brain.qa.semantic;

import ai.maum.brain.qa.util.PropertiesManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchResult;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchResult.Builder;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExtractTask implements Callable<SemanticPassageSearchResult> {

  private static Logger logger = LoggerFactory.getLogger(ExtractTask.class);

  private static Properties properties = PropertiesManager.getProperties();

  private Map map;
  private float thresHold;
  private boolean hasSent;
  private int windowSize;
  private HttpSolrServer solrServer;

  public ExtractTask(Map map, float thresHold, boolean hasSent, int windowSize,
      HttpSolrServer solrServer) {
    this.map = map;
    this.thresHold = thresHold;
    this.hasSent = hasSent;
    this.windowSize = windowSize;
    this.solrServer = solrServer;
  }

  @Override
  public SemanticPassageSearchResult call() throws Exception {
    logger.info("Extract Value Start");
    if (map == null) {
      logger.info("extractValue 1 is null");
      return null;
    }

    float weight = Float.parseFloat(String.valueOf(map.get("weight")) + "f");
    String cpname = (String) map.get("cpname");
    int ranking = (int) map.get("Ranking");
    String id = StringUtils
        .splitByWholeSeparatorPreserveAllTokens((String) map.get("StructPageID"), ":")[0];
    int sid = (int) map.get("S_ID");
    if (weight <= thresHold) {
      logger.info("extractValue 2 is null");
      return null;
    }

    Builder semanticSearchResult = SemanticPassageSearchResult.newBuilder();
    semanticSearchResult.setWeight(weight);
    semanticSearchResult.setDocType(cpname);
    semanticSearchResult.setRank(ranking);

    List<String> idList = new ArrayList<>();

    if (hasSent) {
      int pre = 0;
      int post = 0;
      if (windowSize % 2 == 0) {
        pre = (windowSize / 2) - 1;
        post = (windowSize / 2);
      } else {
        pre  = (windowSize / 2);
        post = (windowSize / 2);
      }

      if (sid - pre < 1) {
        post = (post + Math.abs((sid - pre)) + 1) + sid;
        pre = 1;
      } else {
        post = post + sid;
        pre = sid - pre;
      }

      for (int i = sid; i >= pre ; i--) {
        idList.add("StructPageID:\"" + id + ":" + i +"\"");
      }

      for (int i = (sid + 1); i <= post; i++) {
        idList.add("StructPageID:\"" + id + ":" + i + "\"");
      }
    } else {
      idList.add("StructPageID:" + id);
    }

    ModifiableSolrParams solrParams = new ModifiableSolrParams();
    solrParams.add("q", StringUtils.join(idList, " OR "));
    solrParams.add("sort", "StructPageID asc");
    solrParams.add("rows", String.valueOf(windowSize));

    try {
      logger.info("Solr before {} {}",  cpname, ranking);
      QueryResponse queryResponse = solrServer.query(solrParams);
      SolrDocumentList solrResults = queryResponse.getResults();
      logger.info("Solr after {} {}", cpname, ranking);
      long docCnt = solrResults.getNumFound();
      StringBuffer passageText = new StringBuffer();
      long createDate = 0;
      for (int i = 0; i < docCnt; i++) {
        SolrDocument solrDoc = solrResults.get(i);
        String rkey = String.valueOf(solrDoc.getFieldValue("Rowkey"));
        String createDateStr = String.valueOf(solrDoc.getFieldValue("create_date")).trim();
        if (StringUtils.isEmpty(createDateStr)) {
          createDateStr = "0";
        }
        createDate = Long.parseLong(createDateStr);
        passageText.append(" ");
        passageText.append(solrDoc.getFieldValue("Description"));
        URL url = new URL(properties.getProperty("HBASE_URL", "http://localhost:9091")  + "/" + cpname + "/" + rkey + "/doc:json");
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
          BufferedReader reader = null;
          try {
            reader = new BufferedReader(
                new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));
            String line = null;
            StringBuffer docJson = new StringBuffer();
            while ((line = reader.readLine()) != null) {
              docJson.append(line);
            }
            semanticSearchResult.addPassageMorp(docJson.toString());
            reader.close();
          } catch (Exception e) {
            e.printStackTrace();
          } finally {
            if (reader != null) {
              reader.close();
            }
          }
        }
      }
      logger.info("Doc Parsing End {} {}", cpname, ranking);
      semanticSearchResult.setCreateDate(createDate);
      semanticSearchResult.setPassageText(passageText.toString().trim());
    } catch (SolrServerException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    } finally {

    }
    return semanticSearchResult.build();
  }
}
