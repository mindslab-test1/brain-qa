package ai.maum.brain.qa.semantic;

import ai.maum.brain.qa.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SemanticSearchMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(SemanticSearchMain.class);

  private Server server;

  public static void main(String[] args) {
    SemanticSearchMain semanticSearchMain = new SemanticSearchMain();
    semanticSearchMain.runServer();
    semanticSearchMain.blockUntilShutdown();
  }

  private void runServer() {
    try {
      int port = Integer.parseInt(properties.getProperty("SEMANTIC_SEARCH_PORT", "30052"));
      server = ServerBuilder.forPort(port).addService(new SemanticSearchService()).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          SemanticSearchMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }
}
