package ai.maum.brain.qa.semantic;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;

public class TestHbase {

  public static void main(String[] args) {
    TestHbase testHbase = new TestHbase();
    testHbase.execute();
  }

  private void execute() {
    Configuration configuration = HBaseConfiguration.create();
    configuration.set("hbase.master", "13.124.168.211");
    configuration.set("hbase.zookeeper.quorum", "13.124.168.211");
    try {
      HTable hTable = new HTable(configuration, "news_passage");
      Get get = new Get(Bytes.toBytes("aaa"));
      System.out.println(get.familySet());
      hTable.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
