package ai.maum.brain.qa.semantic;

import ai.maum.brain.qa.util.PropertiesManager;
import com.nanum.exobrain.search.etri.PrimarySearchBM25;
import com.nanum.exobrain.search.etri.PrimarySearchBoolean;
import com.nanum.exobrain.search.etri.PrimarySearchTIC;
import io.grpc.stub.StreamObserver;
import java.util.Properties;
import maum.brain.qa.SemanticSearch.SemanticSearchInput;
import maum.brain.qa.SemanticSearch.SemanticSearchOutput;
import maum.brain.qa.SemanticSearchServiceGrpc.SemanticSearchServiceImplBase;

public class SemanticSearchService extends SemanticSearchServiceImplBase {

  private static Properties properties = PropertiesManager.getProperties();

  private static String CONFIG_PATH = "";

  public SemanticSearchService() {
    this.CONFIG_PATH = properties.getProperty("CONFIG_PATH", "ETRI_WIKIPEDIA_QA.cfg");
  }

  @Override
  public void searchBM25(SemanticSearchInput request,
      StreamObserver<SemanticSearchOutput> responseObserver) {
    PrimarySearchBM25 primarySearchBM25 = null;
    try {
      primarySearchBM25 = new PrimarySearchBM25(CONFIG_PATH);
      primarySearchBM25.readAnsUnit(request.getQuestionAnalysis());
      primarySearchBM25.do_primarySearch();
      String result = primarySearchBM25.writeAnsUnit();
      SemanticSearchOutput semanticSearchOutput = SemanticSearchOutput.newBuilder().setSemanticSearchResult(result).build();
      responseObserver.onNext(semanticSearchOutput);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public void searchTIC(SemanticSearchInput request,
      StreamObserver<SemanticSearchOutput> responseObserver) {
    PrimarySearchTIC primarySearchTIC = null;
    try {
      primarySearchTIC = new PrimarySearchTIC(CONFIG_PATH);
      primarySearchTIC.readAnsUnit(request.getQuestionAnalysis());
      primarySearchTIC.do_primarySearch();
      String result = primarySearchTIC.writeAnsUnit();
      SemanticSearchOutput semanticSearchOutput = SemanticSearchOutput.newBuilder().setSemanticSearchResult(result).build();
      responseObserver.onNext(semanticSearchOutput);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public void searchBoolean(SemanticSearchInput request,
      StreamObserver<SemanticSearchOutput> responseObserver) {
    PrimarySearchBoolean primarySearchBoolean = null;
    try {
      primarySearchBoolean = new PrimarySearchBoolean(CONFIG_PATH);
      primarySearchBoolean.readAnsUnit(request.getQuestionAnalysis());
      primarySearchBoolean.do_primarySearch();
      String result = primarySearchBoolean.writeAnsUnit();
      SemanticSearchOutput semanticSearchOutput = SemanticSearchOutput.newBuilder().setSemanticSearchResult(result).build();
      responseObserver.onNext(semanticSearchOutput);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }
}
