package ai.maum.brain.qa.semantic;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import maum.brain.qa.SemanticSearch.SemanticPassageSearchResult;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchTask implements Callable<List<SemanticPassageSearchResult>> {

  private static Logger logger = LoggerFactory.getLogger(SearchTask.class);

  private JsonNode node;
  private ObjectMapper mapper;
  private float thresHold;
  private int windowSize;
  private boolean hasSent;
  private HttpSolrServer solrServer;
  private ExecutorService executorService;

  public SearchTask(JsonNode node, ObjectMapper mapper, float thresHold, int windowSize,
      boolean hasSent, HttpSolrServer solrServer,
      ExecutorService executorService) {
    this.node = node;
    this.mapper = mapper;
    this.thresHold = thresHold;
    this.windowSize = windowSize;
    this.hasSent = hasSent;
    this.solrServer = solrServer;
    this.executorService = executorService;
  }

  @Override
  public List<SemanticPassageSearchResult> call() throws Exception {
    if (node != null) {
      logger.info("Thread Start");
      Iterator<JsonNode> iter = node.iterator();
      List<SemanticPassageSearchResult> resultList = new ArrayList<>();
      List<Future<SemanticPassageSearchResult>> futures = new LinkedList<>();
      while (iter.hasNext()) {
        JsonNode wikiNode = iter.next();
        HashMap wikiMap = mapper.convertValue(wikiNode, HashMap.class);
//        SemanticSearchResult semanticSearchResult = extractValue(wikiMap, thresHold, windowSize, hasSent);
        ExtractTask extractTask = new ExtractTask(wikiMap, thresHold, hasSent, windowSize, solrServer);
        futures.add(executorService.submit(extractTask));
      }
      for (Future<SemanticPassageSearchResult> future : futures) {
        SemanticPassageSearchResult semanticSearchResult = future.get();
        if (semanticSearchResult != null) {
          resultList.add(semanticSearchResult);
        }
      }
      return resultList;
    }
    logger.info("GetAnswer is Null");
    return null;
  }
}
