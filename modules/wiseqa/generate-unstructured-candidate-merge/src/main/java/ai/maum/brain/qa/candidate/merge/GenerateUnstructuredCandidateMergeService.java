package ai.maum.brain.qa.candidate.merge;

import com.nanum.exobrain.util.CandidateMerge;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.GenerateUnstructuredCandidateMerge.GenerateUnstructuredCandidateMergeInput;
import maum.brain.qa.GenerateUnstructuredCandidateMerge.GenerateUnstructuredCandidateMergeOutput;
import maum.brain.qa.GenerateUnstructuredCandidateMergeServiceGrpc.GenerateUnstructuredCandidateMergeServiceImplBase;

public class GenerateUnstructuredCandidateMergeService extends
    GenerateUnstructuredCandidateMergeServiceImplBase {

  @Override
  public void mergeCandidate(GenerateUnstructuredCandidateMergeInput request,
      StreamObserver<GenerateUnstructuredCandidateMergeOutput> responseObserver) {
    try {
      String[] inputs = new String[3];
      inputs[0] = request.getBm25Result();
      inputs[1] = request.getTicResult();
      inputs[2] = request.getBooleanResult();
      CandidateMerge candidateMerge = new CandidateMerge();
      candidateMerge.readAnsUnit(inputs);
      candidateMerge.mergeAnswerUnit();
      candidateMerge.reranking();
      String result = candidateMerge.writeAnsUnit();
      GenerateUnstructuredCandidateMergeOutput generateUnstructuredCandidateMergeOutput = GenerateUnstructuredCandidateMergeOutput
          .newBuilder().setGenerateStructuredCandidateResult(result).build();
      responseObserver.onNext(generateUnstructuredCandidateMergeOutput);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }
}
