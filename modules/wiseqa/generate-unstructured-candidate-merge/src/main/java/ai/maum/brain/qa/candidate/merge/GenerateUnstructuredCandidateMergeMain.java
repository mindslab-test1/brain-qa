package ai.maum.brain.qa.candidate.merge;

import ai.maum.brain.qa.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;
import java.util.Properties;

public class GenerateUnstructuredCandidateMergeMain {

  private static Properties properties = PropertiesManager.getProperties();

  private Server server;

  public static void main(String[] args) {
    GenerateUnstructuredCandidateMergeMain generateUnstructuredCandidateMergeMain = new GenerateUnstructuredCandidateMergeMain();
    generateUnstructuredCandidateMergeMain.runServer();
    generateUnstructuredCandidateMergeMain.blockUntilShutdown();
  }

  private void runServer() {
    try {
      int port = Integer.parseInt(properties.getProperty("GENERATE_UNSTRUCTURED_CANDIDATE_MERGE_PORT", "30053"));
      server = ServerBuilder.forPort(port).addService(new GenerateUnstructuredCandidateMergeService()).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          GenerateUnstructuredCandidateMergeMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }
}
