package ai.maum.brain.qa.candidate;

import com.nanum.exobrain.knowledgebase.KBCandidateManager;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.GenerateStructuredCandidate.GenerateStructuredCandidateInput;
import maum.brain.qa.GenerateStructuredCandidate.GenerateStructuredCandidateOutput;
import maum.brain.qa.GenerateStructuredCandidateServiceGrpc.GenerateStructuredCandidateServiceImplBase;


public class GenerateStructuredCandidateService extends GenerateStructuredCandidateServiceImplBase {

  @Override
  public void makeCandidate(GenerateStructuredCandidateInput request,
      StreamObserver<GenerateStructuredCandidateOutput> responseObserver) {
    try {
      KBCandidateManager kbCandidateManager = new KBCandidateManager();
      boolean flag = kbCandidateManager.init();
      if (!flag) {
        throw new Exception();
      }
      String softFilterResult = request.getSoftFilterResult() + "||0";
      String candidateResult = kbCandidateManager.do_kbcangen(softFilterResult);
      kbCandidateManager.close();
      GenerateStructuredCandidateOutput generateStructuredCandidateOutput = GenerateStructuredCandidateOutput
          .newBuilder().setGenerateStructuredCandidateResult(candidateResult).build();
      responseObserver.onNext(generateStructuredCandidateOutput);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }
}
