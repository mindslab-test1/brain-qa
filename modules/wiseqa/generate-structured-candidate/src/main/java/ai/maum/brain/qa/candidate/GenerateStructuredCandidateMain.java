package ai.maum.brain.qa.candidate;

import ai.maum.brain.qa.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateStructuredCandidateMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(GenerateStructuredCandidateMain.class);

  private Server server;

  public static void main(String[] args) {
    GenerateStructuredCandidateMain generateStructuredCandidateMain = new GenerateStructuredCandidateMain();
    generateStructuredCandidateMain.runServer();
    generateStructuredCandidateMain.blockUntilShutdown();
  }

  private void runServer() {
    try {
      int port = Integer.parseInt(properties.getProperty("GENERATE_STRUCTURED_CANDIDATE_PORT", "30051"));
      server = ServerBuilder.forPort(port).addService(new GenerateStructuredCandidateService()).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          GenerateStructuredCandidateMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }
}
