#include "filter-answer-candidate-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void FilterAnswerCandidateImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 정답 후보 축소 관련 리소스 경로 설정
  rsc_path_ = c.Get("brain-qa.filter-answer-candidate.path");
}

