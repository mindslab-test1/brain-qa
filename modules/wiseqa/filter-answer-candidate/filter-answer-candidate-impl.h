#ifndef PROJECT_FILTER_ANSWER_CANDIDATE_IMPL_H
#define PROJECT_FILTER_ANSWER_CANDIDATE_IMPL_H

#pragma execution_character_set("utf-8")

#include <iostream>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "Lheader/common_util.h" 
#include "QAHeader/AnswerRank.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;
using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::FilterAnswerCandidateInputText;
using maum::brain::qa::FilterAnswerCandidateResultDocument;

class AnswerRank;

class FilterAnswerCandidateImpl : public QuestionAnswerService::Service {
public:
  FilterAnswerCandidateImpl();
  virtual ~FilterAnswerCandidateImpl();
  const char *Name() {
    return "Question Answering Service";
  }

  grpc::Status FilterAnswerCandidate(ServerContext *context, const FilterAnswerCandidateInputText *text, 
                                  FilterAnswerCandidateResultDocument *result_document) override;
  void Start(bool lock = true);
private:
  void LoadConfig();
  // 정답후보 축소 관련 class 선언
  AnswerRank *answer_rank_;
  void AnalyzeOne(const FilterAnswerCandidateInputText *text, FilterAnswerCandidateResultDocument *result_document);

  std::string rsc_path_;
  void Stop(bool lock = true);

  bool CanService() {
    std::lock_guard <std::mutex> guard(m_);
    return loaded_ && loading_;
  }

  void LockRunning() {
    {
      std::lock_guard <std::mutex> guard(m_);
      running_++;
    }
    cv_.notify_one();
  }
  void UnlockRunning() {
    {
      std::lock_guard <std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
    public:
      Accessor(FilterAnswerCandidateImpl *ptr) : parent_(ptr) {
        parent_->LockRunning();
      }

      ~Accessor() {
        parent_->UnlockRunning();
      }
    private:
      FilterAnswerCandidateImpl *parent_ = nullptr;
  };

  void Restart();
};
#endif // PROJECT_FILTER_ANSWER_CANDIDATE_IMPL_H
