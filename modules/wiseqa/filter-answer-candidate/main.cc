#include <grpc++/grpc++.h>
#include "filter-answer-candidate-impl.h"
#include <cinttypes>
#include <getopt.h>
#include <gitversion/version.h>
#include <libmaum/common/config.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

void RunServer() {
  libmaum::Config &c = libmaum::Config::Instance();
  std::string server_address("0.0.0.0:");
  server_address += c.Get("brain-qa.filter-answer-candidate.port");

  // filter-answer-candidate-impl
  c.DumpPid();
  FilterAnswerCandidateImpl service;

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(&service);
  unique_ptr <Server> server(builder.BuildAndStart());

  if (server) {
    LOGGER()->info("Server listening on {} with {}",
        server_address, service.Name());
    service.Start();
    server->Wait();
  }
}

// gitversion을 보여주기 위한 함수
void Help(const char *prog) {
  cout << prog << " [--version] [--help]" << endl;
}

// gitversion을 보여주기 위한 함수
void ProcessOption(int argc, char *argv[]) {
  bool do_exit = false;
  int c;
  while (true) {
    static const struct option long_options[] = {
      {"version", no_argument, 0, 'v'},
      {"help", no_argument, 0, 'h'},
      {NULL, no_argument, NULL, 0}
    };

    int option_index = 0;
    char * prog = basename(argv[0]);
    c = getopt_long(argc, argv, "vh", long_options, &option_index);

    if (c == -1) break;

    switch (c) {
      case 0:
        break;
      case 'v':
        cout << prog << " version " << version::VERSION_STRING << endl;
        do_exit = true;
        break;
      case 'h':
        Help(prog);
        do_exit = true;
        break;
      default:
        Help(prog);
        do_exit = true;
    }
  }
  if (do_exit) exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
  ProcessOption(argc, argv);
  libmaum::Config::Init(argc, argv, "brain-qa.conf");
  spdlog::set_level(spdlog::level::debug);
  spdlog::set_async_mode((1 << 12), spdlog::async_overflow_policy::discard_log_msg, nullptr,
      std::chrono::milliseconds(10), nullptr);
  RunServer();
}
