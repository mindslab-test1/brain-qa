#pragma execution_character_set("utf-8")

#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <thread>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <libmaum/common/encoding.h>
#include "filter-answer-candidate-impl.h"

using std::unique_ptr;
using grpc::Status;
using grpc::StatusCode;

FilterAnswerCandidateImpl::FilterAnswerCandidateImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void FilterAnswerCandidateImpl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  unique_ptr<char[]> rsc_path(new char[rsc_path_.size() + 1]);
  strcpy(rsc_path.get(), rsc_path_.c_str());
  // 정답후보 축소를 위한 class 선언
  answer_rank_ = new AnswerRank();
  // 정답후보 축소 초기화 함수
  answer_rank_->ARank_THREAD_init();

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }
  std::cout << "Completed to Load!!" << endl;  
}

void FilterAnswerCandidateImpl::Stop(bool lock) {
  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  delete answer_rank_;
}

FilterAnswerCandidateImpl::~FilterAnswerCandidateImpl() {
  Stop();
}

/**
  @breif 정답후보 축소를 실제 수행하는 함수
  @param FilterAnswerCandidateInputText *text : 클라이언트에서 보낸 message
  @param FilterAnswerCandidateResultDocument *result_document : 정답후보 축소 결과를 클라이언트에 보낼 message
*/
void FilterAnswerCandidateImpl::AnalyzeOne(const FilterAnswerCandidateInputText *text,
                                          FilterAnswerCandidateResultDocument *result_document) {
  const string input_text = text->text();
  string result;
  // 멀티쓰레드를 위해 사용 (추후 변경 예정)
#pragma omp parallel for
  // 정답후보 축소를 위한 함수
  result = answer_rank_->soft_filter_4UIMA(input_text);
  result_document->set_result(result);
}

/**
  @breif 클라이언트에서 정답후보 축소를 위해 호출하는 함수
  @date 2017/12/20
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance 
  @param FilterAnswerCandidateInputText *text : 클라이언트에서 보낸 message
  @param FilterAnswerCandidateResultDocument *result_document : 정답후보 축소 결과를 클라이언트에 보낼 message
  @return grpc Status Code
*/
grpc::Status FilterAnswerCandidateImpl::FilterAnswerCandidate(ServerContext *context,
                                                        const FilterAnswerCandidateInputText *text,
                                                        FilterAnswerCandidateResultDocument *result_document) {
  if(!CanService()) {
    return grpc::Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return grpc::Status::OK;
}
