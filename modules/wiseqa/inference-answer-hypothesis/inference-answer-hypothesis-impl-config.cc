#include "inference-answer-hypothesis-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void InferenceAnswerHypothesisImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 정답후보 타입/제약 추론 관련 리소스 경로 설정
  inference_answer_hypothesis_path_ = c.Get("brain-qa.inference-answer-hypothesis.path");
  // 근거추론 관련 리소스 경로 설정
  inference_answer_constraint_path_ = c.Get("brain-qa.inference-answer-constraint.path");
  // hbase ip 및 port 설정
  hbase_ip_ = c.Get("brain-qa.hbase.ip");
  hbase_port_ = c.Get("brain-qa.hbase.port");
}

