#ifndef PROJECT_INFERENCE_ANSWER_HYPOTHESIS_IMPL_H
#define PROJECT_INFERENCE_ANSWER_HYPOTHESIS_IMPL_H

#pragma execution_character_set("utf-8")

#include <iostream>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "HbaseUtils.h"
#include "WWN/Searcher.hpp"
#include "CD/CD.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;

using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::InferenceAnswerHypothesisInputText;
using maum::brain::qa::InferenceAnswerHypothesisResultDocument;

class InferenceAnswerHypothesisImpl : public QuestionAnswerService::Service {
public:
  InferenceAnswerHypothesisImpl();
  virtual ~InferenceAnswerHypothesisImpl();
  const char *Name() {
    return "Question Answering Service";
  }

  grpc::Status InferenceAnswerHypothesis(ServerContext *context, const InferenceAnswerHypothesisInputText *text, 
                                        InferenceAnswerHypothesisResultDocument *result_document) override;
  void Start(bool lock = true);

private:
  void LoadConfig();
  // 사전 검색기를 위한 class 선언
  Searcher *searcher_;
  // hbase에 접근하기 위한 class 선언
  HbaseUtils *hu_;

  // 근거추론을 위한 class 선언
  CD *inference_answer_hypothesis_;
  void AnalyzeOne(const InferenceAnswerHypothesisInputText *text, 
              InferenceAnswerHypothesisResultDocument *result_document);

  std::string inference_answer_hypothesis_path_;
  std::string inference_answer_constraint_path_;
  std::string hbase_ip_;
  std::string hbase_port_;

  void Stop(bool lock = true);

  bool CanService() {
    std::lock_guard <std::mutex> guard(m_);
    return loaded_ && loading_;
  } 

  void LockRunning() {
    {
      std::lock_guard <std::mutex> guard(m_);
      running_++;
    } 
    cv_.notify_one();
  } 

  void UnlockRunning() {
    {
      std::lock_guard <std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
    public:
      Accessor(InferenceAnswerHypothesisImpl *ptr) : parent_(ptr) {
        parent_->LockRunning();
      }

      ~Accessor() {
        parent_->UnlockRunning();
      }
    private:
      InferenceAnswerHypothesisImpl *parent_ = nullptr;
  };

  void Restart();
};
#endif // PROJECT_INFERENCE_ANSWER_HYPOTHESIS_IMPL_H
