#pragma execution_character_set("utf-8")

#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <thread>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <libmaum/common/encoding.h>
#include "inference-answer-hypothesis-impl.h"

using std::unique_ptr;
using grpc::Status;
using grpc::StatusCode;

InferenceAnswerHypothesisImpl::InferenceAnswerHypothesisImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void InferenceAnswerHypothesisImpl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  unique_ptr<char[]> inference_answer_hypothesis_path(new char[inference_answer_hypothesis_path_.size() + 1]);
  strcpy(inference_answer_hypothesis_path.get(), inference_answer_hypothesis_path_.c_str());

  unique_ptr<char[]> inference_answer_constraint_path(new char[inference_answer_constraint_path_.size() + 1]);
  strcpy(inference_answer_constraint_path.get(), inference_answer_constraint_path_.c_str());

  unique_ptr<char[]> hbase_ip(new char[hbase_ip_.size() + 1]);
  strcpy(hbase_ip.get(), hbase_ip_.c_str());

  unique_ptr<char[]> hbase_port(new char[hbase_port_.size() + 1]);
  strcpy(hbase_port.get(), hbase_port_.c_str());

  // 사전 검색기를 위한 class 선언
  searcher_ = new Searcher();
  // inference_answer_constraint_path.get() 파일로 접근하여 설정
  searcher_->init(inference_answer_constraint_path.get());
  // 사전타입 설정(KUSA, KORLEX, UWORDMAP, SYNONYM, ANTONYM)
  searcher_->load(KUSA);

  // 초기화 시 inference-answer-hypothesis-config.cc에서 설정한 ip 및 port 번호를 통해 설정
  hu_ = new HbaseUtils(hbase_ip.get(), std::stoi(hbase_port.get()));

  // hbase 관련 설정을 inference_answer_hypothesis_path.get() 파일로 접근하여 설정
  hu_->init(inference_answer_hypothesis_path.get());

  // 근거추론을 위한 class 선언
  inference_answer_hypothesis_ = new CD(searcher_, hu_);

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }
  std::cout << "Completed to Load!!" << endl;
}

void InferenceAnswerHypothesisImpl::Stop(bool lock) {
  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  delete inference_answer_hypothesis_;
}

InferenceAnswerHypothesisImpl::~InferenceAnswerHypothesisImpl() {
  Stop();
}

/**
  @breif 근거추론을 실제 수행하는 함수
  @param UnstructuredCandidateInputText *text : 클라이언트에서 보낸 message
  @param UnstructuredCandidateResultDocument *result_document : 근거추론 결과를 클라이언트에 보낼 message
*/
void InferenceAnswerHypothesisImpl::AnalyzeOne(const InferenceAnswerHypothesisInputText *text,
                                            InferenceAnswerHypothesisResultDocument *result_document) {
  const string input_text = text->text();
  auto start_index = text->start_index();
  auto end_index = text->end_index();
  string result;
  // 멀티 쓰레드를 위해 사용 (추후 변경 예정)
#pragma omp parallel for
  // 근거 추론 결과를 가져오는 함수
  result = inference_answer_hypothesis_->CDScr(input_text, start_index, end_index);
  result_document->set_result(result);
}

/**
  @breif 클라이언트에서 근거추론을 위해 호출하는 함수
  @param UnstructuredCandidateInputText *text : 클라이언트에서 보낸 message
  @param UnstructuredCandidateResultDocument *result_document : 근거추론 결과를 클라이언트에 보낼 message
  @return None
*/
grpc::Status InferenceAnswerHypothesisImpl::InferenceAnswerHypothesis(ServerContext *context,
                                                    const InferenceAnswerHypothesisInputText *text,
                                                    InferenceAnswerHypothesisResultDocument *result_document) {
  if(!CanService()) {
    return grpc::Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return grpc::Status::OK;
}
