#pragma execution_character_set("utf-8")

#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <thread>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <libmaum/common/encoding.h>
#include "inference-answer-constraint-impl.h"

using std::unique_ptr;
using grpc::Status;
using grpc::StatusCode;

InferenceAnswerConstraintImpl::InferenceAnswerConstraintImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void InferenceAnswerConstraintImpl::Start(bool lock) {
  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  unique_ptr<char[]> rsc_path(new char[rsc_path_.size() + 1]);
  strcpy(rsc_path.get(), rsc_path_.c_str());

  // 정답 타입 및 제약을 위한 class 선언
  type_cor_ = new TypeCor();

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }
  std::cout << "Completed to Load!!" << endl;  
}

void InferenceAnswerConstraintImpl::Stop(bool lock) {
  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }
  type_cor_->close();
}

InferenceAnswerConstraintImpl::~InferenceAnswerConstraintImpl() {
  Stop();
}

/**
  @breif 정답 타입 및 제약 추론을 실제로 수행하는 함수
  @param InferenceAnswerConstraintInputText *text : 클라이언트에서 보낸 message
  @param InferenceAnswerConstraintResultDocument *result_document : 정답 타입 및 제약 추론 결과를 클라이언트에 보낼 message
  @return None
*/
void InferenceAnswerConstraintImpl::AnalyzeOne(const InferenceAnswerConstraintInputText *text,
                                      InferenceAnswerConstraintResultDocument *result_document) {
  const string input_text = text->text();
  auto start_index = text->start_index();
  auto end_index = text->end_index();
  string result;

  // 멀티쓰레드를 위해 사용 (추후 변경 예정)
#pragma omp parallel for
  // 정답 제약 및 추론 결과를 반환하는 함수
  result = type_cor_->Type(input_text, start_index, end_index);
  result_document->set_result(result);
}


/**
  @breif 클라이언트에서 정답 타입 및 제약 추론을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param UnstructuredCandidateInputText *text : 클라이언트에서 보낸 message
  @param UnstructuredCandidateResultDocument *result_document : 정답 타입 및 제약 추론 결과를 클라이언트에 보낼 message
  @return grpc Status Code
*/
grpc::Status InferenceAnswerConstraintImpl::InferenceAnswerConstraint(ServerContext *context,
                                                                  const InferenceAnswerConstraintInputText *text,
                                                                  InferenceAnswerConstraintResultDocument *result_document) {
  if(!CanService()) {
    return grpc::Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return grpc::Status::OK;
}
