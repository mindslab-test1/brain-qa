#ifndef PROJECT_INFERENCE_ANSWER_CONSTRAINT_IMPL_H
#define PROJECT_INFERENCE_ANSWER_CONSTRAINT_IMPL_H

#pragma execution_character_set("utf-8")

#include <iostream>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "CI/TypeCor.h"
#include "WWN/Searcher.hpp"
#include "QAHeader/AnswerType.h"
#include "leveldb/db.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;

using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::InferenceAnswerConstraintInputText;
using maum::brain::qa::InferenceAnswerConstraintResultDocument;

class InferenceAnswerConstraintImpl : public QuestionAnswerService::Service {
public:
  InferenceAnswerConstraintImpl();
  virtual ~InferenceAnswerConstraintImpl();
  const char *Name() {
    return "Question Answering Service";
  }

  grpc::Status InferenceAnswerConstraint(ServerContext *context, const InferenceAnswerConstraintInputText *text, 
                                        InferenceAnswerConstraintResultDocument *result_document) override;
  void Start(bool lock = true);
private:
  void LoadConfig();

  void AnalyzeOne(const InferenceAnswerConstraintInputText *text, InferenceAnswerConstraintResultDocument *result_document);
  // 정답 제약 관련 class 선언(문화제 제약, 순차 제약, 속담 제약 등)
  TypeCor *type_cor_;
  std::string rsc_path_;
  void Stop(bool lock = true);

  bool CanService() {
    std::lock_guard <std::mutex> guard(m_);
    return loaded_ && loading_;
  }

  void LockRunning() {
    {   
      std::lock_guard <std::mutex> guard(m_);
      running_++;
    }   
    cv_.notify_one();
  }
  void UnlockRunning() {
    {
      std::lock_guard <std::mutex> guard(m_);
      running_--;
    }
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
  public:
    Accessor(InferenceAnswerConstraintImpl *ptr) : parent_(ptr) {
      parent_->LockRunning();
    }

    ~Accessor() {
      parent_->UnlockRunning();
    }
  private:
    InferenceAnswerConstraintImpl *parent_ = nullptr;
  };

  void Restart();
};
#endif // PROJECT_INFERENCE_ANSWER_CONSTRAINT_IMPL_H
