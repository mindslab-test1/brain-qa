#include "inference-answer-constraint-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void InferenceAnswerConstraintImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 정답 타입 및 제약 추론 관련 리소스 경로 설정
  rsc_path_ = c.Get("brain-qa.inference-answer-constraint.path");
}

