#include "generate-unstructured-candidate-impl.h"

#include <libmaum/common/config.h>
#include <fstream>

using std::ifstream;

void UnstructuredCandidateImpl::LoadConfig() {
  auto c = libmaum::Config::Instance();
  // 비정형 정답 후보 생성에 필요한 리소스 경로를 불러오기 위함
  rsc_path_ = c.Get("brain-qa.generate-unstructured-candidate.path");
}

