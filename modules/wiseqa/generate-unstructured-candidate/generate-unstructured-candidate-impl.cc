#pragma execution_character_set("utf-8")

#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <thread>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <libmaum/common/encoding.h>
#include "generate-unstructured-candidate-impl.h"

using std::unique_ptr;
using grpc::Status;
using grpc::StatusCode;

QADOMAIN QA_DOMAIN = WIKI_DOMAIN;
QAConfig QAConfig_core;

UnstructuredCandidateImpl::UnstructuredCandidateImpl() : running_(0){
  LoadConfig();
}

/**
  @breif 서버 실행시 초기화 하는 함수
  @param bool lock : 서버 가동 여부를 확인하기 위해
  @return None
*/
void UnstructuredCandidateImpl::Start(bool lock) {

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }

  unique_ptr<char[]> path(new char[rsc_path_.size() + 1]);
  strcpy(path.get(), rsc_path_.c_str());

  // Wikipedia 관련 Configuration Setting 관련 로딩 함수
  if (!QAConfig_core.QAConfig_initialize((char*)"/data2/CanGen/rsc/AMcfg/ETRI_WIKIPEDIA_QA.cfg")) {
    std::cout<< " Loading False... " << endl;
  }

  answer_hypo_ = new AnswerHypo;
  // 정해진 Domain 내에서 정답을 추론하기 위한 함수
  answer_hypo_->AHypo_THREAD_init(QA_DOMAIN, true);

  std::cout << "Completed to Load!!" << endl;

  if (lock) {
    std::lock_guard<std::mutex> guard(m_);
    loading_ = true;
    loaded_ = true;
  } else {
    loading_ = true;
    loaded_ = true;
  }

}
void UnstructuredCandidateImpl::Stop(bool lock) {

  if (lock) {
    std::lock_guard <std::mutex> guard(m_);
    loading_ = true;
    loaded_ = false;
  } else {
    loading_ = true;
    loaded_ = false;
  }

  delete answer_hypo_;

}

UnstructuredCandidateImpl::~UnstructuredCandidateImpl() {
  Stop();
}

/**
  @breif 비정형 정답 후보 생성을 실제 수행하는 함수
  @param UnstructuredCandidateInputText *text : 클라이언트에서 보낸 message
  @param UnstructuredCandidateResultDocument *result_document : 비정형 정답 후보 생성 결과를 클라이언트에 보낼 message
  @return None
*/
void UnstructuredCandidateImpl::AnalyzeOne(const UnstructuredCandidateInputText *text,
                                          UnstructuredCandidateResultDocument *result_document) {
  const string input_text = text->text();
  string result;
  // 멀티쓰레드를 활용하기 위해 사용 (추후 변경 예정)
#pragma omp parallel
  {
#pragma omp for

    // UIMA API를 통해 비정형 정답 후보 생성하는 함수
    result = answer_hypo_->generate_AnswerHypo_4UIMA(input_text);
    result_document->set_result(result);
  };
}


/**
  @breif 클라이언트에서 비정형 정답 후보 생성을 위해 호출하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param UnstructuredCandidateInputText *text : 클라이언트에서 보낸 message
  @param UnstructuredCandidateResultDocument *result_document : 비정형 정답 후보 생성 결과를 클라이언트에 보낼 message
*/
grpc::Status UnstructuredCandidateImpl::GenerateUnstructuredCandidate(ServerContext *context,
                                                                const UnstructuredCandidateInputText *text,
                                                                UnstructuredCandidateResultDocument *result_document) {
  if(!CanService()) {
    return grpc::Status(StatusCode::INTERNAL, "loading...");
  }
  Accessor(this);
  AnalyzeOne(text, result_document);
  return grpc::Status::OK;
}

/**
  @breif 엔진 및 공급자 정보 제공하는 함수
  @param ServerContext *context : grpc 통신을 수행하기 위해 필요한 instance
  @param Empty *empty : google protobuf에서 제공하는 generic empty message
  @param Provider *provider : 엔진 공급 정보를 클라이언트에 보낼 message
*/
grpc::Status UnstructuredCandidateImpl::GetProvider(ServerContext *context, const Empty *empty, 
                                                  Provider *provider) {
  provider->set_name("MindsLAB Generate Unstructured Candidate System");
  provider->set_vendor("ETRI");
  provider->set_version("1.0");
  provider->set_description("MindsLAB Generate Unstructured Candidate System provided by ETRI");
  provider->set_support_encoding("UTF-8");

  return grpc::Status::OK;
}

