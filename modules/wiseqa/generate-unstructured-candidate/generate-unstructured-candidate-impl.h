#ifndef PROJECT_GENERATE_UNSTRUCTURED_CANDIDATE_IMPL_H
#define PROJECT_GENERATE_UNSTRUCTURED_CANDIDATE_IMPL_H

#pragma execution_character_set("utf-8")

#include <iostream>

#include <unistd.h>
#include <iconv.h>
#include <string.h>

#include <grpc++/grpc++.h>
#include <mutex>
#include <condition_variable>
#include <unordered_set>

#include "maum/brain/qa/brain_qa.grpc.pb.h"
#include "Lheader/common_util.h"
#include "QAHeader/QAengine.h"

using grpc::ServerContext;
using grpc::Status;
using grpc::StatusCode;
using grpc::ServerReaderWriter;
using google::protobuf::Empty;
using maum::brain::qa::Provider;
using maum::brain::qa::QuestionAnswerService;
using maum::brain::qa::UnstructuredCandidateInputText;
using maum::brain::qa::UnstructuredCandidateResultDocument;

class UnstructuredCandidateImpl : public QuestionAnswerService::Service {
public:
  UnstructuredCandidateImpl();
  virtual ~UnstructuredCandidateImpl();
  const char *Name() {
    return "Question Answering Service";
  }
  grpc::Status GetProvider(ServerContext *context, const Empty *empty, Provider *provider) override; 
  grpc::Status GenerateUnstructuredCandidate(ServerContext *context, const UnstructuredCandidateInputText *text, 
                                          UnstructuredCandidateResultDocument *result_document) override;
  void Start(bool lock = true);
private:
  void LoadConfig();
  void AnalyzeOne(const UnstructuredCandidateInputText *text, UnstructuredCandidateResultDocument *result_document);

  // 정답을 생성하기 위한 class
  AnswerHypo *answer_hypo_;
  std::string rsc_path_;
  void Stop(bool lock = true);

  bool CanService() {
    std::lock_guard <std::mutex> guard(m_);
    return loaded_ && loading_;
  }

  void LockRunning() {
    {   
      std::lock_guard <std::mutex> guard(m_);
       running_++;
    }   
    cv_.notify_one();
  }
  void UnlockRunning() {
    {   
      std::lock_guard <std::mutex> guard(m_);
      running_--;
    }   
    cv_.notify_one();
  }

  bool loaded_ = false;
  bool loading_ = false;
  int32_t running_ = 0;
  std::mutex m_;
  std::condition_variable cv_;

  class Accessor {
    public:
      Accessor(UnstructuredCandidateImpl *ptr) : parent_(ptr) {
        parent_->LockRunning();
      }

      ~Accessor() {
        parent_->UnlockRunning();
      }
      private:
        UnstructuredCandidateImpl *parent_ = nullptr;
  };

  void Restart();
};
#endif // PROJECT_GENERATE_UNSTRUCTURED_CANDIDATE_IMPL_H
