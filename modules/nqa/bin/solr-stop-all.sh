#!/usr/bin/env bash
  PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/../" >/dev/null
  APP_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

  cd $APP_HOME
  ./solr1/bin/solr stop -p 8983
  ./solr2/bin/solr stop -p 8984
  ./solr3/bin/solr stop -p 8985