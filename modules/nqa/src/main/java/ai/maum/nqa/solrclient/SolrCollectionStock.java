package ai.maum.nqa.solrclient;

import ai.maum.util.PropertiesManager;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.LBHttpSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class SolrCollectionStock {
  private static Logger logger = LoggerFactory.getLogger(SolrCollectionStock.class);

  private static final LBHttpSolrClient lbHttpSolrClient;
  private static final List<String> zkList;

  private List<String> collectionsList;
  private final Map<String, SolrService> solrServiceMap = new HashMap<>();

  static {
    Properties properties = PropertiesManager.getProperties();
    String solr1_url = properties.getProperty("SOLR_URL_1", "http://localhost:8983/solr");
    String solr2_url = properties.getProperty("SOLR_URL_2", "http://localhost:8984/solr");
    String solr3_url = properties.getProperty("SOLR_URL_3", "http://localhost:8985/solr");
    lbHttpSolrClient = new LBHttpSolrClient.Builder().withBaseSolrUrls(solr1_url, solr2_url, solr3_url).build();
    zkList = new ArrayList<>();
    zkList.add(properties.getProperty("ZOO_URL_1", "localhost:2181"));
    zkList.add(properties.getProperty("ZOO_URL_2", "localhost:2182"));
    zkList.add(properties.getProperty("ZOO_URL_3", "localhost:2183"));
  }

  private SolrCollectionStock() {
  }

  private void stockUp(String channel) {
    CloudSolrClient solrClientQuestion = new CloudSolrClient.Builder()
      .withLBHttpSolrClient(lbHttpSolrClient)
      .withZkHost(zkList).build();
    CloudSolrClient solrClientAnswer = new CloudSolrClient.Builder()
      .withLBHttpSolrClient(lbHttpSolrClient)
      .withZkHost(zkList).build();

    if (collectionsList == null || collectionsList.isEmpty()) {
      try {
        collectionsList = CollectionAdminRequest.List.listCollections(solrClientQuestion);
        logger.info("Collections: {}", collectionsList);
      } catch (SolrServerException | IOException e) {
        logger.error("", e);
      }
    }

    final String lowerCaseChannel = channel.toLowerCase();
    final String questionCollection =
      collectionsList.contains(lowerCaseChannel + "_question") ?
        lowerCaseChannel + "_question" :
        "question";
    final String answerCollection =
      collectionsList.contains(lowerCaseChannel + "_answer") ?
        lowerCaseChannel + "_answer" :
        "answer";

    solrClientQuestion.setDefaultCollection(questionCollection);
    solrClientAnswer.setDefaultCollection(answerCollection);

    logger.info("Q/A Collections of [{}] ==> [{}, {}]", channel, questionCollection, answerCollection);

    SolrService solrService = new SolrService(solrClientQuestion, solrClientAnswer);

    solrServiceMap.put(channel, solrService);
  }

  public synchronized SolrService getService(String channel) {
    if (!solrServiceMap.containsKey(channel)) {
      stockUp(channel);
    }

    return solrServiceMap.get(channel);
  }

  public CloudSolrClient getCloudSolrClientQuestion(String channel) {
    return getService(channel).getCloudSolrClientQuestion();
  }

  public CloudSolrClient getCloudSolrClientAnswer(String channel) {
    return getService(channel).getCloudSolrClientAnswer();
  }

  private static final SolrCollectionStock stock = new SolrCollectionStock();

  public static SolrCollectionStock getInstance() {
    return stock;
  }
}
