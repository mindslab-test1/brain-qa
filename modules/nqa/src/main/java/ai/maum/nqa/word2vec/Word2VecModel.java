package ai.maum.nqa.word2vec;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Word2VecModel {
    private static Logger logger = LoggerFactory.getLogger(Word2VecModel.class);

    private Word2Vec vec = null;
    private int ntop;
    private float weight;

    public Word2VecModel(String modelPath, int ntop, float weight) {
        try {
            this.ntop = ntop;
            this.weight = weight;
            this.vec = WordVectorSerializer.readWord2VecModel(modelPath);
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
        }
    }

    public List<String> getW2VWords(String word) {
        Collection<String> w2vWords =  vec.wordsNearest(word, ntop);
        logger.info(w2vWords.toString());
        List<String> w2vList = new ArrayList<>();
        int i = 0;
        for (String w2vWord : w2vWords) {
            w2vList.add(w2vWord.split("/")[0] + "^" + String.valueOf(weight));
            i++;
            if (i >= ntop) {
                break;
            }
        }
        return w2vList;
    }
}
