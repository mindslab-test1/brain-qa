package ai.maum.nqa;

import ai.maum.util.PropertiesManager;
import ai.maum.util.SqlSessionManager;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import io.grpc.stub.StreamObserver;
import maum.brain.qa.nqa.Admin;
import maum.brain.qa.nqa.Admin.CollectionType;
import maum.brain.qa.nqa.Admin.IndexType;
import maum.brain.qa.nqa.Admin.IndexingRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexingCore extends Thread {

  private static Logger logger = LoggerFactory.getLogger(IndexingCore.class);

  private static Properties properties = PropertiesManager.getProperties();

  private static String lockObj;

  private Properties indexingPriperties;

  private IndexingRequest request;

  private DocumentIndexingHandler documentIndexingHandler;

  private SolrClient solrClient;

  private Map<String, String> dbParams = new HashMap<>();

  public IndexingCore(IndexingRequest request, DocumentIndexingHandler documentIndexingHandler,
      SolrClient solrClient, Properties indexingPriperties, String lockObj) {
    this.request = request;
    this.documentIndexingHandler = documentIndexingHandler;
    this.solrClient = solrClient;
    this.indexingPriperties = indexingPriperties;
    this.lockObj = lockObj;
    // set DB params
    if (request.getChannelId() != 0) {
      dbParams.put("channel_id", String.valueOf(request.getChannelId()));
    }
    if (request.getCategoryId() != 0) {
      dbParams.put("category_id", String.valueOf(request.getCategoryId()));
    }
  }

  private String buildDeleteQuery(String channelName, String categoryName) {
    StringBuffer query = new StringBuffer();
    query.append("channel:");
    if (channelName.isEmpty()) {
      query.append("*");
    } else {
      query.append("\"").append(channelName).append("\"");
    }
    query.append(" AND ");
    query.append("category:");
    if (categoryName.isEmpty()) {
      query.append("*");
    } else {
      query.append("\"").append(categoryName).append("\"");
    }
    logger.debug("delete indexed data Query => {}", query.toString());
    return query.toString();
  }

  private void deleteIndexedData(String channelName, String categoryName) throws Exception {
    try {
      String query = buildDeleteQuery(channelName, categoryName);
      solrClient.deleteByQuery(query);
      solrClient.commit();
    } catch (Exception e) {
      logger.error("Delete Indexed Data Fail : {} => ", e, e.getMessage());
      throw e;
    }
  }

  @Override
  public void run() {

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String nextIndexTime = dateFormat.format(new Date());
    Map<String, List<String>> paraMap = new HashMap<>();
    SqlSessionManager sqlSessionManager = SqlSessionManager.getInstance();
    SqlSession sqlSession = null;

    try {

      synchronized (lockObj) {
        logger.info("{} Indexing Start::: ", request.getCollectionType());

        sqlSession = sqlSessionManager.getSqlSession();
        logger.info("request => {}", request.toString());
        logger.info("DB params => {}", dbParams);

        // Full Indexing 경우 indexed data clear
        if (request.getIndexType() == IndexType.FULL) {
          int channelId = request.getChannelId();
          int categoryId = request.getCategoryId();
          String channelName = (channelId == 0) ? "" : sqlSession.selectOne("nqa.selectChannelName", channelId);
          String categoryName = (categoryId == 0) ? "" : sqlSession.selectOne("nqa.selectCategoryName", categoryId);
          deleteIndexedData(channelName, categoryName);
        }

        documentIndexingHandler.setParaMap(paraMap);
        logger.info("Para Map: {}", paraMap.toString());

//      if (StringUtils.isNotEmpty(request.getTargetDate())) {
//        params.put("target_date", request.getTargetDate());
//      } else if (indexingPriperties != null) {
//        String lastIndexTime = indexingPriperties.getProperty("last_index_time");
//        params.put("target_date", lastIndexTime);
//      }

      // get total count
        int total = 0;
        if (request.getCollectionType() == CollectionType.QUESTION) {
          total = sqlSession.selectOne("nqa.selectAllQuestionCount", dbParams);
          logger.info("Question total count ==> {}", total);
        } else if (request.getCollectionType() == CollectionType.ANSWER) {
          total = sqlSession.selectOne("nqa.selectAllAnswerCount", dbParams);
          logger.info("Answer total count ==> {}", total);
        }

        documentIndexingHandler.setTotal(total);
        documentIndexingHandler.setFetched(0);
        documentIndexingHandler.setProcessed(0);
      }
      // Indexing With DB data
      if (request.getCollectionType() == CollectionType.QUESTION) {
        sqlSession.select("nqa.selectAllQuestion", dbParams, documentIndexingHandler);
      } else if (request.getCollectionType() == CollectionType.ANSWER) {
        sqlSession.select("nqa.selectAllAnswer", dbParams, documentIndexingHandler);
      }

      String indexingTimeFile = PropertiesManager
              .resolveValueWithProVars(properties.getProperty("INDEXING_TIME_FILE_PATH"));
      if (StringUtils.isNotEmpty(indexingTimeFile)) {
        Properties newIndexProperties = new Properties();
        newIndexProperties.setProperty("last_index_time", nextIndexTime);
        FileOutputStream fileOutputStream = new FileOutputStream(indexingTimeFile);
        newIndexProperties.store(fileOutputStream, "Last Index Time");
        fileOutputStream.close();
      }
      sqlSession.close();

    } catch (IOException e) {
      logger.error("{} => ", e, e.getMessage());
    } catch (Exception e) {
      logger.error("{} => ", e, e.getMessage());
    } finally {
      documentIndexingHandler.setIndexing(false);
      if (sqlSession != null) {
        sqlSession.close();
      }
    }

    logger.info("{} Indexing End::: ", request.getCollectionType());
  }
}
