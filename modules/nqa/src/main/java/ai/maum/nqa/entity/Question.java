package ai.maum.nqa.entity;

import java.sql.Date;
import lombok.Data;
import maum.brain.qa.nqa.Admin.NQaAdminCategory;
import maum.brain.qa.nqa.Admin.NQaAdminQuestion;

@Data
public class Question {
  private int id;
  private int answerId;
  private int answerCopyId;
  private String question;
  private String creatorId;
  private String updaterId;
  private Date createDtm;
  private Date updateDtm;


  public NQaAdminQuestion getProto() {
    NQaAdminQuestion.Builder questionProto = NQaAdminQuestion.newBuilder();

    questionProto.setId(this.id);
    questionProto.setAnswerId(this.answerId);
    questionProto.setAnswerCopyId(this.answerCopyId);

    if (this.question != null) {
      questionProto.setQuestion(this.question);
    }

    if (this.creatorId != null) {
      questionProto.setCreatorId(this.creatorId);
    }
    if (this.updaterId != null) {
      questionProto.setUpdaterId(this.updaterId);
    }
    if (this.createDtm != null) {
      questionProto.setCreateDtm(this.createDtm.toString());
    }
    if (this.updateDtm != null) {
      questionProto.setUpdateDtm(this.updateDtm.toString());
    }

    return questionProto.build();
  }

  public Question setEntityByProto(NQaAdminQuestion questionProto) {
    this.id = questionProto.getId();
    this.answerId = questionProto.getAnswerId();
    this.answerCopyId = questionProto.getAnswerCopyId();
    this.question = questionProto.getQuestion();
    this.creatorId = questionProto.getCreatorId();
    this.updaterId = questionProto.getUpdaterId();

    return this;
  }
}
