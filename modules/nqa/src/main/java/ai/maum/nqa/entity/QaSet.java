package ai.maum.nqa.entity;

import lombok.Data;

@Data
public class QaSet {
    private int answerId;
    private String answer;
    private String question;
}
