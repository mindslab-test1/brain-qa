package ai.maum.nqa.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import maum.brain.qa.nqa.Admin.NQaAdminAnswer;
import maum.brain.qa.nqa.Admin.NQaAdminCategory;
import maum.brain.qa.nqa.Admin.NQaAdminLayer;

@Data
public class Answer {
  private int id;
  private int copyId;
  private String answer;
  private String answerView;
  private String source;
  private String summary;
  private int categoryId;
  private String categoryName;
  private String attr1;
  private String attr2;
  private String attr3;
  private String attr4;
  private String attr5;
  private String attr6;
  private int layer1Id;
  private String layer1Name;
  private int layer2Id;
  private String layer2Name;
  private int layer3Id;
  private String layer3Name;
  private int layer4Id;
  private String layer4Name;
  private int layer5Id;
  private String layer5Name;
  private int layer6Id;
  private String layer6Name;
  private List<String> tags;
  private String tempTags; // db에서 string으로 오는 tags를 임시로 담기위함
  private String creatorId;
  private String updaterId;
  private Date createDtm;
  private Date updateDtm;
  private int rownum;

  public NQaAdminAnswer getProto() {
    NQaAdminAnswer.Builder answerProto = NQaAdminAnswer.newBuilder();

    answerProto.setId(this.id);
    answerProto.setCopyId(this.copyId);
    answerProto.setAnswer(this.answer);
    if (this.answerView != null) {
      answerProto.setAnswerView(this.answerView);
    }
    if (this.source != null) {
      answerProto.setSrc(this.source);
    }

    if (this.summary != null) {
      answerProto.setSummary(this.summary);
    }
    answerProto.setCategoryId(this.categoryId);
    if (this.attr1 != null) {
      answerProto.setAttr1(this.attr1);
    }
    if (this.attr2 != null) {
      answerProto.setAttr2(this.attr2);
    }
    if (this.attr3 != null) {
      answerProto.setAttr3(this.attr3);
    }
    if (this.attr4 != null) {
      answerProto.setAttr4(this.attr4);
    }
    if (this.attr5 != null) {
      answerProto.setAttr5(this.attr5);
    }
    if (this.attr6 != null) {
      answerProto.setAttr6(this.attr6);
    }
    if (this.layer1Name != null) {
      answerProto.setLayer1(NQaAdminLayer.newBuilder()
          .setId(this.layer1Id)
          .setName(this.layer1Name)
          .build());
    } else {
      answerProto.setLayer1(NQaAdminLayer.newBuilder()
          .setId(this.layer1Id)
          .build());
    }
    if (this.layer2Name != null) {
      answerProto.setLayer2(NQaAdminLayer.newBuilder()
          .setId(this.layer2Id)
          .setName(this.layer2Name)
          .build());
    } else {
      answerProto.setLayer2(NQaAdminLayer.newBuilder()
          .setId(this.layer2Id)
          .build());
    }
    if (this.layer3Name != null) {
      answerProto.setLayer3(NQaAdminLayer.newBuilder()
          .setId(this.layer3Id)
          .setName(this.layer3Name)
          .build());
    } else {
      answerProto.setLayer3(NQaAdminLayer.newBuilder()
          .setId(this.layer3Id)
          .build());
    }
    if (this.layer4Name != null) {
      answerProto.setLayer4(NQaAdminLayer.newBuilder()
          .setId(this.layer4Id)
          .setName(this.layer4Name)
          .build());
    } else {
      answerProto.setLayer4(NQaAdminLayer.newBuilder()
          .setId(this.layer4Id)
          .build());
    }
    if (this.layer5Name != null) {
      answerProto.setLayer5(NQaAdminLayer.newBuilder()
              .setId(this.layer5Id)
              .setName(this.layer5Name)
              .build());
    } else {
      answerProto.setLayer5(NQaAdminLayer.newBuilder()
              .setId(this.layer5Id)
              .build());
    }
    if (this.layer6Name != null) {
      answerProto.setLayer6(NQaAdminLayer.newBuilder()
              .setId(this.layer6Id)
              .setName(this.layer6Name)
              .build());
    } else {
      answerProto.setLayer6(NQaAdminLayer.newBuilder()
              .setId(this.layer6Id)
              .build());
    }

    if (this.tags != null && this.tags.size() != 0) {
      for(String tag : this.tags) {
        answerProto.addTags(tag);
      }
    }
    if (this.creatorId != null) {
      answerProto.setCreatorId(this.creatorId);
    }
    if (this.updaterId != null) {
      answerProto.setUpdaterId(this.updaterId);
    }
    if (this.createDtm != null) {
      answerProto.setCreateDtm(this.createDtm.toString());
    }
    if (this.updateDtm != null) {
      answerProto.setUpdateDtm(this.updateDtm.toString());
    }
    answerProto.setRownum(this.rownum);

    return answerProto.build();
  }

  public Answer setEntityByProto(NQaAdminAnswer answerProto) {
    this.id = answerProto.getId();
    this.copyId = answerProto.getCopyId();
    this.answer = answerProto.getAnswer();
    this.answerView = answerProto.getAnswerView();
    this.source = answerProto.getSrc();
    this.summary = answerProto.getSummary();
    this.categoryId = answerProto.getCategoryId();
//    this.categoryName = answerProto.getCategory().getName();
    this.attr1 = answerProto.getAttr1();
    this.attr2 = answerProto.getAttr2();
    this.attr3 = answerProto.getAttr3();
    this.attr4 = answerProto.getAttr4();
    this.attr5 = answerProto.getAttr5();
    this.attr6 = answerProto.getAttr6();
    this.layer1Id = answerProto.getLayer1().getId();
    this.layer1Name = answerProto.getLayer1().getName();
    this.layer2Id = answerProto.getLayer2().getId();
    this.layer2Name = answerProto.getLayer2().getName();
    this.layer3Id = answerProto.getLayer3().getId();
    this.layer3Name = answerProto.getLayer3().getName();
    this.layer4Id = answerProto.getLayer4().getId();
    this.layer4Name = answerProto.getLayer4().getName();
    this.layer5Id = answerProto.getLayer5().getId();
    this.layer5Name = answerProto.getLayer5().getName();
    this.layer6Id = answerProto.getLayer6().getId();
    this.layer6Name = answerProto.getLayer6().getName();
    this.tags = new ArrayList<>();
    for (String tagProto : answerProto.getTagsList()) {
      this.tags.add(tagProto);
    }
    // [tag1, tag2]
    this.tempTags = answerProto.getTagsList().toString();
    this.tempTags = this.tempTags.substring(1, this.tempTags.length() - 1);
    this.creatorId = answerProto.getCreatorId();
    this.updaterId = answerProto.getUpdaterId();
    this.rownum = answerProto.getRownum();

    return this;
  }

  public void setTempTags(String tempTags) {
    // tempTags에 '태그명','태그명'  이런형태로 받아올 때 동시에
    // 이것을 , 로 split해서 tagList에 담음
    this.tempTags = tempTags;
    this.tags = new ArrayList<>();
    String[] tempTagsArr = this.tempTags.split(",");
    for (String tag: tempTagsArr) {
      this.tags.add(tag.trim());
    }
  }
}
