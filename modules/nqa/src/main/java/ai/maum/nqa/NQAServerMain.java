package ai.maum.nqa;

//import ai.maum.nqa.util.BuildQueryUtil;

import ai.maum.util.PropertiesManager;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import io.grpc.Server;
import io.grpc.netty.NettyServerBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class NQAServerMain {

  private static Properties properties = PropertiesManager.getProperties();

  private static Logger logger = LoggerFactory.getLogger(NQAServerMain.class);

  private Server server;

//  private BuildQueryUtil buildQueryUtil;

  public static void main(String[] args) {
    NQAServerMain qaServerMain = new NQAServerMain();
    if ("Y".equalsIgnoreCase(properties.getProperty("TLO_USE"))) {
      qaServerMain.setLog();
    }
    qaServerMain.execute();
    qaServerMain.blockUntilShutdown();
  }

  private void setLog() {
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    JoranConfigurator joranConfigurator = new JoranConfigurator();
    joranConfigurator.setContext(loggerContext);
    loggerContext.reset();
    try {
      joranConfigurator.doConfigure(PropertiesManager.resolveValueWithProVars(properties.getProperty("TLO_CONFIG_PATH")));

      FileAppender fileAppender = new FileAppender();
      fileAppender.setContext(loggerContext);
      fileAppender.setName("FILE");
      fileAppender.setFile(getLogFileName());

      PatternLayoutEncoder encoder = new PatternLayoutEncoder();
      encoder.setContext(loggerContext);
      encoder.setPattern("%msg%n");
      encoder.start();

      fileAppender.setEncoder(encoder);
      fileAppender.start();

      ch.qos.logback.classic.Logger logbackLogger = loggerContext.getLogger("TLO");
      logbackLogger.setLevel(Level.INFO);
      logbackLogger.addAppender(fileAppender);


      new ClassPathXmlApplicationContext("classpath:/spring/tlo-quartz.xml");
    } catch (JoranException e) {
      e.printStackTrace();
    }
    StatusPrinter.printInCaseOfErrorsOrWarnings(loggerContext);

  }

  private void execute() {
    try {
      logger.info("{}", "NQA Server Start");
      ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring/index-quartz.xml");
      initSchedule(context);
      long idleTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      long ageTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      int port = Integer.parseInt(properties.getProperty("QA_PORT", "50052"));

      server = NettyServerBuilder.forPort(port)
        .maxConnectionIdle(idleTimeout, TimeUnit.SECONDS)
        .maxConnectionAge(ageTimeout, TimeUnit.SECONDS)
        .addService(new NQAService(context))
        .addService(new NQaApiService())
        .addService(new NQaAdminService(context)).build().start();
      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          NQAServerMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private void initSchedule(ClassPathXmlApplicationContext context) {
    String schedulePath = PropertiesManager.resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
    if (StringUtils.isNotEmpty(schedulePath)) {
      File scheduleFile = new File(schedulePath);
      if (scheduleFile.exists() && scheduleFile.isFile()) {
        try {
          FileInputStream fileInputStream = new FileInputStream(scheduleFile);
          Properties scheduleProperties = new Properties();
          scheduleProperties.load(fileInputStream);
          ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
          CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
          StdScheduler stdScheduler = beanfactory
            .getBean("indexSchedulerFactoryBean", StdScheduler.class);
          String scheduleInfo = scheduleProperties.getProperty("schedule_info");
          if (StringUtils.isNotEmpty(scheduleInfo)) {
            cronTrigger.setCronExpression(scheduleInfo);
            stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
          }
          fileInputStream.close();
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        } catch (SchedulerException e) {
          e.printStackTrace();
        } catch (ParseException e) {
          e.printStackTrace();
        }
      } else {
        return;
      }
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  public String getLogFileName() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    Date now = new Date();
    String nowDT = simpleDateFormat.format(now);
    int min = Integer.parseInt(StringUtils.substring(nowDT, -2));
    int amount = min % 5;
    Date logDate = DateUtils.addMinutes(now, -amount);
    String dt = simpleDateFormat.format(logDate);
    String logFileName = properties.getProperty("TLO_FILE_PRE_FORMAT", "NQA.001.") + dt + ".log";
    String logPathName = PropertiesManager.resolveValueWithProVars(properties.getProperty("TLO_OUTPUT_PATH")) + File.separator + StringUtils.substring(dt, 0, 8);
    File logPath = new File(logPathName);
    if (!logPath.exists()) {
      logPath.mkdirs();
    }
    String fullPathName = logPathName + File.separator + logFileName;
    File logFile = new File(fullPathName);
    if (!logFile.exists()) {
      try {
        logFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return fullPathName;
  }
}
