package ai.maum.nqa.solrclient;

import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import maum.brain.qa.nqa.Nqa.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.FieldAnalysisRequest;
import org.apache.solr.client.solrj.response.AnalysisResponseBase.AnalysisPhase;
import org.apache.solr.client.solrj.response.AnalysisResponseBase.TokenInfo;
import org.apache.solr.client.solrj.response.FieldAnalysisResponse;
import org.apache.solr.client.solrj.response.FieldAnalysisResponse.Analysis;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SolrService {

  private static Logger logger = LoggerFactory.getLogger(SolrService.class);

  private CloudSolrClient cloudSolrClientQuestion;
  private CloudSolrClient cloudSolrClientAnswer;

  public SolrService(CloudSolrClient cloudSolrClientQuestion, CloudSolrClient cloudSolrClientAnswer) {
    this.cloudSolrClientQuestion = cloudSolrClientQuestion;
    this.cloudSolrClientAnswer = cloudSolrClientAnswer;
  }

  public CloudSolrClient getCloudSolrClientQuestion() {
    return cloudSolrClientQuestion;
  }

  public CloudSolrClient getCloudSolrClientAnswer() {
    return cloudSolrClientAnswer;
  }

  public String[] getSolrSynonymWord(String[] words) {
    try {
      FieldAnalysisRequest request = new FieldAnalysisRequest();
      request.addFieldName("question_morph");
      request.setFieldValue(StringUtils.join(words, " "));
      FieldAnalysisResponse response = request.process(cloudSolrClientQuestion);
      Analysis analysis = response.getFieldNameAnalysis("question_morph");
      Iterator<AnalysisPhase> ite = analysis.getIndexPhases().iterator();
      AnalysisPhase analysisPhase = null;
      while (ite.hasNext()) {
        analysisPhase = ite.next();
      }
      List<TokenInfo> tokenInfos = analysisPhase.getTokens();
      String[] wordArray = new String[tokenInfos.size()];
      for (int i = 0; i < tokenInfos.size(); i++) {
        wordArray[i] = tokenInfos.get(i).getText();
      }
      logger.info("Before Synonym => " + Arrays.toString(words));
      logger.info("After Synonym => " + Arrays.toString(wordArray));
      return wordArray;
    } catch (Exception e) {
      e.printStackTrace();
      return words;
    }
  }

  public String getSolrField(QueryTarget queryTarget) {
    String solrField = "";
    switch (queryTarget) {
      case Q:
      case WQ:
        solrField = "question_morph:";
        break;
      case GRAMQ:
      case WGRAMQ:
        solrField = "question_gram:";
        break;
      case A:
      case WA:
        solrField = "answer_morph:";
        break;
      case AID:
        solrField = "id:";
        break;
      case GRAMA:
      case WGRAMA:
        solrField = "answer_gram:";
        break;
      case NER:
        solrField = "ner:";
        break;
      case TAG:
        solrField = "tags_morph:";
        break;
      case ATTRALL:
        solrField = "attr:";
        break;
      case LAYERALL:
        solrField = "layer:";
        break;
    }
    return solrField;
  }

  private void joinAndQuery(StringBuffer qbuffer, String field, String value) {
    if (StringUtils.isNotEmpty(value)) {
      qbuffer.append(" AND " + field + ":\"" + value + "\"");
    }
  }

  private void joinCategoryQuery(StringBuffer qbuffer, List<String> categoryList) {
    if (categoryList.size() > 0) {
      qbuffer.append(buildCategoryQuery(categoryList));
    }
  }

  private String buildCategoryQuery(List<String> categoryArray) {
    return " AND (category:\"" + String.join("\" OR category:\"", categoryArray) + "\")";
  }

  private void joinAnswerIdyQuery(StringBuffer qbuffer, List<String> AnswerIdList) {
    if (AnswerIdList.size() > 0) {
      qbuffer.append(buildAnswerIdQuery(AnswerIdList));
    }
  }

  private String buildAnswerIdQuery(List<String> answerIdArray) {
    return " AND (answer_id:\"" + String.join("\" OR answer_id:\"", answerIdArray) + "\")";
  }

  private void joinNerQuery(StringBuffer qbuffer, List<QueryUnit> nerList) {
    if (nerList.size() > 0) {
      addNer(nerList, qbuffer);
    }
  }

  public void addNer(List<QueryUnit> nerList, StringBuffer qbuffer) {
    for (QueryUnit queryUnit : nerList) {
      qbuffer.append(" AND ner:\"" + queryUnit.getPhrase() + "\"");
      if (queryUnit.getWeight() != 0) {
        qbuffer.append("^" + String.valueOf(queryUnit.getWeight()));
      }
    }
  }

  private void buildAttrQuery(StringBuffer qbuffer, AttributeQueryUnit attribute) {
    joinAndQuery(qbuffer, "attr1", attribute.getAttr1().getPhrase());
    joinAndQuery(qbuffer, "attr2", attribute.getAttr2().getPhrase());
    joinAndQuery(qbuffer, "attr3", attribute.getAttr3().getPhrase());
    joinAndQuery(qbuffer, "attr4", attribute.getAttr4().getPhrase());
    joinAndQuery(qbuffer, "attr5", attribute.getAttr5().getPhrase());
    joinAndQuery(qbuffer, "attr6", attribute.getAttr6().getPhrase());
  }

  private void buildFilterAttrQuery(StringBuffer qbuffer, Attribute attribute) {
    joinFilterQuery(qbuffer, "attr1", attribute.getAttr1());
    joinFilterQuery(qbuffer, "attr2", attribute.getAttr2());
    joinFilterQuery(qbuffer, "attr3", attribute.getAttr3());
    joinFilterQuery(qbuffer, "attr4", attribute.getAttr4());
    joinFilterQuery(qbuffer, "attr5", attribute.getAttr5());
    joinFilterQuery(qbuffer, "attr6", attribute.getAttr6());
  }

  private void buildFilterLayerQuery(StringBuffer qbuffer, Layer layer) {
    joinFilterQuery(qbuffer, "layer1", layer.getLayer1());
    joinFilterQuery(qbuffer, "layer2", layer.getLayer2());
    joinFilterQuery(qbuffer, "layer3", layer.getLayer3());
    joinFilterQuery(qbuffer, "layer4", layer.getLayer4());
    joinFilterQuery(qbuffer, "layer5", layer.getLayer5());
    joinFilterQuery(qbuffer, "layer6", layer.getLayer6());
  }

  private void buildLayerQuery(StringBuffer qbuffer, LayerQueryUnit layer, String operator) {
    joinAndQuery(qbuffer, "layer1", layer.getLayer1().getPhrase());
    joinAndQuery(qbuffer, "layer2", layer.getLayer2().getPhrase());
    joinAndQuery(qbuffer, "layer3", layer.getLayer3().getPhrase());
    joinAndQuery(qbuffer, "layer4", layer.getLayer4().getPhrase());
    joinAndQuery(qbuffer, "layer5", layer.getLayer5().getPhrase());
    joinAndQuery(qbuffer, "layer6", layer.getLayer6().getPhrase());
  }

  private void buildTagQuery(StringBuffer qbuffer, List<QueryUnit> tags) {
    for (QueryUnit queryUnit : tags) {
      qbuffer.append(" AND tags:\"" + queryUnit.getPhrase() + "\"");
      if (queryUnit.getWeight() != 0) {
        qbuffer.append("^" + String.valueOf(queryUnit.getWeight()));
      }
    }
  }

  private void joinFilterQuery(StringBuffer qbuffer, String field, String value) {
    if (StringUtils.isNotEmpty(value)) {
      qbuffer.append(" -" + field + ":" + value);
    }
  }

  private void buildFilterQuery(StringBuffer qbuffer, Filter filter) {
    joinFilterQuery(qbuffer, "src", filter.getSrc());
    if (StringUtils.isNotEmpty(filter.getUserQuery())) {
      qbuffer.append(" -question_morph:" + filter.getUserQuery());
      qbuffer.append(" -answer:" + filter.getUserQuery());
    }
    if (filter.getTagsCount() != 0) {
      for (String tag : filter.getTagsList()) {
        qbuffer.append(" -tags:" + tag);
      }
    }
    if (filter.getNerCount() != 0) {
      for (String ner : filter.getNerList()) {
        qbuffer.append(" -ner:" + ner);
      }
    }
    buildFilterAttrQuery(qbuffer, filter.getAttribute());
    buildFilterLayerQuery(qbuffer, filter.getLayer());
  }

  private void expandQueryWords(String[] queryWords) {
    for (int i = 0; i < queryWords.length; i++) {
      String[] queryWordArray = queryWords[i].split(":");
      String[] fullQueryWord = new String[6];
      for (int j = 0; j < 6; j++) {
        fullQueryWord[j] = queryWordArray[0] + String.valueOf(j + 1) + ":" + queryWordArray[1];
      }
      queryWords[i] = "(" + StringUtils.join(fullQueryWord, " OR ") + ")";
    }
  }

  private void doDebugQuery(Struct.Builder allResult, Map<String, String> debugMap, QueryTarget queryTarget, HashSet<String> allIdResult, List<String> IdListByScore, float mmSize) {
    String solrField = getSolrField(queryTarget);
    if ("layer:".equalsIgnoreCase(solrField) || "attr:".equalsIgnoreCase(solrField)) {
      solrField = solrField.replace(":", "[1-6]:");
    }
    HashMap<String, String> wordIdMap = new HashMap<>();
    setIndexWordIdMap(debugMap, wordIdMap, solrField, allIdResult, IdListByScore, mmSize);
    logger.info("WordIdMap => " + wordIdMap.toString());
    for (Map.Entry<String, String> entry : wordIdMap.entrySet()) {
      allResult.putFields(entry.getKey(), Value.newBuilder().setStringValue(entry.getValue()).build());
    }
  }

  // 매칭된 단어 Map 처리
  // 최다 매칭 형태소 결과의 ID Set 만 리턴
  private void setIndexWordIdMap(Map<String, String> debugMap, Map<String, String> wordIdMap, String solrField, HashSet<String> allIdResult, List<String> IdListByScore, float mmSize) {
    boolean fFlag = true;
    int maxSize = 0;
    for (String Id : IdListByScore) {
      String debug = debugMap.get(Id);

      List<String> qTTempMorph = Arrays.asList(debug.split(solrField));
      List<String> qTempMorph = new ArrayList<>();

      for (int j = 1; j < qTTempMorph.size(); j++) {
        qTempMorph.add(qTTempMorph.get(j));
      }
      HashSet<String> qMorph = new HashSet<>();
      for (String morp : qTempMorph) {
        qMorph.add(morp.split("in")[0].trim());
      }
      int qMorphSize = qMorph.size();
      if (qMorphSize >= mmSize) {
        if (fFlag) {
          maxSize = qMorphSize;
          fFlag = false;
        } else {
          if (maxSize > qMorphSize) {
            break;
          }
        }
        String values = String.join(",", qMorph);

        if (wordIdMap.containsKey(values)) {
          wordIdMap.put(values, wordIdMap.get(values) + "," + Id);
        } else {
          wordIdMap.put(values, Id);
        }
        allIdResult.add(Id);
      } else {
        break;
      }
    }
  }

  private SolrQuery buildQuery(SearchQuestionRequest searchQuestionRequest, String[] words, String separator, HashMap<String, String> processLogMap) {
    String solrField = getSolrField(searchQuestionRequest.getQueryTarget());
    String[] fieldWords = new String[words.length];
    for (int i = 0; i < words.length; i++) {
      fieldWords[i] = solrField + words[i];
    }

    // TODO: query로 들어온 질문이 없을경우 처리??

    SolrQuery solrQuery = new SolrQuery();
    StringBuffer qbuffer = new StringBuffer("(((" + StringUtils.join(fieldWords, separator) + ")");

    qbuffer.append((" OR " + "(" + StringUtils.join(fieldWords, " OR ") + ")").replace("question_", "tags_"));

    if (searchQuestionRequest.getUserQuery().getWeight() != 0) {
      qbuffer.append("^" + String.valueOf(searchQuestionRequest.getUserQuery().getWeight()));
    }
    qbuffer.append(")");

    qbuffer.append(" AND (" + StringUtils.join(fieldWords, separator) + "))");

    joinAndQuery(qbuffer, "src", searchQuestionRequest.getSrc());
    joinAndQuery(qbuffer, "id", searchQuestionRequest.getId());
    joinAndQuery(qbuffer, "channel", searchQuestionRequest.getChannel());

    joinCategoryQuery(qbuffer, searchQuestionRequest.getCategoryList());
    joinAnswerIdyQuery(qbuffer, searchQuestionRequest.getAnswerIdList());
    joinNerQuery(qbuffer, searchQuestionRequest.getNerList());

    buildAttrQuery(qbuffer, searchQuestionRequest.getAttributes());

    buildTagQuery(qbuffer, searchQuestionRequest.getTagsList());

    buildFilterQuery(qbuffer, searchQuestionRequest.getFilter());

    logger.info("Build Query => " + qbuffer.toString());
    processLogMap.put("Solr_query", qbuffer.toString());

    int ntop = 100;
    if (searchQuestionRequest.getNtop() > 0) {
      ntop = searchQuestionRequest.getNtop();
    }

    solrQuery.set("q", qbuffer.toString());
    solrQuery.setFields("*", "score");
    solrQuery.addSort("score", ORDER.desc);
    solrQuery.set("group", "true");
    solrQuery.set("group.field", "answer_id");
    solrQuery.setRows(ntop);
    if (separator.equalsIgnoreCase(" AND ")) {
      solrQuery.setParam("defType", "edismax");
      solrQuery.setParam("mm", "100");
    }
    solrQuery.set("debugQuery", "on");
    return solrQuery;
  }

  private SolrQuery buildQuery(SearchAnswerRequest searchAnswerRequest, String[] wordArray, String separator, LinkedHashMap<String, String> processLogMap) {
    String solrField = getSolrField(searchAnswerRequest.getQueryTarget());
    for (int i = 0; i < wordArray.length; i++) {
      wordArray[i] = solrField + wordArray[i];
    }
    // TODO: attrall, layerall일 경우 처리
    if (searchAnswerRequest.getQueryTarget().equals(QueryTarget.ATTRALL) || searchAnswerRequest.getQueryTarget().equals(QueryTarget.LAYERALL)) {
      expandQueryWords(wordArray);
    }

    // TODO: query로 들어온 질문이 없을경우 처리??
//    if (answerWords.length==0) {
//      qbuffer = new StringBuffer("(answer_morph:*)");
//    } else {
//      qbuffer = new StringBuffer("(" + StringUtils.join(answerWords," OR ") + ")");
////        qbuffer = new StringBuffer("(" + "answer_morph:\"" + StringUtils.join(words," ") + "\")");

    SolrQuery solrQuery = new SolrQuery();
    StringBuffer qbuffer = new StringBuffer("(((" + StringUtils.join(wordArray, separator) + ")");

    // TODO: 가중치 어떤 상황에서 줄지 의사결정
    if (searchAnswerRequest.getQueryTarget() == QueryTarget.A || searchAnswerRequest.getQueryTarget() == QueryTarget.GRAMA) {
      qbuffer.append((" OR " + "(" + StringUtils.join(wordArray, " OR ") + ")").replace("answer_", "tags_"));
      if (searchAnswerRequest.getUserQuery().getWeight() != 0) {
        qbuffer.append("^" + String.valueOf(searchAnswerRequest.getUserQuery().getWeight()));
      }
    }

    qbuffer.append(")");
    qbuffer.append(" AND (" + StringUtils.join(wordArray, separator) + "))");

    joinAndQuery(qbuffer, "src", searchAnswerRequest.getSrc());
    joinAndQuery(qbuffer, "channel", searchAnswerRequest.getChannel());

    joinCategoryQuery(qbuffer, searchAnswerRequest.getCategoryList());
    joinNerQuery(qbuffer, searchAnswerRequest.getNerList());


    buildAttrQuery(qbuffer, searchAnswerRequest.getAttributes());

    buildLayerQuery(qbuffer, searchAnswerRequest.getLayer(), "AND");

    buildTagQuery(qbuffer, searchAnswerRequest.getTagsList());

    buildFilterQuery(qbuffer, searchAnswerRequest.getFilter());

    logger.info("ANSWER query : " + qbuffer.toString());
    processLogMap.put("Solr_query", qbuffer.toString());

    int ntop = 100;
    if (searchAnswerRequest.getNtop() > 0) {
      ntop = searchAnswerRequest.getNtop();
    }

    solrQuery.set("q", qbuffer.toString());
    solrQuery.setFields("*", "score");
    solrQuery.addSort("score", ORDER.desc);
    solrQuery.setRows(ntop);
    if (separator.equalsIgnoreCase(" AND ")) {
      solrQuery.setParam("defType", "edismax");
      solrQuery.setParam("mm", "100");
    }
    solrQuery.set("debugQuery", "on");
    return solrQuery;
  }

  public List<Group> getSolrResult(SearchQuestionRequest searchQuestionRequest, String[] words,
                                   String operator, HashMap<String, String> processLogMap, Struct.Builder allResult, HashSet<String> allIdResult) {
    List<Group> groupCommand = new ArrayList<>();
    try {
      SolrQuery solrQuery = buildQuery(searchQuestionRequest, words, operator, processLogMap);
      logger.info("Solr_Query => " + solrQuery.toString());
      QueryResponse queryResponse = cloudSolrClientQuestion.query(solrQuery);

      // TODO : debug쿼리 proto 파라미터로 사용(on.off)??

      Map<String, String> debugMap = queryResponse.getExplainMap();

      GroupResponse groupResponse = queryResponse.getGroupResponse();
      groupCommand = groupResponse.getValues().get(0).getValues();
      logger.info("All_documentList_size : " + groupCommand.size());
      processLogMap.put("Question_" + operator + "_result_all_count", String.valueOf(groupCommand.size()));

      List<String> qIdListByScore = new ArrayList<>();
      float score = searchQuestionRequest.getScore();
      for (Group group : groupCommand) {
        if (score > Float.valueOf(group.getResult().get(0).getFieldValue("score").toString())) {
          break;
        }
        qIdListByScore.add(String.valueOf(group.getResult().get(0).getFieldValue("id")));
      }

      // TODO : doDebugQuery(searchQuestionResponse, ...)

      doDebugQuery(allResult, debugMap, searchQuestionRequest.getQueryTarget(), allIdResult, qIdListByScore, getMmSize(words, searchQuestionRequest.getQueryTarget(), searchQuestionRequest.getMm()));
      processLogMap.put("Q_detail", "see details");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return groupCommand;
  }

  private float getMmSize(String[] words, QueryTarget queryTarget, float mm) {
    switch (queryTarget) {
      case A:
      case Q:
      case NER:
      case TAG:
      case ATTRALL:
      case LAYERALL:
        return words.length * mm;
      case GRAMA:
      case GRAMQ:
        int fullSize = 0;
        for (String word : words) {
          if (word.length() > 1) {
            fullSize += 2 * word.length() - 3;
          }
        }
        return fullSize * mm;
      case AID:
        return 0;
      default:
        return 0;
    }
  }

  public SolrDocumentList getSolrResult(SearchAnswerRequest searchAnswerRequest, String[] words, String operator,
                                        LinkedHashMap<String, String> processLogMap, Struct.Builder allResult, HashSet<String> allIdResult) {
    SolrDocumentList solrDocumentList = new SolrDocumentList();
    try {
      String[] wordArray;
      if (searchAnswerRequest.getQueryTarget() == QueryTarget.AID) {
        operator = " OR ";
        wordArray = searchAnswerRequest.getIdsList().toArray(new String[words.length]);
        processLogMap.put("idList_result", Arrays.toString(wordArray));
      } else {
        wordArray = getSolrSynonymWord(words);
        processLogMap.put("Synonym_result", Arrays.toString(wordArray));
      }

      SolrQuery solrQuery = buildQuery(searchAnswerRequest, wordArray, operator, processLogMap);

      logger.info("solrquery to string : " + solrQuery.toString());
      QueryResponse queryResponse = cloudSolrClientAnswer.query(solrQuery);

      // TODO : debug쿼리 proto 파라미터로 사용(on,off)??

      List<String> idList = new ArrayList<>();

      for (SolrDocument document : queryResponse.getResults()) {
        idList.add(document.get("id").toString());
      }

      Map<String, String> debugMap = queryResponse.getExplainMap();

      doDebugQuery(allResult, debugMap, searchAnswerRequest.getQueryTarget(), allIdResult, idList, getMmSize(words, searchAnswerRequest.getQueryTarget(), searchAnswerRequest.getMm()));

      processLogMap.put("A_detail", "see details");
      solrDocumentList = queryResponse.getResults();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return solrDocumentList;
  }

  public SolrDocumentList getExactSolrResult(SearchQuestionRequest searchQuestionRequest, HashMap<String, String> processLogMap) {
    SolrDocumentList solrDocumentList = new SolrDocumentList();
    try {
      StringBuffer query = new StringBuffer("question:\"");
      query.append(searchQuestionRequest.getUserQuery().getPhrase() + "\"");
      joinAndQuery(query, "src", searchQuestionRequest.getSrc());
      joinAndQuery(query, "channel", searchQuestionRequest.getChannel());
      buildAttrQuery(query, searchQuestionRequest.getAttributes());
      joinCategoryQuery(query, searchQuestionRequest.getCategoryList());
      joinNerQuery(query, searchQuestionRequest.getNerList());

      SolrQuery solrQuery = new SolrQuery();
      logger.info("Exact query : " + query.toString());
      processLogMap.put("Exact_query", query.toString());
      solrQuery.set("q", query.toString());
      solrQuery.setFields("*", "score");
      solrQuery.addSort("score", ORDER.desc);
      solrQuery.set("defType", "dismax");
      solrQuery.set("mm", 100);
      QueryResponse queryResponse = cloudSolrClientQuestion.query(solrQuery);
      solrDocumentList = queryResponse.getResults();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return solrDocumentList;
  }
}
