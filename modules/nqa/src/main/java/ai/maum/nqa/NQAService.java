package ai.maum.nqa;

import ai.maum.nqa.solrclient.SolrCollectionStock;
import ai.maum.nqa.word2vec.Word2VecModel;
import ai.maum.util.PropertiesManager;
import com.google.protobuf.Struct;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.nqa.NQaServiceGrpc.NQaServiceImplBase;
import maum.brain.qa.nqa.Nqa.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.*;

public class NQAService extends NQaServiceImplBase {

  private static final Properties properties = PropertiesManager.getProperties();
  private static final Logger logger = LoggerFactory.getLogger(NQAService.class);
  private static final Logger TLO = LoggerFactory.getLogger("TLO");
  private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
  private static final SolrCollectionStock solrCollections = SolrCollectionStock.getInstance();
  private Word2VecModel word2VecModel = null;

  private final boolean useTLO;
  private final ClassPathXmlApplicationContext context;

  public NQAService(ClassPathXmlApplicationContext context) {
    this.useTLO = StringUtils.equalsIgnoreCase(properties.getProperty("TLO_USE", "N"), "Y");
    this.context = context;
    this.word2VecModel = new Word2VecModel(properties.getProperty("W2V_PATH"),
            Integer.valueOf(properties.getProperty("W2V_NTOP", "5")),
            Float.valueOf(properties.getProperty("W2V_WEIGHT", "0.5")));
  }

  @Override
  public void searchQuestion(SearchQuestionRequest searchQuestionRequest, StreamObserver<SearchQuestionResponse> responseObserver) {
    Map<String, String> meta = new HashMap<>();
    LinkedHashMap<String, String> processLogMap = new LinkedHashMap<>();
    meta.putAll(searchQuestionRequest.getMetaMap());
    meta.put("req_time", simpleDateFormat.format(new Date()));
    meta.put("qa_engine", "NQA");
    SearchQuestionResponse.Builder resultQ = SearchQuestionResponse.newBuilder();
    try {
      logger.info("QueryTarget,Type => " + searchQuestionRequest.getQueryTarget().name() + "-" + searchQuestionRequest.getQueryType().name());
      logger.info("Input_Question : " + searchQuestionRequest.getUserQuery().getPhrase());
      resultQ.setUserQuery(searchQuestionRequest.getUserQuery().getPhrase());
      getSearchQuestion(searchQuestionRequest, resultQ, processLogMap);
      resultQ.putAllResultProcess(processLogMap);
      logger.info(processLogMap.toString());
      meta.put("result_code", "20000000");
    } catch (Exception e) {
      e.printStackTrace();
      meta.put("result_code", "40000103");
    }
    responseObserver.onNext(resultQ.build());
    responseObserver.onCompleted();
    meta.put("rsp_time", simpleDateFormat.format(new Date()));
    if (useTLO) {
      writeTLO(meta);
    }
  }

  private SearchQuestionResponse.Builder getSearchQuestion(SearchQuestionRequest searchQuestionRequest, SearchQuestionResponse.Builder searchQuestionResponse, HashMap<String, String> processLogMap)
    throws Exception {
    final QueryTarget queryTarget = searchQuestionRequest.getQueryTarget();
    final String channel = searchQuestionRequest.getChannel();
    final String phrase = searchQuestionRequest.getUserQuery().getPhrase();

    boolean useW2V = false;
    if (searchQuestionRequest.getQueryTarget() == QueryTarget.WQ ||
            searchQuestionRequest.getQueryTarget() == QueryTarget.WGRAMQ ||
            searchQuestionRequest.getQueryTarget() == QueryTarget.WA ||
            searchQuestionRequest.getQueryTarget() == QueryTarget.WGRAMA) {
      useW2V = true;
    }

    String question = queryTarget == QueryTarget.NER ?
      new NlpService().doNerNlp(phrase, channel) :
      new NlpService().doMorphNlp(phrase, channel, word2VecModel, useW2V);

    logger.info("NLP Result: {}", question);
    processLogMap.put("NLP_result", question);
//    String[] words = solrCollections
//      .getService(channel)
//      .getSolrSynonymWord(StringUtils.split(question, "||=||"));
    String[] words = StringUtils.split(question, "||=||");
    processLogMap.put("Synonym_result", Arrays.toString(words));

    if (words.length != 0 || searchQuestionRequest.getAnswerIdCount() > 0) {
      if (searchQuestionRequest.getQueryType() != QueryType.OR) {
        boolean isFind = doQuestionExactMatch(searchQuestionRequest, searchQuestionResponse, processLogMap);
        if (isFind) {  // Exact 매칭될 경우
          return searchQuestionResponse;
        }
        isFind = getSolrQuestionResult(searchQuestionRequest, searchQuestionResponse, words, " AND ", processLogMap);
        if (isFind) {  // AND 매칭될 경우
          return searchQuestionResponse;
        }
      }
      if (searchQuestionRequest.getQueryType() != QueryType.AND) {  // OR 매칭
        getSolrQuestionResult(searchQuestionRequest, searchQuestionResponse, words, " OR ", processLogMap);
      }
    }

    return searchQuestionResponse;
  }

  private boolean doQuestionExactMatch(SearchQuestionRequest searchQuestionRequest, SearchQuestionResponse.Builder searchQuestionResponse, HashMap<String, String> processLogMap) {
    try {
      float score = 0;
      if (StringUtils.isNotEmpty(String.valueOf(searchQuestionRequest.getScore()))) {
        score = searchQuestionRequest.getScore();
      }

      SolrDocumentList documentList = solrCollections
        .getService(searchQuestionRequest.getChannel())
        .getExactSolrResult(searchQuestionRequest, processLogMap);

      searchQuestionResponse.setUserQuery(String.valueOf(searchQuestionRequest.getUserQuery()));
      for (SolrDocument solrDocument : documentList) {
        if (Float.parseFloat(String.valueOf(solrDocument.get("score"))) >= score) {
          Question question = Question.newBuilder()
            .setId(getSolrValue(solrDocument, "id"))
            .setAnswerId(getSolrValue(solrDocument, "answer_id"))
            .setSrc(getSolrValue(solrDocument, "src"))
            .setChannel(getSolrValue(solrDocument, "channel"))
            .setCategory(getSolrValue(solrDocument, "category"))
            .setQuestion(getSolrValue(solrDocument, "question"))
            .setQuestionMorp(getSolrValue(solrDocument, "question_morph"))
            .setScore(Float.parseFloat(getSolrValue(solrDocument, "score")))
            .setAttribute(Attribute.newBuilder()
              .setAttr1(getSolrValue(solrDocument, "attr1"))
              .setAttr2(getSolrValue(solrDocument, "attr2"))
              .setAttr3(getSolrValue(solrDocument, "attr3"))
              .setAttr4(getSolrValue(solrDocument, "attr4"))
              .setAttr5(getSolrValue(solrDocument, "attr5"))
              .setAttr6(getSolrValue(solrDocument, "attr6")).build())
            .build();
          searchQuestionResponse.addIds(String.valueOf(solrDocument.get("answer_id")));
          searchQuestionResponse.addQuestions(question);
        }
      }
      processLogMap.put("Exact_result_answer_id", searchQuestionResponse.getIdsList().toString());
      if (searchQuestionResponse.getQuestionsCount() > 0) {
        return true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private boolean getSolrQuestionResult(SearchQuestionRequest searchQuestionRequest, SearchQuestionResponse.Builder searchQuestionResponse,
                                        String[] words, String operator, HashMap<String, String> processLogMap) {
    try {
      HashSet<String> allIdResult = new HashSet<>();
      Struct.Builder allResult = Struct.newBuilder();
      List<Group> groupCommand = solrCollections
        .getService(searchQuestionRequest.getChannel())
        .getSolrResult(searchQuestionRequest, words, operator, processLogMap, allResult, allIdResult);
      searchQuestionResponse.setAllResult(allResult);

      int i = 0;
      float score = searchQuestionRequest.getScore();
      float maxSize = 5.0F;

      // query nlp set
      List<String> queryWordList = new ArrayList<>();

      for (String word : words) {
        for (String splitWord : ((word.split("\\)\\^")[0]).replace("(","")).split(" ")) {
          String tempWrod = splitWord.split("\\^")[0];
          if(!queryWordList.contains(tempWrod)) {
            queryWordList.add(tempWrod);
          }
        }
      }

      if (searchQuestionRequest.getMaxSize() != 0.0) {
        maxSize = searchQuestionRequest.getMaxSize();
      }

      if (groupCommand.size() > 0) {
        for (Group group : groupCommand) {
          if (Float.parseFloat(getGroupValue(group, "score")) < score) {
            break;
          }
          if (!allIdResult.contains(getGroupValue(group, "id")) || Float.parseFloat(getGroupValue(group, "score")) < score) {
            break;
          }

          // index_mm filter
          List<String> indexWordList = new ArrayList<String>(Arrays.asList(getGroupValue(group, "question_morph").split(" ")));
          float indexFullLength = indexWordList.size();

//          logger.info("indexWordList==>" + indexWordList.toString());
//          logger.info("queryWordList==>" + queryWordList.toString());

          indexWordList.retainAll(queryWordList);
          if ((indexWordList.size()/indexFullLength) < searchQuestionRequest.getIndexMm()) {
            break;
          }

//          logger.info("question_size -> " + String.valueOf(Float.intBitsToFloat(words.length) / Float.intBitsToFloat(StringUtils.split(getGroupValue(group, "question_morph"), " ").length)));
          if ((Float.intBitsToFloat(words.length) / Float.intBitsToFloat(StringUtils.split(getGroupValue(group, "question_morph"), " ").length)) <= maxSize) {
            String id = (String.valueOf(group.getGroupValue()));
            Question question = Question.newBuilder()
              .setId(getGroupValue(group, "id"))
              .setAnswerId(id)
              .setSrc(getGroupValue(group, "src"))
              .setChannel(getGroupValue(group, "channel"))
              .setCategory(getGroupValue(group, "category"))
              .setQuestion(getGroupValue(group, "question"))
              .setQuestionMorp(getGroupValue(group, "question_morph"))
              .setScore(Float.parseFloat(getGroupValue(group, "score")))
              .setNer(getGroupValue(group, "ner"))
              .setAttribute(Attribute.newBuilder()
                .setAttr1(getGroupValue(group, "attr1"))
                .setAttr2(getGroupValue(group, "attr2"))
                .setAttr3(getGroupValue(group, "attr3"))
                .setAttr4(getGroupValue(group, "attr4"))
                .setAttr5(getGroupValue(group, "attr5"))
                .setAttr6(getGroupValue(group, "attr6")).build()).build();
            searchQuestionResponse.addIds(id);
            searchQuestionResponse.addQuestions(question);
            i++;
          }
        }
      }
      logger.info("return documentList size : " + String.valueOf(i));

      if (i > 0) {
        return true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public void searchAnswer(SearchAnswerRequest searchAnswerRequest, StreamObserver<SearchAnswerResponse> responseObserver) {
    Map<String, String> meta = new HashMap<>();
    LinkedHashMap<String, String> processLogMap = new LinkedHashMap<>();
    meta.putAll(searchAnswerRequest.getMetaMap());
    meta.put("req_time", simpleDateFormat.format(new Date()));
    meta.put("qa_engine", "NQA");
    SearchAnswerResponse.Builder resultAnswer = SearchAnswerResponse.newBuilder();
    try {
      logger.info("QueryTarget,Type => " + searchAnswerRequest.getQueryTarget().name() + "-" + searchAnswerRequest.getQueryType().name());
      logger.info("Answer ids => " + String.valueOf(searchAnswerRequest.getIdsList()));
      logger.info("input_Answer => " + searchAnswerRequest.getUserQuery().getPhrase());
      getSearchAnswer(searchAnswerRequest, resultAnswer, processLogMap);
      resultAnswer.putAllResultProcess(processLogMap);
      logger.info(processLogMap.toString());
      meta.put("result_code", "20000000");
    } catch (Exception e) {
      e.printStackTrace();
      meta.put("result_code", "40000103");
    }
    responseObserver.onNext(resultAnswer.build());
    responseObserver.onCompleted();
    meta.put("rsp_time", simpleDateFormat.format(new Date()));
    if (useTLO) {
      writeTLO(meta);
    }
  }

  private SearchAnswerResponse.Builder getSearchAnswer(SearchAnswerRequest searchAnswerRequest, SearchAnswerResponse.Builder searchAnswerResponse, LinkedHashMap<String, String> processLogMap)
    throws Exception {
    NlpService nlpService = new NlpService();

    boolean useW2V = false;
    if (searchAnswerRequest.getQueryTarget() == QueryTarget.WQ ||
            searchAnswerRequest.getQueryTarget() == QueryTarget.WGRAMQ ||
            searchAnswerRequest.getQueryTarget() == QueryTarget.WA ||
            searchAnswerRequest.getQueryTarget() == QueryTarget.WGRAMA) {
      useW2V = true;
    }

    String question = nlpService.doMorphNlp(searchAnswerRequest.getUserQuery().getPhrase(), searchAnswerRequest.getChannel(), word2VecModel, useW2V);
    logger.info("NLP Result: {}", question);
    processLogMap.put("NLP_result", question);
    String[] words = StringUtils.split(question, "||=||");

    if (searchAnswerRequest.getQueryType() == QueryType.ALL) {
      boolean isFind = getSolrAnswerResult(searchAnswerRequest, searchAnswerResponse, words, " AND ", processLogMap);
      if (!isFind) {
        getSolrAnswerResult(searchAnswerRequest, searchAnswerResponse, words, " OR ", processLogMap);
      }
    } else if (searchAnswerRequest.getQueryType() == QueryType.AND) {
      getSolrAnswerResult(searchAnswerRequest, searchAnswerResponse, words, " AND ", processLogMap);
    } else {
      getSolrAnswerResult(searchAnswerRequest, searchAnswerResponse, words, " OR ", processLogMap);
    }

    return searchAnswerResponse;
  }

  private boolean getSolrAnswerResult(SearchAnswerRequest searchAnswerRequest, SearchAnswerResponse.Builder searchAnswerResponse,
                                      String[] words, String operator, LinkedHashMap<String, String> processLogMap) {
    try {
      Struct.Builder allResult = Struct.newBuilder();
      HashSet<String> allIdResult = new HashSet<>();
      SolrDocumentList documentList = solrCollections
        .getService(searchAnswerRequest.getChannel())
        .getSolrResult(searchAnswerRequest, words, operator, processLogMap, allResult, allIdResult);
      searchAnswerResponse.setAllResult(allResult);

      int i = 0;
      logger.info("documentList size : " + documentList.size());
      processLogMap.put("Answer_" + operator + "_result_all_count", String.valueOf(documentList.size()));
      if (documentList.size() > 0) {
        for (SolrDocument solrDocument : documentList) {
          if (i >= searchAnswerRequest.getNtop() ||
            !allIdResult.contains(String.valueOf(solrDocument.get("id")))) {
            break;
          }
          Answer answer = Answer.newBuilder()
            .setId(getSolrValue(solrDocument, "id"))
            .setSrc(getSolrValue(solrDocument, "src"))
            .setChannel(getSolrValue(solrDocument, "channel"))
            .setCategory(getSolrValue(solrDocument, "category"))
            .setAnswer(getSolrValue(solrDocument, "answer"))
            .setAnswerView(getSolrValue(solrDocument, "answer_view"))
            .setScore(Float.parseFloat(getSolrValue(solrDocument, "score")))
            .setNer(getSolrValue(solrDocument, "ner"))
            .setAttributes(Attribute.newBuilder()
              .setAttr1(getSolrValue(solrDocument, "attr1"))
              .setAttr2(getSolrValue(solrDocument, "attr2"))
              .setAttr3(getSolrValue(solrDocument, "attr3"))
              .setAttr4(getSolrValue(solrDocument, "attr4"))
              .setAttr5(getSolrValue(solrDocument, "attr5"))
              .setAttr6(getSolrValue(solrDocument, "attr6")).build())
            .setLayers(Layer.newBuilder()
              .setLayer1(getSolrValue(solrDocument, "layer1"))
              .setLayer2(getSolrValue(solrDocument, "layer2"))
              .setLayer3(getSolrValue(solrDocument, "layer3"))
              .setLayer4(getSolrValue(solrDocument, "layer4"))
              .setLayer5(getSolrValue(solrDocument, "layer5"))
              .setLayer6(getSolrValue(solrDocument, "layer6")).build())
            .setSummary(getSolrValue(solrDocument, "summary"))
            .addAllTags(Arrays.asList(getSolrValue(solrDocument, "tags").split(",")))
            .build();
          searchAnswerResponse.addAnswerResult(answer);
          i++;
        }
        return true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    processLogMap.put("Answer_result_ids", "");
    return false;
  }

  @Override
  public void checkQuestion(SearchQuestionRequest searchQuestionRequest, StreamObserver<CheckQuestionResponse> responseObserver) {
    LinkedHashMap<String, String> processLogMap = new LinkedHashMap<>();

    SearchQuestionResponse.Builder resultQ = SearchQuestionResponse.newBuilder();
    try {
      logger.info("QueryTarget,Type => " + searchQuestionRequest.getQueryTarget().name() + "-" + searchQuestionRequest.getQueryType().name());
      logger.info("Input_Question : " + searchQuestionRequest.getUserQuery().getPhrase());
      resultQ.setUserQuery(searchQuestionRequest.getUserQuery().getPhrase());
      getSearchQuestion(searchQuestionRequest, resultQ, processLogMap);
      resultQ.putAllResultProcess(processLogMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    CheckQuestionResponse.Builder checkQuestionResponse = CheckQuestionResponse.newBuilder();
    checkQuestionResponse.setNlpResult(resultQ.getResultProcessMap().get("NLP_result"));
    checkQuestionResponse.addAllQuestions(resultQ.getQuestionsList());
    checkQuestionResponse.addWords("test");
    checkQuestionResponse.setSolrQuery(resultQ.getResultProcessMap().get("Solr_query"));
    checkQuestionResponse.setNqaQuery(searchQuestionRequest.getQueryTarget().name() + "-" + searchQuestionRequest.getQueryType().name());
    checkQuestionResponse.setResultMap(resultQ.getAllResult());

    responseObserver.onNext(checkQuestionResponse.build());
    responseObserver.onCompleted();
  }

  @Override
  public void checkAnswer(SearchAnswerRequest searchAnswerRequest, StreamObserver<CheckAnswerResponse> responseObserver) {
    LinkedHashMap<String, String> processLogMap = new LinkedHashMap<>();

    SearchAnswerResponse.Builder resultAnswer = SearchAnswerResponse.newBuilder();
    try {
      logger.info("QueryTarget,Type => " + searchAnswerRequest.getQueryTarget().name() + "-" + searchAnswerRequest.getQueryType().name());
      logger.info("Answer ids => " + String.valueOf(searchAnswerRequest.getIdsList()));
      logger.info("input_Answer => " + searchAnswerRequest.getUserQuery().getPhrase());
      getSearchAnswer(searchAnswerRequest, resultAnswer, processLogMap);
      resultAnswer.putAllResultProcess(processLogMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    CheckAnswerResponse.Builder checkAnswerResponse = CheckAnswerResponse.newBuilder();
    checkAnswerResponse.setNlpResult(resultAnswer.getResultProcessMap().get("NLP_result"));
    checkAnswerResponse.addAllAnswerResult(resultAnswer.getAnswerResultList());
    checkAnswerResponse.addWords("test");
    checkAnswerResponse.setSolrQuery(resultAnswer.getResultProcessMap().get("Solr_query"));
    checkAnswerResponse.setNqaQuery(searchAnswerRequest.getQueryTarget().name() + "-" + searchAnswerRequest.getQueryType().name());
    checkAnswerResponse.setResultMap(resultAnswer.getAllResult());

    responseObserver.onNext(checkAnswerResponse.build());
    responseObserver.onCompleted();
  }

  private String getGroupValue(Group group, String field) {
    return String.valueOf(group.getResult().get(0).getFieldValue(field));
  }

  private String getSolrValue(SolrDocument solrDocument, String field) {
    return String.valueOf(solrDocument.get(field));
  }

  private String checkNull(String value) {
    return StringUtils.isEmpty(value) ? "" : value;
  }

  private void writeTLO(Map<String, String> meta) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    StringBuffer buffer = new StringBuffer();
    buffer.append("SEQ_ID=");
    buffer.append(checkNull(meta.get("seq_id")));
    buffer.append("|");
    buffer.append("LOG_TIME=");
    buffer.append(checkNull(simpleDateFormat.format(new Date())));
    buffer.append("|");
    buffer.append("LOG_TYPE=");
    buffer.append(checkNull(meta.get("log_type")));
    buffer.append("|");
    buffer.append("SID=");
    buffer.append(checkNull(meta.get("sid")));
    buffer.append("|");
    buffer.append("RESULT_CODE=");
    buffer.append(checkNull(meta.get("result_code")));
    buffer.append("|");
    buffer.append("REQ_TIME=");
    buffer.append(checkNull(meta.get("req_time")));
    buffer.append("|");
    buffer.append("RSP_TIME=");
    buffer.append(checkNull(meta.get("rsp_time")));
    buffer.append("|");
    buffer.append("CLIENT_IP=");
    buffer.append(checkNull(meta.get("client_ip")));
    buffer.append("|");
    buffer.append("DEV_INFO=");
    buffer.append(checkNull(meta.get("dev_info")));
    buffer.append("|");
    buffer.append("OS_INFO=");
    buffer.append(checkNull(meta.get("os_info")));
    buffer.append("|");
    buffer.append("NW_INFO=");
    buffer.append(checkNull(meta.get("nw_info")));
    buffer.append("|");
    buffer.append("SVC_NAME=");
    buffer.append(checkNull(meta.get("svc_name")));
    buffer.append("|");
    buffer.append("DEV_MODEL=");
    buffer.append(checkNull(meta.get("dev_model")));
    buffer.append("|");
    buffer.append("CARRIER_TYPE=");
    buffer.append(checkNull(meta.get("carrier_type")));
    buffer.append("|");
    buffer.append("TR_ID=");
    buffer.append(checkNull(meta.get("transaction_id")));
    buffer.append("|");
    buffer.append("MSG_ID=");
    buffer.append(checkNull(meta.get("message_id")));
    buffer.append("|");
    buffer.append("FROM_SVC_NAME=");
    buffer.append(checkNull(meta.get("from_svc_name")));
    buffer.append("|");
    buffer.append("TO_SVC_NAME=");
    buffer.append(checkNull(meta.get("to_svc_name")));
    buffer.append("|");
    buffer.append("SVC_TYPE=");
    buffer.append(checkNull(meta.get("svc_type")));
    buffer.append("|");
    buffer.append("DEV_TYPE=");
    buffer.append(checkNull(meta.get("dev_type")));
    buffer.append("|");
    buffer.append("DEVICE_TOKEN=");
    buffer.append(checkNull(meta.get("device_token")));
    buffer.append("|");
    buffer.append("QA_ENGINE=");
    buffer.append(checkNull(meta.get("qa_engine")));
    TLO.info(buffer.toString());
  }
}
