package ai.maum.nqa.index;

import ai.maum.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.nqa.NQaAdminServiceGrpc;
import maum.brain.qa.nqa.NQaAdminServiceGrpc.NQaAdminServiceBlockingStub;
import maum.brain.qa.nqa.Admin.IndexingRequest;
import maum.brain.qa.nqa.Admin.IndexStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component(value = "indexTask")
public class IndexTask {

  private static Logger logger = LoggerFactory.getLogger(IndexTask.class);

  private static Properties properties = PropertiesManager.getProperties();

//  public void doIndexing() {
//    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",
//        Integer.parseInt(properties.getProperty("QA_PORT", "50052"))).usePlaintext().build();
//    NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
//    IndexingRequest indexingInput = IndexingRequest.newBuilder().setClean(false).build();
//    IndexStatus status = stub.additionalIndexing(indexingInput);
//    logger.info("Indexing Schedule Status: {}", status.getStatus());
//    try {
//      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
//  }
}
