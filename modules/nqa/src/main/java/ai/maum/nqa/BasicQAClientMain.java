package ai.maum.nqa;

import com.google.protobuf.InvalidProtocolBufferException;
//import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.nqa.NQaServiceGrpc;
import maum.brain.qa.nqa.NQaServiceGrpc.NQaServiceBlockingStub;
import maum.brain.qa.nqa.Nqa.QueryTarget;
import maum.brain.qa.nqa.Nqa.QueryType;
import maum.brain.qa.nqa.Nqa.QueryUnit;
import maum.brain.qa.nqa.Nqa.SearchAnswerRequest;
import maum.brain.qa.nqa.Nqa.SearchAnswerResponse;

public class BasicQAClientMain {

  private ManagedChannel channel;


  public static void main(String[] args) {
    BasicQAClientMain qaClientMain = new BasicQAClientMain();
    qaClientMain.connect();
  }

  private void connect() {
//    channel = ManagedChannelBuilder.forAddress("10.122.64.73", 50052).usePlaintext().build();
//    NQaServiceBlockingStub stub = NQaServiceGrpc.newBlockingStub(channel);
//    SearchAnswerRequest searchAnswerRequest = SearchAnswerRequest.newBuilder()
//        .setUserQuery(QueryUnit.newBuilder().setPhrase("통장").build())
//        .setQueryType(QueryType.ALL).build();
//    SearchAnswerResponse searchAnswerResponse = stub.searchAnswer(searchAnswerRequest);
//    try {
//      System.out.println(JsonFormat.printer().print(searchAnswerResponse));
//    } catch (InvalidProtocolBufferException e) {
//      e.printStackTrace();
//    }
//    try {
//      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
  }
}
