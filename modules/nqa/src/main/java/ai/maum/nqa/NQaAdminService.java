package ai.maum.nqa;

import ai.maum.nqa.entity.*;
import ai.maum.nqa.solrclient.SolrCollectionStock;
import ai.maum.nqa.solrclient.SolrService;
import ai.maum.util.PropertiesManager;
import ai.maum.util.SqlSessionManager;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.nqa.Admin.*;
import maum.brain.qa.nqa.Admin.AnswerList.QaSet;
import maum.brain.qa.nqa.Admin.RemoveAnswerRequest.AnswerIdSet;
import maum.brain.qa.nqa.NQaAdminServiceGrpc.NQaAdminServiceImplBase;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class NQaAdminService extends NQaAdminServiceImplBase {

  private static final Properties properties = PropertiesManager.getProperties();

  private static final Logger logger = LoggerFactory.getLogger(NQaAdminService.class);

  private static final String qLockObj = "qLOCK";
  private static final String aLockObj = "aLOCK";

  private final ClassPathXmlApplicationContext context;
  private final SqlSessionManager sqlSessionManager;

  private CloudSolrClient cloudSolrClientQuestion;
  private CloudSolrClient cloudSolrClientAnswer;

  private DocumentIndexingHandler documentIndexingHandlerQuestion;
  private DocumentIndexingHandler documentIndexingHandlerAnswer;

  private IndexingCore indexingCoreQuestion;
  private IndexingCore indexingCoreAnswer;

  public NQaAdminService(ClassPathXmlApplicationContext context) {
    this.context = context;
    this.sqlSessionManager = SqlSessionManager.getInstance();
  }

  @Override
  public void indexing(IndexingRequest request, StreamObserver<IndexStatus> responseObserver) {
    String channel;
    {
      int channelId = request.getChannelId();
      Channel channelResult = selectChannelById(channelId);
      if (channelResult == null) {
        Exception e = new NullPointerException("Channel ID[" + channelId + "] is not exist.");
        logger.error("indexing FAIL {} => ", e.getMessage(), e);
        responseObserver.onError(e);
        return;
      }
      channel = channelResult.getName();
    }
    SolrService service = SolrCollectionStock.getInstance().getService(channel);
    this.cloudSolrClientQuestion = service.getCloudSolrClientQuestion();
    this.cloudSolrClientAnswer = service.getCloudSolrClientAnswer();
    this.documentIndexingHandlerQuestion = new DocumentIndexingHandler(cloudSolrClientQuestion);
    this.documentIndexingHandlerAnswer = new DocumentIndexingHandler(cloudSolrClientAnswer);

    if (request.getCollectionType() != CollectionType.ANSWER) {
      indexingCoreQuestion = indexingTask(
        request.toBuilder().setCollectionType(CollectionType.QUESTION).build(),
        cloudSolrClientQuestion, documentIndexingHandlerQuestion, responseObserver, qLockObj);
    }
    if (request.getCollectionType() != CollectionType.QUESTION) {
      indexingCoreAnswer = indexingTask(
        request.toBuilder().setCollectionType(CollectionType.ANSWER).build(),
        cloudSolrClientAnswer, documentIndexingHandlerAnswer, responseObserver, aLockObj);
    }
    getStatus(responseObserver);
  }

  private IndexingCore indexingTask(IndexingRequest request,
                                    CloudSolrClient cloudSolrClient,
                                    DocumentIndexingHandler documentIndexingHandler,
                                    StreamObserver<IndexStatus> responseObserver,
                                    String lockObj) {
    try {

      if (documentIndexingHandler.isIndexing()) {
        getStatus(responseObserver);
        return null;
      }

      documentIndexingHandler.setIndexing(true);
      IndexingCore indexingCore = new IndexingCore(request, documentIndexingHandler,
        cloudSolrClient,
        checkIndexTimeFile(request), lockObj);
      indexingCore.start();

      Thread.sleep(10);
      return indexingCore;
    } catch (Exception e) {
      logger.error("indexingTask Failed:", e);
      responseObserver.onError(e);
      return null;
    }
  }

  private Properties checkIndexTimeFile(IndexingRequest indexingRequest) throws Exception {
    if (indexingRequest.getIndexType() == IndexType.ADD) {
      String indexingFilePath = PropertiesManager
        .resolveValueWithProVars(properties.getProperty("INDEXING_TIME_FILE_PATH"));
      logger.info("Indexing FIle: {}", indexingFilePath);
      if (StringUtils.isEmpty(indexingFilePath)) {
        logger.warn("INDEXING_FILE_PATH is not defined");
        throw new Exception("INDEXING_FILE_PATH is not defined");
      } else {
        FileInputStream fileInputStream = new FileInputStream(indexingFilePath);
        Properties indexingProperties = new Properties();
        indexingProperties.load(fileInputStream);
        return indexingProperties;
      }
    }
    return null;
  }

  @Override
  public void getIndexingStatus(Empty request, StreamObserver<IndexStatus> responseObserver) {
    logger.info("getIndexingStatus");
    try {
      getStatus(responseObserver);
    } catch (Exception e) {
      logger.error("getIndexingStatus Failed:", e);
      responseObserver.onError(e);
    }
  }

  private void getStatus(StreamObserver<IndexStatus> responseObserver) {
    synchronized (qLockObj) {
      synchronized (aLockObj) {
        boolean isIndexing = documentIndexingHandlerQuestion.isIndexing() || documentIndexingHandlerAnswer.isIndexing();
        int total = documentIndexingHandlerQuestion.getTotal() + documentIndexingHandlerAnswer.getTotal();
        int fetched = documentIndexingHandlerQuestion.getFetched() + documentIndexingHandlerAnswer.getFetched();
        int processed = documentIndexingHandlerQuestion.getProcessed() + documentIndexingHandlerAnswer.getProcessed();

        logger.info("getStatus ::: {}, {} / {}", fetched, processed, total);
        IndexStatus searchStatus = IndexStatus.newBuilder()
          .setStatus(isIndexing)
          .setTotal(total)
          .setFetched(fetched)
          .setProcessed(processed).build();
        responseObserver.onNext(searchStatus);
        responseObserver.onCompleted();
      }
    }
  }

//  private void getStatus(DocumentIndexingHandler documentIndexingHandler,
//      StreamObserver<IndexStatus> responseObserver) {
//    IndexStatus searchStatus = IndexStatus.newBuilder()
//        .setStatus(documentIndexingHandler.isIndexing())
//        .setTotal(documentIndexingHandler.getTotal())
//        .setFetched(documentIndexingHandler.getFetched())
//        .setProcessed(documentIndexingHandler.getProcessed()).build();
//    responseObserver.onNext(searchStatus);
//    responseObserver.onCompleted();
//  }

  @Override
  public void abortIndexing(IndexingRequest indexingInput,
                            StreamObserver<IndexStatus> responseObserver) {
    logger.info("abortIndexing request : {}", indexingInput);
    try {
      boolean isQuestionIndexing = indexingCoreQuestion != null && indexingCoreQuestion.isAlive();
      boolean isAnswerIndexing = indexingCoreAnswer != null && indexingCoreAnswer.isAlive();
      if (isQuestionIndexing || isAnswerIndexing) {
        if (isQuestionIndexing) {
          logger.info("abort Question indexing");
          documentIndexingHandlerQuestion.setIndexing(false);
          indexingCoreQuestion.interrupt();
          indexingCoreQuestion.stop();
        }
        if (isAnswerIndexing) {
          logger.info("abort Answer indexing");
          documentIndexingHandlerAnswer.setIndexing(false);
          indexingCoreAnswer.interrupt();
          indexingCoreAnswer.stop();
        }
        IndexStatus searchStatus = IndexStatus.newBuilder().setStatus(false).setTotal(0)
          .setFetched(0).setProcessed(0).setMessage("Indexing Abort Success").build();
        responseObserver.onNext(searchStatus);
        responseObserver.onCompleted();
      } else {
        IndexStatus searchStatus = IndexStatus.newBuilder().setStatus(false).setTotal(0)
          .setFetched(0).setProcessed(0).setMessage("Indexing Already Aborted").build();
        responseObserver.onNext(searchStatus);
        responseObserver.onCompleted();
      }
    } catch (Exception e) {
      IndexStatus searchStatus = IndexStatus.newBuilder().setStatus(false).setTotal(0)
        .setFetched(0).setProcessed(0).setMessage("Indexing Abort Error: " + e.getMessage())
        .build();
      responseObserver.onNext(searchStatus);
      responseObserver.onCompleted();
    }
  }

  @Override
  public void getIndexedKeywords(GetIndexedKeywordsRequest getIndexWordsRequest,
                                 StreamObserver<GetIndexedKeywordsResponse> responseObserver) {
    logger.info("getIndexedKeywords request : {}", getIndexWordsRequest.toString());

    try {
      LinkedHashMap<String, Long> wordMap = new LinkedHashMap<>();
      getSolrKeywords(getIndexWordsRequest, wordMap);

      GetIndexedKeywordsResponse getIndexedKeywordsResponse = GetIndexedKeywordsResponse
        .newBuilder()
        .putAllIndexWords(wordMap).build();

      responseObserver.onNext(getIndexedKeywordsResponse);
      responseObserver.onCompleted();
    } catch (Exception e) {
      responseObserver.onError(e);
    }
  }

  private void getSolrKeywords(GetIndexedKeywordsRequest getIndexedKeywordsRequest,
                               LinkedHashMap<String, Long> wordMap) {
    SolrQuery solrQuery = new SolrQuery();
    if (0 != getIndexedKeywordsRequest.getCategoryId()
      && 0 != getIndexedKeywordsRequest.getChannelId()) {
      int categoryId = getIndexedKeywordsRequest.getCategoryId();
      int channelId = getIndexedKeywordsRequest.getChannelId();
      SqlSession sqlSession = sqlSessionManager.getSqlSession();
      try {
        solrQuery.set("q",
          "category:\"" + sqlSession.selectOne("nqa.selectCategoryName", categoryId) + "\"" +
            " AND channel:\"" + sqlSession.selectOne("nqa.selectChannelName", channelId) + "\"");
      } catch (Exception e) {
        logger.error("sqlSession.selectCategoryName FAIL {} => ", e.getMessage(), e);
        throw e;
      } finally {
        sqlSession.close();
      }
    } else {
      solrQuery.set("q", "*:*");
    }
    solrQuery.setRows(0);
    solrQuery.setFacet(true);
    solrQuery.setFacetMinCount(1);
    final String section = getIndexedKeywordsRequest.getSection().toString();
    solrQuery.addFacetField(setFacetField(section.toLowerCase()));
    solrQuery.setFacetLimit(getIndexedKeywordsRequest.getNtop());
    CloudSolrClient cloudSolrClient = getSolrClient(section);

    try {
      QueryResponse queryResponse = cloudSolrClient.query(solrQuery);
      logger.info(queryResponse.toString());
      List<FacetField> facetFieldList = queryResponse.getFacetFields();
      for (FacetField facetField : facetFieldList) {
        List<Count> countList = facetField.getValues();
        for (Count count : countList) {
          if (wordMap.containsKey(count.getName())) {
            Long wordCount = wordMap.get(count.getName());
            wordMap.put(count.getName(), wordCount + count.getCount());
          } else {
            wordMap.put(count.getName(), count.getCount());
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private String setFacetField(String section) {
    if ("question".equalsIgnoreCase(section)) {
      return "question_morph";
    } else if ("answer".equalsIgnoreCase(section)) {
      return "answer_morph";
    } else {
      return section;
    }
  }

  private CloudSolrClient getSolrClient(String section) {
    if ("question".equalsIgnoreCase(section)) {
      return cloudSolrClientQuestion;
    } else {
      return cloudSolrClientAnswer;
    }
  }

  @Override
  public void updateSynonyms(Empty request, StreamObserver<Empty> responseObserver) {
    // todo
  }

  @Override
  public void setIndexingSchedule(IndexingSchedule request,
                                  StreamObserver<IndexingSchedule> responseObserver) {
    ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
    CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
    StdScheduler stdScheduler = beanfactory
      .getBean("indexSchedulerFactoryBean", StdScheduler.class);
    try {
      cronTrigger.setCronExpression(request.getCron());
      stdScheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
      String schedulePath = PropertiesManager
        .resolveValueWithProVars(properties.getProperty("SCHEDULE_FILE_PATH"));
      logger.info("schedulePath: {}", schedulePath);
      if (StringUtils.isNotEmpty(schedulePath)) {
        File scheduleFile = new File(schedulePath);
        Properties newScheduleProperties = new Properties();
        newScheduleProperties.setProperty("schedule_info", request.getCron());
        FileOutputStream fileOutputStream = new FileOutputStream(scheduleFile);
        newScheduleProperties.store(fileOutputStream, "Schedule Info");
        fileOutputStream.close();
      }
      responseObserver.onNext(request);
      responseObserver.onCompleted();
    } catch (ParseException | SchedulerException | IOException e) {
      e.printStackTrace();
      responseObserver.onError(e);
      responseObserver.onCompleted();
    }
  }

  @Override
  public void getIndexingSchedule(Empty request,
                                  StreamObserver<IndexingSchedule> responseObserver) {
    logger.info("getIndexingSchedule.");
    try {
      ConfigurableListableBeanFactory beanfactory = context.getBeanFactory();
      CronTriggerImpl cronTrigger = beanfactory.getBean("indexCronTrigger", CronTriggerImpl.class);
      IndexingSchedule scheduleInfo = IndexingSchedule.newBuilder()
        .setCron(cronTrigger.getCronExpression())
        .build();
      responseObserver.onNext(scheduleInfo);
      responseObserver.onCompleted();
    } catch (Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  // CRUD

  @Override
  public void getChannelList(Empty request, StreamObserver<ChannelList> responseObserver) {
    getChannelListTask(responseObserver);
  }

  private void getChannelListTask(StreamObserver<ChannelList> responseObserver) {

    SqlSession sqlSession = sqlSessionManager.getSqlSession();
    try {
      List<Channel> channelListResult = sqlSession.selectList("nqa.selectChannelList");
      logger.info("@#@# getChannelListTask channelListResult :: {}", channelListResult);
      ChannelList.Builder channelList = ChannelList.newBuilder();
      for (Channel channel : channelListResult) {
        channelList.addChannels(channel.getProto());
      }
      responseObserver.onNext(channelList.build());
      responseObserver.onCompleted();

    } catch (Exception e) {
      logger.error("sqlSession.selectChannelList FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getChannelById(NQaAdminChannel request,
                             StreamObserver<NQaAdminChannel> responseObserver) {
    getChannelByIdTask(request, responseObserver);
  }

  private Channel selectChannelById(int channelId) {
    SqlSession sqlSession = sqlSessionManager.getSqlSession();
    Channel channelResult = sqlSession.selectOne("nqa.selectChannelById", channelId);
    sqlSession.close();

    return channelResult;
  }

  private void getChannelByIdTask(NQaAdminChannel request,
                                  StreamObserver<NQaAdminChannel> responseObserver) {
    logger.info("@#@# getChannelByIdTask request [{}]", request);
    int channelId = request.getId();

    try {
      Channel channelResult = selectChannelById(channelId);
      logger.info("@#@# getChannelByIdTask channelEntity :: {}", channelResult);

      if (channelResult == null) {
        logger.info("Channel ID[" + channelId + "] is not exist.");
        responseObserver.onNext(null);
      } else {
        responseObserver.onNext(channelResult.getProto());
      }
      responseObserver.onCompleted();

    } catch (Exception e) {
      logger.error("sqlSession.selectChannelById FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    }
  }

  @Override
  public void addChannel(NQaAdminChannel request,
                         StreamObserver<NQaAdminChannel> responseObserver) {
    addChannelTask(request, responseObserver);
  }

  private void addChannelTask(NQaAdminChannel request,
                              StreamObserver<NQaAdminChannel> responseObserver) {
    logger.info("@#@# addChannelTask request :: {}", request);
    Channel channelEntity = new Channel();
    channelEntity.setEntityByProto(request);
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      int retCate = sqlSession.insert("nqa.insertChannel", channelEntity);
      logger.info("#@ addChannelTask retCate [{}] channelEntity [{}]", retCate, channelEntity);
      sqlSession.commit();
      responseObserver.onNext(channelEntity.getProto());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.insertChannel FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }

  }

  @Override
  public void editChannel(NQaAdminChannel request,
                          StreamObserver<NQaAdminChannel> responseObserver) {
    editChannelTask(request, responseObserver);
  }

  private void editChannelTask(NQaAdminChannel request,
                               StreamObserver<NQaAdminChannel> responseObserver) {
    logger.info("@#@# editChannelTask request :: {}", request);
    Channel channelEntity = new Channel();
    channelEntity.setEntityByProto(request);
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      sqlSession.update("nqa.updateChannel", channelEntity);
      sqlSession.commit();
      responseObserver.onNext(channelEntity.getProto());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.updateChannel FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void removeChannel(RemoveChannelRequest request,
                            StreamObserver<RemoveChannelResponse> responseObserver) {
    removeChannelTask(request, responseObserver);
  }

  private void removeChannelTask(RemoveChannelRequest request,
                                 StreamObserver<RemoveChannelResponse> responseObserver) {
    int channelId = request.getId();
    logger.info("@#@# removeChannelTask RemoveChannelRequest :: {}", channelId);
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      // get channelName for Deleting Indexed Data
      String channelName = sqlSession.selectOne("nqa.selectChannelName", channelId);

      int totalCnt = sqlSession.delete("nqa.deleteChannel", channelId);

      int totalCCnt = 0;
      int totalACnt = 0;
      int totalQCnt = 0;

      List<Category> categoryList =
        sqlSession.selectList("nqa.selectCategoryListByChannelId", channelId);

      for (Category category : categoryList) {
        int categoryId = category.getId();
        int cnt = sqlSession.delete("nqa.deleteCategory", categoryId);

        List<Answer> answerList = sqlSession.selectList("nqa.selectAnswerList", categoryId);
        for (Answer answer : answerList) {
          Map<String, Integer> params = new HashMap<>();
          int answerId = answer.getId();
          int answerCopyId = answer.getCopyId();

          params.put("answerId", answerId);
          params.put("answerCopyId", answerCopyId);

          int aCnt = sqlSession.delete("nqa.deleteAnswer", params);
          int qCnt = sqlSession.delete("nqa.deleteQuestionByAnswerId", params);

          totalACnt += aCnt;
          totalQCnt += qCnt;

        }
        // Delete Indexed Data
        deleteIndexedData(cloudSolrClientAnswer, channelName, "");
        deleteIndexedData(cloudSolrClientQuestion, channelName, "");

        totalCCnt += cnt;
      }
      sqlSession.commit();

      logger.debug("===== removed Channel Count : {}", totalCnt);
      logger.debug("===== removed Category Count : {}", totalCCnt);
      logger.debug("===== removed Answer Count : {}", totalACnt);
      logger.debug("===== removed Question Count : {}", totalQCnt);

      RemoveChannelResponse.Builder removeChannelResponse = RemoveChannelResponse.newBuilder();
      removeChannelResponse.setCount(1);
      responseObserver.onNext(removeChannelResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("removeChannel FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getCategoryListByChannelId(NQaAdminChannel request,
                                         StreamObserver<CategoryList> responseObserver) {
    getCategoryListByChannelIdTask(request, responseObserver);
  }

  private void getCategoryListByChannelIdTask(NQaAdminChannel request,
                                              StreamObserver<CategoryList> responseObserver) {
    logger.info("@#@# getCategoryListByChannelIdTask request :: {}", request);
    int channelId = request.getId();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      List<Category> categoryListResult
        = sqlSession.selectList("nqa.selectCategoryListByChannelId", channelId);
      logger.info("@#@# getCategoryListByChannelIdTask categoryListResult count:: {}", categoryListResult.size());
      logger.debug("@#@# getCategoryListByChannelIdTask categoryListResult :: {}", categoryListResult);

      CategoryList.Builder categoryList = CategoryList.newBuilder();
      for (int i = 0; i < categoryListResult.size(); i++) {
        categoryList.addCategories(categoryListResult.get(i).getProto());
      }
      responseObserver.onNext(categoryList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.selectCategoryListByChannelId FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getCategoryListByName(maum.brain.qa.nqa.Admin.NQaAdminCategory request,
                                    StreamObserver<CategoryList> responseObserver) {
    getCategoryListByNameTask(request, responseObserver);
  }

  private void getCategoryListByNameTask(maum.brain.qa.nqa.Admin.NQaAdminCategory request,
                                         StreamObserver<CategoryList> responseObserver) {
    logger.info("@#@# getCategoryListByNameTask request [{}]", request);

    Category category = new Category();
    if (request.getChannelId() != 0) {
      category.setChannelId(request.getChannelId());
    }
    category.setName(request.getName());
    category.setPageIndex(request.getPageIndex());
    category.setPageSize(request.getPageSize());
    logger.info("@#@# getCategoryListByNameTask category [{}]", category);
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      List<Category> categoryListResult = sqlSession
        .selectList("nqa.selectCategoryListByName", category);
      logger.info("@#@# getCategoryListByNameTask categoryListResult count :: {}", categoryListResult.size());
      logger.debug("@#@# getCategoryListByNameTask categoryListResult :: {}", categoryListResult);
      CategoryList.Builder categoryList = CategoryList.newBuilder();
      for (int i = 0; i < categoryListResult.size(); i++) {
        categoryList.addCategories(categoryListResult.get(i).getProto());
      }
      responseObserver.onNext(categoryList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.selectCategoryListByName FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getCategoryById(GetCategoryByIdRequest request,
                              StreamObserver<GetCategoryByIdResponse> responseObserver) {
    getCategoryByIdTask(request, responseObserver);
  }

  private void getCategoryByIdTask(GetCategoryByIdRequest request,
                                   StreamObserver<GetCategoryByIdResponse> responseObserver) {
    logger.info("@#@# getCategoryByIdTask GetCategoryByIdRequest :: {}", request);
    int categoryId = request.getId();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      Category categoryEntity = sqlSession.selectOne("nqa.selectCategoryById", categoryId);
      logger.info("@#@# getCategoryByIdTask categoryEntity :: {}", categoryEntity);
      GetCategoryByIdResponse.Builder getCategoryByIdResponse = GetCategoryByIdResponse
        .newBuilder();
      getCategoryByIdResponse.setCategory(categoryEntity.getProto());
      responseObserver.onNext(getCategoryByIdResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.selectCategoryById FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void addCategory(AddCategoryRequest request,
                          StreamObserver<AddCategoryResponse> responseObserver) {
    addCategoryTask(request, responseObserver);
  }

  private void addCategoryTask(AddCategoryRequest request,
                               StreamObserver<AddCategoryResponse> responseObserver) {
    logger.info("@#@# addCategoryTask request :: {}", request);
    Category categoryEntity = new Category();
    categoryEntity.setEntityByProto(request.getCategory());
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      int retCate = sqlSession.insert("nqa.insertCategory", categoryEntity);
      logger.info("#@ addCategoryTask retCate [{}] categoryEntity [{}]", retCate, categoryEntity);
      sqlSession.commit();
      AddCategoryResponse.Builder addCategoryResponse = AddCategoryResponse.newBuilder();
      addCategoryResponse.setCategory(categoryEntity.getProto());
      responseObserver.onNext(addCategoryResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.insertCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }


  @Override
  public void editCategory(EditCategoryRequest request,
                           StreamObserver<EditCategoryResponse> responseObserver) {
    editCategoryTask(request, responseObserver);
  }

  private void editCategoryTask(EditCategoryRequest request,
                                StreamObserver<EditCategoryResponse> responseObserver) {
    logger.info("@#@# editCategoryTask request :: {}", request);
    Category categoryEntity = new Category();
    categoryEntity.setEntityByProto(request.getCategory());
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      sqlSession.update("nqa.updateCategory", categoryEntity);
      sqlSession.commit();
      EditCategoryResponse.Builder editCategoryResponse = EditCategoryResponse.newBuilder();
      editCategoryResponse.setCategory(categoryEntity.getProto());
      responseObserver.onNext(editCategoryResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.updateCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void removeCategory(RemoveCategoryRequest request,
                             StreamObserver<RemoveCategoryResponse> responseObserver) {
    removeCategoryTask(request, responseObserver);
  }

  private void removeCategoryTask(RemoveCategoryRequest request,
                                  StreamObserver<RemoveCategoryResponse> responseObserver) {
    logger.info("@#@# removeCategoryTask RemoveCategoryRequest :: {}", request);

    int totalCnt = 0;
    int totalACnt = 0;
    int totalQCnt = 0;

    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {

      for (int i = 0; i < request.getIdCount(); i++) {
        int categoryId = request.getId(i); // TODO 한꺼번에 삭제하도록 쿼리 변경 필요
        Category categoryEntity = sqlSession.selectOne("nqa.selectCategoryById", categoryId);
        int channelId = categoryEntity.getChannelId();
        // get channelName, categoryName for Deleting Indexed Data
        String channelName = sqlSession.selectOne("nqa.selectChannelName", channelId);
        String categoryName = sqlSession.selectOne("nqa.selectCategoryName", categoryId);

        // Delete Category
        int cnt = sqlSession.delete("nqa.deleteCategory", categoryId);
        // Delete Answer, Question
        List<Answer> answerList = sqlSession.selectList("nqa.selectAnswerList", categoryId);
        for (Answer answer : answerList) {
          Map<String, Integer> params = new HashMap<>();
          int answerId = answer.getId();
          int answerCopyId = answer.getCopyId();

          params.put("answerId", answerId);
          params.put("answerCopyId", answerCopyId);

          int aCnt = sqlSession.delete("nqa.deleteAnswer", params);
          int qCnt = sqlSession.delete("nqa.deleteQuestionByAnswerId", params);

          totalACnt += aCnt;
          totalQCnt += qCnt;

        }
        // Delete Indexed Data
        deleteIndexedData(cloudSolrClientAnswer, channelName, "");
        deleteIndexedData(cloudSolrClientQuestion, channelName, "");

        totalCnt += cnt;
        logger.info("=== remove Category '{}' In Channel '{}'", channelName, categoryName);
      }
      sqlSession.commit();

      logger.debug("===== removed Category Count : {}", totalCnt);
      logger.debug("===== removed Answer Count : {}", totalACnt);
      logger.debug("===== removed Question Count : {}", totalQCnt);

      RemoveCategoryResponse.Builder removeCategoryResponse = RemoveCategoryResponse.newBuilder();
      removeCategoryResponse.setCount(totalCnt);
      responseObserver.onNext(removeCategoryResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("removeCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getLayerListByCategory(NQaAdminCategory request,
                                     StreamObserver<LayerList> responseObserver) {
    getLayerListByCategoryTask(request, responseObserver);
  }

  private void getLayerListByCategoryTask(NQaAdminCategory request,
                                          StreamObserver<LayerList> responseObserver) {
    int categoryId = request.getId();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      List<Layer> layerListResult = sqlSession
        .selectList("nqa.getLayerListByCategory", categoryId);
      logger.info("@#@# getLayerListByCategoryTask layerListResult :: {}", layerListResult);
      LayerList.Builder layerList = LayerList.newBuilder();
      for (int i = 0; i < layerListResult.size(); i++) {
        layerList.addLayers(layerListResult.get(i).getProto());
      }
      responseObserver.onNext(layerList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("sqlSession.getLayerListByCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getAnswerListByCategory(GetAnswerListByCategoryRequest request,
                                      StreamObserver<AnswerList> responseObserver) {
    getAnswerListByCategoryTask(request, responseObserver); // TODO 페이징 추가
  }

  private void getAnswerListByCategoryTask(GetAnswerListByCategoryRequest request,
                                           StreamObserver<AnswerList> responseObserver) {
    HashMap<String, String> param = new HashMap<>();
    Answer answer = new Answer();
    answer.setCategoryId(request.getCategoryId());
    int categoryId = request.getCategoryId();
    int startRow = request.getStartRow();
    int endRow = request.getEndRow();
    String sortModelBefore = request.getSortModel();
    String sortModel = sortModelBefore.replaceAll("\\.", "");
    String sortType = request.getSortType();

    if (!request.getSearchListList().isEmpty()) {
      for (int i = 0; i < request.getSearchListList().get(0).getSearchFiledList().size(); i++) {
        String searchField = request.getSearchList(0).getSearchFiled(i);
        String searchFieldAfter = searchField.replaceAll("\\.", "");
        String searchValue = request.getSearchList(0).getSearchValue(i);
        param.put(searchFieldAfter, searchValue);
      }
    }

    SqlSession sqlSession = sqlSessionManager.getSqlSession();
    param.put("categoryId", Integer.toString(categoryId));

    try {
      int answerListConut = sqlSession.selectOne("nqa.pagingCount", param);
      int questionCount = sqlSession.selectOne("nqa.questionCount", categoryId);
      logger.info("answerListCount : " + answerListConut);
      logger.info("questionCount : " + questionCount);
      logger.info("startRow : " + startRow);
      logger.info("endRow : " + endRow);

      // endRow가 -1인 경우 카테고리의 전체 Answer List를 가져오도록 한다. (=> Excel Download)
      if (endRow == -1) {
        logger.info("getAllAnswerListByCategory (NO PAGINATION) : " + answerListConut);
        startRow = 0;
        endRow = answerListConut;
      }

      param.put("startRowNo", Integer.toString(startRow));
      param.put("endRowNo", Integer.toString(endRow));


      param.put("sortModel", sortModel);
      param.put("sortType", sortType);
      List<Answer> answerListResult = sqlSession
        .selectList("nqa.selectAnswerListPagination", param);

      logger.info("@#@# getAnswerListTask answerListResult count: {}", answerListResult.size());
      logger.debug("@#@# getAnswerListTask answerListResult :: {}", answerListResult);
      AnswerList.Builder answerList = AnswerList.newBuilder();
      answerList.setTotalCount(answerListConut);
      answerList.setQuestionCount(questionCount);

      for (int i = 0; i < answerListResult.size(); i++) {
        QaSet.Builder qaSet = QaSet.newBuilder();
        // 전체 리스트를 조회할때만 여러 tag를 가져오면 sql returns more than 1 row 에러 때문에
        // GROUP_CONCAT 등을 이용해

        qaSet.setAnswer(answerListResult.get(i).getProto());
        // question 조회
        Map<String, Integer> params = new HashMap<>();
        params.put("answerId", answerListResult.get(i).getId());
        params.put("answerCopyId", answerListResult.get(i).getCopyId());

        List<Question> questionList
          = sqlSession.selectList("nqa.selectQuestionByAnswerIdAndCopyId", params);
        for (int j = 0; j < questionList.size(); j++) {
          qaSet.addQuestions(questionList.get(j).getProto());
        }
        answerList.addQaSets(qaSet.build());
      }
      responseObserver.onNext(answerList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("getAnswerListByCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getQAsetByCategory(getQAsetByCategoryRequest request,
                                      StreamObserver<AnswerList> responseObserver) {
    getQAsetByCategoryTask(request, responseObserver);
  }

  public void getQAsetByCategoryTask(getQAsetByCategoryRequest request,
                                 StreamObserver<AnswerList> responseObserver) {

    HashMap<String, String> param = new HashMap<>();
    int categoryId = request.getCategoryId();
    AnswerList.Builder answerList = AnswerList.newBuilder();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();
    param.put("categoryId", Integer.toString(categoryId));

    try {
      int answerListConut = sqlSession.selectOne("nqa.pagingCount", param);
      int questionCount = sqlSession.selectOne("nqa.questionCount", categoryId);

      answerList.setTotalCount(answerListConut);
      answerList.setQuestionCount(questionCount);

      List<ai.maum.nqa.entity.QaSet> QaSets
              = sqlSession.selectList("nqa.selectQaByCategory", categoryId);

      for (ai.maum.nqa.entity.QaSet qaSet : QaSets) {
        QaSet.Builder qaItem = QaSet.newBuilder();
        qaItem.setAnswer(NQaAdminAnswer.newBuilder()
                .setId(qaSet.getAnswerId()).setAnswer(qaSet.getAnswer()).build());
        qaItem.addQuestions(NQaAdminQuestion.newBuilder().setQuestion(qaSet.getQuestion()).build());
        answerList.addQaSets(qaItem);
      }

      responseObserver.onNext(answerList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("getQAsetByCategory FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void getAnswerById(GetAnswerByIdRequest request,
                            StreamObserver<AnswerList> responseObserver) {
    getAnswerByIdTask(request, responseObserver);
  }

  private void getAnswerByIdTask(GetAnswerByIdRequest request,
                                 StreamObserver<AnswerList> responseObserver) {
    logger.info("@#@# getAnswerByIdTask request :: {}", request);
    int answerId = request.getId();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      // answer를 answerId로 조회함
      List<Answer> answerEntities = sqlSession
        .selectList("nqa.selectAnswerById", answerId);
      logger.info("@#@# getAnswerByIdTask answerEntities count :: {}", answerEntities.size());
      logger.debug("@#@# getAnswerByIdTask answerEntities :: {}", answerEntities);

      // answerId로 question List 조회함
      List<Question> questionEntities = sqlSession
        .selectList("nqa.selectQuestionByAnswerId", answerId);
      logger.info("@#@# getAnswerByIdTask questionEntities count :: {}", questionEntities.size());
      logger.debug("@#@# getAnswerByIdTask questionEntities :: {}", questionEntities);

      AnswerList.Builder answerList = AnswerList.newBuilder();
      for (Answer answerEntity : answerEntities) {
        // answerId, answerCopyId 가 같은 q, a 가 하나의 qaSet이 됨.
        QaSet.Builder qaSet = QaSet.newBuilder();
        qaSet.setAnswer(answerEntity.getProto());
        for (Question question : questionEntities) {
          if (question.getAnswerCopyId() == answerEntity.getCopyId()) {
            qaSet.addQuestions(question.getProto());
          }
        }
        answerList.addQaSets(qaSet.build());
      }
      responseObserver.onNext(answerList.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("getAnswerById FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  public void addAnswer(AddAnswerRequest request,
                        StreamObserver<AddAnswerResponse> responseObserver) {
    addAnswerTask(request, responseObserver);
  }

  private void addAnswerTask(AddAnswerRequest request,
                             StreamObserver<AddAnswerResponse> responseObserver) {
    logger.info("@#@# addAnswerTask request :: {}", request);

    // answer 등록
    NQaAdminAnswer answer = request.getAnswer();
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      HashMap<String, Integer> answerIdMap = insertAnswer(answer, sqlSession);

      AddAnswerResponse.Builder addAnswerResponse = AddAnswerResponse.newBuilder();
      addAnswerResponse.setCount(1);
      // id, copyId 값 포함된 answer로 교체
      answer = answer.toBuilder()
              .setId(answerIdMap.get("answerId"))
              .setCopyId(answerIdMap.get("copyId"))
              .build();
      addAnswerResponse.setAnswer(answer);

      // answer의 question 등록
      for (int i = 0; i < request.getQuestionsCount(); i++) {
        NQaAdminQuestion question = request.getQuestions(i);
        insertQuestion(question, answerIdMap, sqlSession);
        // id 값 포함된 question으로 교체
        if (answerIdMap.containsKey("questionId") && answerIdMap.get("questionId") != null) {
          question = question.toBuilder().setId(answerIdMap.get("questionId")).build();
        }
        addAnswerResponse.addQuestions(i, question);
      }
      sqlSession.commit();

      responseObserver.onNext(addAnswerResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("addAnswer FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  @Override
  @Transactional
  public void uploadAnswerList(UploadAnswerListRequest request,
                               StreamObserver<UploadAnswerListResponse> responseObserver) {
    logger.info("start upload");
    uploadAnswerListTask(request, responseObserver);
  }

  private void uploadAnswerListTask(UploadAnswerListRequest request,
                                    StreamObserver<UploadAnswerListResponse> responseObserver) {
    int totalCount = request.getTotalCount();
    int processedCount = 0;
    int successCount = 0;

    UploadAnswerListResponse.Builder uploadAnswerListResponse = UploadAnswerListResponse
      .newBuilder();
    int categoryId = request.getCategoryId();

    try {
      clearDbByCategory(categoryId);

      for (SingleQaSet singleQaSet : request.getQaSetsList()) {  // 모든 QA_Set에 대하여 업로드
        try {
          if (singleQaSet.getAnswer().getCategoryId() == categoryId) {
            insertQaSet(singleQaSet);  // Upload single QA
            successCount += 1;
          } else {
            throw new Exception("category error");
          }
        } catch (Exception e) {
          e.printStackTrace();
          uploadAnswerListResponse.addFailedQaSets(singleQaSet);
        }
        processedCount += 1;
        logger.info("Upload_Status : Total=>" + totalCount + " Process=>"
          + processedCount + " Success=>" + successCount);
      }
      logger.info("Upload_Result : Total=>" + totalCount + " Process=>"
        + processedCount + " Success=>" + successCount);
      uploadAnswerListResponse.setSuccessCount(successCount);
      responseObserver.onNext(uploadAnswerListResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      responseObserver.onError(e);
    }
  }

  private void clearDbByCategory(int categoryId) {
    logger.info("targetCategory=>" + categoryId);

    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      sqlSession.delete("nqa.deleteQuestionByCategory", categoryId);
      sqlSession.delete("nqa.deleteAnswerByCategory", categoryId);
      sqlSession.delete("nqa.deleteLayerByCategory", categoryId);
      sqlSession.commit();
    } catch (Exception e) {
      logger.error("clearDbByCategory FAIL {} => ", e.getMessage(), e);
      throw e;
    } finally {
      sqlSession.close();
    }
  }

  private void insertQaSet(SingleQaSet singleQaSet) {
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {
      // insert Answer task
      HashMap<String, Integer> answerIdMap = insertAnswer(singleQaSet.getAnswer(), sqlSession);
      // insert Question task
      insertQuestion(singleQaSet.getQuestions(), answerIdMap, sqlSession);
      sqlSession.commit();
    } catch (Exception e) {
      logger.error("insertQaSet FAIL {} => ", e.getMessage(), e);
      throw e;
    } finally {
      sqlSession.close();
    }
  }

  private void insertQuestion(NQaAdminQuestion nQaAdminQuestion,
                              HashMap<String, Integer> answerIdMap, SqlSession sqlSession) {
    // todo: 동일한 question 중복 정책?
    Question questionEntity = new Question();
    questionEntity.setEntityByProto(nQaAdminQuestion);
    questionEntity.setAnswerId(answerIdMap.get("answerId"));
    questionEntity.setAnswerCopyId(answerIdMap.get("copyId"));

    sqlSession.insert("insertQuestion", questionEntity);

    answerIdMap.put("questionId", questionEntity.getId());
  }

  private HashMap<String, Integer> insertAnswer(NQaAdminAnswer nQaAdminAnswer,
                                                SqlSession sqlSession) {
    Answer answerEntity = new Answer();
    HashMap<String, Integer> idMap = new HashMap<>();

    //Answer에 관한 정보 다 파싱하여 answerEntity에 저장함
    answerEntity.setEntityByProto(nQaAdminAnswer);
    answerEntity.setLayer1Id(getLayerId(nQaAdminAnswer.getLayer1(), sqlSession));
    answerEntity.setLayer2Id(getLayerId(nQaAdminAnswer.getLayer2(), sqlSession));
    answerEntity.setLayer3Id(getLayerId(nQaAdminAnswer.getLayer3(), sqlSession));
    answerEntity.setLayer4Id(getLayerId(nQaAdminAnswer.getLayer4(), sqlSession));
    answerEntity.setLayer5Id(getLayerId(nQaAdminAnswer.getLayer5(), sqlSession));
    answerEntity.setLayer6Id(getLayerId(nQaAdminAnswer.getLayer6(), sqlSession));

    List<Answer> answerList = sqlSession.selectList("selectAnswerId", answerEntity);
    if (answerList.size() > 0) { // 답변이 이미 있다면
      idMap = getAnswerIdCopyId(answerList, answerEntity);  //id, copy_id 체크
      answerEntity.setId(idMap.get("answerId"));
      answerEntity.setCopyId(idMap.get("copyId"));
    }
    if (!idMap.containsKey("exist")) {  // layer순열과 답변이 다 같은게 없는경우(Answer를 추가해야 하는 경우)
      if (!idMap.containsKey("copyId") || idMap.get("copyId") == 0) {
        sqlSession.insert("nqa.insertAnswer", answerEntity);
      } else {
        sqlSession.insert("nqa.insertAnswerById", answerEntity);
      }
      idMap.put("answerId", answerEntity.getId());
      if (!idMap.containsKey("copyId")) {
        idMap.put("copyId", 0);
      }
    }
    return idMap;
  }

  // layer를 비교하여 DB에 넣을 answerId, copyid 리턴
  private HashMap<String, Integer> getAnswerIdCopyId(List<Answer> answerList, Answer answerEntity) {
    HashMap<String, Integer> idMap = new HashMap<>();
    idMap.put("answerId", answerList.get(0).getId());
    for (Answer answer : answerList) {
      if (checkAnswerLayer(answer, answerEntity)) {
        idMap.put("copyId", answer.getCopyId());
        idMap.put("exist", 1);
        return idMap;
      }
    }
    int maxCopyId = 0;
    for (Answer answer : answerList) {
      if (maxCopyId < answer.getCopyId()) {
        maxCopyId = answer.getCopyId();
      }
    }
    idMap.put("copyId", maxCopyId + 1);
    return idMap;
  }

  private boolean checkAnswerLayer(Answer answer, Answer answerEntity) {
    return ((answer.getLayer1Id() == answerEntity.getLayer1Id()) &&
      (answer.getLayer2Id() == answerEntity.getLayer2Id()) &&
      (answer.getLayer3Id() == answerEntity.getLayer3Id()) &&
      (answer.getLayer4Id() == answerEntity.getLayer4Id()) &&
      (answer.getLayer5Id() == answerEntity.getLayer5Id()) &&
      (answer.getLayer6Id() == answerEntity.getLayer6Id()));
  }

  private int getLayerId(NQaAdminLayer nQaAdminLayer, SqlSession sqlSession) {
    if (nQaAdminLayer.getId() != 0) {
      return nQaAdminLayer.getId();
    }
    // layer 값이 없어도 null이 아니므로 id, name이 모두 없을 때로 구분한다.
    if (nQaAdminLayer.getName().isEmpty()) {
      return 0;
    }
    Layer layerEntity = new Layer();
    layerEntity.setEntityByProto(nQaAdminLayer);
    layerEntity = sqlSession.selectOne("nqa.selectLayer", layerEntity);
    if (layerEntity == null) {
      layerEntity = new Layer();
      layerEntity.setEntityByProto(nQaAdminLayer);
      sqlSession.insert("nqa.insertLayer", layerEntity);
    }
    //Layer가 있으면 Map에서 ID 반환
    //없으면 DB에 넣고 해당 ID 반환
    return layerEntity.getId();
  }

  @Override
  @Transactional
  public void editAnswer(EditAnswerRequest request,
                         StreamObserver<AnswerList> responseObserver) {
    editAnswerTask(request, responseObserver);
  }

  private void editAnswerTask(EditAnswerRequest request,
                              StreamObserver<AnswerList> responseObserver) {
    logger.info("@#@# editAnswerTask request :: {}", request);

    SqlSession sqlSession = sqlSessionManager.getSqlSession();
    Answer answerEntity = new Answer();
    // 추후 for문으로 upgrade (다중 copyId 동시에 edit 가능하도록)
    EditAnswerRequest.EditQaSet editQaSet = request.getEditQaSets(0);
    answerEntity.setEntityByProto(editQaSet.getAnswer());
    int answerId = answerEntity.getId();

    // 1. answer str이 바뀌었을 시 기존에 있는 str인지 비교
    //    기존에 있으면 copyId + 1, answerId 가져오고 q의 mapping 다 바꾸기
    //    기존에 없으면 copyId, id 그대로 끗!

    Map<String, Integer> idMap = new HashMap<>();
    Map<String, Integer> oldIdMap = new HashMap<>();
    oldIdMap.put("answerId", answerId);
    oldIdMap.put("answerCopyId", answerEntity.getCopyId());
    // answer를 answerId로 조회함
    List<Answer> answerEntities = sqlSession.selectList("nqa.selectAnswerById", answerId);

    // answer 문자열이 바뀌었을 경우
    if (!answerEntities.get(0).getAnswer().equals(answerEntity.getAnswer())) {
      List<Answer> answerList = sqlSession.selectList("nqa.selectAnswerId", answerEntity);

      if (answerList.size() > 0) { // 답변이 이미 있다면
        idMap = getAnswerIdCopyId(answerList, answerEntity);  //id, copy_id 체크
        answerEntity.setId(idMap.get("answerId"));
        answerEntity.setCopyId(idMap.get("copyId"));
      }
      if (!idMap.containsKey("exist")) {  // layer순열과 답변이 다 같은게 없는경우(Answer를 추가해야 하는 경우)
        if (!idMap.containsKey("copyId") || idMap.get("copyId") == 0) {
          sqlSession.insert("nqa.insertAnswer", answerEntity);
        } else {
          sqlSession.insert("nqa.insertAnswerById", answerEntity);
        }
        idMap.put("answerId", answerEntity.getId());
        if (!idMap.containsKey("copyId")) {
          idMap.put("copyId", 0);
        }
      }

      changeAnswerIdInQuestion(oldIdMap, answerEntity, sqlSession);
      sqlSession.delete("nqa.deleteAnswer", oldIdMap);

    } else {
      // answer 같은 경우 layer 비교
      Boolean layerExist = false;
      Boolean changeCopyId = false;
      for (Answer entity : answerEntities) {
        if (checkAnswerLayer(entity, answerEntity)) {
          if (entity.getCopyId() != answerEntity.getCopyId()) {
            // layer 바뀌었으나 answer 중 있음.
            // -> 그럼 attr, tag, answerView, summary 모두 바뀔 수 있어 고민 필요
            changeCopyId = true;
            answerEntity.setCopyId(entity.getCopyId());
          } else {
            // layer 동일
          }
          layerExist = true;
          break;
        }
      }

      if (layerExist) {
        sqlSession.update("nqa.updateAnswer", answerEntity);
        if (changeCopyId) {
          sqlSession.delete("nqa.deleteAnswer", oldIdMap);
        }
      } else {

        int maxCopyId = 0;
        for (Answer answer : answerEntities) {
          if (maxCopyId < answer.getCopyId()) {
            maxCopyId = answer.getCopyId();
          }
        }
        answerEntity.setCopyId(maxCopyId + 1);
        sqlSession.insert("nqa.insertAnswerById", answerEntity);
        sqlSession.delete("nqa.deleteAnswer", oldIdMap);
      }
      changeAnswerIdInQuestion(oldIdMap, answerEntity, sqlSession);
    }

    // question 바뀌
    // insert new question
    for (int i = 0; i < editQaSet.getAddedQuestionsCount(); i++) {
      Question questionEntity = new Question();
      questionEntity.setEntityByProto(editQaSet.getAddedQuestions(i));
      questionEntity.setAnswerId(answerEntity.getId());
      questionEntity.setAnswerCopyId(answerEntity.getCopyId());
      sqlSession.insert("nqa.insertQuestion", questionEntity);
    }

    // update question
    for (int i = 0; i < editQaSet.getEditedQuestionsCount(); i++) {
      Question questionEntity = new Question();
      questionEntity.setEntityByProto(editQaSet.getEditedQuestions(i));
      questionEntity.setAnswerId(answerEntity.getId());
      sqlSession.insert("nqa.updateQuestion", questionEntity);
    }

    // delete question
    for (int i = 0; i < editQaSet.getRemovedQuestionsCount(); i++) {
      int questionId = editQaSet.getRemovedQuestions(i).getId();
      sqlSession.insert("nqa.deleteQuestion", questionId);
    }

    AnswerList.Builder resultAnswerList = AnswerList.newBuilder();
    QaSet.Builder qaSet = QaSet.newBuilder();
    qaSet.setAnswer(answerEntity.getProto());
    List<Question> questionEntities = sqlSession
      .selectList("nqa.selectQuestionByAnswerId", answerEntity.getId());
    for (int i = 0; i < questionEntities.size(); i++) {
      qaSet.addQuestions(i, questionEntities.get(i).getProto());
    }

    sqlSession.commit();

    resultAnswerList.addQaSets(qaSet);
    responseObserver.onNext(resultAnswerList.build());
    responseObserver.onCompleted();
  }

  private int changeAnswerIdInQuestion(Map<String, Integer> oldAnswerIdMap, Answer newAnswerEntity,
                                       SqlSession sqlSession) {
    Map<String, Integer> param = new HashMap<>();
    param.put("oldAnswerId", oldAnswerIdMap.get("answerId"));
    param.put("oldAnswerCopyId", oldAnswerIdMap.get("answerCopyId"));
    param.put("newAnswerId", newAnswerEntity.getId());
    param.put("newAnswerCopyId", newAnswerEntity.getCopyId());
    int result = sqlSession.update("nqa.updateQuestionWithAnswerId", param);
    return result;
  }

  @Override
  public void removeAnswer(RemoveAnswerRequest request,
                           StreamObserver<RemoveAnswerResponse> responseObserver) {
    removeAnswerTask(request, responseObserver);
  }

  private void removeAnswerTask(RemoveAnswerRequest request,
                                StreamObserver<RemoveAnswerResponse> responseObserver) {
    logger.info("@#@# removeAnswerTask request :: {}", request);

    Map<String, Integer> params = new HashMap<>();
    int totalCnt = 0;
    int totalQCnt = 0;
    SqlSession sqlSession = sqlSessionManager.getSqlSession();

    try {

      for (AnswerIdSet idSet : request.getAnswerIdSetsList()) {
        int answerId = idSet.getId();
        int answerCopyId = idSet.getCopyId();
        params.put("answerId", answerId);
        params.put("answerCopyId", answerCopyId);

        int cnt = sqlSession.delete("nqa.deleteAnswer", params);
        int qCnt = sqlSession.delete("nqa.deleteQuestionByAnswerId", params);
        totalCnt += cnt;
        totalQCnt += qCnt;
      }
      sqlSession.commit();

      logger.debug("===== removed Answer Count : {}", totalCnt);
      logger.debug("===== removed Question Count : {}", totalQCnt);

      RemoveAnswerResponse.Builder removeAnswerResponse = RemoveAnswerResponse.newBuilder();
      removeAnswerResponse.setCount(totalCnt);
      responseObserver.onNext(removeAnswerResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("removeAnswer FAIL {} => ", e.getMessage(), e);
      responseObserver.onError(e);
    } finally {
      sqlSession.close();
    }
  }

  private String buildDeleteQuery(String channelName, String categoryName) {
    StringBuffer query = new StringBuffer();
    query.append("channel:");
    if (channelName.isEmpty()) {
      query.append("*");
    } else {
      query.append("\"").append(channelName).append("\"");
    }
    query.append(" AND ");
    query.append("category:");
    if (categoryName.isEmpty()) {
      query.append("*");
    } else {
      query.append("\"").append(categoryName).append("\"");
    }
    logger.debug("delete indexed data Query => {}", query.toString());
    return query.toString();
  }

  private void deleteIndexedData(SolrClient solrClient, String channelName, String categoryName)
    throws Exception {
    try {
      String query = buildDeleteQuery(channelName, categoryName);
      solrClient.deleteByQuery(query);
      solrClient.commit();
    } catch (Exception e) {
      logger.error("Delete Indexed Data Fail : {} => ", e, e.getMessage());
      throw e;
    }
  }
}
