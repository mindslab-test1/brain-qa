package ai.maum.nqa;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import maum.brain.qa.nqa.ApiService.AbortIndexingRequest;
import maum.brain.qa.nqa.ApiService.IndexingQaSet;
import maum.brain.qa.nqa.ApiService.IndexingResponse;
import maum.brain.qa.nqa.NQaApiServiceGrpc.NQaApiServiceImplBase;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NQaApiService extends NQaApiServiceImplBase {

  private static Logger logger = LoggerFactory.getLogger(NQaApiService.class);

  private ApiIndexingCore apiIndexingCore;
  private ApiDocumentIndexingHandler apiDocumentIndexingHandler;

  @Override
  public void indexing(IndexingQaSet indexingQaSet, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("start indexing");

    try {
      this.apiDocumentIndexingHandler = new ApiDocumentIndexingHandler(indexingQaSet.getChannel());
      indexingTask(indexingQaSet, responseObserver);
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  private void indexingTask(IndexingQaSet indexingQaSet, StreamObserver<IndexingResponse> responseObserver) {
    IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder();
    try {
      // check indexing
      if (apiDocumentIndexingHandler.isIndexing()) {
        // fill indexing status
        indexingResponse.setMessage("NQA is already in Indexing");
      } else {
        apiDocumentIndexingHandler.setIndexing(true);
        indexingResponse.setMessage("NQA Indexing Start");
        apiIndexingCore = new ApiIndexingCore(indexingQaSet, apiDocumentIndexingHandler);
        apiIndexingCore.start();
        Thread.sleep(10);
      }
      fillIndexingResponse(indexingResponse);
      responseObserver.onNext(indexingResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  @Override
  public void abortIndexing(AbortIndexingRequest abortIndexingRequest, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("start abortIndexing");
    try {
      IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder().setStatus(false);
      if (apiDocumentIndexingHandler == null) {
        indexingResponse.setStatus(false);
        indexingResponse.setMessage("NQA_Indexing_Not_Executed");
      } else {
        if (apiDocumentIndexingHandler.isIndexing()) {
          // stop indexing
          apiIndexingCore.interrupt();
          apiIndexingCore.stop();
          apiDocumentIndexingHandler.setIndexing(false);
          indexingResponse.setMessage("Indexing_Abort_Success");
        } else {
          indexingResponse.setMessage("NQA_Not_in_Indexing");
        }
        // fill indexing status
        fillIndexingResponse(indexingResponse);
      }
      logger.trace("abortIndexing response : {}", indexingResponse.toString());
      responseObserver.onNext(indexingResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }

  @Override
  public void getIndexingStatus(Empty empty, StreamObserver<IndexingResponse> responseObserver) {
    logger.info("get indexStatus");
    try {
      IndexingResponse.Builder indexingResponse = IndexingResponse.newBuilder();
      if (apiDocumentIndexingHandler == null) {
        indexingResponse.setStatus(false);
        indexingResponse.setMessage("NQA_Indexing_Not_Executed");
      } else {
        indexingResponse.setMessage(apiDocumentIndexingHandler.isIndexing() ?
          "NQA_in_Indexing" : "NQA_Not_in_Indexing");
        // fill indexing status
        fillIndexingResponse(indexingResponse);
      }
      logger.trace("getIndexingStatus response : {}", indexingResponse.toString());
      responseObserver.onNext(indexingResponse.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      responseObserver.onError(e);
    }
  }


  private void fillIndexingResponse(IndexingResponse.Builder indexingResponse) {
    indexingResponse
      .setStatus(apiDocumentIndexingHandler.isIndexing())
      .setTotal(apiDocumentIndexingHandler.getTotal())
      .setFetched(apiDocumentIndexingHandler.getFetched())
      .setProcessed(apiDocumentIndexingHandler.getProcessed());
  }
}
