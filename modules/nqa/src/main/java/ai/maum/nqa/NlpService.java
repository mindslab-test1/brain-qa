package ai.maum.nqa;

import ai.maum.nqa.word2vec.Word2VecModel;
import ai.maum.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import lombok.Getter;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.*;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class NlpService {

  private static final Logger logger = LoggerFactory.getLogger(NlpService.class);

  private static final Properties properties = PropertiesManager.getProperties();
  private static final Map<String, String[]> nlpConfigs = new HashMap<>();
  private static final Map<String, String[]> morphTypes = new HashMap<>();
  private static final Map<String, List<String>> removeMorphTypes = new HashMap<>();

  private static final String[] nesTypes;
  private static final String[] hanaNerTypes;
  private static final String[] w2vTypes;
  private static final String nlpWeight;

  private static final String useSynonym;

  static {
    nesTypes = StringUtils.split(properties.getProperty("NER_TYPES", "PRD_INSV, PRD_DP, PRD_LON, PRD_FUND"), ",");
    hanaNerTypes = StringUtils.split(properties.getProperty("HANA_NER_TYPES", ""), ",");
    useSynonym = properties.getProperty("SYNONYM_YN", "N");
    w2vTypes = StringUtils.split(properties.getProperty("W2V_TYPES", "NNG,VV,VA,MM,SL"), ",");
    nlpWeight = properties.getProperty("NLP_WEIGHT", "");
  }

  @Getter
  private String noun = "";

  private static synchronized String[] getChannelConf(String channel) {
    String nlpConfigKey = channel.toUpperCase() + "_NLP_CONFIG";
    if (!properties.containsKey(nlpConfigKey)) {
      nlpConfigKey = "NLP_CONFIG";
    }

    if (!nlpConfigs.containsKey(nlpConfigKey)) {
      final String property = properties.getProperty(nlpConfigKey);
      logger.info("{} = {}", nlpConfigKey, property);
      nlpConfigs.put(nlpConfigKey, property.split(","));
    }

    return nlpConfigs.get(nlpConfigKey);
  }

  private static synchronized List<String> getRemoveMorphTypes(final String channel) {
    String propertyKey = channel.toUpperCase() + "_REMOVE_TYPES";
    if (!properties.containsKey(propertyKey)) {
      propertyKey = "REMOVE_TYPES";
    }

    if (!removeMorphTypes.containsKey(propertyKey)) {
      final String property = properties.getProperty(propertyKey);
      final String removeTypes = new String(
        property.getBytes(StandardCharsets.ISO_8859_1),
        StandardCharsets.UTF_8);
      logger.info("{} = {}", propertyKey, removeTypes);
      removeMorphTypes.put(propertyKey,
        Arrays.asList(StringUtils.split(removeTypes, ",")));
    }

    return removeMorphTypes.get(propertyKey);
  }

  private static synchronized String[] getMorphTypes(final String channel) {
    String propertyKey = channel.toUpperCase() + "_MORPH_TYPES";
    if (!properties.containsKey(propertyKey)) {
      propertyKey = "MORPH_TYPES";
    }

    if (!morphTypes.containsKey(propertyKey)) {
      final String property = properties.getProperty(propertyKey);
      final String morphTypes = new String(
        property.getBytes(StandardCharsets.ISO_8859_1),
        StandardCharsets.UTF_8);
      logger.info("{} = {}", propertyKey, morphTypes);
      NlpService.morphTypes.put(propertyKey, StringUtils.split(morphTypes, ","));
    }

    return morphTypes.get(propertyKey);
  }

  // NLP to Ner Search, Indexing
  public String doNerNlp(String question, String channel) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    String[] channelConf = getChannelConf(channel);

    try {
      if (question == null || question.isEmpty()) {
        logger.debug("question is null or empty.");
        return question;
      }
      // Set Channel
      nlpChannel = ManagedChannelBuilder.forAddress(channelConf[0], Integer.parseInt(channelConf[1])).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);

      // Set Nlp Header
      Metadata nlpHeaders = new Metadata();
      Metadata.Key<String> keyUUID = Metadata.Key.of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyChannel = Metadata.Key.of("channel-id", Metadata.ASCII_STRING_MARSHALLER);
      nlpHeaders.put(keyUUID, UUID.randomUUID().toString());
      nlpHeaders.put(keyChannel, channel);
      nlpStub = MetadataUtils.attachHeaders(nlpStub, nlpHeaders);

      // Set Nlp Request
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
        .setSplitSentence(true).setUseTokenizer(false)
        .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY)
        .setUseSpace(StringUtils.equalsIgnoreCase("Y", channelConf[2]))
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

      // Do Nlp Task
      Document document = null;
      if ("Y".equalsIgnoreCase(useSynonym)) {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
      } else {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      }
      nlpChannel.shutdown();

      // Parse Nlp Result
      List<String> nesList = new ArrayList<>();
      List<Sentence> sentenceList = document.getSentencesList();
      setNesSentence(nesList, sentenceList);
      String result = StringUtils.join(nesList, " ");

      return result;
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  // NLP to Search
  public String doMorphNlp(String question, String channel, Word2VecModel word2VecModel, boolean useW2V) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    String[] channelConf = getChannelConf(channel);

    try {
      if (question == null || question.isEmpty()) {
        logger.debug("question is null or empty.");
        return question;
      }
      // Set Channel
      nlpChannel = ManagedChannelBuilder.forAddress(channelConf[0], Integer.parseInt(channelConf[1])).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);

      // Set Nlp Header
      Metadata nlpHeaders = new Metadata();
      Metadata.Key<String> keyUUID = Metadata.Key.of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyChannel = Metadata.Key.of("channel-id", Metadata.ASCII_STRING_MARSHALLER);
      nlpHeaders.put(keyUUID, UUID.randomUUID().toString());
      nlpHeaders.put(keyChannel, channel);
      nlpStub = MetadataUtils.attachHeaders(nlpStub, nlpHeaders);

      // Set Nlp Request
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
        .setSplitSentence(true).setUseTokenizer(true)
        .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY)
        .setUseSpace(StringUtils.equalsIgnoreCase("Y", channelConf[2]))
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

      // Do Nlp
      Document document = null;
      if ("Y".equalsIgnoreCase(useSynonym)) {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
      } else {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      }
      nlpChannel.shutdown();

      // Parse Nlp Result
      List<String> strList = new ArrayList<>();
      List<String> strWeightedList = new ArrayList<>();
      List<String> nounList = new ArrayList<>();
      List<String> nesList = new ArrayList<>();
      List<String> nlpTagList = new ArrayList<>();
      List<Sentence> sentenceList = document.getSentencesList();
      setMorphSentence(channel, strList, nounList, sentenceList, nlpTagList);
      noun = StringUtils.join(nounList, " ");
      setHanaNesSentence(nesList, sentenceList);

//      List<String> w2vList = new ArrayList<>();
//
//      if (useW2V) {
//        for (String nlpWord : nlpTagList) {
//          if (StringUtils.equalsAny(nlpWord.split("/")[1], w2vTypes)) {
//            w2vList.addAll(word2VecModel.getW2VWords(nlpWord.toLowerCase()));
//          }
//        }
//      }

      String[] nlpChannelWeight = StringUtils.split(properties.getProperty(channel + "_TYPES", nlpWeight), ",");
      HashMap<String, String> nlpWeightMap = new HashMap<>();
      for (String tempWeight : nlpChannelWeight) {
        String[] tempArray = tempWeight.split(":");
        nlpWeightMap.put(tempArray[0], tempArray[1]);
      }

      for (String nlpWord : strList) {
        String[] tempNlp = nlpWord.split("/");
        String tempWord = "(" + tempNlp[0] + "^1)";
        if (useW2V) {
          if (StringUtils.equalsAny(tempNlp[1], w2vTypes)) {
            tempWord = "(" + tempNlp[0] + "^1 " + StringUtils.join(word2VecModel.getW2VWords(nlpWord.toLowerCase()), " ") + ")";
          }
        }
        if (nlpWeightMap.containsKey(tempNlp[1])) {
          strWeightedList.add(tempWord + "^" + nlpWeightMap.get(tempNlp[1]));
        } else {
          strWeightedList.add(tempWord + "^1");
        }
      }

      return StringUtils.join(strWeightedList, "||=||");
//      return StringUtils.join(strWeightedList, " ") + " " +
//              StringUtils.join(nesList, " ") + " " +
//              StringUtils.join(w2vList, " ");
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      return question;
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

  // NLP to Indexing
  public void doNlp(String question, HashMap<String, String> nlpResult, String channel) {
    ManagedChannel nlpChannel = null;
    NaturalLanguageProcessingServiceBlockingStub nlpStub = null;
    String[] channelConf = getChannelConf(channel);

    try {
      if (question == null || question.isEmpty()) {
        logger.debug("question is null or empty.");
        nlpResult.put("morph", question);
        nlpResult.put("ner", "");
        return;
      }
      // Set Channel
      nlpChannel = ManagedChannelBuilder.forAddress(channelConf[0], Integer.parseInt(channelConf[1])).usePlaintext().build();
      nlpStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(nlpChannel);

      // Set Nlp Header
      Metadata nlpHeaders = new Metadata();
      Metadata.Key<String> keyUUID = Metadata.Key.of("x-operation-sync-id", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyChannel = Metadata.Key.of("channel-id", Metadata.ASCII_STRING_MARSHALLER);
      nlpHeaders.put(keyUUID, UUID.randomUUID().toString());
      nlpHeaders.put(keyChannel, channel);
      nlpStub = MetadataUtils.attachHeaders(nlpStub, nlpHeaders);

      // Set Nlp Request
      InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
        .setSplitSentence(true).setUseTokenizer(true)
        .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY)
        .setUseSpace(StringUtils.equalsIgnoreCase("Y", channelConf[2]))
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

      // Do Nlp
      Document document = null;
      if ("Y".equalsIgnoreCase(useSynonym)) {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyzeWithSpace(inputText);
      } else {
        document = nlpStub.withDeadlineAfter(Integer.parseInt(properties.getProperty("NLP_TIMEOUT", "3")), TimeUnit.SECONDS).analyze(inputText);
      }
      nlpChannel.shutdown();

      // Parse Nlp Result
      List<String> strList = new ArrayList<>();
      List<String> strWeightedList = new ArrayList<>();
      List<String> nesList = new ArrayList<>();
      List<String> nounList = new ArrayList<>();
      List<String> nlpTagList = new ArrayList<>();
      List<Sentence> sentenceList = document.getSentencesList();
      setMorphSentence(channel, strList, nounList, sentenceList, nlpTagList);
//      setNesSentence(nesList, sentenceList);
      setHanaNesSentence(nesList, sentenceList);

      for (String nlpWord : strList) {
        String[] tempNlp = nlpWord.split("/");
        strWeightedList.add(tempNlp[0]);
      }

      nlpResult.put("morph", StringUtils.join(strWeightedList, " "));
      nlpResult.put("ner", StringUtils.join(nesList, " "));
      noun = StringUtils.join(nounList, " ");
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
      nlpResult.put("morph", question);
      nlpResult.put("ner", "");
    } finally {
      if (nlpChannel != null) {
        nlpChannel.shutdown();
      }
    }
  }

//  public List<String> doMultiNlp(List<String> questions) {
//    ManagedChannel nlpChannel = null;
//    NaturalLanguageProcessingServiceStub nlpStub = null;
//    List<String> results = new ArrayList<>();
//    try {
//      nlpChannel = ManagedChannelBuilder.forAddress(properties.getProperty("NLP_SERVER"),
//          Integer.parseInt(properties.getProperty("NLP_PORT"))).usePlaintext().build();
//      nlpStub = NaturalLanguageProcessingServiceGrpc.newStub(nlpChannel);
//
//      DocumentObserver documentObserver = new DocumentObserver();
//      CountDownLatch finishLatch = new CountDownLatch(1);
//      documentObserver.setCountDownLatch(finishLatch);
//      StreamObserver<InputText> requestObserver = nlpStub.analyzeMultiple(documentObserver);
//      for (String question : questions) {
//        InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor)
//            .setSplitSentence(true).setUseTokenizer(true)
//            .setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
//            .setUseSpace(StringUtils.equalsIgnoreCase("Y", properties.getProperty("SPACE_YN", "N")))
//            .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
//        requestObserver.onNext(inputText);
//      }
//      requestObserver.onCompleted();
//
//      if (!finishLatch.await(100, TimeUnit.SECONDS)) {
//        logger.error("NLP Timeout");
//        return questions;
//      }
//
//      List<Document> documentList = documentObserver.getDocumentList();
//      nlpChannel.shutdown();
//      for (Document document : documentList) {
//        List<String> strList = new ArrayList<>();
//        List<String> nounList = new ArrayList<>();
//        List<Sentence> sentenceList = document.getSentencesList();
//        setMorphSentence(channel, strList, nounList, sentenceList);
//        String result = StringUtils.join(strList, " ");
//        results.add(result);
//      }
//      return results;
//    } catch (Exception e) {
//      e.printStackTrace();
//      return questions;
//    } finally {
//      if (nlpChannel != null) {
//        nlpChannel.shutdown();
//      }
//    }
//  }

  private void setMorphSentence(String channel,
                                List<String> strList,
                                List<String> nounList,
                                List<Sentence> sentenceList,
                                List<String> nlpTagList) {
    String[] morphTypes = getMorphTypes(channel);
    List<String> removeMorphTypes = getRemoveMorphTypes(channel);
    for (Sentence sentence : sentenceList) {
      List<Morpheme> morpsList = sentence.getMorpsList();
      morpsList = removeUnnecessaryMorph(removeMorphTypes, morpsList);
      for (int i = 0; i < morpsList.size(); i++) {
        Morpheme morpheme = morpsList.get(i);
        String morphemeType = morpheme.getType();
        String prevMorphemeType = i > 0 ? morpsList.get(i - 1).getType() : null;
        if (strList.size() > 0
          && prevMorphemeType != null
          && ((StringUtils.equalsAny(prevMorphemeType, "NNG", "NNP", "NNB", "NP", "NR")
          && StringUtils.equals(morphemeType, "XSN"))
          || (StringUtils.equals(prevMorphemeType, "VV") && StringUtils.equals(morphemeType, "ETN"))
          || (StringUtils.equals(prevMorphemeType, "XPN")
          && StringUtils.equalsAny(morphemeType, "NNG", "NNP", "NNB", "NP", "NR")))) {
          String temp = strList.get(strList.size() - 1);
          strList.remove(strList.size() - 1);
          strList.add(temp + morpheme.getLemma() + "/" + morpheme.getType());
          nlpTagList.remove(nlpTagList.size() - 1);
          nlpTagList.add(temp + morpheme.getLemma() + "/NNG");
          if (nounList.size() > 0) {
            nounList.remove(nounList.size() - 1);
          }
          nounList.add(temp + morpheme.getLemma());
        } else if (StringUtils.equalsAny(morphemeType, morphTypes)) {
          strList.add(morpheme.getLemma() + "/" + morpheme.getType());
          nlpTagList.add(morpheme.getLemma() + "/" + morpheme.getType());
          if (!StringUtils.equalsAny(morphemeType, "VV", "VA")) {
            nounList.add(morpheme.getLemma());
          }
        }
      }
    }
  }

  private List<Morpheme> removeUnnecessaryMorph(List<String> removeTypes, List<Morpheme> morphemeList) {
    // [ REMOVE_MODE = Y|N ]을 없애고 REMOVE_TYPES에 값이 있을 때만 불용어 처리를 한다.
    if (removeTypes.size() == 0) {
      return morphemeList;
    }
    List<Morpheme> newMorphemeList = new ArrayList<>();
    for (int i = 0; i < morphemeList.size(); i++) {
      if (!removeTypes.contains(morphemeList.get(i).getLemma() + "/" + morphemeList.get(i).getType())) {
        newMorphemeList.add(morphemeList.get(i));
      }
//      else {
//        logger.info("remove type => " + morphemeList.get(i).getLemma() + "/" + morphemeList.get(i).getType());
//      }
    }
    return newMorphemeList;
  }

  private void setNesSentence(List<String> strList, List<Sentence> sentenceList) {
    for (Sentence sentence : sentenceList) {
      for (NamedEntity namedEntity : sentence.getNesList()) {
        for (String nes : nesTypes) {
          if (namedEntity.getType().contains(nes)) {
            strList.add(namedEntity.getText());
            strList.add(namedEntity.getText() + "/" + namedEntity.getType());
            break;
          }
        }
      }
    }
  }

  private void setHanaNesSentence(List<String> strList, List<Sentence> sentenceList) {
    for (Sentence sentence : sentenceList) {
      for (NamedEntity namedEntity : sentence.getNesList()) {
        for (String hanaNer : hanaNerTypes) {
          if (namedEntity.getType().contains(hanaNer)) {
            strList.add(namedEntity.getText());
            break;
          }
        }
      }
    }
  }
}
