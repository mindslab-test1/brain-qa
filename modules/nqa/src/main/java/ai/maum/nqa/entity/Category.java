package ai.maum.nqa.entity;

import java.sql.Date;
import lombok.Data;
import maum.brain.qa.nqa.Admin.NQaAdminCategory;

@Data
public class Category {
  private int id;
  private String name;
  private int channelId;
  private String creatorId;
  private String updaterId;
  private Date createDtm;
  private Date updateDtm;
  private int pageIndex;
  private int pageSize;

  public NQaAdminCategory getProto() {
    NQaAdminCategory.Builder categoryProto = NQaAdminCategory.newBuilder();

    categoryProto.setId(this.id);

    if (this.name != null) {
      categoryProto.setName(this.name);
    }
    if (this.channelId != 0) {
      categoryProto.setChannelId(this.channelId);
    }
    if (this.creatorId != null) {
      categoryProto.setCreatorId(this.creatorId);
    }
    if (this.updaterId != null) {
      categoryProto.setUpdaterId(this.updaterId);
    }
    if (this.createDtm != null) {
      categoryProto.setCreateDtm(this.createDtm.toString());
    }
    if (this.updateDtm != null) {
      categoryProto.setUpdateDtm(this.updateDtm.toString());
    }
    categoryProto.setPageIndex(this.pageIndex);
    categoryProto.setPageIndex(this.pageSize);

    return categoryProto.build();
  }

  public Category setEntityByProto(NQaAdminCategory categoryProto) {

    this.id = categoryProto.getId();
    this.name = categoryProto.getName();
    this.channelId = categoryProto.getChannelId();
    this.creatorId = categoryProto.getCreatorId();
    this.updaterId = categoryProto.getUpdaterId();
    this.pageIndex = categoryProto.getPageIndex();
    this.pageSize = categoryProto.getPageSize();

    return this;
  }
}
