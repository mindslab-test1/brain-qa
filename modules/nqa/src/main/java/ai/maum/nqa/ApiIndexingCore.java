package ai.maum.nqa;

import ai.maum.util.SqlSessionManager;
import maum.brain.qa.nqa.ApiService.IndexingQaSet;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ApiIndexingCore extends Thread {

  private static Logger logger = LoggerFactory.getLogger(ApiIndexingCore.class);

  private Map<String, String> dbParams = new HashMap<>();

  private IndexingQaSet indexingQaSet;

  private ApiDocumentIndexingHandler apiDocumentIndexingHandler;

  public ApiIndexingCore(IndexingQaSet indexingQaSet, ApiDocumentIndexingHandler apiDocumentIndexingHandler) {
    this.indexingQaSet = indexingQaSet;
    this.apiDocumentIndexingHandler = apiDocumentIndexingHandler;
  }

  @Override
  public void run() {
    SqlSessionManager sqlSessionManager = SqlSessionManager.getInstance();
    try (SqlSession sqlSession = sqlSessionManager.getSqlSession()) {
      apiDocumentIndexingHandler.clearValue();
      apiDocumentIndexingHandler.setSqlSession(sqlSession);

      // print to check channel, category
      logger.info("channel=>" + indexingQaSet.getChannel() + "/ category=>" + indexingQaSet.getCategory() + "/ indexId=>" + indexingQaSet.getIndexId());

      ////////////////
      // start indexing task
      dbParams.put("channelName", indexingQaSet.getChannel());
      dbParams.put("categoryName", indexingQaSet.getCategory());
      dbParams.put("indexId", indexingQaSet.getIndexId());

      int total = sqlSession.selectOne("nqa.selectAllDbDataCount", dbParams);

      apiDocumentIndexingHandler.setTotal(total);

      // Select All Data and Parse to Solr
      sqlSession.select("nqa.selectAllDbData", dbParams, apiDocumentIndexingHandler);
      sqlSession.close();
    } catch (Exception e) {
      logger.error(ExceptionUtils.getStackTrace(e));
    } finally {
      apiDocumentIndexingHandler.setIndexing(false);
    }
  }
}
