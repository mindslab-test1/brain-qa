#!/usr/bin/env bash

PRG="$0"
  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
      ls=`ls -ld "$PRG"`
      link=`expr "$ls" : '.*-> \(.*\)$'`
      if expr "$link" : '/.*' > /dev/null; then
          PRG="$link"
      else
          PRG=`dirname "$PRG"`"/$link"
      fi
  done
  SAVED="`pwd`"
  cd "`dirname \"$PRG\"`/" >/dev/null
  BUILD_HOME="`pwd -P`"
  cd "$SAVED" >/dev/null

nqa_root=${NQA_ROOT}
if [ -z "${nqa_root}" ]; then
  echo "NQA_ROOT is not defined!"
  exit 1
fi

test -d ${nqa_root} || mkdir -p ${nqa_root}
test -d ${nqa_root}/lib/ || mkdir -p ${nqa_root}/lib/
test -d ${nqa_root}/logs/ || mkdir -p ${nqa_root}/logs/


cd $BUILD_HOME

chmod 755 ${BUILD_HOME}/gradlew

APP_VERSION="1.1.1"
./gradlew clean shadowJar

if [ $? -eq 0 ]; then
  chmod 755 ${BUILD_HOME}/bin/*.sh
  cp -r ${BUILD_HOME}/bin ${nqa_root}/
  cp -r ${BUILD_HOME}/conf ${nqa_root}/
  cp ${BUILD_HOME}/build/libs/nqa-${APP_VERSION}.jar ${nqa_root}/lib/
  cd ${nqa_root}/lib/
  ln -s nqa-${APP_VERSION}.jar nqa.jar
  cd -
  cp -r ${BUILD_HOME}/.aws ~/
  python pysrc/aws-s3/get_nqa_resources.py nqa-solr 20190710 ${BUILD_HOME}/build/tmp/ ${BUILD_HOME}/build/tmp/
  mv ${BUILD_HOME}/build/tmp/solr1 ${nqa_root}/
  mv ${BUILD_HOME}/build/tmp/solr2 ${nqa_root}/
  mv ${BUILD_HOME}/build/tmp/solr3 ${nqa_root}/
  mv ${BUILD_HOME}/build/tmp/zookeeper ${nqa_root}/
  cp ${BUILD_HOME}/nqa.conf ${nqa_root}/
  sed -i "s#\${APP_HOME}#${nqa_root}#g" ${nqa_root}/conf/qa.properties
  sed -i "s#\${APP_HOME}#${nqa_root}#g" ${nqa_root}/zookeeper/conf/zoo1.cfg
  sed -i "s#\${APP_HOME}#${nqa_root}#g" ${nqa_root}/zookeeper/conf/zoo2.cfg
  sed -i "s#\${APP_HOME}#${nqa_root}#g" ${nqa_root}/zookeeper/conf/zoo3.cfg
  else
    echo "Gradle Build Fail!"
    exit 1
fi