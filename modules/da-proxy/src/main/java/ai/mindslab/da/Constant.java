package ai.mindslab.da;

import io.grpc.Context;
import io.grpc.Metadata;

public class Constant {

  public static final Context.Key<String> transactionId_ctx_key = Context.key("transactionId");
  public static final Context.Key<String> messageId_ctx_key = Context.key("messageId");
  public static final Context.Key<String> sid_ctx_key = Context.key("sid");
  public static final Context.Key<String> clientIp_ctx_key = Context.key("clientIp");
  public static final Context.Key<String> devInfo_ctx_key = Context.key("devInfo");
  public static final Context.Key<String> osInfo_ctx_key = Context.key("osInfo");
  public static final Context.Key<String> nwInfo_ctx_key = Context.key("nwInfo");
  public static final Context.Key<String> svcName_ctx_key = Context.key("svcName");
  public static final Context.Key<String> devModel_ctx_key = Context.key("devModel");
  public static final Context.Key<String> carrierType_ctx_key = Context.key("carrierType");
  public static final Context.Key<String> trId_ctx_key = Context.key("trId");
  public static final Context.Key<String> msgId_ctx_key = Context.key("msgId");
  public static final Context.Key<String> fromSvcName_ctx_key = Context.key("fromSvcName");
  public static final Context.Key<String> toSvcName_ctx_key = Context.key("toSvcName");
  public static final Context.Key<String> svcType_ctx_key = Context.key("svcType");
  public static final Context.Key<String> devType_ctx_key = Context.key("devType");
  public static final Context.Key<String> deviceToken_ctx_key = Context.key("deviceToken");



  public static final Metadata.Key<String> transactionId_key = Metadata.Key.of("transaction_id", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> messageId_key = Metadata.Key.of("message_id", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> sid_key = Metadata.Key.of("sid", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> clientIp_key = Metadata.Key.of("client_ip", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> devInfo_key = Metadata.Key.of("dev_info", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> osInfo_key = Metadata.Key.of("os_info", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> nwInfo_key = Metadata.Key.of("nw_info", Metadata.ASCII_STRING_MARSHALLER);
//  public static final Metadata.Key<String> svcName_key = Metadata.Key.of("svcName", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> devModel_key = Metadata.Key.of("dev_model", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> carrierType_key = Metadata.Key.of("carrier_type", Metadata.ASCII_STRING_MARSHALLER);
//  public static final Metadata.Key<String> trId_key = Metadata.Key.of("trId", Metadata.ASCII_STRING_MARSHALLER);
//  public static final Metadata.Key<String> msgId_key = Metadata.Key.of("msgId", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> fromSvcName_key = Metadata.Key.of("from_svc_name", Metadata.ASCII_STRING_MARSHALLER);
//  public static final Metadata.Key<String> toSvcName_key = Metadata.Key.of("toSvcName", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> svcType_key = Metadata.Key.of("svc_type", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> devType_key = Metadata.Key.of("dev_type", Metadata.ASCII_STRING_MARSHALLER);
  public static final Metadata.Key<String> deviceToken_key = Metadata.Key.of("device_token", Metadata.ASCII_STRING_MARSHALLER);
}
