package ai.mindslab.da;

import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;

public class ProxyServerInterceptor implements ServerInterceptor {

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, final Metadata requestHeaders,
      ServerCallHandler<ReqT, RespT> next) {


    Context context = Context.current()
    .withValue(Constant.transactionId_ctx_key, requestHeaders.get(Constant.transactionId_key))
    .withValue(Constant.messageId_ctx_key, requestHeaders.get(Constant.messageId_key))
    .withValue(Constant.sid_ctx_key, requestHeaders.get(Constant.sid_key))
    .withValue(Constant.clientIp_ctx_key, requestHeaders.get(Constant.clientIp_key))
    .withValue(Constant.devInfo_ctx_key, requestHeaders.get(Constant.devInfo_key))
    .withValue(Constant.osInfo_ctx_key, requestHeaders.get(Constant.osInfo_key))
    .withValue(Constant.nwInfo_ctx_key, requestHeaders.get(Constant.nwInfo_key))
//    .withValue(Constant.svcName_ctx_key, requestHeaders.get(Constant.svcName_key))
    .withValue(Constant.devModel_ctx_key, requestHeaders.get(Constant.devModel_key))
    .withValue(Constant.carrierType_ctx_key, requestHeaders.get(Constant.carrierType_key))
//    .withValue(Constant.trId_ctx_key, requestHeaders.get(Constant.trId_key))
//    .withValue(Constant.msgId_ctx_key, requestHeaders.get(Constant.msgId_key))
    .withValue(Constant.fromSvcName_ctx_key, requestHeaders.get(Constant.fromSvcName_key))
//    .withValue(Constant.toSvcName_ctx_key, requestHeaders.get(Constant.toSvcName_key))
    .withValue(Constant.svcType_ctx_key, requestHeaders.get(Constant.svcType_key))
    .withValue(Constant.devType_ctx_key, requestHeaders.get(Constant.devType_key))
    .withValue(Constant.deviceToken_ctx_key, requestHeaders.get(Constant.deviceToken_key));
    return Contexts.interceptCall(context, call, requestHeaders, next);
  }
}
