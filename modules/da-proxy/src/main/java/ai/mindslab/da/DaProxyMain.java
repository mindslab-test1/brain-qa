package ai.mindslab.da;

import ai.mindslab.util.PropertiesManager;
import io.grpc.Server;
import io.grpc.ServerInterceptors;
import io.grpc.netty.NettyServerBuilder;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DaProxyMain {

  private Properties properties = PropertiesManager.getProperties();

  private Logger logger = LoggerFactory.getLogger(DaProxyMain.class);

  private Server server = null;


  public static void main(String[] args) {
    DaProxyMain daProxyMain = new DaProxyMain();
    daProxyMain.runProxy();
    daProxyMain.blockUntilShutdown();
  }

  private void runProxy() {
    try {
      logger.info("{}", "Start DA Proxy");
      long idleTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      long ageTimeout = Integer.parseInt(properties.getProperty("TIMEOUT", "5"));
      int port = Integer.parseInt(properties.getProperty("DA_PROXY_PORT", "9907"));
      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      server = NettyServerBuilder.forPort(port)
          .maxConnectionIdle(idleTimeout, TimeUnit.SECONDS)
          .maxConnectionAge(ageTimeout, TimeUnit.SECONDS)
          .addService(ServerInterceptors.intercept(new DialogAgentProviderService(daIP, daPort), new ProxyServerInterceptor()))
          .maxConcurrentCallsPerConnection(Runtime.getRuntime().availableProcessors() * 256)
//          .flowControlWindow(NettyChannelBuilder.DEFAULT_FLOW_CONTROL_WINDOW * 10)
//          .executor(Executors.newFixedThreadPool(2000))
//          .directExecutor()
          .build()
          .start();
//      server = ServerBuilder.forPort(port).addService(ServerInterceptors.intercept(new DialogAgentProviderService(daIP, daPort), new ProxyServerInterceptor())).build().start();

      Runtime.getRuntime().addShutdownHook(new Thread() {
        @Override
        public void run() {
          DaProxyMain.this.stop();
        }
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void blockUntilShutdown() {
    if (server != null) {
      try {
        server.awaitTermination();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }
}
