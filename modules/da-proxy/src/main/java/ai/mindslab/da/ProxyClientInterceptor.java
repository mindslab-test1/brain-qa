package ai.mindslab.da;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;

public class ProxyClientInterceptor implements ClientInterceptor {

  private String transactionId;
  private String messageId;
  private String sid;
  private String clientIp;
  private String devInfo;
  private String osInfo;
  private String nwInfo;
  private String svcName;
  private String devModel;
  private String carrierType;
  private String trId;
  private String msgId;
  private String fromSvcName;
  private String toSvcName;
  private String svcType;
  private String devType;
  private String deviceToken;


  public ProxyClientInterceptor(String transactionId, String messageId, String sid, String clientIp,
      String devInfo, String osInfo,
      String nwInfo, String svcName, String devModel, String carrierType, String trId, String msgId,
      String fromSvcName, String toSvcName, String svcType,
      String devType, String deviceToken) {
    this.transactionId = transactionId;
    this.messageId = messageId;
    this.sid = sid;
    this.clientIp = clientIp;
    this.devInfo = devInfo;
    this.osInfo = osInfo;
    this.nwInfo = nwInfo;
    this.svcName = svcName;
    this.devModel = devModel;
    this.carrierType = carrierType;
    this.trId = trId;
    this.msgId = msgId;
    this.fromSvcName = fromSvcName;
    this.toSvcName = toSvcName;
    this.svcType = svcType;
    this.devType = devType;
    this.deviceToken = deviceToken;
  }

  @Override
  public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method,
      CallOptions callOptions, Channel next) {
    return new SimpleForwardingClientCall<ReqT, RespT>(next.newCall(method, callOptions)) {
      @Override
      public void start(Listener<RespT> responseListener, Metadata headers) {
        headers.put(Constant.transactionId_key, transactionId);
        headers.put(Constant.messageId_key, messageId);
        headers.put(Constant.sid_key, sid);
        headers.put(Constant.clientIp_key, clientIp);
        headers.put(Constant.devInfo_key, devInfo);
        headers.put(Constant.osInfo_key, osInfo);
        headers.put(Constant.nwInfo_key, nwInfo);
//        headers.put(Constant.svcName_key, svcName);
        headers.put(Constant.devModel_key, devModel);
        headers.put(Constant.carrierType_key, carrierType);
//        headers.put(Constant.trId_key, trId);
//        headers.put(Constant.msgId_key, msgId);
        headers.put(Constant.fromSvcName_key, fromSvcName);
//        headers.put(Constant.toSvcName_key, toSvcName);
        headers.put(Constant.svcType_key, svcType);
        headers.put(Constant.devType_key, devType);
        headers.put(Constant.deviceToken_key, deviceToken);
        super.start(responseListener, headers);
      }
    };
  }
}
