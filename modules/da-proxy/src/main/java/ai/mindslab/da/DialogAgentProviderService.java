package ai.mindslab.da;

import com.google.protobuf.Empty;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.JsonFormat.Printer;
import elsa.facade.Userattr.UserAttributeList;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import java.util.concurrent.TimeUnit;
import minds.maum.da.DialogAgentProviderGrpc;
import minds.maum.da.DialogAgentProviderGrpc.DialogAgentProviderBlockingStub;
import minds.maum.da.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import minds.maum.da.Provider.DialogAgentProviderParam;
import minds.maum.da.Provider.DialogAgentStatus;
import minds.maum.da.Provider.InitParameter;
import minds.maum.da.Provider.RuntimeParameterList;
import minds.maum.da.Provider.TalkKey;
import minds.maum.da.Provider.TalkRequest;
import minds.maum.da.Provider.TalkResponse;
import minds.maum.da.Provider.TalkStat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogAgentProviderService extends DialogAgentProviderImplBase {

//  private Properties properties = PropertiesManager.getProperties();

  private Logger logger = LoggerFactory.getLogger(DialogAgentProviderService.class);

  String daIP;
  int daPort;

//  private ManagedChannel channel = null;

//  private DialogAgentProviderBlockingStub stub = null;


  public DialogAgentProviderService(String daIP, int daPort) {
    this.daIP = daIP;
    this.daPort = daPort;
  }

  @Override
  public void init(InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("init start");
      DialogAgentProviderParam dialogAgentProviderParam = stub.init(request);
      responseObserver.onNext(dialogAgentProviderParam);
      responseObserver.onCompleted();
      logger.info("init end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        try {
          channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("getProviderParameter start");
      DialogAgentProviderParam dialogAgentProviderParam = stub.getProviderParameter(request);
      responseObserver.onNext(dialogAgentProviderParam);
      responseObserver.onCompleted();
      logger.info("getProviderParameter end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }

  @Override
  public void isReady(Empty request, StreamObserver<DialogAgentStatus> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
//      logger.info("aaaaaaaaaaaaaa" + Constant.transactionId_ctx_key.get());
//      ProxyClientInterceptor proxyClientInterceptor = new ProxyClientInterceptor(
//          Constant.transactionId_ctx_key.get(),
//          Constant.messageId_ctx_key.get(),
//          Constant.sid_ctx_key.get(),
//          Constant.clientIp_ctx_key.get(),
//          Constant.devInfo_ctx_key.get(),
//          Constant.osInfo_ctx_key.get(),
//          Constant.nwInfo_ctx_key.get(),
//          Constant.svcName_ctx_key.get(),
//          Constant.devModel_ctx_key.get(),
//          Constant.carrierType_ctx_key.get(),
//          Constant.trId_ctx_key.get(),
//          Constant.msgId_ctx_key.get(),
//          Constant.fromSvcName_ctx_key.get(),
//          Constant.toSvcName_ctx_key.get(),
//          Constant.svcType_ctx_key.get(),
//          Constant.devType_ctx_key.get(),
//          Constant.deviceToken_ctx_key.get()
//      );
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("isReady start");
      DialogAgentStatus dialogAgentStatus = stub.isReady(request);
      responseObserver.onNext(dialogAgentStatus);
      responseObserver.onCompleted();
      logger.info("isReady end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        try {
          channel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void talk(TalkRequest request, StreamObserver<TalkResponse> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));

      ProxyClientInterceptor proxyClientInterceptor = new ProxyClientInterceptor(
          Constant.transactionId_ctx_key.get(),
          Constant.messageId_ctx_key.get(),
          Constant.sid_ctx_key.get(),
          Constant.clientIp_ctx_key.get(),
          Constant.devInfo_ctx_key.get(),
          Constant.osInfo_ctx_key.get(),
          Constant.nwInfo_ctx_key.get(),
          Constant.svcName_ctx_key.get(),
          Constant.devModel_ctx_key.get(),
          Constant.carrierType_ctx_key.get(),
          Constant.trId_ctx_key.get(),
          Constant.msgId_ctx_key.get(),
          Constant.fromSvcName_ctx_key.get(),
          Constant.toSvcName_ctx_key.get(),
          Constant.svcType_ctx_key.get(),
          Constant.devType_ctx_key.get(),
          Constant.deviceToken_ctx_key.get()
      );
      if (Constant.transactionId_ctx_key.get() == null) {
        channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      } else {
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true)
          .intercept(proxyClientInterceptor).build();
      }

      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("talk start");
      Printer printter = JsonFormat.printer();
      logger.info("input>> {}", printter.print(request));
      TalkResponse talkResponse = stub.talk(request);
      responseObserver.onNext(talkResponse);
      responseObserver.onCompleted();
      logger.info(printter.print(talkResponse));
      logger.info("talk end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        try {
          channel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("getRuntimeParameters start");
      RuntimeParameterList runtimeParameterList = stub.getRuntimeParameters(request);
      responseObserver.onNext(runtimeParameterList);
      responseObserver.onCompleted();
      logger.info("getRuntimeParameters end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }

  @Override
  public void getUserAttributes(Empty request, StreamObserver<UserAttributeList> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("getUserAttributes start");
      UserAttributeList userAttributeList = stub.getUserAttributes(request);
      responseObserver.onNext(userAttributeList);
      responseObserver.onCompleted();
      logger.info("getUserAttributes end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }

  @Override
  public void close(TalkKey request, StreamObserver<TalkStat> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("close start");
      TalkStat talkStat = stub.close(request);
      responseObserver.onNext(talkStat);
      responseObserver.onCompleted();
      logger.info("close end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }

  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    ManagedChannel channel = null;
    DialogAgentProviderBlockingStub stub = null;
    try {
//      String daIP = properties.getProperty("DA_IP", "127.0.0.1");
//      int daPort = Integer.parseInt(properties.getProperty("DA_PORT", "9908"));
      channel = ManagedChannelBuilder.forAddress(daIP, daPort).usePlaintext(true).build();
      stub = DialogAgentProviderGrpc.newBlockingStub(channel);
      logger.info("terminate start");
      Empty empty = stub.terminate(request);
      responseObserver.onNext(empty);
      responseObserver.onCompleted();
      logger.info("terminate end");
    } catch (Exception e) {
      responseObserver.onError(e);
    } finally {
      if (channel != null) {
        channel.shutdown();
      }
    }
  }
}
