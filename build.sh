#!/usr/bin/env bash

export MAUM_ROOT=${HOME}/maum
maum_root=${MAUM_ROOT}
echo ${MAUM_ROOT}

if [ -z "${maum_root}" ]; then
  echo MAUM_ROOT is not defined!
  exit 1
fi
test -d ${maum_root} || mkdir -p ${maum_root}

repo_root=$(pwd)
[ -n $(which nproc) ] && {
  NPROC=$(nproc)
} || {
  NPROC=$(cat /proc/cpuinfo | grep cores | wc -l)
}
echo "[brain-qa]" repo_root: ${repo_root}, NPROC: ${NPROC}, MAUM_ROOT: ${MAUM_ROOT}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "${OS}" = "ubuntu" ]; then
    sudo apt-get install libarchive13 libarchive-dev \
      make cmake g++ g++-4.8 automake libtool
    sudo apt-get install python-pip python-dev build-essential
  else
    sudo yum install -y epel-release
    sudo yum install -y gcc gcc-c++ libarchive-devel.x86_64 \
      python-devel.x86_64  glibc.x86_64 autoconf automake \
      libtool make
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    sudo python get-pip.py
    rm get-pip.py
  fi
  sudo pip install --upgrade pip 
  sudo pip install --upgrade virtualenv
  sudo pip install boto3 grpcio==1.7.0  requests numpy theano gensim==2.2.0
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo "Last commit for build.sh: ${sha1}"

# not docker build && update commit
if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo " get_requirements had been done!"
fi

if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc
    __CC=/usr/bin/gcc
  fi  
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++
    __CXX=/usr/bin/g++
  fi  
  CMAKE=/usr/bin/cmake3
else
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc-4.8
    __CC=/usr/bin/gcc-4.8
  fi  
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++-4.8
    __CXX=/usr/bin/g++-4.8
  fi  
  CMAKE=/usr/bin/cmake
fi

build_base="build-debug" && [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && build_base="build-deploy-debug"
build_dir=${PWD}/${build_base}

test -d ${build_dir} || mkdir -p ${build_dir}
cd ${build_dir}

echo ${__CC}
echo ${__CXX}
${CMAKE} \
  -DCMAKE_PREFIX_PATH=${maum_root} \
  -DCMAKE_BUILD_TYPE=Debug \
  -DCMAKE_C_COMPILER=${__CC} \
  -DCMAKE_CXX_COMPILER=${__CXX} \
  -DCMAKE_INSTALL_PREFIX=${maum_root} ..

if [ "$1" = "proto" ]; then
  (cd proto && make install -j ${NPROC})
  (cd pysrc/proto && make install -j ${NPROC})
else
  if [ "${MAUM_BUILD_DEPLOY}" = "true" ]; then
    make install -j ${NPROC}
  else
    make install
  fi  
fi
