cmake_minimum_required(VERSION 2.8.12)

project(brain-qa-pb)
SET(OUTLIB brain-qa-pb)

set(PROTOBUF_GENERATE_CPP_APPEND_PATH "FALSE")
find_package(Protobuf REQUIRED)
include(grpc)

set(PROTOBUF_IMPORT_DIRS
  "${CMAKE_INSTALL_PREFIX}/include"
  "${CMAKE_CURRENT_SOURCE_DIR}")

file(GLOB_RECURSE
  PROTO_SRCS
  RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
  "*.proto")

protobuf_generate_grpc_cpp(PB_SRCS PB_HDRS ${PROTO_SRCS})

include_directories(.)

include_directories("${CMAKE_INSTALL_PREFIX}/include")
LINK_DIRECTORIES("${CMAKE_INSTALL_PREFIX}/lib")

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif ()

add_library(${OUTLIB} SHARED ${PB_SRCS} ${PB_HDRS})
if (APPLE)
  set_target_properties(${OUTLIB}
    PROPERTIES MACOSX_RPATH ${CMAKE_INSTALL_PREFIX}/lib)
endif ()

target_link_libraries(${OUTLIB}
  maum-pb grpc++ grpc protobuf)

install(TARGETS ${OUTLIB}
  RUNTIME DESTINATION bin COMPONENT libraries
  LIBRARY DESTINATION lib COMPONENT libraries
  ARCHIVE DESTINATION lib COMPONENT libraries)

install(DIRECTORY ./ DESTINATION include
  FILES_MATCHING PATTERN "*.h")

install(DIRECTORY ./ DESTINATION include
  FILES_MATCHING PATTERN "*.proto")
