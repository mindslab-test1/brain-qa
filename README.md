# brain-qa
- See brain-qa [Wiki](https://github.com/mindslab-ai/brain-qa/wiki/brain-qa-%EC%84%A4%EB%AA%85)
- See kbqa [Wiki](https://github.com/mindslab-ai/brain-qa/wiki/kbqa-%EC%84%A4%EB%AA%85)

# build for development
- ``` ./build.sh ```
- install to `${HOME}/maum`
``` sh
export MAUM_ROOT=${HOME}/maum
cd ~/git/brain-qa
mkdir build-debug
cd build-debug
CC=/usr/bin/gcc
CXX=/usr/bin/g++
cmake \
  -DCMAKE_C_COMPILER=${CC} \
  -DCMAKE_CXX_COMPILER=${CXX} \
  -DCMAKE_INSTALL_PREFIX=${MAUM_ROOT} \
  -DCMAKE_BUILD_TYPE=Debug \
  ..  
make
make install
```
